﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using CardEditor.Dialogs;
using Core.Documents;
using Core.Exceptions;
using Core.Util;
using Core.View_Models;
using ReactiveUI;
using Splat;
using Application = System.Windows.Application;
using ICommand = System.Windows.Input.ICommand;
using Size = Core.Drawing.Size;

namespace CardEditor
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window, IViewFor<WorkspaceViewModel>, IEnableLogger
	{
		private readonly List<IDisposable> _bindings = new List<IDisposable>();

		/// <summary>
		/// Dependency property
		/// </summary>
		public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
			typeof(WorkspaceViewModel),
			typeof(MainWindow));

		/// <summary>
		/// Custom quit command, since WPF doesn't have one by default
		/// </summary>
		public static readonly RoutedUICommand Quit = new RoutedUICommand("Quit", "Quit",
			typeof(MainWindow),
			new InputGestureCollection
			{
				new KeyGesture(Key.Q, ModifierKeys.Control)
			});

		/// <summary>
		/// Constructs a MainWindow
		/// </summary>
		public MainWindow()
		{
			InitializeSettings();
			InitializeComponent();

			if (Properties.History.Default.RecentDocuments.Entries.Count > 0)
				InitializeRecentDocs(false);

			IObservable<bool> viewModelIsBound =
				this
					.WhenAny(v => v.ViewModel, d => d)
					.Select(d => d.Value != null);
			viewModelIsBound.BindTo(this, v => v.WorkspaceLayout.IsEnabled);
		}

		#region Implementation of IViewFor

		/// <summary>
		/// </summary>
		object IViewFor.ViewModel
		{
			get => ViewModel;
			set => ViewModel = (WorkspaceViewModel)value;
		}

		#endregion

		#region Implementation of IViewFor<WorkspaceViewModel>

		/// <summary>
		/// The ViewModel corresponding to this specific View. This should be
		/// a DependencyProperty if you're using XAML.
		/// </summary>
		public WorkspaceViewModel ViewModel
		{
			get => (WorkspaceViewModel) GetValue(ViewModelProperty);
			set
			{
				if (ViewModel != null)
					UnbindFromViewModel();
				SetValue(ViewModelProperty, value);
				if (value != null)
					BindToViewModel();

				UpdateTitleBar();
			}
		}

		#endregion

		#region Private Methods

		private void InitializeSettings()
		{
			// delay maximizing until the window is loaded so it goes to the correct screen
			Top = Properties.WindowSettings.Default.Top;
			Left = Properties.WindowSettings.Default.Left;
			Height = Properties.WindowSettings.Default.Height;
			Width = Properties.WindowSettings.Default.Width;

			if (Properties.History.Default.RecentDocuments == null)
				Properties.History.Default.RecentDocuments = new WorkspaceHistory();
		}

		private void InitializeRecentDocs(bool needsUnbind)
		{
			if (needsUnbind)
				foreach (MenuItem menuItem in RecentWorkspaces.Items)
					menuItem.Click -= RecentDocClickHandler;

			RecentWorkspaces.Items.Clear();
			foreach (WorkspaceHistory.Entry entry in Properties.History.Default.RecentDocuments.Entries)
			{
				MenuItem newItem = new MenuItem
				{
					Header = entry.WorkspacePath,
					CommandParameter = entry.WorkspacePath
				};
				newItem.Click += RecentDocClickHandler;
				RecentWorkspaces.Items.Add(newItem);
			}
		}

		private void UnbindFromViewModel()
		{
			foreach (IDisposable binding in _bindings)
				binding.Dispose();
			_bindings.Clear();

			CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(DocList.ItemsSource);
			view.GroupDescriptions.Clear();
			view.SortDescriptions.Clear();

			DocList.ItemsSource = null;
			OpenDocuments.ItemsSource = null;
		}

		private void BindToViewModel()
		{
			BindDocList();
			BindDocCommands();
			BindOpenDocTabs();

			//this.BindCommand(ViewModel, vm => vm.Save, v => v.SaveWorkspace);
			ViewModel.Save.ThrownExceptions.Subscribe(ex =>
			{
				FailedOperationException savedFailed = ex as FailedOperationException;
				if (savedFailed != null)
				{
					HandleSaveFailed(savedFailed);
				}
			});

			ViewModel.ConfirmRemoveDirtyDoc.RegisterHandler(_ =>
			{
				MessageBoxResult result = MessageBox.Show(Application.Current.MainWindow, "This document has unsaved changes. Are you sure you want to remove it?",
					"Remove document", MessageBoxButton.YesNo, MessageBoxImage.Warning);
				_.SetOutput(result == MessageBoxResult.Yes);
			});
		}

		private void UpdateTitleBar()
		{
			StringBuilder builder = new StringBuilder(Properties.Resources.Version);
			if (ViewModel != null)
				builder.Append($" - {ViewModel.FullPath}");

			Title = builder.ToString();
		}

		private void BindDocList()
		{
			_bindings.Add(this.OneWayBind(ViewModel, vm => vm.Documents, v => v.DocList.ItemsSource));
			_bindings.Add(this.Bind(ViewModel, vm => vm.SelectedDocument, v => v.DocList.SelectedItem));

			CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(DocList.ItemsSource);
			view.GroupDescriptions.Add(new PropertyGroupDescription("DocGroupName"));
			view.SortDescriptions.Add(new SortDescription("DocGroupName", ListSortDirection.Ascending));

			// this resizes the DocList column when the contents change
			_bindings.Add(ViewModel.Documents.Changed.Subscribe(_ =>
			{
				if (double.IsNaN(DocColumn.Width))
				{
					DocColumn.Width = DocColumn.ActualWidth;
				}

				DocColumn.Width = double.NaN;
			}));
		}

		private void BindDocCommands()
		{
			_bindings.Add(this.BindCommand(ViewModel, vm => vm.CreateAddDocuments, v => v.AddDataSets, Observable.Return(typeof(DataSet))));
			_bindings.Add(this.BindCommand(ViewModel, vm => vm.CreateAddDocuments, v => v.AddGlyphSets, Observable.Return(typeof(GlyphSet))));
			_bindings.Add(this.BindCommand(ViewModel, vm => vm.CreateAddDocuments, v => v.AddLayouts, Observable.Return(typeof(Layout))));
			_bindings.Add(this.BindCommand(ViewModel, vm => vm.CreateAddDocuments, v => v.AddDeckPrintSet, Observable.Return(typeof(DeckPrintSet))));
			ViewModel.CreateAddDocuments.ThrownExceptions.Subscribe(ex =>
			{
				FailedOperationException loadFailed = ex as FailedOperationException;
				if (loadFailed != null)
					HandleLoadFailed(loadFailed);
			});

			_bindings.Add(this.BindCommand(ViewModel, vm => vm.RemoveSelectedDocument, v => v.RemoveDocButton));
			ViewModel.RemoveSelectedDocument.ThrownExceptions.Subscribe(ex =>
			{
				FailedOperationException removeFailed = ex as FailedOperationException;
				if (removeFailed != null)
					HandleRemoveFailed(removeFailed);
			});

			_bindings.Add(this.BindCommand(ViewModel, vm => vm.DeleteSelectedDocument, v => v.DeleteDocButton));
			ViewModel.DeleteSelectedDocument.ThrownExceptions.Subscribe(ex =>
			{
				FailedOperationException deleteFailed = ex as FailedOperationException;
				if (deleteFailed != null)
					HandleDeleteFailed(deleteFailed);
			});
		}

		private void BindOpenDocTabs()
		{
			_bindings.Add(this.OneWayBind(ViewModel, vm => vm.OpenDocs, v => v.OpenDocuments.ItemsSource));
			_bindings.Add(this.Bind(ViewModel, vm => vm.SelectedOpenDoc, v => v.OpenDocuments.SelectedItem));
			_bindings.Add(this.Bind(ViewModel, vm => vm.SelectedOpenDoc, v => v.OpenDocuments.SelectedContent));

			_bindings.Add(ViewModel.WhenAnyValue(vm => vm.SelectedOpenDoc)
				.Where(_ => !ViewModel.IsNew)
				.Subscribe(_ =>
				{
                    Properties.History.Default.RecentDocuments.AddOrUpdate(ViewModel);
                    UpdateHistory();
				}));
			_bindings.Add(ViewModel.OpenDocs.Changed
				.Where(_ => !ViewModel.IsNew)
				.Subscribe(_ =>
				{
					Properties.History.Default.RecentDocuments.AddOrUpdate(ViewModel);
					UpdateHistory();
				}));
		}

		private void UpdateHistory()
		{
			Properties.History.Default.Save();

			InitializeRecentDocs(true);
		}

		private void OpenWorkspaceFile(string fileName)
		{
			if (string.IsNullOrEmpty(fileName))
				throw new ArgumentOutOfRangeException(nameof(fileName));

			string workspaceRoot = Path.GetDirectoryName(fileName);
			string workspaceFile = Path.GetFileName(fileName);
			WorkspaceViewModel vm;

			WorkspaceHistory.Entry prevEntry = Properties.History.Default.RecentDocuments.Entries.FirstOrDefault(entry =>
				entry.WorkspacePath.Equals(fileName));

			try
			{
				vm = new WorkspaceViewModel(workspaceRoot, workspaceFile, true);
			}
			catch (FailedOperationException ex)
			{
				Properties.History.Default.RecentDocuments.Remove(fileName);
				UpdateHistory();
				HandleLoadFailed(ex);
				return;
			}
			
			if (prevEntry != null)
			{
				foreach (string docPath in prevEntry.OpenDocPaths)
				{
					BaseDocumentViewModel openDoc = vm.Documents.FirstOrDefault(doc => doc.RelativePath == docPath);
					if (openDoc != null)
						vm.OpenDocs.Add(openDoc);
				}

				vm.SelectedOpenDoc = vm.OpenDocs.FirstOrDefault(doc => doc.RelativePath == prevEntry.SelectedDocPath);
			}

			ViewModel = vm;

			Properties.History.Default.RecentDocuments.AddOrUpdate(ViewModel);
			UpdateHistory();
		}

		private bool CheckSaveDirtyWorkspace()
		{
			if (ViewModel != null)
			{
				if (ViewModel.IsDirty)
				{
					MessageBoxResult result = MessageBox.Show(Application.Current.MainWindow, 
						"There are unsaved changes in some documents. Would you like to save them first?", 
						"Unsaved changes found", 
						MessageBoxButton.YesNoCancel, 
						MessageBoxImage.Warning);

					switch (result)
					{
						case MessageBoxResult.Yes:
							((ICommand) ViewModel.Save).Execute(new Unit());
							return true;

						case MessageBoxResult.No:
							return true;

						case MessageBoxResult.Cancel:
							return false;

						default:
							throw new InvalidOperationException();
					}
				}
			}

			return true;
		}

		private void HandleLoadFailed(FailedOperationException loadFailed)
		{
			MessageBox.Show(Application.Current.MainWindow, loadFailed.Message, "Failed to load", MessageBoxButton.OK, MessageBoxImage.Error);
		}

		private void HandleSaveFailed(FailedOperationException saveFailed)
		{
			MessageBox.Show(Application.Current.MainWindow, saveFailed.Message, "Failed to save", MessageBoxButton.OK, MessageBoxImage.Error);
		}

		private void HandleRemoveFailed(FailedOperationException saveFailed)
		{
			StringBuilder builder = new StringBuilder("The following documents could not be removed:\n");
			foreach (FailedOperationException.FailedDocument failedDoc in saveFailed.FailedDocs)
				builder.AppendLine(failedDoc.Path);

			MessageBox.Show(Application.Current.MainWindow, builder.ToString(), "Failed to remove", MessageBoxButton.OK, MessageBoxImage.Error);
		}

		private void HandleDeleteFailed(FailedOperationException saveFailed)
		{
			StringBuilder builder = new StringBuilder("The following documents could not be deleted:\n");
			foreach (FailedOperationException.FailedDocument failedDoc in saveFailed.FailedDocs)
				builder.AppendLine(failedDoc.Path);

			MessageBox.Show(Application.Current.MainWindow, builder.ToString(), "Failed to delete", MessageBoxButton.OK, MessageBoxImage.Error);
		}

		#endregion

		#region WPF Handlers

		private void AlwaysTrue(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = true;
		}

		private void New_ExecutedHandler(object sender, ExecutedRoutedEventArgs e)
		{
			if (!CheckSaveDirtyWorkspace())
				return;

			IFileSystem fileSystem = Locator.CurrentMutable.GetService<IFileSystem>();
			IList<string> filePaths = fileSystem.GetDocumentPaths<Workspace>(
				Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), 
				FileSystem.Operation.Create, 
				false);
			if (filePaths == null || filePaths.Count == 0)
				return;

			string workspaceRoot = Path.GetDirectoryName(filePaths[0]);
			string workspaceFile = Path.GetFileName(filePaths[0]);
			ViewModel = new WorkspaceViewModel(workspaceRoot, workspaceFile, false);
		}

		private void Open_ExecutedHandler(object sender, ExecutedRoutedEventArgs e)
		{
			if (!CheckSaveDirtyWorkspace())
				return;

			IFileSystem fileSystem = Locator.CurrentMutable.GetService<IFileSystem>();
			IList<string> filePaths = fileSystem.GetDocumentPaths<Workspace>(
				Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), 
				FileSystem.Operation.Open, 
				false);
			if (filePaths == null || filePaths.Count == 0)
				return;

			OpenWorkspaceFile(filePaths[0]);
		}

		private void Save_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			if (ViewModel == null)
			{
				e.CanExecute = false;
				return;
			}

			ICommand command = ViewModel.Save;
			e.CanExecute = command.CanExecute(null);
		}

		private void Save_ExecutedHandler(object sender, ExecutedRoutedEventArgs e)
		{
			ICommand command = ViewModel.Save;
			command.Execute(null);

			Properties.History.Default.RecentDocuments.AddOrUpdate(ViewModel);
			UpdateHistory();
		}

		private void Close_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = ViewModel != null;
		}

		private void Close_ExecutedHandler(object sender, ExecutedRoutedEventArgs e)
		{
			if (!CheckSaveDirtyWorkspace())
				return;

			ViewModel = null;
		}

		private void Quit_ExecutedHandler(object sender, ExecutedRoutedEventArgs e)
		{
			if (!CheckSaveDirtyWorkspace())
				return;

			Application.Current.Shutdown();
		}

		private void RecentDocClickHandler(object sender, RoutedEventArgs e)
		{
			MenuItem docMenuItem = sender as MenuItem;
			if (docMenuItem == null)
				throw new InvalidCastException("Received click for recent document that is not from a MenuItem");

			string workspacePath = docMenuItem.CommandParameter as string;
			if (docMenuItem == null)
				throw new InvalidCastException("Could not retrieve path for recent document");

			// sanity check for selecting the currently open workspace
			if (ViewModel?.FullPath == workspacePath)
				return;

			if (!CheckSaveDirtyWorkspace())
				return;

			OpenWorkspaceFile(workspacePath);
		}

		private void AddDocsButtonClickHandler(object sender, RoutedEventArgs e)
		{
			AddDocMenu.IsOpen = true;
		}

		private void AddDocMenuClickHandler(object sender, RoutedEventArgs e)
		{
			MenuItem menuItem = sender as MenuItem;
			Type docType = menuItem?.CommandParameter as Type;
			if (docType == null)
				return;

			ICommand command = ViewModel.CreateAddDocuments;
			command.Execute(docType);

			e.Handled = true;
		}

		private void RemoveDocClickHandler(object sender, RoutedEventArgs e)
		{
			ICommand command = ViewModel.RemoveSelectedDocument;
			command.Execute(null);

			e.Handled = true;
		}

		private void DeleteDocClickHandler(object sender, RoutedEventArgs e)
		{
			ICommand command = ViewModel.DeleteSelectedDocument;
			command.Execute(null);

			e.Handled = true;
		}

		private void TabItemClickHandler(object sender, RoutedEventArgs e)
		{
			((TabItem)sender).IsSelected = true;
			e.Handled = true;

			if (ViewModel == null)
				return;

			ICommand command = ViewModel.CloseSelectedDocument;
			command.Execute(null);
		}

		private void DocListDoubleClickedHandler(object sender, MouseButtonEventArgs e)
		{
			e.Handled = true;

			if (ViewModel == null)
				return;

			ICommand command = ViewModel.OpenSelectedDocument;
			if (command.CanExecute(null))
				command.Execute(new Unit());
		}

		private void ClosingHandler(object sender, CancelEventArgs e)
		{
			if (!CheckSaveDirtyWorkspace())
			{
				e.Cancel = true;
				return;
			}

			if (WindowState == WindowState.Maximized)
			{
				Properties.WindowSettings.Default.Top = RestoreBounds.Top;
				Properties.WindowSettings.Default.Left = RestoreBounds.Left;
				Properties.WindowSettings.Default.Width = RestoreBounds.Width;
				Properties.WindowSettings.Default.Height = RestoreBounds.Height;
				Properties.WindowSettings.Default.Maximized = true;
			}
			else
			{
				Properties.WindowSettings.Default.Top = Top;
				Properties.WindowSettings.Default.Left = Left;
				Properties.WindowSettings.Default.Width = Width;
				Properties.WindowSettings.Default.Height = Height;
				Properties.WindowSettings.Default.Maximized = false;
			}

			Properties.WindowSettings.Default.Save();
		}

		private void LoadedHandler(object sender, RoutedEventArgs e)
		{
			PresentationSource source = PresentationSource.FromVisual(Application.Current.MainWindow);
			double dpiX = 0, dpiY = 0;
			if (source != null)
			{
				dpiX = 96.0 * (source.CompositionTarget?.TransformToDevice.M11 ?? 1.0f);
				dpiY = 96.0 * (source.CompositionTarget?.TransformToDevice.M22 ?? 1.0f);
			}
			App.Ppi = new Size((float)dpiX, (float)dpiY);

			// now that we have loaded the window, it should be placed in the correct screen and
			// we can safely maximize it
			if (Properties.WindowSettings.Default.Maximized)
				WindowState = WindowState.Maximized;
		}

		private void GoToWikiClickHandler(object sender, RoutedEventArgs e)
		{
			System.Diagnostics.Process.Start("https://bitbucket.org/mattsinger/card-editor/wiki/Home");
		}

		private void ViewLogsItemClickHandler(object sender, RoutedEventArgs e)
		{
			ShowLogsDialog dialog = new ShowLogsDialog
			{
				Owner = Application.Current.MainWindow
			};
			dialog.ShowDialog();
		}

		private void AboutMenuItemClickHandler(object sender, RoutedEventArgs e)
		{
			AboutDialog dialog = new AboutDialog
			{
				Owner = Application.Current.MainWindow
			};
			dialog.ShowDialog();
		}

		private void SettingsItemClickHandler(object sender, RoutedEventArgs e)
		{
			SettingsWindow window = new SettingsWindow
			{
				Owner = Application.Current.MainWindow
			};
			window.Show();
		}

		#endregion
	}
}
