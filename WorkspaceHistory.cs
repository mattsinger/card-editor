﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Core.View_Models;

namespace CardEditor
{
	/// <summary>
	/// Simple class to manage the history of recently-opened Workspaces
	/// </summary>
	[Serializable]
	public class WorkspaceHistory : ISerializable
	{
		/// <summary>
		/// The maximum number of history entries
		/// </summary>
		public const int MaxEntries = 10;

		/// <summary>
		/// A single recently-opened Workspace
		/// </summary>
		[Serializable]
		public class Entry : ISerializable
		{
			#region Constructors

			/// <summary>
			/// Default constructor
			/// </summary>
			public Entry() {}

			/// <summary>
			/// Constructs an Entry with a WorkspaceViewModel
			/// </summary>
			/// <param name="workspace">The workspace represented by this Entry</param>
			public Entry(WorkspaceViewModel workspace)
			{
				WorkspacePath = workspace.FullPath;
				foreach (BaseDocumentViewModel openDoc in workspace.OpenDocs)
					OpenDocPaths.Add(openDoc.RelativePath);
				SelectedDocPath = workspace.SelectedOpenDoc?.RelativePath;
			}

			#endregion

			#region Properties

			/// <summary>
			/// The path to the Workspace file
			/// </summary>
			public string WorkspacePath { get; set; }

			/// <summary>
			/// The list of relative paths to the Workspace's OpenDocs
			/// </summary>
			public List<string> OpenDocPaths { get; set; } = new List<string>();

			/// <summary>
			/// The relative path to the Workspace's SelectedDoc
			/// </summary>
			public string SelectedDocPath { get; set; }

			#endregion

			#region Implementation of ISerializable

			/// <summary>Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with the data needed to serialize the target object.</summary>
			/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to populate with data. </param>
			/// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext" />) for this serialization. </param>
			/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
			public void GetObjectData(SerializationInfo info, StreamingContext context)
			{
				throw new NotImplementedException();
			}

			#endregion
		}

		/// <summary>
		/// The list of entries
		/// </summary>
		public List<Entry> Entries = new List<Entry>(MaxEntries);

		/// <summary>
		/// Adds a workspace to the history front if no entry exists, or moves the existing entry
		/// to the front and updates it for the current state.
		/// </summary>
		/// <param name="workspace"></param>
		public void AddOrUpdate(WorkspaceViewModel workspace)
		{
			Remove(workspace.FullPath);

			Entries.Insert(0, new Entry(workspace));

			if (Entries.Count > MaxEntries)
				Entries.RemoveRange(MaxEntries - 1, Entries.Count - MaxEntries);
		}

		/// <summary>
		/// Removes a workspace from the history if the entry exists
		/// </summary>
		/// <param name="fullPath">The full path of the workspace to remove</param>
		public void Remove(string fullPath)
		{
			Entry curEntry = Entries.FirstOrDefault(entry => entry.WorkspacePath.Equals(fullPath));
			if (curEntry != null)
				Entries.Remove(curEntry);
		}

		#region Implementation of ISerializable

		/// <summary>Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with the data needed to serialize the target object.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to populate with data. </param>
		/// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext" />) for this serialization. </param>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			
		}

		#endregion
	}
}
