﻿<Window x:Class="CardEditor.MainWindow"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
		xmlns:reactiveUi="clr-namespace:ReactiveUI;assembly=ReactiveUI"
		xmlns:viewModels="clr-namespace:Core.View_Models;assembly=Core"
		xmlns:converters="clr-namespace:CardEditor.Converters"
		xmlns:self="clr-namespace:CardEditor"
		xmlns:prop="clr-namespace:CardEditor.Properties"
		Title="{x:Static prop:Resources.Version}" Icon="/CardEditor;component/Resources/playing-card--pencil.ico"
		Closing="ClosingHandler"
		Loaded="LoadedHandler">
	<Window.Resources>
		<converters:DocStatusImageConverter x:Key="StatusImageConverter"/>
		<converters:DocTypeConverter x:Key="DocTypeConverter" />
		<converters:DocTypeImageConverter x:Key="DocTypeImageConverter"/>
		<converters:DocTypeGroupingConverter x:Key="DocTypeGroupingConverter"/>
		<BooleanToVisibilityConverter x:Key="BooleanToVisibilityConverter"/>
		<Style x:Key="DocListHeaderStyle" TargetType="{x:Type GridViewColumnHeader}">
			<Setter Property="Visibility" Value="Collapsed" />
		</Style>
	</Window.Resources>
	<Window.CommandBindings>
		<CommandBinding Command="New"  CanExecute="AlwaysTrue" Executed="New_ExecutedHandler"/>
		<CommandBinding Command="Open" CanExecute="AlwaysTrue" Executed="Open_ExecutedHandler"/>
		<CommandBinding Command="Save" CanExecute="Save_CanExecute" Executed="Save_ExecutedHandler"/>
		<CommandBinding Command="Close" CanExecute="Close_CanExecute" Executed="Close_ExecutedHandler"/>
        <CommandBinding Command="self:MainWindow.Quit" CanExecute="AlwaysTrue" Executed="Quit_ExecutedHandler"/>
	</Window.CommandBindings>
	<Window.InputBindings>
		<KeyBinding Command="Close" Key="C" Modifiers="Control" />
	</Window.InputBindings>
	<DockPanel HorizontalAlignment="Stretch" Background="LightGray">
        <Menu x:Name="MenuBar" 
			DockPanel.Dock="Top" 
			HorizontalAlignment="Stretch">
            <MenuItem Header="_File">
                <MenuItem Header="_New" Name="NewWorkspace" Command="New">
					<MenuItem.Icon>
						<Image Source="/CardEditor;component/Resources/document--plus.png" />
					</MenuItem.Icon>
				</MenuItem>
				<MenuItem Header="_Open" Name="OpenWorkspace" Command="Open">
                    <MenuItem.Icon>
                        <Image Source="/CardEditor;component/Resources/folder-open-document.png" />
                    </MenuItem.Icon>
                </MenuItem>
				<MenuItem Header="_Save" Name ="SaveWorkspace" Command="Save">
					<MenuItem.Icon>
						<Image Source="/CardEditor;component/Resources/disk-black.png" />
					</MenuItem.Icon>
				</MenuItem>
				<Separator/>
				<MenuItem Header="_Close" Name="CloseMenuItem" Command="Close" InputGestureText="Ctrl+C"/>
				<Separator/>
				<MenuItem Header="Recent" Name="RecentWorkspaces">
					<MenuItem Header="No recent workspaces" Foreground="DarkGray"/>
				</MenuItem>
				<Separator/>
				<MenuItem Header="_Quit" Name="QuitMenuItem" Command="self:MainWindow.Quit" InputGestureText="Ctrl+Q" />
            </MenuItem>
            <MenuItem Header="_Help">
                <MenuItem Header="Settings" Name="SettingsItem" Click="SettingsItemClickHandler"/>
				<MenuItem Header="Go to Wiki" Name="GoToWiki" Click="GoToWikiClickHandler" />
				<MenuItem Header="View Logs" Name="ViewLogsItem" Click="ViewLogsItemClickHandler"/>
				<Separator />
				<MenuItem Header="_About" Name="AboutMenuItem" Click="AboutMenuItemClickHandler">
					<MenuItem.Icon>
						<Image Source="/CardEditor;component/Resources/information.png" />
					</MenuItem.Icon>
				</MenuItem>
			</MenuItem>
        </Menu>
        <Grid x:Name="WorkspaceLayout" Margin="2,2">
			<Grid.ColumnDefinitions>
				<ColumnDefinition Width="Auto" MinWidth="100"/>
				<ColumnDefinition Width="4" />
				<ColumnDefinition Width="*" />
			</Grid.ColumnDefinitions>
			<DockPanel Grid.Column="0">
				<StackPanel Orientation="Horizontal" DockPanel.Dock="Top" Height="20">
					<Button Name="AddDocsButton" ToolTip="Add documents to the workspace" Click="AddDocsButtonClickHandler">
						<Image Source="/CardEditor;component/Resources/document--plus.png"/>
						<Button.ContextMenu>
							<ContextMenu Name="AddDocMenu">
                                <MenuItem Header="Data Set" Name="AddDataSets" />
							    <MenuItem Header="Deck Print Set" Name="AddDeckPrintSet" />
                                <MenuItem Header="Glyph Set" Name="AddGlyphSets" />
								<MenuItem Header="Layout" Name="AddLayouts" />
							</ContextMenu>
						</Button.ContextMenu>
					</Button>
					<Button Name="RemoveDocButton" ToolTip="Remove the selected document">
						<Image Source="/CardEditor;component/Resources/document--minus.png"/>
					</Button>
					<Button Name="DeleteDocButton" ToolTip="Delete the selected document"
							Margin="5,0,0,0">
						<Image Source="/CardEditor;component/Resources/bin.png"/>
					</Button>
				</StackPanel>
				<ListView Name="DocList" MouseDoubleClick="DocListDoubleClickedHandler" DockPanel.Dock="Bottom" VerticalAlignment="Stretch">
					<ListView.GroupStyle>
						<GroupStyle>
							<GroupStyle.ContainerStyle>
								<Style TargetType="{x:Type GroupItem}">
									<Setter Property="Template">
										<Setter.Value>
											<ControlTemplate TargetType="{x:Type GroupItem}">
												<Expander IsExpanded="True" Header="{Binding}">
													<Expander.HeaderTemplate>
														<DataTemplate>
															<DockPanel LastChildFill="True" Height="Auto" HorizontalAlignment="Stretch">
																<Image Source="{Binding Path=Items[0],Converter={StaticResource DocTypeImageConverter}}"
																	   DockPanel.Dock="Left"/>
																<TextBlock FontWeight="Bold" 
																		   Text="{Binding Path=Items[0],Converter={StaticResource DocTypeGroupingConverter}}"
																		   HorizontalAlignment="Stretch"/>
																<DockPanel.ContextMenu>
																	<ContextMenu>
																		<MenuItem Header="Add" 
																				  CommandParameter="{Binding Path=Items[0],Converter={StaticResource DocTypeConverter}}"
																				  Click="AddDocMenuClickHandler"/>
																	</ContextMenu>
																</DockPanel.ContextMenu>
															</DockPanel>
														</DataTemplate>
													</Expander.HeaderTemplate>
													<ItemsPresenter />
												</Expander>
											</ControlTemplate>
										</Setter.Value>
									</Setter>
								</Style>
							</GroupStyle.ContainerStyle>
						</GroupStyle>
					</ListView.GroupStyle>
					<ListView.View>
						<GridView AllowsColumnReorder="False" ColumnHeaderContainerStyle="{StaticResource DocListHeaderStyle}">
							<GridViewColumn x:Name="DocColumn">
								<GridViewColumn.CellTemplate>
									<DataTemplate DataType="viewModels:BaseDocumentViewModel">
										<DockPanel Height="20" HorizontalAlignment="Stretch" LastChildFill="True">
                                            <Image Source="{Binding Path=CurrentStatus,Converter={StaticResource StatusImageConverter}}"
												   DockPanel.Dock="Left"/>
                                            <TextBlock Text="*" Visibility="{Binding Path=IsDirty, Converter={StaticResource BooleanToVisibilityConverter}}"/>
                                            <TextBlock Text="{Binding Path=DisplayName}" VerticalAlignment="Center"/>
											<DockPanel.ContextMenu>
												<ContextMenu>
													<MenuItem Header="Remove" Click="RemoveDocClickHandler"/>
													<MenuItem Header="Delete" Click="DeleteDocClickHandler"/>
												</ContextMenu>
											</DockPanel.ContextMenu>
										</DockPanel>
									</DataTemplate>
								</GridViewColumn.CellTemplate>
							</GridViewColumn>
						</GridView>
					</ListView.View>
				</ListView>
			</DockPanel>
			<GridSplitter Grid.Column="1" HorizontalAlignment="Left" Width="4" />
			<TabControl Grid.Column="2" Name="OpenDocuments" Background="LightGray">
				<TabControl.ItemContainerStyle>
					<Style TargetType="{x:Type TabItem}">
						<EventSetter Event="ButtonBase.Click" Handler="TabItemClickHandler" />
					</Style>
				</TabControl.ItemContainerStyle>
				<TabControl.ItemTemplate>
					<DataTemplate DataType="{x:Type viewModels:BaseDocumentViewModel}">
						<StackPanel Orientation="Horizontal" HorizontalAlignment="Stretch">
							<Image Source="{Binding Converter={StaticResource DocTypeImageConverter}}"
								   Margin="0, 0, 2, 0"/>
                            <TextBlock Text="*" Visibility="{Binding IsDirty, Converter={StaticResource BooleanToVisibilityConverter}}"/>
                            <TextBlock Text="{Binding DisplayName}"/>
							<Button Name="CloseDocButton" Margin="4, 0, 0, 0" Background="Transparent" BorderThickness="0">
								<Image Source="/CardEditor;component/Resources/cross-button.png"/>
							</Button>
						</StackPanel>
					</DataTemplate>
				</TabControl.ItemTemplate>
				<TabControl.ContentTemplate>
					<DataTemplate>
						<reactiveUi:ViewModelViewHost ViewModel="{Binding}" HorizontalContentAlignment="Stretch" VerticalContentAlignment="Stretch"/>
					</DataTemplate>
				</TabControl.ContentTemplate>
			</TabControl>
        </Grid>
    </DockPanel>
</Window>
