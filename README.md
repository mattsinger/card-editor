Card Editor tool v1.4.2
=======================
Card Editor is a free tool to help board and card game developers design cards and other components for easy printing.
It is designed to run on Windows 10 but should work with Windows 8, though it has not been tested.

Copyright© 2016-2020  Matt Singer

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

See LICENSE.md for more information. Licenses for 3rd party libraries used in Card Editor can be found in THIRD PARTY LICENSES.md

Tutorials and documentation can be found on the [wiki](https://bitbucket.org/mattsinger/card-editor/wiki/Home).

Pre-built binaries can be found on the [downloads page](https://bitbucket.org/mattsinger/card-editor/downloads/).

If you have any questions, comments, feature requests, bug reports, or other feedback, please direct them to <cardeditor.feedback@gmail.com>

##*DISCLAIMER*##
This is a side project of mine, done in my spare time. While I would like to see this project flourish and gain wide-spread adoption, 
I make no guarantee of delivery on any bug fixes or feature requests. This is an open-source project, however, so feel free to make 
your own contributions!

### Required to build:
* [Visual Studio 2015 or later](https://www.visualstudio.com/downloads/)
* [.NET v4.7.2](https://dotnet.microsoft.com/download/dotnet-framework-runtime/net472)
* Visual Studio Shared Projects Extension
* Nuget Package Manager and the following packages:
    * CsvReader
	* ReactiveUI, which automatically includes Rx (Reactive Extensions) and Splat
	* ImageSharp
	* Json.NET
	* WPF Extended Toolkit, Community Edition
	* NUnit
	* Moq
	
## Change Log ##
### v1.4.2 ###
* "Flip Backsides" toggle on Deck Print Sets now actually sets the orientations of backsides based on the set value

### v1.4.1 ###
* Deck Print Sets can now support Data Sets whose Layouts are equivalent in size/bleed when rotated
* Unbound/static Text Elements in Layouts now render correctly in previews and output
* Excel importer now imports cards correctly
* CSV importer can now import fields with new-line characters in them
* Importing cards properly updates underlying data and saves out correctly
* Better error reporting when files fail to load

### v1.4.0 ###
* Users can now print decks of cards via Deck Print Sets
** Add Data Sets to the Deck Print Set
*** All Data Sets must have their Front and Back Layouts set
*** All Data Sets must have the equivalent card and bleed sizes
** For each card in the Data Set, set the number of times each card appears in the deck
** For each Data Set, choose whether the Back Layout should be flipped when printing
** Set the margins and spacing between cards, as well as whether you want to include bleed regions or print cards horizontally or vertically
** Click the print button, wait for processing, then select your printer
* Changed image loading library for loading images from disk, so now it can handle much larger images than before
* Perf boosts on Data Sets, should work much more quickly.
** It's worth noting that it still doesn't handle particularly large Data Sets well. There's no harm in breaking things up into multiple Data Sets though.
* If Data Sets are still too slow for you, you can now export them back to CSV or Excel spreadsheets for editing externally
* Layout Elements now support borders, users can set the color and width
* Fixed exception thrown when importing Excel spreadsheet with empty cells in import columns

### v1.3.2 ###
* Importing card data now supports relative paths
* Importing CSVs properly unescapes double quotations
* Updating a field name now updates the data correctly
* Focus is no longer stolen from color picker in data set editor
* Improved layout of data set editor so the data grid does not force the layout previews off screen 

### v1.3.1 ###
* fix for crash that would occur when selecting two different Excel spreadsheets for importing
* improved importer clean-up

### v1.3.0 ###
* Data Sets now support importing data from CSV files and Excel spreadsheets
* fixed issue where Layout preview would render Elements with the wrong size/origin on platforms with non-standard PPIs

### v1.2.0 ###
* Added undo/redo stack to Layout Editor. Stack size is editable in user settings.
* Removed IsBound checkbox from Element Properties. An element is considered bound is the Bound Field is not empty/null.

### v1.1.2 ###
* fixed issue where bound Fields would not display in output properly
* various usability improvements

### v1.1.1 ###
* various bug fixes

### v1.1.0 ###
* Added user settings for CMYK converter and snap sensitivity
* Card previews in Data Set and Layout Editors can use CMYK converter to give accurate displays
    * Removed CMYK converter from image output parameters
* Various minor UI tweaks

### v1.0.1 ###
* added version info

### v1.0.0 ###
* initial release