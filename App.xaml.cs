﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Windows;
using CardEditor.Util;
using CardEditor.Views.Deck_Print_Set;
using Core.Util;
using Core.View_Models.Data_Set;
using Core.View_Models.Deck_Print_Set;
using Core.View_Models.Glyph_Set;
using Core.View_Models.Layout;
using ReactiveUI;
using Splat;
using DataSetView = CardEditor.Views.Data_Set.DataSetView;
using GlyphSetView = CardEditor.Views.Glyph_Set.GlyphSetView;
using ImageFieldView = CardEditor.Views.Data_Set.ImageFieldView;
using LayoutView = CardEditor.Views.Layout.LayoutView;
using Size = Core.Drawing.Size;
using TextFieldView = CardEditor.Views.Data_Set.TextFieldView;

namespace CardEditor
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		#region Static Members
		
		private static readonly Logger Logger = new Logger
		{
			Level = LogLevel.Info
		};
		private static readonly WindowsFileSystem FileSystem = new WindowsFileSystem();
		private static readonly WpfCardRenderer CardRenderer = new WpfCardRenderer();
		private static readonly WindowsPrinter Printer = new WindowsPrinter();

		/// <summary>
		/// The application's resolution in Pixels Per Inch
		/// </summary>
		/// <remarks>Should be set by MainWindow on creation</remarks>
		public static Size Ppi { get; set; }

		#endregion

		#region Private Methods

		private void App_OnStartup(object sender, StartupEventArgs e)
		{
			InitViewModels();
			InitOtherServices();

			ILogger logger = Locator.Current.GetService<ILogger>();
			logger.Write("Starting application", LogLevel.Info);
		}

		private void InitViewModels()
		{
			Locator.CurrentMutable.Register(() => FileSystem, typeof(IFileSystem));
			Locator.CurrentMutable.Register(() => CardRenderer, typeof(ICardRenderer));
			Locator.CurrentMutable.Register(() => Printer, typeof(ICardPrinter));

			Locator.CurrentMutable.Register(() => new DataSetView(), typeof(IViewFor<DataSetViewModel>));
			Locator.CurrentMutable.Register(() => new TextFieldView(), typeof(IViewFor<TextFieldViewModel>));
			Locator.CurrentMutable.Register(() => new ImageFieldView(), typeof(IViewFor<ImageFieldViewModel>));

			Locator.CurrentMutable.Register(() => new GlyphSetView(), typeof(IViewFor<GlyphSetViewModel>));
			Locator.CurrentMutable.Register(() => new LayoutView(), typeof(IViewFor<LayoutViewModel>));
			Locator.CurrentMutable.Register(() => new DeckPrintSetView(), typeof(IViewFor<DeckPrintSetViewModel>));
		}

		private void InitOtherServices()
		{
			Locator.CurrentMutable.Register(() => Logger, typeof(ILogger));
		}

		private void App_OnExit(object sender, ExitEventArgs e)
		{
			ILogger logger = Locator.Current.GetService<ILogger>();
			logger.Write("Exiting application", LogLevel.Info);

			Logger loggerImpl = logger as Logger;
			loggerImpl?.Dispose();
		}

		#endregion
	}
}
