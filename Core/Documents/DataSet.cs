﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Core.Documents
{
	/// <summary>
	/// Document that defines card data
	/// </summary>
	[Document(Extension = ".cds", Description = "Card Data Set", GroupingHeader = "Data Sets")]
	public class DataSet : IDocument
	{
		/// <summary>
		/// Describes a field present in the data set
		/// </summary>
		public class FieldDescriptor
		{
			/// <summary>
			/// The type of data stored in this field
			/// </summary>
			[JsonConverter(typeof(StringEnumConverter))]
			public FieldType Type { get; set; }

			/// <summary>
			/// The field's unique identifier
			/// </summary>
			public string Key { get; set; }
		}

		/// <summary>
		/// The data of cards defined by this DataSet
		/// </summary>
		public List<CardData> Data { get; } = new List<CardData>();

		/// <summary>
		/// The list of fields present in this DataSet
		/// </summary>
		/// <remarks>
		/// This is stored as a List rather than a Dictionary because
		/// we want to preserve field order for load/save/UI
		/// </remarks>
		public List<FieldDescriptor> FieldDescriptors { get; } = new List<FieldDescriptor>();

		/// <summary>
		/// The path of the front Layout for cards in this set, relative to the workspace root
		/// </summary>
		public string FrontLayout { get; set; }

		/// <summary>
		/// The path of the front Layout for cards in this set, relative to the workspace root
		/// </summary>
		public string BackLayout { get; set; }
	}
}
