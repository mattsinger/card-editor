﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using Core.Drawing;

namespace Core.Documents
{
	/// <summary>
	/// Document that defines a deck of cards and how to print it
	/// </summary>
	[Document(Extension = ".dps", Description = "Deck Print Set", GroupingHeader = "Deck Print Sets")]
	public class DeckPrintSet : IDocument
	{
		/// <summary>
		/// Mapping between a card Id and how many times the card appears in the deck
		/// </summary>
		public class CardCount
		{
			/// <summary>
			/// The id of the card
			/// </summary>
			public string Id;

			/// <summary>
			/// The number of times the card appears in the deck
			/// </summary>
			public int Count;
		}

		/// <summary>
		/// A reference to a DataSet and the cards used from it
		/// </summary>
		public class DataSetRef
		{
			/// <summary>
			/// The path to the DataSet, relative to the workspace root
			/// </summary>
			public string Path { get; set; }

			/// <summary>
			/// Whether the back side of the card should be flipped when printing
			/// </summary>
			public bool FlipBackSide { get; set; }

			/// <summary>
			/// A mapping between Ids of cards found in the DataSet and how many times that card appears in the deck
			/// </summary>
			public List<CardCount> Cards { get; } = new List<CardCount>();
		};

		/// <summary>
		/// Options for printing cards
		/// </summary>
		public class PrintOptionValues
		{
			/// <summary>
			/// Size of the paper, in inches. Default is 8.5"x11"
			/// </summary>
			/// <remarks>Default is 8.5" x 11"</remarks>
			public Size PaperSize { get; set; } = new Size(8.5f, 11.0f);

			/// <summary>
			/// Amount of horizontal and veritcal space between cards on the page, in inches
			/// </summary>
			public Size Spacing { get; set; }

			/// <summary>
			/// Amount of space from the edges of the paper, in inches
			/// </summary>
			public Size Margins { get; set; } = new Size(1, 1);

			/// <summary>
			/// Whether we should include the bleed when printing cards on a page
			/// </summary>
			public bool IncludeBleed { get; set; }

			/// <summary>
			/// Whether the cards should be printed in landsacpe (longer dimension horizontally) 
			/// or portrait (longer dimension vertically)
			/// </summary>
			public bool Landscape { get; set; }
		}

		/// <summary>
		/// A list of all DataSetRefs in the deck
		/// </summary>
		public List<DataSetRef> DataSets { get; } = new List<DataSetRef>();

		/// <summary>
		/// Options for printing
		/// </summary>
		public PrintOptionValues PrintOptions { get; set; } = new PrintOptionValues();
	}
}
