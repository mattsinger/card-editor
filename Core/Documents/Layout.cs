﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using Core.Drawing;

namespace Core.Documents
{
	/// <summary>
	/// Defines the layout of a card
	/// </summary>
	[Document(Extension = ".cld", Description = "Card Layout Descriptor", GroupingHeader = "Layouts")]
	public class Layout : IDocument
	{
		/// <summary>
		/// The size of the card layout in inches
		/// </summary>
		public Size Size { get; set; }

		/// <summary>
		/// The thickness of the bleed area surrounding the card in inches
		/// </summary>
		public Size Bleed { get; set; }

		/// <summary>
		/// The size of the safe zone from the card's edge in inches
		/// </summary>
		public Size SafeZone { get; set; }

		/// <summary>
		/// The elements that appear in the layout, in order of bottom-most to top
		/// </summary>
		public List<Element> Elements { get; } = new List<Element>();
	}
}
