﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Core.Drawing;

namespace Core.Documents
{
	/// <summary>
	/// A Layout Element that contains formatted text
	/// </summary>
	public class TextElement : Element
	{
		/// <summary>
		/// The contained formatted text. Only used if FieldName is null or an empty string.
		/// </summary>
		public string Text { get; set; }

	    /// <summary>
	    /// The base text font, uses default values
	    /// </summary>
	    public Font Font { get; } = new Font(null, null)
	    {
	        Bold = false,
	        Italics = false
	    };
		
		/// <summary>
		/// The text's horizontal alignment
		/// </summary>
		public HorizontalAlignment HorizontalAlignment { get; set; } = HorizontalAlignment.Left;

		/// <summary>
		/// The text's vertical alignment
		/// </summary>
		public VerticalAlignment VerticalAlignment { get; set; } = VerticalAlignment.Top;

		/// <summary>
		/// The text color
		/// </summary>
		public Color Color { get; set; } = Color.Black;
	}
}
