﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Core.Drawing;

namespace Core.Documents
{
	/// <summary>
	/// A Layout Element that contains an image
	/// </summary>
	public class ImageElement : Element
	{
		/// <summary>
		/// The path to the image, relative to the workspace root. Only used if FieldName is null or empty
		/// </summary>
		public string Path { get; set; }

		/// <summary>
		/// Whether the image should stretch to fill the available space
		/// </summary>
		public bool Stretch { get; set; }

		/// <summary>
		/// The image's horizontal alignment
		/// </summary>
		public HorizontalAlignment HorizontalAlignment { get; set; } = HorizontalAlignment.Center;

		/// <summary>
		/// The image's vertical alignment
		/// </summary>
		public VerticalAlignment VerticalAlignment { get; set; } = VerticalAlignment.Center;
	}
}
