﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Core.Documents
{
	/// <summary>
	/// An individual field of data within a card
	/// </summary>
	public class CardField
	{
		/// <summary>
		/// The field's unique Key
		/// </summary>
		public string Key { get; set; }

		/// <summary>
		/// The field's data
		/// </summary>
		/// <remarks>
		/// The context of this data depends on the Field's type, defined in a DataSet's FieldDescriptor:
		/// * If FieldType is Text, then the value is formatted text
		/// * If FieldType is Image, then the value is the workspace-relative path to an image file
		/// * If FieldType is Color, then the value is a color-parseable string
		/// </remarks>
		public string Value
		{
			get;
			set;
		}
	}
}
