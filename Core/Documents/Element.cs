﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using Core.Drawing;

namespace Core.Documents
{
	/// <summary>
	/// Abstract base class for a layout element
	/// </summary>
	public abstract class Element
	{
		/// <summary>
		/// Default size of elements, in inches
		/// </summary>
		public const float DefaultSize = 1.0f;

		/// <summary>
		/// The element's unique id. Auto-generated for backwards compatability.
		/// </summary>
		public Guid Id { get; set; } = Guid.NewGuid();

		/// <summary>
		/// Friendly name of the element
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// An optional name of a card data field used to populate the element
		/// </summary>
		public string FieldName { get; set; }

		/// <summary>
		/// The element's origin with respect to the card's top-left corner, in millimeters
		/// </summary>
		public Point Origin { get; set; }

		/// <summary>
		/// The element's size, in inches
		/// </summary>
		public Size Size { get; set; }

		/// <summary>
		/// The element's orientation, in degrees clockwise
		/// </summary>
		public float Orientation { get; set; }

		/// <summary>
		/// The radius of the element's corners, in inches
		/// </summary>
		public float CornerRadius { get; set; }

		/// <summary>
		/// Enum used to define which corners should be rounded
		/// </summary>
		[Flags]
		public enum RoundedCornersFlags
		{
			/// <summary>
			/// No corners
			/// </summary>
			None = 0,
			/// <summary>
			/// The top-left corner of the element
			/// </summary>
			TopLeft = 1,
			/// <summary>
			/// The top-right corner of the element
			/// </summary>
			TopRight = 2,
			/// <summary>
			/// The bottom-left corner of the element
			/// </summary>
			BottomLeft = 4,
			/// <summary>
			/// The bottom-right corner of the element
			/// </summary>
			BottomRight = 8,
			/// <summary>
			/// All corners
			/// </summary>
			All = TopLeft | TopRight | BottomLeft | BottomRight
		}

		/// <summary>
		/// Which corners of the element should be rounded. Default is all.
		/// </summary>
		public RoundedCornersFlags RoundedCorners { get; set; } = RoundedCornersFlags.All;

		/// <summary>
		/// Thickness of the solid-color border around the element, in inches
		/// </summary>
		public float BorderThickness { get; set; } = 0.0f;

		/// <summary>
		/// Color of the border around the element
		/// </summary>
		public Color? BorderColor { get; set; }

		/// <summary>
		/// Constructs an Element
		/// </summary>
		protected Element()
		{
			Size = new Size(DefaultSize, DefaultSize);
		}
	}
}
