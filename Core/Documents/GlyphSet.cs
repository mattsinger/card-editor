﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;

namespace Core.Documents
{
	/// <summary>
	/// Defines the sets of glyphs (images) that can appear in card text
	/// </summary>
	[Document(Extension = ".cgs", Description = "Card Glyph Set", GroupingHeader = "Glyph Sets")]
	public class GlyphSet : IDocument
	{
		/// <summary>
		/// An individual glyph
		/// </summary>
		public class Glyph
		{
			/// <summary>
			/// The glyph's unique identifier
			/// </summary>
			public string Id { get; set; }

			/// <summary>
			/// The relative path to the 
			/// </summary>
			public string RelativePath { get; set; }
		}

		/// <summary>
		/// The list of Glyphs in this set
		/// </summary>
		public List<Glyph> Glyphs { get; } = new List<Glyph>();
	}
}
