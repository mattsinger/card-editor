﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;

namespace Core.Documents
{
	/// <summary>
	/// Attribute used to define document metadata
	/// </summary>
	[AttributeUsage(AttributeTargets.Class)]
	public class DocumentAttribute : Attribute
	{
		/// <summary>
		/// The document's default file extension
		/// </summary>
		public string Extension { get; set; }

		/// <summary>
		/// Description of the file type
		/// </summary>
		public string Description { get; set; }

		/// <summary>
		/// Grouping header name
		/// </summary>
		public string GroupingHeader { get; set; }

		/// <summary>
		/// Whether the document can be referenced in a Workspace
		/// </summary>
		public bool CanBeReferenced { get; set; } = true;
	}
}
