﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Core.Documents
{
	/// <summary>
	/// The type of field
	/// </summary>
	public enum FieldType
	{
		/// <summary>
		/// A field containing formatted text
		/// </summary>
		Text,
		/// <summary>
		/// A field containing an image path
		/// </summary>
		Image,
		/// <summary>
		/// A field containing a Color
		/// </summary>
		Color
	}
}
