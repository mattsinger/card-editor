﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;

namespace Core.Documents
{
	/// <summary>
	/// Defines a workspace, all of the referenced documents
	/// </summary>
	[Document(Extension = ".cew", Description = "Card Editor Workspace", GroupingHeader = "Workspaces", CanBeReferenced = false)]
	public class Workspace : IDocument
	{
		#region Referenced Documents

		/// <summary>
		/// Relative paths to documents referenced in this workspace
		/// </summary>
		public List<string> Documents { get; } = new List<string>();

		#endregion
	}
}
