﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Core.Drawing;
using Core.View_Models.Data_Set;
using Core.View_Models.Layout;
using Splat;

namespace Core.Util
{
	/// <summary>
	/// Interface for rendering cards to file
	/// </summary>
	public interface ICardRenderer
	{
		/// <summary>
		/// Renders a card to an image file
		/// </summary>
		/// <param name="card">The card to be rendered</param>
		/// <param name="layout">The layout dictating how it should be rendered</param>
		/// <param name="outputParams">The parameters for output</param>
		/// <param name="useFrontOrient">Whether the card should be rendered using the front orientation or the back orientation</param>
		/// <returns>A bitmap containing the rendered card or null if it failed to render</returns>
		IBitmap RenderCard(CardDataViewModel card, LayoutViewModel layout, OutputParams outputParams, bool useFrontOrient);
	}
}
