﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using ReactiveUI;

namespace Core.Util
{
	public static class Linq
	{
		/// <summary>
		/// Performs an action on each item in an enumerable collection
		/// </summary>
		/// <typeparam name="T">The type of object contained in the collection</typeparam>
		/// <param name="list">The enumerable collection</param>
		/// <param name="action">The action to perform</param>
		/// <returns>The originl list that was operated on</returns>
		public static IEnumerable<T> ForEach<T>(this IEnumerable<T> list, Action<T> action)
		{
			foreach (T value in list)
			{
				action(value);
			}

			return list;
		}

		/// <summary>
		/// Returns the set of all objects that appear in one enumeration but not in another, comparing equality
		/// via a lambda function rather than an IEqualityComparer
		/// </summary>
		/// <typeparam name="T">The type of object contained in the enumerations</typeparam>
		/// <param name="original">The first enumeration to select values from</param>
		/// <param name="excludes">The second enumeration of values we want to exclude</param>
		/// <param name="compareFunc">The equality comparison function</param>
		/// <returns>An enumeration that contains all elements appearing in original but not in excludes</returns>
		public static IEnumerable<T> Except<T>(this IEnumerable<T> original, IEnumerable<T> excludes,
			Func<T, T, bool> compareFunc)
		{
			foreach (T first in original)
			{
				if (excludes.All(second => !compareFunc(first, second)))
					yield return first;
			}
		}

		/// <summary>
		/// Returns the index of the first item in a list that meets the conditions set by the predicate
		/// </summary>
		/// <typeparam name="T">The type of object contained in the list</typeparam>
		/// <param name="list">The list of objects to search</param>
		/// <param name="predicate">The comparison predicate used to locate the object</param>
		/// <returns>The index of the first item in list that meets the predicate, or -1 if such an item does not exist</returns>
		public static int IndexOf<T>(this IList<T> list, Func<T, bool> predicate)
		{
			return list.IndexOf(list.FirstOrDefault(predicate));
		}

		/// <summary>
		/// Removes all objects from a ReactiveList that meet conditions set by some predicate
		/// </summary>
		/// <typeparam name="T">The type of object contained in the enumeration</typeparam>
		/// <param name="list">The ReactiveList from which we remove object</param>
		/// <param name="predicate">A predicate used to determine if an object should be removed</param>
		public static void RemoveAll<T>(this ReactiveList<T> list, Func<T, bool> predicate)
		{
			int i = 0;
			while (i < list.Count)
			{
				if (predicate(list[i]))
					list.RemoveAt(i);
				else
					i++;
			}
		}
	}
}
