﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Core.Util
{
	/// <summary>
	/// Utility functions for nullables
	/// </summary>
	public static class Nullable
	{
		/// <summary>
		/// Determines if a nullable bool is not null and true
		/// </summary>
		/// <param name="nullBool">The bool? in question</param>
		/// <returns>True if nullBool is defined and true</returns>
		public static bool IsValidAndTrue(this bool? nullBool)
		{
			return nullBool != null && nullBool.Value;
		}

		/// <summary>
		/// Determines if a nullable bool is null or false
		/// </summary>
		/// <param name="nullBool">The bool? in question</param>
		/// <returns>True if the nullBool is null or false</returns>
		public static bool IsNullOrFalse(this bool? nullBool)
		{
			return nullBool == null || !nullBool.Value;
		}
	}
}
