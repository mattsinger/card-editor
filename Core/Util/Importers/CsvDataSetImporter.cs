﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Core.Documents;
using Core.View_Models.Data_Set;
using CsvHelper;
using Splat;

namespace Core.Util.Importers
{
	[DataSetImporter(".csv")]
	internal class CsvDataSetImporter : IDataSetImporter
	{
		/// <summary>
		/// Gets the list of subsections within the import data. The exact meaning of this is 
		/// specific to the importer.
		/// </summary>
		/// <param name="filepath">The absolute path to the file to load</param>
		/// <returns>An empty list</returns>
		public List<string> GetSubsections(string filepath)
		{
			return new List<string>();
		}

		/// <summary>
		/// Retrieves the headers from a CSV file
		/// </summary>
		/// <param name="filepath">The absolute path to the file</param>
		/// <param name="subsection">The subsection to load from. Unused for CSVs.</param>
		/// <param name="useHeaderRow">Whether it should use the first row as headers</param>
		/// <returns>A list of field headers, auto-populated with keys and types</returns>
		public List<FieldImporterViewModel> LoadHeaders(string filepath, string subsection, bool useHeaderRow)
		{
			List<FieldImporterViewModel> headers = new List<FieldImporterViewModel>();

			IFileSystem fileSystem = Locator.Current.GetService<IFileSystem>();
			if (!fileSystem.Exists(filepath))
				return headers;

			using (StreamReader fsReader = new StreamReader(fileSystem.ReadLocalFile(filepath)))
			{
				using (CsvReader reader = new CsvReader(fsReader, CultureInfo.InvariantCulture))
				{
					reader.Read();
					reader.ReadHeader();

					int numColumns = reader.Context.Record.Length;

					for (int i = 0; i < numColumns; i++)
					{
						string header = null;
						if (useHeaderRow)
							header = reader.GetField(i);

						if (string.IsNullOrEmpty(header))
							header = $"Field{i}";

						headers.Add(new FieldImporterViewModel
						{
							Key = header
						});
					}

					reader.Read();
					for (int i = 0; i < numColumns; i++)
					{
						headers[i].FieldType = DataSetImportHelpers.GetFieldTypeFromData(reader.GetField(i));
					}
				}
			}

			return headers;
		}

		/// <summary>
		/// Retrieves the set of cards from a CSV file
		/// </summary>
		/// <param name="filepath">The absolute path to the file</param>
		/// <param name="subsection">The subsection to read from. Unused for CSVs.</param>
		/// <param name="useHeaderRow">Whether it should use the first row as headers</param>
		/// <param name="fields">The card field descriptors in column order, including the id column if present</param>
		/// <returns>A list of cards</returns>
		public List<CardData> LoadCards(string filepath, string subsection, bool useHeaderRow, 
			List<FieldImporterViewModel> fields)
		{
			List<CardData> cards = new List<CardData>();

			IFileSystem fileSystem = Locator.Current.GetService<IFileSystem>();
			if (!fileSystem.Exists(filepath))
				return cards;

			string fileName = Path.GetFileName(filepath);
			using (StreamReader fsReader = new StreamReader(fileSystem.ReadLocalFile(filepath)))
			{
				using (CsvReader reader = new CsvReader(fsReader, CultureInfo.InvariantCulture))
				{
					if (useHeaderRow)
					{
						reader.Read();
						reader.ReadHeader();
					}

					int cardCount = 1;
					while (reader.Read())
					{
						CardData cardData = new CardData
						{
							Id = $"{fileName}{cardCount:D3}"
						};

						for (int i = 0; i < fields.Count; i++)
						{
							if (!fields[i].Include)
								continue;

							if (fields[i].IsId)
								cardData.Id = reader.GetField(i);
							else
								cardData.Fields.Add(new CardField
								{
									Key = fields[i].Key,
									Value = reader.GetField(i)
								});
						}

						cards.Add(cardData);
						cardCount++;
					}
				}
			}

			return cards;
		}

		/// <summary>
		/// Disposes the importer
		/// </summary>
		public void Dispose()
		{
		}
	}
}
