﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Core.Documents;
using Core.Drawing;
using Core.View_Models.Data_Set;

namespace Core.Util.Importers
{
	/// <summary>
	/// Common interface for Data Set importers
	/// </summary>
	public interface IDataSetImporter : IDisposable
	{
		/// <summary>
		/// Gets the list of subsections within the import data. The exact meaning of this is 
		/// specific to the importer
		/// </summary>
		/// <param name="filepath">The absolute path to the file to load</param>
		/// <returns>A list of user-readable subsections</returns>
		List<string> GetSubsections(string filepath);

			/// <summary>
		/// Loads headers from the file
		/// </summary>
		/// <param name="filepath">The absolute path to the file to load</param>
		/// <param name="subsection">The name of the subsection to read from</param>
		/// <param name="useHeaderRow">Whether the first row of data is a header row</param>
		/// <returns>A list of FieldDescriptors, auto-generated from data stored in the file</returns>
		List<FieldImporterViewModel> LoadHeaders(string filepath, string subsection, bool useHeaderRow);

		/// <summary>
		/// Loads cards from the file
		/// </summary>
		/// <param name="filepath">The absoluate path to the file to load</param>
		/// <param name="subsection">The subsection to read from</param>
		/// <param name="useHeaderRow">Whether the first row of dat is a header row</param>
		/// <param name="fields">The card fields as they appear in order in the file, including an id column if present</param>
		/// <returns>A list of CardData</returns>
		List<CardData> LoadCards(string filepath, string subsection, bool useHeaderRow,
			List<FieldImporterViewModel> fields);
	}

	/// <summary>
	/// Helper methods used to import data
	/// </summary>
	public static class DataSetImportHelpers
	{
		/// <summary>
		/// Deduces the FieldType of a piece of imported data
		/// </summary>
		/// <param name="data">The imported data</param>
		/// <returns>A FiledType that most closely matches the data</returns>
		/// <remarks>
		/// This function will return Color if the data can parsed as such.
		/// This function will return Image if the data ends in a supported image file extension (e.g. "icon.png").
		/// However, while CardEditor generally expects a URI to a local file location, this function does not
		/// take that into account and ONLY checks for an ending file extension.
		/// If all other parsing checks fail, it falls back to Text type.</remarks>
		public static FieldType GetFieldTypeFromData(string data)
		{
			if (Color.Parse(data) != null)
				return FieldType.Color;

			try
			{
				string fileExt = Path.GetExtension(data);
				if (!string.IsNullOrEmpty(fileExt) && File.SupportedImageExtensions.Contains(fileExt))
					return FieldType.Image;
			}
			catch (ArgumentException)
			{
				// ArgumentExceptions are expected, and indicate data that is not a valid file path.
				// Swallow them and fall back to other parsing types
			}

			return FieldType.Text;
		}

		/// <summary>
		/// Creates an importer for the appropriate file extension
		/// </summary>
		/// <param name="extension">The extension of the file we want to import</param>
		/// <returns>The first Data Set Importer found that can import a file with the given extension.
		/// Returns null if no such importer is found.</returns>
		public static IDataSetImporter GetImporterForExtension(string extension)
		{
			if (string.IsNullOrEmpty(extension))
				return null;

			foreach (Tuple<TypeInfo, DataSetImporterAttribute> tuple in AttributeLocator<DataSetImporterAttribute>.GetAllInstances)
				if (tuple.Item2.Extensions.Contains(extension))
					return Activator.CreateInstance(tuple.Item1.AsType()) as IDataSetImporter;

			return null;
		}
	}
}
