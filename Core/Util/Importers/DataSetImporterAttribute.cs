﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;

namespace Core.Util.Importers
{
	/// <summary>
	/// Attribute that goes on DataSetImporters that help define what files it can import
	/// </summary>
	[AttributeUsage(AttributeTargets.Class)]
	public class DataSetImporterAttribute : Attribute
	{
		/// <summary>
		/// The extensions of files that the attached class can import
		/// </summary>
		public string[] Extensions { get; }

		/// <summary>
		/// Constructs a DataSetImporterAttribute
		/// </summary>
		/// <param name="extensions">The viable file extensions the attached class can import</param>
		public DataSetImporterAttribute(params string[] extensions)
		{
			Extensions = extensions;
		}
	}
}
