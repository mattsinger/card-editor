﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Core.Documents;
using Core.Drawing;
using Splat;

namespace Core.Util
{
	/// <summary>
	/// Interface for a platform-specific service for printing cards
	/// </summary>
	public interface ICardPrinter
	{

		/// <summary>
		/// Creates a new document to print
		/// </summary>
		/// <param name="printOptions">The size of the page</param>
		/// <param name="cardSize">The expected card size, in inches</param>
		IPrinterDocument CreateNewDocument(DeckPrintSet.PrintOptionValues printOptions, Size cardSize);

		/// <summary>
		/// Send a page to the printer
		/// </summary>
		/// <param name="page">The page to print</param>
		void Print(IPrinterDocument page);
	}

	/// <summary>
	/// Interface for a platform-specific printer document
	/// </summary>
	public interface IPrinterDocument
	{
		/// <summary>
		/// Adds a card
		/// </summary>
		/// <param name="front">The card's front image</param>
		/// <param name="back">The card's back image</param>
		void AddCard(IBitmap front, IBitmap back);
	}
}
