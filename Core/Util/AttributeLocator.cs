﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Core.Util
{
	/// <summary>
	/// Helper methods about finding classes with Attributes
	/// </summary>
	public static class AttributeLocator<TAttr> where TAttr : Attribute
	{
		/// <summary>
		/// Returns all types that have the requested attribute
		/// </summary>
		public static IEnumerable<Tuple<TypeInfo, TAttr>> GetAllInstances
		{
			get
			{
				var currentdomain = typeof(string).GetTypeInfo().Assembly.GetType("System.AppDomain").GetRuntimeProperty("CurrentDomain").GetMethod.Invoke(null, new object[] { });
				var getassemblies = currentdomain.GetType().GetRuntimeMethod("GetAssemblies", new Type[] { });
				Assembly[] assemblies = getassemblies.Invoke(currentdomain, new object[] { }) as Assembly[];

				if (assemblies != null)
					foreach (Assembly assembly in assemblies)
					{
						foreach (TypeInfo info in assembly.DefinedTypes)
						{
							IEnumerable<TAttr> attrs = info.GetCustomAttributes<TAttr>();
							if (attrs == null || !attrs.Any())
								continue;

							foreach (TAttr attr in attrs)
							{
								yield return new Tuple<TypeInfo, TAttr>(info, attr);
							}
						}
					}
			}
		}
	}
}
