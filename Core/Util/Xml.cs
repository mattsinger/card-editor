﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Xml.Linq;

namespace Core.Util
{
	public static class Xml
	{
		/// <summary>
		/// Gets whether the element matches a given name, ignoring case
		/// </summary>
		/// <param name="element">The named element</param>
		/// <param name="name">The name to check</param>
		/// <returns>True if the element name macthes, ignoring case. False otherwise.</returns>
		public static bool IsNamed(this XElement element, string name)
		{
			return string.Compare(element.Name.LocalName, name, StringComparison.OrdinalIgnoreCase) == 0;
		}

		/// <summary>
		/// Gets the value of an attribute in an XElement
		/// </summary>
		/// <param name="element">The element containing the attribute</param>
		/// <param name="attributeName">The name of the attribute</param>
		/// <returns>The attribute value, or null if the attribute does not exist</returns>
		public static string GetAttributeValue(this XElement element, string attributeName)
		{
			XAttribute attr = element.Attribute(attributeName);
			return attr?.Value.Trim();
		}
	}
}
