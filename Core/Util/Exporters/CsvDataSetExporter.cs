﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System.IO;
using System.Linq;
using Core.Documents;
using Splat;

namespace Core.Util.Exporters
{
	[DataSetExporter(".csv", "CSV table")]
	internal class CsvDataSetExporter : IDataSetExporter
	{
		/// <summary>
		/// Performs formatting to turn some initial string value into CSV-ready output
		/// </summary>
		/// <param name="initialOutput">The initial string value</param>
		/// <returns>A CSV-ready string</returns>
		public string GetOutputableString(string initialOutput)
		{
			bool needsQuotes = initialOutput.Contains(',') || 
								initialOutput.Contains('\n');
			string output = initialOutput;
			if (output.Contains("\""))
			{
				output = output.Replace("\"", "\"\"");
				needsQuotes = true;
			}
			if (needsQuotes)
			{
				output = $"\"{output}\"";
			}

			return output;
		}

		public void WriteCards(string filepath, DataSet dataSet)
		{
			IFileSystem fileSystem = Locator.Current.GetService<IFileSystem>();

			Stream fileStream = null;

			try
			{
				fileStream = fileSystem.WriteLocalFile(filepath, !fileSystem.Exists(filepath));
				StreamWriter writer = new StreamWriter(fileStream);

				// header line
				writer.Write("Id");
				foreach (DataSet.FieldDescriptor field in dataSet.FieldDescriptors)
				{
					writer.Write(',');
					writer.Write(GetOutputableString(field.Key));
				}
				writer.WriteLine();

				// card data
				for (int i = 0; i < dataSet.Data.Count; i++)
				{
					CardData cardData = dataSet.Data[i];
					writer.Write(cardData.Id);

					foreach (CardField cardField in cardData.Fields)
					{
						writer.Write(',');
						writer.Write(GetOutputableString(cardField.Value));
					}

					if (i < dataSet.Data.Count - 1)
						writer.WriteLine();
				}

				writer.Flush();
				writer.Dispose();
			}
			finally
			{
				fileStream?.Dispose();
			}
		}
	}
}
