﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Linq;
using System.Reflection;
using Core.Documents;

namespace Core.Util.Exporters
{
	/// <summary>
	/// Common interface for Data Set exporters
	/// </summary>
	public interface IDataSetExporter
	{
		/// <summary>
		/// Writes the cards in a DataSet to a spreadsheet
		/// </summary>
		/// <param name="filepath">The absolute path to the file to save</param>
		/// <param name="cards">The list of cards to save</param>
		void WriteCards(string filepath, DataSet cards);
	}

	/// <summary>
	/// Helper methods used to export card data
	/// </summary>
	public static class DataSetExportHelpers
	{
		/// <summary>
		/// Creates an exporter for the appropriate file extension
		/// </summary>
		/// <param name="extension">The extension of the file we want to export</param>
		/// <returns>The first Data Set Exporter found that can export a file with the given extension.
		/// Returns null if no such exporter is found.</returns>
		public static IDataSetExporter GetImporterForExtension(string extension)
		{
			if (string.IsNullOrEmpty(extension))
				return null;

			foreach (Tuple<TypeInfo, DataSetExporterAttribute> tuple in AttributeLocator<DataSetExporterAttribute>.GetAllInstances)
				if (tuple.Item2.Extension.Equals(extension))
					return Activator.CreateInstance(tuple.Item1.AsType()) as IDataSetExporter;

			return null;
		}
	}
}
