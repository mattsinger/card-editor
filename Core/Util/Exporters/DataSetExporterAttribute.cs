﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace Core.Util.Exporters
{
	[AttributeUsage(AttributeTargets.Class)]
	public class DataSetExporterAttribute : Attribute
	{
		/// <summary>
		/// The extension of the type of file that the attached class can export
		/// </summary>
		public string Extension { get; }

		/// <summary>
		/// A user-friendly description of the type of file that can be exported
		/// </summary>
		public string Description { get; }

		/// <summary>
		/// Constructs a DataSetExporterAttribute
		/// </summary>
		/// <param name="extension">The viable file extension the attached class can export</param>
		/// <param name="description">A user-friendly description of the exported file type</param>
		public DataSetExporterAttribute(string extension, string description)
		{
			Extension = extension;
			Description = description;
		}
	}
}
