﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.IO;
using Core.Documents;
using Core.Drawing;
using Core.View_Models;
using Splat;

namespace Core.Util
{
	/// <summary>
	/// Placeholder for useful enums
	/// </summary>
	public static class FileSystem
	{
		/// <summary>
		/// File operation types
		/// </summary>
		public enum Operation
		{
			/// <summary>
			/// Included for completeness
			/// </summary>
			None,
			/// <summary>
			/// Create a new file
			/// </summary>
			Create,
			/// <summary>
			/// Open an existing file
			/// </summary>
			Open,
			/// <summary>
			/// Shorthand for creating or opening
			/// </summary>
			CreateOrOpen,
			/// <summary>
			/// Save to a file
			/// </summary>
			Save
		}
	}

	/// <summary>
	/// Interface for file pickers
	/// </summary>
	public interface IFileSystem
	{

		#region Images

		/// <summary>
		/// Requests the path to an existing image file
		/// </summary>
		/// <param name="startPath">The start path to begin looking for files</param>
		/// <param name="imgPath">The path to the selected image file</param>
		/// <returns>True if an image file was selected, false otherwise</returns>
		bool GetExistingImage(string startPath, out string imgPath);

		/// <summary>
		/// Requests the path to an image file to create for output, expected to be a PNG (for now)
		/// </summary>
		/// <param name="startDirectory">The start path to begin looking for locations. Includes suggested filename.</param>
		/// <param name="startFile">The suggested filename for the image file</param>
		/// <param name="format">The file format for the output image</param>
		/// <param name="imgPath">The resulting path to the output file</param>
		/// <returns>True if an image file was selected, false otherwise</returns>
		bool GetOutputImage(string startDirectory, string startFile, Image.Format format, out string imgPath);

		/// <summary>
		/// Saves a bitmap to a file
		/// </summary>
		/// <param name="imgPath">The path of the image file</param>
		/// <param name="format">The format of the image</param>
		/// <param name="image">A bitmap containing the rendered card</param>
		/// <returns>True if the file saved successfully, false otherwise</returns>
		bool SaveImageToFile(string imgPath, Image.Format format, IBitmap image);

		#endregion

		#region Documents

		/// <summary>
		/// Requests the path to a document files
		/// </summary>
		/// <typeparam name="T">The type of document, which must implement IDocument</typeparam>
		/// <param name="startDirectory">The start directory to begin looking for files</param>
		/// <param name="operation">The file operation we want to perform</param>
		/// <param name="allowMultiple">Whether we allow multiple file paths or only one, ignored when operation is Save</param>
		/// <returns>Returns a list of filepaths to document files. Returns null if operation was cancelled</returns>
		IList<string> GetDocumentPaths<T>(string startDirectory, FileSystem.Operation operation, bool allowMultiple) where T : IDocument;

		/// <summary>
		/// Requests the path to a single existing document file
		/// </summary>
		/// <param name="docType">The type of document, which must implement IDocument</param>
		/// <param name="startDirectory">The start directory to begin looking for files</param>
		/// <param name="operation">The file operation we want to perform</param>
		/// <param name="allowMultiple">Whether we allow multiple file paths or only one, ignored when operation is Save</param>
		/// <returns>Returns a list of filepaths to document files. Returns null if operation was cancelled</returns>
		IList<string> GetDocumentPaths(Type docType, string startDirectory, FileSystem.Operation operation, bool allowMultiple);

		/// <summary>
		/// Requests a document from a given list
		/// </summary>
		/// <param name="documents">The list of documents to select from</param>
		/// <param name="title">A title to include describing the request</param>
		/// <returns>The selected document, or null</returns>
		BaseDocumentViewModel SelectDocument(IList<BaseDocumentViewModel> documents, string title);

		#endregion

		#region Import/Export Files

		/// <summary>
		/// Asks the user for the path to a file that can be imported to a DataSet
		/// </summary>
		/// <param name="startDirectory">The directory to start searching</param>
		/// <param name="filePath">The filepath to the chosen import file</param>
		/// <returns>True if the user selects an importable file, false otherwise</returns>
		bool GetDataSetImportFile(string startDirectory, out string filePath);

		/// <summary>
		/// Asks the user for the path to a file to which a DataSet can be exported
		/// </summary>
		/// <param name="startDirectory">The directory to start searching</param>
		/// <param name="filePath">The filepath of the chosen export file</param>
		/// <returns>True if the user selects a file, false otherwise</returns>
		bool GetDataSetExportFile(string startDirectory, out string filePath);

		#endregion

		#region Directories

		/// <summary>
		/// Requests the path to a directory
		/// </summary>
		/// <param name="startDirectory">The directory to start searching</param>
		/// <param name="finalDirectory">The directory the user actually selected</param>
		/// <returns>True if a directory was selected, false otherwise</returns>
		bool GetDirectory(string startDirectory, out string finalDirectory);

		#endregion

		#region Files

		/// <summary>
		/// Checks whether or not a file exists
		/// </summary>
		/// <param name="path">The full path to the file</param>
		/// <returns>True if the file exists, false otherwise</returns>
		bool Exists(string path);

		/// <summary>
		/// Checks whether the file is read-only or not
		/// </summary>
		/// <param name="path">The full path to the file</param>
		/// <returns>True if the file is read-only, false otherwise</returns>
		bool IsReadOnly(string path);

		/// <summary>
		/// Opens a local file for reading
		/// </summary>
		/// <param name="path">The path to the file</param>
		/// <returns>A stream that references the file</returns>
		/// <remarks>File opening can be platform-specific, so FileStream is unavailable in PCLs</remarks>
		Stream ReadLocalFile(string path);

		/// <summary>
		/// Opens or creates a local file for writing
		/// </summary>
		/// <param name="path">The path to the file</param>
		/// <param name="create">If true, the file will be created. Otherwise the file is assumed to exist.</param>
		/// <returns>A stream suitable for writing to the defined file</returns>
		Stream WriteLocalFile(string path, bool create);

		/// <summary>
		/// Deletes a file from the file system
		/// </summary>
		/// <param name="path">The path of the file to be deleted</param>
		/// <returns>True if it was successfully deleted, false otherwise</returns>
		bool DeleteLocalFile(string path);

		/// <summary>
		/// Gets the directory separating character
		/// </summary>
		/// <remarks>Path.DirectorySeparatorChar is platform-specific, so unavailable in PCLs</remarks>
		char DirectorySeparatorChar { get; }

		/// <summary>
		/// Gets the alternate directory separating character
		/// </summary>
		/// <remarks>Path.AltDirectorySeparatorChar is platform-specific, so unavailable in PCLs</remarks>
		char AltDirectorySeparatorChar { get; }

		#endregion
	}
}
