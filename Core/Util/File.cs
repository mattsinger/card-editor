﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Core.Documents;
using Core.Drawing;
using Core.Util.Exporters;
using Core.Util.Importers;
using Splat;

namespace Core.Util
{
	/// <summary>
	/// File-related utility functions
	/// </summary>
	public static class File
	{
		/// <summary>
		/// Returns all classes that inherit from IDocument and have a DocumentAttribute
		/// </summary>
		public static IEnumerable<Type> ReferenceableDocumentTypes
		{
			get
			{
				Assembly assembly = typeof(File).GetTypeInfo().Assembly;
				foreach (TypeInfo info in assembly.DefinedTypes)
				{
					if (!info.ImplementedInterfaces.Contains(typeof(IDocument)))
						continue;

					DocumentAttribute attr = info.GetCustomAttribute<DocumentAttribute>();
					if (attr == null || !attr.CanBeReferenced)
						continue;
						
					yield return info.AsType();
				}
			}
		}

		/// <summary>
		/// Returns extensions for all supported image formats
		/// </summary>
		public static IEnumerable<string> SupportedImageExtensions
		{
			get
			{
				return Image.InputImageFormats.Select(format =>
				{
					Image.ImageFormatAttribute attr = typeof(Image.Format).GetRuntimeField(format.ToString()).GetCustomAttribute<Image.ImageFormatAttribute>();
					return attr.Extension;
				});
			}
		}

		/// <summary>
		/// Returns extensions for all support file formats to import Data Sets
		/// </summary>
		public static IEnumerable<string> SupportedDataSetImportExtensions
		{
			get
			{
				foreach (Tuple<TypeInfo, DataSetImporterAttribute> tuple in AttributeLocator<DataSetImporterAttribute>.GetAllInstances)
				{
					foreach (string extension in tuple.Item2.Extensions)
						yield return extension;
				}
			}
		}

		/// <summary>
		/// Calculates a file's relative path with respect to a particular folder. Copied from 
		/// http://stackoverflow.com/questions/703281/getting-path-relative-to-the-current-working-directory
		/// </summary>
		/// <param name="filepath">The file whose relative path we want</param>
		/// <param name="folder">The root folder</param>
		/// <returns>A path to filespec relative to folder's path</returns>
		public static string GetRelativePath(string filepath, string folder)
		{
			Uri pathUri = new Uri(filepath, UriKind.RelativeOrAbsolute);

			if (pathUri.IsAbsoluteUri)
			{
				// Folders must end in a slash
				IFileSystem system = Locator.Current.GetService<IFileSystem>();
				char separator = system.DirectorySeparatorChar;
				if (!folder.EndsWith(separator.ToString()))
				{
					folder += separator;
				}
				Uri folderUri = new Uri(folder);
				return Uri.UnescapeDataString(folderUri.MakeRelativeUri(pathUri).ToString().Replace('/', separator));
			}

			return filepath;
		}

		/// <summary>
		/// Gets the correct full path to a file given a root and relative path
		/// </summary>
		/// <param name="rootPath">The root folder</param>
		/// <param name="relativePath">The path to the file relative to the root</param>
		/// <returns>The properly escaped complete filepath</returns>
		public static string GetFullPath(string rootPath, string relativePath)
		{
			IFileSystem system = Locator.Current.GetService<IFileSystem>();
			char separator = system?.DirectorySeparatorChar ?? '\\';
			char altSeparator = system?.AltDirectorySeparatorChar ?? '/';
			if (rootPath.Last() == separator ||
			    rootPath.Last() == altSeparator)
			{
				return rootPath + relativePath;
			}

			return rootPath + separator + relativePath;
		}

		/// <summary>
		/// Returns the expected document Type from the file extension
		/// </summary>
		/// <param name="filepath">The file's path</param>
		/// <returns>A Type corresponding to the appropriate Document</returns>
		public static Type GetDocumentTypeFromFilePath(string filepath)
		{
			string ext = Path.GetExtension(filepath);
			foreach (Type documentType in ReferenceableDocumentTypes)
			{
				DocumentAttribute attr = documentType.GetTypeInfo().GetCustomAttribute<DocumentAttribute>();

				if (attr.Extension.Equals(ext, StringComparison.OrdinalIgnoreCase))
					return documentType;
			}
			return ReferenceableDocumentTypes.ToList().Find(
				t => t.GetTypeInfo().GetCustomAttribute<DocumentAttribute>().Extension
					.Equals(ext, StringComparison.OrdinalIgnoreCase));
		}
	}
}
