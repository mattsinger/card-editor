﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using Core.Drawing;

namespace Core.Util
{
	public static class Math
	{
		public const float MmPerInch = 25.4f;

		/// <summary>
		/// Clamps a comparable value to some min or max
		/// </summary>
		/// <typeparam name="T">The clamped type, which must implement IComparable</typeparam>
		/// <param name="val">The value to clamp</param>
		/// <param name="min">The minimum value</param>
		/// <param name="max">The maximum</param>
		/// <returns>The initial value if it falls within the range of min and max, or the closest extreme</returns>
		/// <remarks>This does not modify the original value. It only returns the clamped result.</remarks>
		public static T Clamp<T>(this T val, T min, T max) where T : IComparable<T>
		{
			if (val.CompareTo(min) < 0)
				return min;

			if (val.CompareTo(max) > 0)
				return max;

			return val;
		}

		/// <summary>
		/// Calculates the corresponding display length for a real side measured in inches
		/// </summary>
		/// <param name="lengthInInches">The real-world length, in inches</param>
		/// <param name="ppi">The display's pixels-per-inch</param>
		/// <returns>The corresponding display size, in pixels</returns>
		public static float GetDisplayLengthFromInches(float lengthInInches, float ppi)
		{
			return ppi * lengthInInches;
		}

		/// <summary>
		/// Calculates the corresponding real length in inches from the display length
		/// </summary>
		/// <param name="lengthInDisplay">The number of pixels in the display size</param>
		/// <param name="ppi">The display's pixels-per-inch</param>
		/// <returns>The corresponding real-world length, in inches</returns>
		public static float GetInchesLengthFromDisplay(float lengthInDisplay, float ppi)
		{
			return lengthInDisplay / ppi;
		}

		/// <summary>
		/// Calculates the corresponding display size for a real size measured in inches
		/// </summary>
		/// <param name="sizeInInches">The real-world size, in inches</param>
		/// <param name="ppi">The display's pixels-per-inch</param>
		/// <returns>The corresponding display size, in pixels</returns>
		public static Size GetDisplaySizeFromInches(Size sizeInInches, Size ppi)
		{
			return new Size(GetDisplayLengthFromInches(sizeInInches.Width, ppi.Width),
				GetDisplayLengthFromInches(sizeInInches.Height, ppi.Height));
		}

		/// <summary>
		/// Calculates the corresponding real size in inches from the display size
		/// </summary>
		/// <param name="sizeInDisplay">The display size</param>
		/// <param name="ppi">The display's pixels-per-inch</param>
		/// <param name="viewScale">The current view scale, normalized</param>
		/// <returns>The corresponding real size, in inches</returns>
		public static Size GetInchesFromDisplaySize(Size sizeInDisplay, Size ppi, float viewScale = 1.0f)
		{
			return new Size(GetInchesLengthFromDisplay(sizeInDisplay.Width, ppi.Width),
				GetInchesLengthFromDisplay(sizeInDisplay.Height, ppi.Height));
		}

		/// <summary>
		/// Checks if two float values are equivalent
		/// </summary>
		/// <param name="a">The first float value</param>
		/// <param name="b">The second float value</param>
		/// <param name="epsilon">The largest acceptable difference before considering two values unequal. Default is .001</param>
		/// <returns>True if the float values are equivalent, defined as being within some small epsilon</returns>
		public static bool AreEquivalent(float a, float b, float epsilon = .001f)
		{
			return System.Math.Abs(a - b) <= epsilon;
		}

		/// <summary>
		/// Converts a string input to a float value
		/// </summary>
		/// <param name="input">The string to convert</param>
		/// <returns>The float representation if the string is a valid float value, null otherwise</returns>
		public static float? GetFloat(this string input)
		{
			if (string.IsNullOrEmpty(input))
				return null;

			float value;
			if (float.TryParse(input, out value))
				return value;
			return null;
		}

		/// <summary>
		/// Converts degrees to radians
		/// </summary>
		/// <param name="degrees">The angle in degrees</param>
		/// <returns>The angle in radians</returns>
		public static double ToRadians(this double degrees)
		{
			return degrees * System.Math.PI / 180;
		}

		/// <summary>
		/// Converts degrees to radians
		/// </summary>
		/// <param name="degrees">The angle in degrees</param>
		/// <returns>The angle in radians</returns>
		public static float ToRadians(this float degrees)
		{
			return degrees * (float)System.Math.PI / 180.0f;
		}

		/// <summary>
		/// Converts radians to degrees
		/// </summary>
		/// <param name="radians">The angle in radians</param>
		/// <returns>The angle in degrees</returns>
		public static double ToDegrees(this double radians)
		{
			return radians * 180 / System.Math.PI;
		}

		/// <summary>
		/// Converts radians to degrees
		/// </summary>
		/// <param name="radians">The angle in radians</param>
		/// <returns>The angle in degrees</returns>
		public static float ToDegrees(this float radians)
		{
			return radians * 180.0f / (float)System.Math.PI;
		}

		/// <summary>
		/// Rotates a Point clockwise
		/// </summary>
		/// <param name="point">The point to rotate</param>
		/// <param name="radians">The angle to rotate</param>
		/// <returns>The rotated Point</returns>
		public static Point Rotate(this Point point, float radians)
		{
			float cos = (float) System.Math.Cos(radians);
			float sin = (float) System.Math.Sin(radians);
			return new Point(
				point.X * cos - point.Y * sin,
				point.X * sin + point.Y * cos);
		}
	}
}
