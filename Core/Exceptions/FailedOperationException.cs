﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Exceptions
{
	/// <summary>
	/// Common base class for exceptions that occur when an operation on documents fails
	/// </summary>
	public class FailedOperationException : Exception
	{
		/// <summary>
		/// A document which failed to save
		/// </summary>
		public struct FailedDocument
		{
			/// <summary>
			/// The path to the document relative to the workspace root
			/// </summary>
			public string Path { get; set; }

			/// <summary>
			/// The exception thrown in the failure
			/// </summary>
			public Exception Reason { get; set; }
		}

		/// <summary>
		/// Message describing the failed documents and why they failed
		/// </summary>
		public override string Message
		{
			get
			{
				StringBuilder builder = new StringBuilder("The following documents failed to load:\n");
				foreach (FailedDocument failedDoc in FailedDocs)
				{
					builder.AppendFormat("{0}: {1}\n", failedDoc.Path, failedDoc.Reason.Message);
				}

				return builder.ToString();
			}
		}

		/// <summary>
		/// The documents which failed to save properly
		/// </summary>
		public List<FailedDocument> FailedDocs { get; } = new List<FailedDocument>();
	}
}
