﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using Core.View_Models.Glyph_Set;
using ReactiveUI;
using Splat;

namespace Core.Text
{
	/// <summary>
	/// Node that represents a glyph, tied to an IGlyphViewModel
	/// </summary>
	public class GlyphNode : Node
	{
		private IGlyphViewModel _glyphViewModel;
		private IDisposable _glyphDisposable;

		/// <summary>
		/// The contained Glyph
		/// </summary>
		public IBitmap Glyph => _glyphViewModel?.Image;

		/// <summary>
		/// The matching GlyphViewModel
		/// </summary>
		/// <remarks>The GlyphViewModel must have a matching Id</remarks>
		public IGlyphViewModel GlyphViewModel
		{
			get { return _glyphViewModel; }
			set
			{
				if (value != null && value.Id != Id)
					throw new ArgumentOutOfRangeException(nameof(value), "GlyphViewModel's Id does not match");

				_glyphDisposable?.Dispose();
				_glyphViewModel = value;
				this.RaisePropertyChanged(nameof(Glyph));

				if (_glyphViewModel != null)
					_glyphDisposable = _glyphViewModel.WhenAnyValue(vm => vm.Image).Subscribe(_ => this.RaisePropertyChanged(nameof(Glyph)));
			}
		}

		/// <summary>
		/// The Id of the represented glyph
		/// </summary>
		public string Id { get; }

		/// <summary>
		/// Constructs a GlyphNode
		/// </summary>
		/// <param name="id">The id of the represented glyph</param>
		/// <exception cref="ArgumentOutOfRangeException">Thrown if id is null or empty</exception>
		public GlyphNode(string id)
		{
			if (string.IsNullOrEmpty(id))
				throw new ArgumentOutOfRangeException(nameof(id));

			Id = id;
		}

		#region Overrides of Node

		/// <summary>
		/// Overridden node-specific dispose behavior
		/// </summary>
		/// <param name="finalize">Whether the call is being made from the finalizer or not</param>
		protected override void DoDispose(bool finalize)
		{
			base.DoDispose(finalize);

			_glyphDisposable?.Dispose();
		}

		#endregion
	}
}
