﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Core.Drawing;
using Core.Util;
using Core.View_Models;
using Core.View_Models.Glyph_Set;

namespace Core.Text
{
	/// <summary>
	/// A tree that describes formatted text as a hierarchy of Nodes
	/// </summary>
	public class Tree : Container
	{
		private readonly IWorkspaceViewModel _workspaceViewModel;
		private readonly List<GlyphNode> _glyphs = new List<GlyphNode>();
		
		/// <summary>
		/// Constructs a parse Tree
		/// </summary>
		/// <param name="workspace">The Tree's workspace</param>
		public Tree(IWorkspaceViewModel workspace)
		{
			_workspaceViewModel = workspace;
			_workspaceViewModel.Glyphs.CollectionChanged += GlyphsChangedHandler;
		}

		#region Overrides of Container

		/// <summary>
		/// Overridden node-specific dispose behavior
		/// </summary>
		/// <param name="finalize">Whether the call is being made from the finalizer or not</param>
		protected override void DoDispose(bool finalize)
		{
			_workspaceViewModel.Glyphs.CollectionChanged -= GlyphsChangedHandler;

			base.DoDispose(finalize);
		}

		#endregion

		/// <summary>
		/// The full list of GlyphNodes within the tree, populated when calling Parse
		/// </summary>
		public IReadOnlyList<GlyphNode> Glyphs => _glyphs;

		/// <summary>
		/// Clears out the current tree and creates a new one based on input text
		/// </summary>
		/// <param name="text">The text to parse</param>
		/// <remarks>
		/// Change notifications for Children will be suppressed while rebuilding the tree, followed by a Reset.
		/// If you are calling this function, register on the Children.ShouldReset observable.
		/// Calling this function will also reset Glyphs.
		/// </remarks>
		public void Parse(string text)
		{
			using (Children.SuppressChangeNotifications())
			{
				// clear out the old children
				foreach (Node child in Children)
					child.Dispose(false);
				Children.Clear();
				_glyphs.ForEach(glyph => glyph.GlyphViewModel = null);
				_glyphs.Clear();

				// to make things simple, wrap everything in a <root> element and parse it as XML. This saves us a lot of grief.
				string parseableText = $"<root>{text}</root>";
				try
				{
					XElement textTree = XElement.Parse(parseableText, LoadOptions.PreserveWhitespace);

					PopulateContainer(this, textTree.FirstNode);
				}
				catch (XmlException)
				{
					Children.Add(new TextNode
					{
						Text = text
					});
				}

				_glyphs.ForEach(glyph => 
					glyph.GlyphViewModel = _workspaceViewModel.Glyphs.FirstOrDefault(gvm => 
						gvm.Id == glyph.Id));
			}
			Children.Reset();
		}

		// TODO: Replace this with registered factories that can detect the correct
		// type of XNode, parse the attributes/contents, and return the resulting subtree.
		// This would drastically simplify this function, as well as make it easier to expand
		// functionality for both existing and future Nodes
		private void PopulateContainer(Container container, XNode firstNode)
		{
			XNode curNode = firstNode;
			while (curNode != null)
			{
				XText xmlText = curNode as XText;
				XElement xmlElement = curNode as XElement;

				if (xmlText != null)
				{
					container.Children.Add(new TextNode
					{
						Text = xmlText.Value
					});
				}
				else if (xmlElement != null)
				{
					if (xmlElement.IsNamed("glyph"))
					{
						string glyphName = xmlElement.GetAttributeValue("name");
						if (!string.IsNullOrEmpty(glyphName))
						{
							GlyphNode newGlyph = new GlyphNode(glyphName);
							container.Children.Add(newGlyph);
							_glyphs.Add(newGlyph);
						}
					}
					else if (xmlElement.IsNamed("font"))
					{
						FontSpan fontSpan = new FontSpan();

						string name = xmlElement.GetAttributeValue("name");
					    if (!string.IsNullOrEmpty(name))
					        fontSpan.Font.Name = name;

						float size;
						if (float.TryParse(xmlElement.GetAttributeValue("size"), out size))
							fontSpan.Font.Size =size;

					    bool bold, italic;
					    if (bool.TryParse(xmlElement.GetAttributeValue("bold"), out bold))
					        fontSpan.Font.Bold = bold;
					    if (bool.TryParse(xmlElement.GetAttributeValue("italics"), out italic))
					        fontSpan.Font.Italics = italic;

						fontSpan.Color = Color.Parse(xmlElement.GetAttributeValue("color"));
                        
						container.Children.Add(fontSpan);

						if (xmlElement.FirstNode != null)
							PopulateContainer(fontSpan, xmlElement.FirstNode);
					}
				}

				curNode = curNode.NextNode;
			}
		}

		private void GlyphsChangedHandler(object sender, NotifyCollectionChangedEventArgs args)
		{
			switch (args.Action)
			{
				case NotifyCollectionChangedAction.Add:
					foreach (GlyphViewModel newItem in args.NewItems)
						_glyphs.Where(glyphNode => glyphNode.Id == newItem.Id)
							.ForEach(glyphNode => glyphNode.GlyphViewModel = newItem);
					break;

				case NotifyCollectionChangedAction.Remove:
					foreach (GlyphViewModel oldItem in args.OldItems)
						_glyphs.Where(glyphNode => glyphNode.GlyphViewModel == oldItem)
							.ForEach(glyphNode => glyphNode.GlyphViewModel = null);
					break;

				case NotifyCollectionChangedAction.Replace:
					foreach (GlyphViewModel oldItem in args.OldItems)
						_glyphs.Where(glyphNode => glyphNode.GlyphViewModel == oldItem)
							.ForEach(glyphNode => glyphNode.GlyphViewModel = null);
					foreach (GlyphViewModel newItem in args.NewItems)
						_glyphs.Where(glyphNode => glyphNode.Id == newItem.Id)
							.ForEach(glyphNode => glyphNode.GlyphViewModel = newItem);
					break;

				case NotifyCollectionChangedAction.Reset:
					foreach (GlyphNode glyphNode in _glyphs)
						glyphNode.GlyphViewModel = _workspaceViewModel.Glyphs.FirstOrDefault(glyph => glyph.Id == glyphNode.Id);
					break;
			}
		}
	}
}
