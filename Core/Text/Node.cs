﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using ReactiveUI;

namespace Core.Text
{
	/// <summary>
	/// Represents a Node in a parsed Text Tree
	/// </summary>
	public abstract class Node : ReactiveObject, IDisposable
	{
		private bool _disposed;

		~Node()
		{
			Dispose(true);
		}

		#region Implementation of IDisposable

		/// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
		public void Dispose()
		{
			Dispose(true);
		}

		#endregion

		#region Protected Methods
		
		internal void Dispose(bool finalize)
		{
			if (_disposed)
				return;

			_disposed = true;
			DoDispose(finalize);
		}

		/// <summary>
		/// Overriden node-specific dispose behavior
		/// </summary>
		/// <param name="finalize">Whether the call is being made from the finalizer or not</param>
		protected virtual void DoDispose(bool finalize)
		{}

		#endregion
	}
}
