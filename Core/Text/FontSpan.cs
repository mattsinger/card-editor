﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Core.Drawing;

namespace Core.Text
{
	/// <summary>
	/// Composite Node that describes a change in font/color. Children are Nodes affected by the font change.
	/// </summary>
	public class FontSpan : Container
	{
		/// <summary>
		/// The new font. If null, then the font has not changed.
		/// </summary>
		public Font Font { get; } = new Font(null);

		/// <summary>
		/// The new text color. If null, then the color has not changed.
		/// </summary>
		public Color? Color { get; set; }
	}
}
