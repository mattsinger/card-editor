﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using ReactiveUI;

namespace Core.Text
{
	/// <summary>
	/// A Node that contains other Nodes as children
	/// </summary>
	public class Container : Node
	{
		/// <summary>
		/// The child Nodes
		/// </summary>
		public ReactiveList<Node> Children { get; } = new ReactiveList<Node>();

		#region Overrides of Node

		/// <summary>
		/// Overridden node-specific dispose behavior
		/// </summary>
		/// <param name="finalize">Whether the call is being made from the finalizer or not</param>
		protected override void DoDispose(bool finalize)
		{
			base.DoDispose(finalize);

			foreach (Node child in Children)
				child.Dispose(finalize);
		}

		#endregion
	}
}
