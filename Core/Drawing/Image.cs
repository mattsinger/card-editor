﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Reflection;

namespace Core.Drawing
{
	public static class Image
	{
		[AttributeUsage(AttributeTargets.Field)]
		public class ImageFormatAttribute : Attribute
		{
			/// <summary>
			/// Whether the format can be used as input
			/// </summary>
			public bool Input { get; set; }

			/// <summary>
			/// Whether the format can be used as output
			/// </summary>
			public bool Output { get; set; }

			/// <summary>
			/// The expected file extension
			/// </summary>
			public string Extension { get; set; }
		}

		/// <summary>
		/// Image formats
		/// </summary>
		public enum Format
		{
			/// <summary>
			/// Bitmap (.bmp) format
			/// </summary>
			[ImageFormat(Input = true, Output = true, Extension = ".bmp")]
			Bmp,

			/// <summary>
			/// Joint Photographics Expert Group (.jpg) format
			/// </summary>
			[ImageFormat(Input = true, Output = true, Extension = ".jpg")]
			Jpg,

			/// <summary>
			/// Portable Network Graphics (.png) format
			/// </summary>
			[ImageFormat(Input = true, Output = true, Extension = ".png")]
			Png,

			/// <summary>
			/// Tag Image File Format
			/// </summary>
			[ImageFormat(Input = true, Output = true, Extension = ".tif")]
			Tiff
		}

		/// <summary>
		/// Returns all acceptable input image formats
		/// </summary>
		public static IEnumerable<Format> InputImageFormats
		{
			get
			{
				Type formatType = typeof(Format);
				foreach (Format format in Enum.GetValues(formatType))
				{
					ImageFormatAttribute attr = formatType.GetRuntimeField(format.ToString()).GetCustomAttribute<ImageFormatAttribute>();
					if (attr != null && attr.Input)
						yield return format;
				}
			}
		}

		/// <summary>
		/// Returns all acceptable output image formats
		/// </summary>
		public static IEnumerable<Format> OutputImageFormats
		{
			get
			{
				Type formatType = typeof(Format);
				foreach (Format format in Enum.GetValues(formatType))
				{
					ImageFormatAttribute attr = formatType.GetRuntimeField(format.ToString()).GetCustomAttribute<ImageFormatAttribute>();
					if (attr != null && attr.Output)
						yield return format;
				}
			}
		}

		/// <summary>
		/// Gets the file extension of an image format
		/// </summary>
		/// <param name="format">The format in question</param>
		/// <returns>The file extension for the format, or null if not defined</returns>
		public static string GetExtension(this Format format)
		{
			ImageFormatAttribute attr = typeof(Format).GetRuntimeField(format.ToString()).GetCustomAttribute<ImageFormatAttribute>();
			return attr?.Extension;
		}
	}
}