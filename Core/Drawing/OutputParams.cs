﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;

namespace Core.Drawing
{
	/// <summary>
	/// Parameters used when outputting cards to file
	/// </summary>
	public class OutputParams
	{
        /// <summary>
        /// Default print resolution
        /// </summary>
	    public const float PrintResolution = 300;

        /// <summary>
        /// Default screen resolution
        /// </summary>
	    public const float ScreenResolution = 96;

		/// <summary>
		/// The PPI used to render the card
		/// </summary>
		public float Ppi = PrintResolution;

		/// <summary>
		/// Whether the output should include the layout bleed region
		/// </summary>
		public bool IncludeBleed = true;

		/// <summary>
		/// Enum defining orientation of the resulting card output
		/// </summary>
		public enum Orientation
		{
			Zero,
			Ninety,
			OneEighty,
			TwoSeventy
		}

		/// <summary>
		/// The orientation for front layouts, measured in degrees clockwise
		/// </summary>
		public Orientation FrontOrientation;

		/// <summary>
		/// The orientation for back layouts, measured in degrees clockwise
		/// </summary>
		public Orientation BackOrientation;

		/// <summary>
		/// The desired image format
		/// </summary>
		public Image.Format Format;

		/// <summary>
		/// Returns the flip of a given orientation
		/// </summary>
		/// <param name="orientation">The orientation to flip</param>
		/// <returns>The flipped orientation value</returns>
		public static Orientation GetFlippedOrientation(Orientation orientation)
		{
			return orientation + 2 % Enum.GetValues(typeof(OutputParams.Orientation)).Length;
		}

		/// <summary>
		/// Determines if two Orientations are flips of each other
		/// </summary>
		/// <param name="lhs">The first orientation to compare</param>
		/// <param name="rhs">The second orientation to compare</param>
		/// <returns>True if the two orientations represent flipped values, false otherwise</returns>
		public bool IsFlippedOrientation(Orientation lhs, Orientation rhs)
		{
			return rhs == GetFlippedOrientation(lhs);
		}
	}
}
