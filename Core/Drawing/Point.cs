﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Core.Drawing
{
	/// <summary>
	/// Defines a 2D position
	/// </summary>
	/// <remarks>Written as a replacement for System.Drawing.PointF, since System.Drawing isn't allowed in PCLs</remarks>
	public struct Point
	{
		/// <summary>
		/// Constructs a Point
		/// </summary>
		/// <param name="x">The x position</param>
		/// <param name="y">The y position</param>
		public Point(float x, float y)
		{
			X = x;
			Y = y;
		}

		/// <summary>
		/// Copy-constructs a Point
		/// </summary>
		/// <param name="point">The original Point</param>
		public Point(Point point)
		{
			X = point.X;
			Y = point.Y;
		}

		/// <summary>
		/// Constructs a Point from a Size
		/// </summary>
		/// <param name="size">The size to copy data from</param>
		public Point(Size size)
		{
			X = size.Width;
			Y = size.Height;
		}

		/// <summary>
		/// Converts the Point to a Size
		/// </summary>
		/// <returns>A Size whose Width and Height are this Point's X and Y, respectively</returns>
		public Size ToSize()
		{
			return new Size(this);
		}

		/// <summary>
		/// The x position
		/// </summary>
		public float X { get; set; }

		/// <summary>
		/// The y position
		/// </summary>
		public float Y { get; set; }

		#region Equality members

		/// <summary>
		/// Equality comparator operator
		/// </summary>
		/// <param name="lhs">The first Point to compare</param>
		/// <param name="rhs">The second Point to compare</param>
		/// <returns>True if both Points's Xs and Ys are within acceptable epsilon</returns>
		public static bool operator ==(Point lhs, Point rhs)
		{
			return Util.Math.AreEquivalent(lhs.X, rhs.X) &&
				Util.Math.AreEquivalent(lhs.Y, rhs.Y);
		}

		/// <summary>
		/// Inequiality comparator operator
		/// </summary>
		/// <param name="lhs">The first Point to compare</param>
		/// <param name="rhs">The second Point to compare</param>
		/// <returns>True if the Point's Xs or Ys are not within acceptable epsilon</returns>
		public static bool operator !=(Point lhs, Point rhs)
		{
			return !(lhs == rhs);
		}

		/// <summary>
		/// Equality comparator
		/// </summary>
		/// <param name="other">The other Point to compare</param>
		/// <returns>True if both Point's Widths and Heights are within acceptable epsilon</returns>
		public bool Equals(Point other)
		{
			return this == other;
		}

		/// <summary>Indicates whether this instance and a specified object are equal.</summary>
		/// <returns>true if <paramref name="obj" /> and this instance are the same type and represent the same value; otherwise, false. </returns>
		/// <param name="obj">The object to compare with the current instance. </param>
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj))
				return false;
			return obj is Point && Equals((Point)obj);
		}

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A 32-bit signed integer that is the hash code for this instance.</returns>
		public override int GetHashCode()
		{
			unchecked
			{
				return (X.GetHashCode() * 397) ^ Y.GetHashCode();
			}
		}

		#endregion
	}
}
