﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Core.Drawing
{
	/// <summary>
	/// Horizontal alignments
	/// </summary>
	public enum HorizontalAlignment
	{
		/// <summary>
		/// Aligned on the left side of the element
		/// </summary>
		Left,
		/// <summary>
		/// Aligned on the center of the element
		/// </summary>
		Center,
		/// <summary>
		/// Aligned on the right side of the element
		/// </summary>
		Right
	}

	/// <summary>
	/// Vertical alignments
	/// </summary>
	public enum VerticalAlignment
	{
		/// <summary>
		/// Aligned to the top side of the element
		/// </summary>
		Top,
		/// <summary>
		/// Aligned on the center of the element
		/// </summary>
		Center,
		/// <summary>
		/// Aligned on the bottom side of the element
		/// </summary>
		Bottom
	}
}
