﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;

namespace Core.Drawing
{
	/// <summary>
	/// Defines a font by its family and size
	/// </summary>
	/// <remarks>Written as a replacement to System.Drawing.Font, since System.Drawing isn't allowed in PCLs</remarks>
	public class Font
	{
		private float? _size;

		/// <summary>
		/// The font family name. If null or empty, then a default font should be used.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// The font size, measured in inches
		/// </summary>
		public float? Size
		{
			get { return _size; }
			set
			{
				if (value.HasValue && value.Value <= 0)
					throw new ArgumentOutOfRangeException(nameof(value), "Size must be a positive value");

				_size = value;
			}
		}

        /// <summary>
        /// Whether or not the font is rendered bold
        /// </summary>
        public bool? Bold { get; set; }

        /// <summary>
        /// Whether or not the font is rendered in italics
        /// </summary>
        public bool? Italics { get; set; }

		/// <summary>
		/// Default font size of .125 inches, approximately 12pt Font on screen
		/// </summary>
		public const float DefaultSize = 0.125f;

		/// <summary>
		/// Constructs a Font
		/// </summary>
		/// <param name="name">The font family name</param>
		/// <param name="size">The font size. If defined, it must be > 0. Default is DefaultSize</param>
		public Font(string name, float? size = null)
		{
			Name = name;
			Size = size;
		}

		/// <summary>
		/// Copy-constructs a Font
		/// </summary>
		/// <param name="rhs">The Font to copy from</param>
		public Font(Font rhs)
		{
			if (rhs != null)
				CopyFrom(rhs);
		}

		/// <summary>
		/// Copies data from an existing Font
		/// </summary>
		/// <param name="rhs">The Font to copy from</param>
		public void CopyFrom(Font rhs)
		{
			Name = rhs.Name;
			Size = rhs.Size;
			Bold = rhs.Bold;
			Italics = rhs.Italics;
		}
	}
}
