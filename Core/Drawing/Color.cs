﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Globalization;
using System.Linq;
using System.Reflection;
using ReactiveUI;

namespace Core.Drawing
{
	/// <summary>
	/// Represents a color via RGBA values
	/// </summary>
	/// <remarks>Written as a replacement for System.Drawing.Color, since System.Drawing isn't allowed in PCLs</remarks>
	public struct Color
	{
		#region Properties

		/// <summary>
		/// Alpha value
		/// </summary>
		public byte A { get; set; }

		/// <summary>
		/// Blue value
		/// </summary>
		public byte B { get; set; }

		/// <summary>
		/// Green value
		/// </summary>
		public byte G { get; set; }

		/// <summary>
		/// Red value
		/// </summary>
		public byte R { get; set; }

		#endregion

		#region Named Colors

		/// <summary>
		/// Red color
		/// </summary>
		public static Color Red => FromArgb(255, 0, 0);

		/// <summary>
		/// Green colors
		/// </summary>
		public static Color Green => FromArgb(0, 255, 0);

		/// <summary>
		/// Blue color
		/// </summary>
		public static Color Blue => FromArgb(0, 0, 255);

		/// <summary>
		/// Cyan color
		/// </summary>
		public static Color Cyan => FromArgb(0, 255, 255);

		/// <summary>
		/// Magenta color
		/// </summary>
		public static Color Magenta => FromArgb(255, 0, 255);

		/// <summary>
		/// Yellow color
		/// </summary>
		public static Color Yellow => FromArgb(255, 255, 0);

		/// <summary>
		/// White color
		/// </summary>
		public static Color White => FromArgb(255, 255, 255);

		/// <summary>
		/// Black color
		/// </summary>
		public static Color Black => FromArgb(0, 0, 0);

		/// <summary>
		/// Transparent color
		/// </summary>
		public static Color Transparent => FromArgb(0, 0, 0, 0);

		#endregion

		#region Constructors

		/// <summary>
		/// Constructs a Color from ARGB values, expected in the range of [0, 255]
		/// </summary>
		/// <param name="alpha">The alpha value</param>
		/// <param name="red">The alpha value</param>
		/// <param name="green">The green value</param>
		/// <param name="blue">The blue value</param>
		/// <returns></returns>
		public static Color FromArgb(int alpha, int red, int green, int blue)
		{
			return new Color
			{
				A = (byte)alpha,
				R = (byte)red,
				G = (byte)green,
				B = (byte)blue
			};
		}

		/// <summary>
		/// Constructs a Color from ARGB values, expected in the range of [0, 255]
		/// </summary>
		/// <param name="red">The alpha value</param>
		/// <param name="green">The green value</param>
		/// <param name="blue">The blue value</param>
		/// <returns></returns>
		public static Color FromArgb(int red, int green, int blue)
		{
			return new Color
			{
				A = Byte.MaxValue,
				R = (byte)red,
				G = (byte)green,
				B = (byte)blue
			};
		}

		/// <summary>
		/// Parses a string representation of a color, which can be in one of several forms:
		/// * Hex RGB ("#FFFFFF")
		/// * Hex ARGB ("#FFFFFFFF")
		/// * Integer RGB ("255,255,255")
		/// * Integer ARGB ("255,255,255,255")
		/// * Named color ("Black", case insensitive)
		/// </summary>
		/// <param name="text">The color in string form</param>
		/// <returns>A Color matching the input values. If the input doesn't match a recognized 
		/// pattern, it instead returns null</returns>
		public static Color? Parse(string text)
		{
			if (string.IsNullOrEmpty(text))
				return null;

			int r, g, b, a;
			if (text.First() == '#')	// hex parsing
			{
				if (text.Length == 7)	// RGB
				{
					if (int.TryParse(text.Substring(1, 2), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out r) &&
					    int.TryParse(text.Substring(3, 2), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out g) &&
					    int.TryParse(text.Substring(5, 2), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out b))
					{
						return FromArgb(r, g, b);
					}
				}
				else if (text.Length == 9)	// ARGB
				{
					if (int.TryParse(text.Substring(1, 2), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out a) &&
						int.TryParse(text.Substring(3, 2), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out r) &&
						int.TryParse(text.Substring(5, 2), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out g) &&
						int.TryParse(text.Substring(7, 2), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out b))
					{
						return FromArgb(a, r, g, b);
					}
				}
			}
			else
			{
				string[] tokens = text.Split(',');
				if (tokens.Length == 3)	// RGB
				{
					if (int.TryParse(tokens[0], out r) &&
						int.TryParse(tokens[1], out g) &&
						int.TryParse(tokens[2], out b))
					{
						return FromArgb(r, g, b);
					}
				}
				else if (tokens.Length == 4) // ARGB
				{
					if (int.TryParse(tokens[0], out a) && 
						int.TryParse(tokens[1], out r) &&
					    int.TryParse(tokens[2], out g) &&
					    int.TryParse(tokens[3], out b))
					{
						return FromArgb(a, r, g, b);
					}
				}
				else if (tokens.Length == 1)
				{
					string colorName = tokens[0].Trim();
					Type colorType = typeof(Color);
					foreach (PropertyInfo propertyInfo in colorType.GetTypeInfo().DeclaredProperties)
					{
						if (!propertyInfo.IsStatic() || propertyInfo.PropertyType != colorType)
							continue;

						if (string.Compare(propertyInfo.Name, colorName, StringComparison.OrdinalIgnoreCase) == 0)
							return propertyInfo.GetValue(null) as Color?;
					}
				}
			}

			return null;
		}

		#endregion

		#region Equality members

		/// <summary>
		/// Equivalence test between two Colors
		/// </summary>
		/// <param name="lhs">The left-hand side color</param>
		/// <param name="rhs">The right-hand side color</param>
		/// <returns>True if the colors are equivalent, false otherwise</returns>
		public static bool operator ==(Color lhs, Color rhs)
		{
			return lhs.Equals(rhs);
		}

		/// <summary>
		/// Non-equivalence test between two Colors
		/// </summary>
		/// <param name="lhs">The left-hand side color</param>
		/// <param name="rhs">The right-hand side color</param>
		/// <returns>True if the colors are not equivalent, false otherwise</returns>
		public static bool operator !=(Color lhs, Color rhs)
		{
			return !lhs.Equals(rhs);
		}

		/// <summary>
		/// Equivalence test with another Color
		/// </summary>
		/// <param name="other">The other color to compare</param>
		/// <returns>True if the colors are equivalent, false otherwise</returns>
		public bool Equals(Color other)
		{
			return A == other.A && B == other.B && G == other.G && R == other.R;
		}

		/// <summary>Indicates whether this instance and a specified object are equal.</summary>
		/// <returns>true if <paramref name="obj" /> and this instance are the same type and represent the same value; otherwise, false. </returns>
		/// <param name="obj">The object to compare with the current instance. </param>
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj))
				return false;
			return obj is Color && Equals((Color)obj);
		}

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A 32-bit signed integer that is the hash code for this instance.</returns>
		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = A.GetHashCode();
				hashCode = (hashCode * 397) ^ B.GetHashCode();
				hashCode = (hashCode * 397) ^ G.GetHashCode();
				hashCode = (hashCode * 397) ^ R.GetHashCode();
				return hashCode;
			}
		}

		#endregion

		#region ToString

		/// <summary>Returns the fully qualified type name of this instance.</summary>
		/// <returns>A <see cref="T:System.String" /> containing a fully qualified type name.</returns>
		public override string ToString()
		{
			return $"#{A.ToString("X2")}{R.ToString("X2")}{G.ToString("X2")}{B.ToString("X2")}";
		}

		#endregion
	}
}
