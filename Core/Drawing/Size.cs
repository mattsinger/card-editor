﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;

namespace Core.Drawing
{
	/// <summary>
	/// Represents the size of a 2D region
	/// </summary>
	/// <remarks>Written as a replacement for System.Drawing.SizeF, since System.Drawing isn't allowed in PCLs</remarks>
	public struct Size
	{
		/// <summary>
		/// Constructs a Size
		/// </summary>
		/// <param name="width">The region's Width</param>
		/// <param name="height">The region's Height</param>
		public Size(float width, float height)
		{
			Width = width;
			Height = height;
		}

		/// <summary>
		/// Copy-constructs a Size
		/// </summary>
		/// <param name="size">The original Size</param>
		public Size(Size size)
		{
			Width = size.Width;
			Height = size.Height;
		}

		/// <summary>
		/// Constructs a Size using a Point
		/// </summary>
		/// <param name="point">The point to copy data from</param>
		public Size(Point point)
		{
			Width = point.X;
			Height = point.Y;
		}

		/// <summary>
		/// The region's Height
		/// </summary>
		public float Height { get; set; }

		/// <summary>
		/// The region's Width
		/// </summary>
		public float Width { get; set; }

		/// <summary>
		/// Whether the Size represents a square shape (i.e. Width and Height are equivalent)
		/// </summary>
		public bool IsSquare => Util.Math.AreEquivalent(Height, Width);

		/// <summary>
		/// The inverse of the Size
		/// </summary>
		/// <returns>A Size with the Width and Height swapped</returns>
		public Size Inverse()
		{
			return new Size(Height, Width);
		}

		#region Arithmetic Operators

		/// <summary>
		/// Sums two Sizes
		/// </summary>
		/// <param name="first">The first Size</param>
		/// <param name="second">The second Size</param>
		/// <returns>A new Size with the combined area of lhs and rhs</returns>
		public static Size operator +(Size first, Size second)
		{
			return new Size(first.Width + second.Width, first.Height + second.Height);
		}

		/// <summary>
		/// Subtracts one Size from another
		/// </summary>
		/// <param name="first">The first Size</param>
		/// <param name="second">The second Size</param>
		/// <returns>A new Size with the difference of the two inputs</returns>
		public static Size operator -(Size first, Size second)
		{
			return new Size(first.Width - second.Width, first.Height - second.Height);
		}

		/// <summary>
		/// Uniform scales a Size
		/// </summary>
		/// <param name="size">The size to scale</param>
		/// <param name="scale">The scale to apply</param>
		/// <returns>A new Size equal to the scaled size</returns>
		public static Size operator *(Size size, float scale)
		{
			return new Size(size.Width * scale, size.Height * scale);
		}

		/// <summary>
		/// Uniform scales a Size
		/// </summary>
		/// <param name="size">The size to scale</param>
		/// <param name="scale">The scale to apply</param>
		/// <returns>A new Size equal to the scaled size</returns>
		public static Size operator /(Size size, float scale)
		{
			if (scale == 0)
				throw new DivideByZeroException();

			return new Size(size.Width / scale, size.Height / scale);
		}

		#endregion

		#region Equality members

		/// <summary>
		/// Equality comparator operator
		/// </summary>
		/// <param name="lhs">The first Size to compare</param>
		/// <param name="rhs">The second Size to compare</param>
		/// <returns>True if both Size's Widths and Heights are within acceptable epsilon</returns>
		public static bool operator ==(Size lhs, Size rhs)
		{
			return Util.Math.AreEquivalent(lhs.Width, rhs.Width) &&
				Util.Math.AreEquivalent(lhs.Height, rhs.Height);
		}

		/// <summary>
		/// Inequiality comparator operator
		/// </summary>
		/// <param name="lhs">The first Size to compare</param>
		/// <param name="rhs">The second Size to compare</param>
		/// <returns>True if the Size's Widths or Heights are not within acceptable epsilon</returns>
		public static bool operator !=(Size lhs, Size rhs)
		{
			return !(lhs == rhs);
		}

		/// <summary>
		/// Equality comparator
		/// </summary>
		/// <param name="other">The other Size to compare</param>
		/// <returns>True if both Size's Widths and Heights are within acceptable epsilon</returns>
		public bool Equals(Size other)
		{
			return this == other;
		}

		/// <summary>Indicates whether this instance and a specified object are equal.</summary>
		/// <returns>true if <paramref name="obj" /> and this instance are the same type and represent the same value; otherwise, false. </returns>
		/// <param name="obj">The object to compare with the current instance. </param>
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj))
				return false;
			return obj is Size && Equals((Size)obj);
		}

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A 32-bit signed integer that is the hash code for this instance.</returns>
		public override int GetHashCode()
		{
			unchecked
			{
				return (Height.GetHashCode() * 397) ^ Width.GetHashCode();
			}
		}

		/// <summary>
		/// Enum used to determine rotational equivalence between two Sizes
		/// </summary>
		public enum RotationalEquivalence
		{
			/// <summary>
			/// Two sizes are equal as they are
			/// </summary>
			Equal,

			/// <summary>
			/// Two sizes are equal if one is rotated
			/// </summary>
			RotateEqual,

			/// <summary>
			/// Two sizes are not equal
			/// </summary>
			NotEqual
		}

		/// <summary>
		/// Checks if two sizes are equivalent to each other, ignoring orientation
		/// </summary>
		/// <param name="lhs">The first Size to compare</param>
		/// <param name="rhs">The second Size to compare</param>
		/// <returns>True if lhs and rhs are of equivalent size, or if they're of equivalent size 
		/// when one of them swaps dimensions</returns>
		public static RotationalEquivalence AreRotationEquivalent(Size lhs, Size rhs)
		{
			if (lhs.Equals(rhs))
				return RotationalEquivalence.Equal;

			Size invertedLhs = lhs.Inverse();
			if (invertedLhs.Equals(rhs))
				return RotationalEquivalence.RotateEqual;

			return RotationalEquivalence.NotEqual;
		}

		#endregion
	}
}
