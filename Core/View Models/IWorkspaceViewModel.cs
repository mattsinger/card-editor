﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Core.Documents;
using Core.View_Models.Data_Set;
using Core.View_Models.Glyph_Set;
using Core.View_Models.Layout;
using ReactiveUI;

namespace Core.View_Models
{
	/// <summary>
	/// Interface for workspace view models
	/// </summary>
	public interface IWorkspaceViewModel
	{
		/// <summary>
		/// The workspace's root path
		/// </summary>
		string RootPath { get; }

		/// <summary>
		/// The full path to the workspace file
		/// </summary>
		string FullPath { get; }

		/// <summary>
		/// Whether the workspace is initializing, useful for document dependencies
		/// </summary>
		bool IsInitializing { get; }

		/// <summary>
		/// The full set of card datas in the workspace
		/// </summary>
		ReactiveList<CardDataViewModel> CardDatas { get; }

		/// <summary>
		/// The full set of glyphs in the workspace
		/// </summary>
		ReactiveList<IGlyphViewModel> Glyphs { get; }

		/// <summary>
		/// The full set of Layouts in this workspace
		/// </summary>
		ReactiveList<LayoutViewModel> Layouts { get; }
	}
}
