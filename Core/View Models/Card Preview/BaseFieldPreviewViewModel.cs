﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using Core.Drawing;
using Core.View_Models.Data_Set;
using Core.View_Models.Layout;
using ReactiveUI;

namespace Core.View_Models.Card_Preview
{
	/// <summary>
	/// Common base class for Preview ViewModels, provides read-only access to common Field and Element properties
	/// </summary>
	public abstract class BaseFieldPreviewViewModel : ReactiveObject, IDisposable
	{
		protected readonly List<IDisposable> _disposables = new List<IDisposable>();

		private string _fieldName;
		protected bool _disposed;

		#region Public Properties

		/// <summary>
		/// The containing CardPreviewViewModel
		/// </summary>
		public CardPreviewViewModel Parent { get; }

		/// <summary>
		/// The name of the field being previewed
		/// </summary>
		/// <remarks>This is defined by either the FieldViewModel or ElementViewModel.
		/// It may be null, if associated with an unbound ElementViewModel and no FieldViewModel.</remarks>
		public string FieldName
		{
			get => _fieldName;
			protected set => this.RaiseAndSetIfChanged(ref _fieldName, value);
		}

		/// <summary>
		/// The Element's Origin
		/// </summary>
		public Point Origin => ElementViewModel.Origin;

		/// <summary>
		/// The Element's Size in inches
		/// </summary>
		public Size Size => ElementViewModel.Size;

		/// <summary>
		/// The Element's Orientation
		/// </summary>
		public float Orientation => ElementViewModel.Orientation;

		/// <summary>
		/// The radius of Element's corners
		/// </summary>
		public float CornerRadius => ElementViewModel.CornerRadius;

		/// <summary>
		/// Which of the Element's corners should be rounded
		/// </summary>
		public ElementViewModel.RoundedCornersFlags RoundedCorners => ElementViewModel.RoundedCorners;

		/// <summary>
		/// Thickness of the Element's border
		/// </summary>
		public float BorderThickness => ElementViewModel.BorderThickness;

		/// <summary>
		/// Color of the Element's border
		/// </summary>
		public Color? BorderColor => ElementViewModel.BorderColor;

		/// <summary>
		/// The associated FieldViewModel
		/// </summary>
		/// <remarks>
		/// This may be null if the Preview is defined only by an Element
		/// </remarks>
		public abstract FieldViewModel FieldViewModel { get; set; }

		/// <summary>
		/// The type of FieldViewModel that this preview can support
		/// </summary>
		public abstract Type SupportedFieldType { get; }

		/// <summary>
		/// The associated ElementViewModel
		/// </summary>
		public abstract ElementViewModel ElementViewModel { get; }

		#endregion

		#region Public Methods

		/// <summary>
		/// Called when the underlying data has changed. ViewModel should grab the latest data
		/// and perform whatever work is necessary for viewing.
		/// </summary>
		public abstract void Refresh();

		#endregion

		#region Constructor & Finalizer

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="elementVm">The wrapped ElementViewModel</param>
		/// <param name="parent">The parent CardPreviewViewModel</param>
		/// <exception cref="ArgumentNullException">Thrown if elementVm or parent is null</exception>
		protected BaseFieldPreviewViewModel(ElementViewModel elementVm, CardPreviewViewModel parent)
		{
			if (elementVm == null)
				throw new ArgumentNullException(nameof(elementVm));

			Parent = parent ?? throw new ArgumentNullException(nameof(parent));

			// Any time the underlying element changes its bound field, clear out the field vm.
			// It will be reset by the containing CardPreviewViewModel.
			FieldName = elementVm.FieldName;
			_disposables.Add(elementVm
				.WhenAnyValue(vm => vm.FieldName)
				.Subscribe(name =>
				{
					FieldName = name;

					if (FieldViewModel != null && FieldViewModel.FieldName != name)
						FieldViewModel = null;
				}));

			// if the underlying element is not bound to a field, remove the field
			_disposables.Add(elementVm
				.WhenAnyValue(vm => vm.IsBoundToField)
				.Where(isBound => !isBound)
				.Subscribe(_ =>
				{
					FieldViewModel = null;
				}));

			// forward along changes to the base element's origin and size
			_disposables.Add(elementVm.WhenAnyValue(vm => vm.Origin)
				.Subscribe(_ => this.RaisePropertyChanged(nameof(Origin))));
			_disposables.Add(elementVm.WhenAnyValue(vm => vm.Size)
				.Subscribe(_ => this.RaisePropertyChanged(nameof(Size))));
			_disposables.Add(elementVm.WhenAnyValue(vm => vm.Orientation)
				.Subscribe(_ => this.RaisePropertyChanged(nameof(Orientation))));
			_disposables.Add(elementVm.WhenAnyValue(vm => vm.CornerRadius)
				.Subscribe(_ => this.RaisePropertyChanged(nameof(CornerRadius))));
			_disposables.Add(elementVm.WhenAnyValue(vm => vm.RoundedCorners)
				.Subscribe(_ => this.RaisePropertyChanged(nameof(RoundedCorners))));
			_disposables.Add(elementVm.WhenAnyValue(vm => vm.BorderThickness)
				.Subscribe(_ => this.RaisePropertyChanged(nameof(BorderThickness))));
			_disposables.Add(elementVm.WhenAnyValue(vm => vm.BorderColor)
				.Subscribe(_ => this.RaisePropertyChanged(nameof(BorderColor))));
		}

		~BaseFieldPreviewViewModel()
		{
			Dispose(true);
		}

		#endregion

		#region Disposable Pattern

		protected virtual void Dispose(bool finalize)
		{
			if (_disposed)
				return;

			_disposables.ForEach(_ => _.Dispose());
			_disposables.Clear();

			_disposed = true;
		}

		#endregion

		#region Implementation of IDisposable

		/// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
		public void Dispose()
		{
			Dispose(false);
		}

		#endregion
	}
}
