﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Core.Drawing;
using Core.Util;
using Core.View_Models.Data_Set;
using Core.View_Models.Layout;
using ReactiveUI;

namespace Core.View_Models.Card_Preview
{
	/// <summary>
	/// ViewModel that previews a Card and ties elements to their bound fields
	/// </summary>
	public class CardPreviewViewModel : ReactiveObject, IDisposable
	{
		#region Private Members

		private readonly WorkspaceViewModel _workspaceViewModel;
		private CardDataViewModel _cardDataViewModel;
		private LayoutViewModel _layoutViewModel;
		private bool _includeBleed;
		private Size _ppi;

		private readonly ReactiveList<BaseFieldPreviewViewModel> _previews = new ReactiveList<BaseFieldPreviewViewModel>
		{
			ChangeTrackingEnabled = true
		};

		private bool _disposed;
		private IDisposable _sizeObservable;
		private IDisposable _fieldsChanged;
		private IDisposable _elementsChanged;
		private Dictionary<BaseFieldPreviewViewModel, IDisposable> _previewFieldChangeDisposables = new Dictionary<BaseFieldPreviewViewModel, IDisposable>();

		#endregion

		#region Constructor

		/// <summary>
		/// Constructs a CardPreviewViewModel
		/// </summary>
		/// <param name="workspaceVm">The active workspace</param>
		/// <exception cref="ArgumentNullException">Thrown if workspaceVm is null</exception>
		public CardPreviewViewModel(WorkspaceViewModel workspaceVm)
		{
			if (workspaceVm == null)
				throw new ArgumentNullException(nameof(workspaceVm));
			_workspaceViewModel = workspaceVm;
		}

		#endregion

		#region Finalizer

		/// <summary>
		/// Finalizer
		/// </summary>
		~CardPreviewViewModel()
		{
			Dispose(true);
		}

		#endregion

		#region Public Properties

		/// <summary>
		/// The current CardData being previewed
		/// </summary>
		public CardDataViewModel CardDataViewModel
		{
			get => _cardDataViewModel;
			set
			{
				if (_cardDataViewModel != null)
					UnbindCardData();

				_cardDataViewModel = value;

				if (_cardDataViewModel != null)
					BindCardData();
			}
		}

		/// <summary>
		/// The current Layout being previewed
		/// </summary>
		public LayoutViewModel LayoutViewModel
		{
			get => _layoutViewModel;
			set
			{
				if (_layoutViewModel == value)
					return;

				if (_layoutViewModel != null)
					UnbindLayout();

				_layoutViewModel = value;

				if (_layoutViewModel != null)
					BindLayout();

				this.RaisePropertyChanged();	// forces resize and redraw
			}
		}

		/// <summary>
		/// The previewed Fields
		/// </summary>
		public IReadOnlyReactiveList<BaseFieldPreviewViewModel> PreviewFields => _previews;

		/// <summary>
		/// The size of the layout in inches
		/// </summary>
		public Size DisplaySize
		{
			get
			{
				if (_layoutViewModel != null)
					return IncludeBleed ? 
						_layoutViewModel.FullSize : 
						_layoutViewModel.Size;

				return new Size();
			}
		}

		/// <summary>
		/// Whether the preview should include bleed size or not
		/// </summary>
		public bool IncludeBleed
		{
			get => _includeBleed;
			set
			{
				this.RaiseAndSetIfChanged(ref _includeBleed, value);
				this.RaisePropertyChanged(nameof(DisplaySize));
			}
		}

		/// <summary>
		/// The pixels-per-inch of the resulting preview
		/// </summary>
		public Size Ppi
		{
			get => _ppi;
			set => this.RaiseAndSetIfChanged(ref _ppi, value);
		}

		#endregion

		#region Public Methods

		public void Refresh()
		{
			foreach (BaseFieldPreviewViewModel previewField in PreviewFields)
			{
				previewField.Refresh();
			}
		}

		#endregion

		#region Private Methods

		private void UnbindCardData()
		{
			_fieldsChanged?.Dispose();
			_fieldsChanged = null;

			foreach (BaseFieldPreviewViewModel previewField in PreviewFields)
				previewField.FieldViewModel = null;
		}

		private void BindCardData()
		{
			foreach (BaseFieldPreviewViewModel previewField in PreviewFields)
				AssignFieldForPreview(previewField);

			if (_fieldsChanged != null)
				throw new InvalidOperationException("Attempting to bind card data when it is already bound");
			_fieldsChanged = _cardDataViewModel.Fields.Changed.Subscribe(HandleFieldsCollectionChanged);
		}

		private void UnbindLayout()
		{
			_sizeObservable?.Dispose();
			_sizeObservable = null;
			_elementsChanged?.Dispose();
			_elementsChanged = null;

			_previews.ForEach(_ => _.Dispose());
			_previews.Clear();

			_previewFieldChangeDisposables.ForEach(pair => pair.Value.Dispose());
			_previewFieldChangeDisposables.Clear();
		}

		private void BindLayout()
		{
			foreach (ElementViewModel element in _layoutViewModel.Elements)
				_previews.Add(CreatePreviewForElement(element));

			if (_elementsChanged != null)
				throw new InvalidOperationException("Attempting to bind layout when it is already bound");
			_elementsChanged = _layoutViewModel.Elements.Changed.Subscribe(HandleElementsCollectionChanged);

			_sizeObservable = _layoutViewModel
				.WhenAnyValue(vm => vm.Size)
				.Subscribe(_ => this.RaisePropertyChanged(nameof(DisplaySize)));
		}

		private BaseFieldPreviewViewModel CreatePreviewForElement(ElementViewModel element)
		{
			BaseFieldPreviewViewModel preview;

			// ReSharper disable once CanBeReplacedWithTryCastAndCheckForNull
			if (element is TextElementViewModel)
			{
				preview = new TextFieldPreviewViewModel((TextElementViewModel) element, this, _workspaceViewModel);
			}
			else if (element is ImageElementViewModel)
			{
				preview = new ImageFieldPreviewViewModel((ImageElementViewModel) element, this);
			}
			else if (element is ColorElementViewModel)
			{
				preview = new ColorFieldPreviewViewModel((ColorElementViewModel) element, this);
			}
			else
			{
				throw new InvalidOperationException($"Cannot create preview for {element.GetType().Name} element");
			}

			_previewFieldChangeDisposables.Add(preview,
					preview.WhenAnyValue(vm => vm.FieldName)
							.Subscribe(_ =>
							{
								AssignFieldForPreview(preview);
							}));

			return preview;
		}

		private void AssignFieldForPreview(BaseFieldPreviewViewModel preview)
		{
			if (_cardDataViewModel == null)
				return;

			if (string.IsNullOrEmpty(preview.FieldName))
				return;

			foreach (FieldViewModel field in _cardDataViewModel.Fields)
			{
				if (field.FieldName == preview.FieldName &&
				    field.GetType() == preview.SupportedFieldType)
				{
					preview.FieldViewModel = field;
				}
			}
		}

		private void Dispose(bool finalize)
		{
			if (_disposed)
				return;

			UnbindLayout();
			_disposed = true;
		}

		#endregion

		#region Event Handlers

		private void HandleFieldsCollectionChanged(NotifyCollectionChangedEventArgs args)
		{
			switch (args.Action)
			{
				case NotifyCollectionChangedAction.Add:
					foreach (FieldViewModel newItem in args.NewItems)
					{
						foreach (BaseFieldPreviewViewModel previewField in PreviewFields)
						{
							if (previewField.FieldViewModel == null &&
							    previewField.FieldName == newItem.FieldName &&
								previewField.SupportedFieldType == newItem.GetType())
							{
								previewField.FieldViewModel = newItem;
							}
						}
					}
					break;

				case NotifyCollectionChangedAction.Remove:
					foreach (FieldViewModel oldItem in args.OldItems)
					{
						foreach (BaseFieldPreviewViewModel previewField in PreviewFields)
						{
							if (previewField.FieldViewModel == oldItem)
							{
								previewField.FieldViewModel = null;
							}
						}
					}
					break;
			}
		}

		private void HandleElementsCollectionChanged(NotifyCollectionChangedEventArgs args)
		{
			switch (args.Action)
			{
				case NotifyCollectionChangedAction.Add:
					int insertIndex = args.NewStartingIndex;
					foreach (ElementViewModel newItem in args.NewItems)
					{
						BaseFieldPreviewViewModel previewField = CreatePreviewForElement(newItem);
						AssignFieldForPreview(previewField);
						_previews.Insert(insertIndex, previewField);
						++insertIndex;
					}
					break;

				case NotifyCollectionChangedAction.Remove:
					int removeIndex = args.OldStartingIndex;
					foreach (ElementViewModel oldItem in args.OldItems)
					{
						if (_previews[removeIndex].ElementViewModel != oldItem)
							throw new InvalidOperationException("Mismatch between removed element and element preview");

						_previews[removeIndex].FieldViewModel = null;
						_previews.RemoveAt(removeIndex);
					}
					break;
			}
		}

		#endregion

		#region Implementation of IDisposable

		/// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
		public void Dispose()
		{
			Dispose(false);
		}

		#endregion
	}
}
