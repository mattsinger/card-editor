﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.IO;
using Core.Drawing;
using Core.View_Models.Data_Set;
using Core.View_Models.Layout;
using ReactiveUI;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using Splat;

namespace Core.View_Models.Card_Preview
{
	public class ImageFieldPreviewViewModel : BaseFieldPreviewViewModel
	{
		#region Private Members

		private readonly ImageElementViewModel _imageElementViewModel;
		private ImageFieldViewModel _imageFieldViewModel;
		
		private IDisposable _imageObservable;
		private Image<Bgra32> _image;

		#endregion

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="elementVm">The wrapped ElementViewModel</param>
		/// <param name="parent">The parent CardPreviewViewModel</param>
		/// <exception cref="ArgumentNullException">Thrown if elementVm or parent is null</exception>
		public ImageFieldPreviewViewModel(ImageElementViewModel elementVm, CardPreviewViewModel parent) : base(elementVm, parent)
		{
			_imageElementViewModel = elementVm;

			_disposables.Add(_imageElementViewModel
				.WhenAnyValue(vm => vm.Stretch)
				.Subscribe(_ =>
				{
					this.RaisePropertyChanged(nameof(Stretch));
				}));
			_disposables.Add(_imageElementViewModel
				.WhenAnyValue(vm => vm.HorizontalAlignment)
				.Subscribe(_ =>
				{
					this.RaisePropertyChanged(nameof(HorizontalAlignment));
				}));
			_disposables.Add(_imageElementViewModel
				.WhenAnyValue(vm => vm.VerticalAlignment)
				.Subscribe(_ =>
				{
					this.RaisePropertyChanged(nameof(VerticalAlignment));
				}));
		}

		#endregion

		#region Public Properties

		/// <summary>
		/// The associated FieldViewModel
		/// </summary>
		/// <remarks>
		/// This may be null if the Preview is defined only by an Element
		/// </remarks>
		public override FieldViewModel FieldViewModel
		{
			get => _imageFieldViewModel;
			set
			{
				if (value == _imageFieldViewModel)
					return;

				if (value != null && !(value is ImageFieldViewModel))
					throw new InvalidCastException("ImageFieldPreviewViewModel can only use ImageFieldViewModels");

				_imageFieldViewModel = (ImageFieldViewModel)value;
				this.RaisePropertyChanged();
				Refresh();
			}
		}

		/// <summary>
		/// The type of FieldViewModel that this preview can support
		/// </summary>
		public override Type SupportedFieldType => typeof(ImageFieldViewModel);

		/// <summary>
		/// The associated ElementViewModel
		/// </summary>
		/// <remarks>
		/// This may be null if the Preview is defined only by a Field.
		/// If a Field is defined, then the field names must match.
		/// </remarks>
		public override ElementViewModel ElementViewModel => _imageElementViewModel;

		#endregion

		#region Public Methods

		public override void Refresh()
		{
			LoadImage();
		}

		/// <summary>
		/// The image to display
		/// </summary>
		public Image<Bgra32> Image => ElementViewModel.IsBoundToField ?
			_image :
			_imageElementViewModel.Image;

		/// <summary>
		/// Whether the image stretches to fill the available space
		/// </summary>
		public bool Stretch => _imageElementViewModel.Stretch;

		/// <summary>
		/// The image's horizontal alignment
		/// </summary>
		public HorizontalAlignment HorizontalAlignment => _imageElementViewModel.HorizontalAlignment;

		/// <summary>
		/// The image's vertical alignment
		/// </summary>
		public VerticalAlignment VerticalAlignment => _imageElementViewModel.VerticalAlignment;

		#endregion

		#region Overrides of BaseFieldPreviewViewModel

		protected override void Dispose(bool finalize)
		{
			if (!_disposed)
			{
				_imageObservable?.Dispose();
				_imageObservable = null;
			}

			base.Dispose(finalize);
		}

		#endregion

		#region Private Methods

		private void LoadImage()
		{
			if (!string.IsNullOrEmpty(_imageFieldViewModel?.Path))
			{
				Image<Bgra32> image = null;
				try
				{
					string imgPath = Util.File.GetFullPath(_imageFieldViewModel.Root, _imageFieldViewModel.Path);
					image = SixLabors.ImageSharp.Image.Load<Bgra32>(imgPath);
				}
				// we might expect the user to type in an incorrect filename, in which case we should do nothing
				catch (FileNotFoundException)
				{
					this.Log().Warn("Preview Card {0}, Layout {1}, Image field {2}: Could not locate image file {3}",
						Parent.CardDataViewModel.Data.Id,
						Parent.LayoutViewModel.DisplayName,
						_imageFieldViewModel.FieldName,
						_imageFieldViewModel.Path);
				}
				catch (Exception ex)
				{
					this.Log().Error("Preview Card {0}, Layout {1}, Image field {2}: Failed to load image file {3}:\n{4}",
						Parent.CardDataViewModel.Data.Id,
						Parent.LayoutViewModel.DisplayName,
						_imageFieldViewModel.FieldName,
						_imageFieldViewModel.Path,
						ex);
				}

				_image = image;
			}
			else
			{
				_image = null;
			}

			this.RaisePropertyChanged(nameof(Image));
		}

		#endregion
	}
}
