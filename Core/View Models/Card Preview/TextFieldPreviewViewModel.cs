﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Core.Drawing;
using Core.Text;
using Core.View_Models.Data_Set;
using Core.View_Models.Layout;
using ReactiveUI;

namespace Core.View_Models.Card_Preview
{
	/// <summary>
	/// ViewModel that provides read-only access to a preview of a card Text field
	/// </summary>
	public class TextFieldPreviewViewModel : BaseFieldPreviewViewModel
	{
		#region Private Members

		private readonly TextElementViewModel _textElementViewModel;
		private TextFieldViewModel _textFieldViewModel;

		#endregion

		#region Properties

		/// <summary>
		/// The associated FieldViewModel
		/// </summary>
		/// <remarks>
		/// This may be null if the Preview is defined only by an Element
		/// </remarks>
		public override FieldViewModel FieldViewModel
		{
			get => _textFieldViewModel;
			set
			{
				if (value == _textFieldViewModel)
					return;

				if (value != null && !(value is TextFieldViewModel))
					throw new InvalidCastException("TextFieldPreviewViewModel can only use TextFieldViewModels");
				
				_textFieldViewModel = (TextFieldViewModel)value;
				this.RaisePropertyChanged();
				Refresh();
			}
		}

		/// <summary>
		/// The type of FieldViewModel that this preview can support
		/// </summary>
		public override Type SupportedFieldType => typeof(TextFieldViewModel);

		/// <summary>
		/// The base element view model
		/// </summary>
		public override ElementViewModel ElementViewModel => _textElementViewModel;

		/// <summary>
		/// The text to be displayed
		/// </summary>
		public string Text => ElementViewModel.IsBoundToField ? 
			_textFieldViewModel?.Text : 
			_textElementViewModel.Text;

		/// <summary>
		/// The Text as a parsed tree
		/// </summary>
		/// <remarks>
		/// Remember that Tree fires its Reset event when Tree.Parse is called, so any View for this VM
		/// should subscribe on ShouldReset
		/// </remarks>
		public Tree ParseTree { get; }

		/// <summary>
		/// The font to use to render the text
		/// </summary>
		public Font Font => _textElementViewModel.Font;

		/// <summary>
		/// The text's horizontal alignment
		/// </summary>
		public HorizontalAlignment HorizontalAlignment => _textElementViewModel.HorizontalAlignment;

		/// <summary>
		/// The text's vertical alignment
		/// </summary>
		public VerticalAlignment VerticalAlignment => _textElementViewModel.VerticalAlignment;

		/// <summary>
		/// The base text color
		/// </summary>
		public Color Color => _textElementViewModel.Color;

		#endregion

		#region Public Methods

		public override void Refresh()
		{
			RecalculateText();
		}

		#endregion

		#region Constructor

		/// <summary>
		/// Constructs a TextFieldPreviewViewModel
		/// </summary>
		/// <param name="textElemVm">The wrapped TextElementViewModel</param>
		/// <param name="parent">The parent CardPreviewViewModel</param>
		/// <param name="workspaceVm">The active workspace</param>
		/// <exception cref="ArgumentNullException">Thrown if textElemVm, parent, or workspaceVm are null</exception>
		public TextFieldPreviewViewModel(TextElementViewModel textElemVm, CardPreviewViewModel parent, WorkspaceViewModel workspaceVm) : base(textElemVm, parent)
		{
			_textElementViewModel = textElemVm ?? throw new ArgumentNullException(nameof(textElemVm));

			if (workspaceVm == null)
				throw new ArgumentNullException(nameof(workspaceVm));
			ParseTree = new Tree(workspaceVm);
			RecalculateText();

			_disposables.Add(_textElementViewModel.WhenAnyValue(elem => elem.Text)
				.Subscribe(_ => RecalculateText()));
			_disposables.Add(_textElementViewModel.WhenAnyValue(elem => elem.Font)
				.Subscribe(_ => this.RaisePropertyChanged(nameof(Font))));
			_disposables.Add(_textElementViewModel.WhenAnyValue(elem => elem.Color)
				.Subscribe(_ => this.RaisePropertyChanged(nameof(Color))));
			_disposables.Add(_textElementViewModel.WhenAnyValue(elem => elem.HorizontalAlignment)
				.Subscribe(_ => this.RaisePropertyChanged(nameof(HorizontalAlignment))));
			_disposables.Add(_textElementViewModel.WhenAnyValue(elem => elem.Font)
				.Subscribe(_ => this.RaisePropertyChanged(nameof(VerticalAlignment))));
		}

		#endregion

		#region Private Methods

		private void RecalculateText()
		{
			ParseTree.Parse(Text);
		}

		#region Overrides of BaseFieldPreviewViewModel

		protected override void Dispose(bool finalize)
		{
			if (_disposed)
				return;

			ParseTree.Dispose(finalize);
			base.Dispose(finalize);
		}

		#endregion

		#endregion
	}
}
