﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using Core.Drawing;
using Core.View_Models.Data_Set;
using Core.View_Models.Layout;
using ReactiveUI;

namespace Core.View_Models.Card_Preview
{
	/// <summary>
	/// ViewModel that provides read-only access to a preview of a card's Color field
	/// </summary>
	public class ColorFieldPreviewViewModel : BaseFieldPreviewViewModel
	{
		#region Private Members

		private readonly ColorElementViewModel _colorElementViewModel;
		private ColorFieldViewModel _colorFieldViewModel;

		#endregion

		#region Properties

		/// <summary>
		/// The color to be displayed
		/// </summary>
		/// <remarks>If unable to determine the appropriate color, uses Color.Transparent</remarks>
		public Color Color => (_colorElementViewModel.IsBoundToField ?
			_colorFieldViewModel?.Color :
			_colorElementViewModel.Color) ?? Color.Transparent;

		#endregion

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="colorElemVm">The wrapped ColorElementViewModel</param>
		/// <param name="parent">The parent CardPreviewViewModel</param>
		/// <exception cref="ArgumentNullException">Thrown if colorElemVm or parent is null</exception>
		public ColorFieldPreviewViewModel(ColorElementViewModel colorElemVm, CardPreviewViewModel parent) : base(colorElemVm, parent)
		{
			_colorElementViewModel = colorElemVm ?? throw new ArgumentNullException(nameof(colorElemVm));
		}

		#region Overrides of BaseFieldPreviewViewModel

		/// <summary>
		/// The associated FieldViewModel
		/// </summary>
		/// <remarks>
		/// This may be null if the Preview is defined only by an Element
		/// </remarks>
		public override FieldViewModel FieldViewModel
		{
			get => _colorFieldViewModel;
			set
			{
				if (value == _colorFieldViewModel)
					return;

				if (value != null && !(value is ColorFieldViewModel))
					throw new InvalidCastException("ColorFieldPreviewViewModel can only use ColorFieldViewModels");

				_colorFieldViewModel = (ColorFieldViewModel) value;
				this.RaisePropertyChanged();
				Refresh();
			}
		}

		/// <summary>
		/// The type of FieldViewModel that this preview can support
		/// </summary>
		public override Type SupportedFieldType => typeof(ColorFieldViewModel);

		/// <summary>
		/// The associated ElementViewModel
		/// </summary>
		public override ElementViewModel ElementViewModel => _colorElementViewModel;

		#endregion

		#region Public Methods

		public override void Refresh()
		{
			this.RaisePropertyChanged(nameof(Color));
		}

		#endregion
	}
}
