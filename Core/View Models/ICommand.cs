﻿namespace Core.View_Models
{
	/// <summary>
	/// Basic interface for commands
	/// </summary>
	public interface ICommand
	{
		/// <summary>
		/// Does the command
		/// </summary>
		void Do();

		/// <summary>
		/// Undoes the command
		/// </summary>
		void Undo();
	}
}
