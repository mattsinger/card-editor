﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Text;
using Core.Documents;
using Core.Exceptions;
using Core.Util;
using Core.View_Models.Data_Set;
using Core.View_Models.Deck_Print_Set;
using Core.View_Models.Glyph_Set;
using Core.View_Models.Layout;
using Newtonsoft.Json;
using ReactiveUI;
using Splat;
using ReactiveCommand = ReactiveUI.ReactiveCommand;

namespace Core.View_Models
{
	/// <summary>
	/// ViewModel that wraps a workspace
	/// </summary>
	public class WorkspaceViewModel : ReactiveObject, IWorkspaceViewModel
	{
		#region Private Members

		private Workspace _workspace;
		private readonly string _fileName;
		private BaseDocumentViewModel _selectedDocument;
		private BaseDocumentViewModel _selectedOpenDoc;
		private bool _isDirty;

		#endregion

		#region Constructor

		/// <summary>
		/// Constructs a WorkspaceViewModel
		/// </summary>
		/// <param name="rootPath">The root directory of the workspace</param>
		/// <param name="fileName">The name of the workspace file</param>
		/// <param name="loadFile">Whether we should attempt to load the file from disk or create new</param>
		/// <exception cref="FailedOperationException">Thrown if the document could not be loaded</exception>
		public WorkspaceViewModel(string rootPath, string fileName, bool loadFile)
		{
			RootPath = rootPath;
			_fileName = fileName;

			IsInitializing = true;

			// This may throw a LoadFailedException. If it does, the workspace is invalid and we should
			// stop here.
			if (loadFile)
				LoadDocument(Util.File.GetFullPath(rootPath, fileName));
			else
			{
				_workspace = new Workspace();
				IsDirty = true;
				IsNew = true;
			}

			FailedOperationException loadFailed = new FailedOperationException();
			InitializeData(loadFailed);
			CreateCommands();

			IsInitializing = false;
			InitializationEventArgs args = new InitializationEventArgs();
			Initialized?.Invoke(this, args);
			
			// individual documents failing to load shouldn't prevent others from loading nor prevent an
			// otherwise valid workspace from initializing
			if (loadFailed.FailedDocs.Any())
				throw loadFailed;

			if (args.FailedDocuments.FailedDocs.Any())
				throw args.FailedDocuments;

			this.Log().Info("Loaded workspace {0}", fileName);
		}

		#endregion

		#region Public Properties

		/// <summary>
		/// The path to the workspace root folder
		/// </summary>
		public string RootPath { get; }

		/// <summary>
		/// The path to the Workspace file
		/// </summary>
		public string FullPath => Util.File.GetFullPath(RootPath, _fileName);

		/// <summary>
		/// Whether the workspace is initializing, useful for document dependencies
		/// </summary>
		public bool IsInitializing { get; }

		/// <summary>
		/// Whether the workspace or any of its documents have unsaved changes in it
		/// </summary>
		public bool IsDirty
		{
			get
			{
				return _isDirty || Documents.Any(doc => doc.IsDirty);
			}
			protected set => this.RaiseAndSetIfChanged(ref _isDirty, value);
		}

		/// <summary>
		/// Whether the workspace is newly created or saved to disk
		/// </summary>
		public bool IsNew { get; set; }

		/// <summary>
		/// The full set of CardDatas in this Workspace
		/// </summary>
		public ReactiveList<CardDataViewModel> CardDatas { get; } = new ReactiveList<CardDataViewModel>();

		/// <summary>
		/// The full set of Glyphs in this Workspace
		/// </summary>
		public ReactiveList<IGlyphViewModel> Glyphs { get; } = new ReactiveList<IGlyphViewModel>();

		/// <summary>
		/// The full set of Layouts in this Workspace
		/// </summary>
		public ReactiveList<LayoutViewModel> Layouts { get; } = new ReactiveList<LayoutViewModel>();

		#endregion

		#region Events

		/// <summary>
		/// Arguments for Initialized event
		/// </summary>
		public class InitializationEventArgs : EventArgs
		{
			/// <summary>
			/// An exception that stores possible failures in initialization
			/// </summary>
			public FailedOperationException FailedDocuments { get; } = new FailedOperationException();
		};

		/// <summary>
		/// Event fired when a workspace has been fully loaded and initialized, so dependencies may be hooked up
		/// </summary>
		public EventHandler<InitializationEventArgs> Initialized;

		#endregion

		#region Referenced Documents

		/// <summary>
		/// The documents referenced by this workspace
		/// </summary>
		public ReactiveList<BaseDocumentViewModel> Documents { get; } = new ReactiveList<BaseDocumentViewModel>();

		/// <summary>
		/// The currently selected document
		/// </summary>
		public BaseDocumentViewModel SelectedDocument
		{
			get => _selectedDocument;
			set
			{
				if (value != null && !Documents.Contains(value))
					throw new ArgumentOutOfRangeException(nameof(value), @"Document not in list");

				this.RaiseAndSetIfChanged(ref _selectedDocument, value);
			}
		}

		/// <summary>
		/// The currently opened documents
		/// </summary>
		public ReactiveList<BaseDocumentViewModel> OpenDocs { get; } = new ReactiveList<BaseDocumentViewModel>();

		/// <summary>
		/// The currently selected open document
		/// </summary>
		public BaseDocumentViewModel SelectedOpenDoc
		{
			get => _selectedOpenDoc;
			set
			{
				if (value != null && !OpenDocs.Contains(value))
					throw new ArgumentOutOfRangeException(nameof(value), @"Document not in list");

				this.RaiseAndSetIfChanged(ref _selectedOpenDoc, value);
			}
		}

		#endregion

		#region Commands

		/// <summary>
		/// Save the workspace and all of its documents
		/// </summary>
		/// <exception cref="FailedOperationException">Thrown if the workspace or any document could not be saved</exception>
		public ReactiveCommand Save { get; protected set; }

		/// <summary>
		/// Create new or add existing documents to the workspace
		/// </summary>
		/// <exception cref="FailedOperationException">Thrown if a document could not be loaded</exception>
		public ReactiveCommand CreateAddDocuments { get; protected set; }

		/// <summary>
		/// Remove the Selected Document from the workspace
		/// </summary>
		/// <exception cref="FailedOperationException">Thrown if the selected document could not be removed</exception>
		public ReactiveCommand RemoveSelectedDocument { get; protected set; }

		/// <summary>
		/// Opens the Selected Document
		/// </summary>
		public ReactiveCommand OpenSelectedDocument { get; protected set; }

		/// <summary>
		/// Closes the Selected Document
		/// </summary>
		public ReactiveCommand CloseSelectedDocument { get; protected set; }

		/// <summary>
		/// Removes the Selected Document from the workspace and deletes the file
		/// </summary>
		/// <exception cref="FailedOperationException">Thrown if the selected document could not be deleted</exception>
		public ReactiveCommand DeleteSelectedDocument { get; protected set; }

		/// <summary>
		/// Asks the user if they want to remove a document when it has unsaved changes in it
		/// </summary>
		public Interaction<Unit, bool> ConfirmRemoveDirtyDoc { get; } = new Interaction<Unit, bool>();

		#endregion

		#region Private Methods

		private void InitializeData(FailedOperationException loadFailed)
		{
			using (Documents.SuppressChangeNotifications())
			{
				foreach (string document in _workspace.Documents)
				{
					BaseDocumentViewModel docVm;
					try
					{
						docVm = CreateViewModelForDoc(document);
					}
					catch (Exception ex)
					{
						FailedOperationException foex = ex as FailedOperationException;
						if (foex != null)
						{
							loadFailed.FailedDocs.AddRange(foex.FailedDocs);
						}
						else
						{
							loadFailed.FailedDocs.Add(new FailedOperationException.FailedDocument
							{
								Path = document,
								Reason = ex
							});
						}
						continue;
					}

					Documents.Add(docVm);
					
				}
				Layouts.AddRange(Documents.OfType<LayoutViewModel>());
			}

			Documents.Changed.Subscribe(args =>
			{
				switch (args.Action)
				{
					case NotifyCollectionChangedAction.Add:
						Layouts.AddRange(args.NewItems.OfType<LayoutViewModel>());
						break;

					case NotifyCollectionChangedAction.Remove:
						Layouts.RemoveAll(args.OldItems.OfType<LayoutViewModel>());
						break;

					case NotifyCollectionChangedAction.Replace:
						Layouts.AddRange(args.NewItems.OfType<LayoutViewModel>());
						Layouts.RemoveAll(args.OldItems.OfType<LayoutViewModel>());
						break;

					case NotifyCollectionChangedAction.Reset:
						Layouts.Clear();
						Layouts.AddRange(Documents.OfType<LayoutViewModel>());
						break;
				}
			});
		}

		private void CreateCommands()
		{
			Save = ReactiveCommand.Create(DoSave);

			CreateAddDocuments = ReactiveCommand.Create<Type>(DoCreateAdd,
				this.WhenAnyValue(vm => vm.Documents)
					.Select(list => list != null));

			RemoveSelectedDocument = ReactiveCommand.Create(() =>
				{
					if (SelectedDocument.IsDirty)
						ConfirmRemoveDirtyDoc.Handle(new Unit()).Subscribe(confirmed =>
						{
							if (confirmed)
								DoRemove();
						});
					else
						DoRemove();
				},
				this.WhenAnyValue(vm => vm.Documents, vm => vm.SelectedDocument)
					.Select(tuple => tuple.Item1 != null && tuple.Item2 != null));

			OpenSelectedDocument = ReactiveCommand.Create(DoOpen,
				this.WhenAnyValue(vm => vm.SelectedDocument).Select(doc => doc != null && doc.CanOpen));

			CloseSelectedDocument = ReactiveCommand.Create(DoClose,
				this.WhenAnyValue(vm => vm.SelectedOpenDoc).Select(doc => doc != null));

			DeleteSelectedDocument = ReactiveCommand.Create(DoDelete,
				this.WhenAnyValue(vm => vm.Documents, vm => vm.SelectedDocument)
					.Select(tuple => tuple.Item1 != null && tuple.Item2 != null));
		}

		private void DoSave()
		{
			FailedOperationException exception = new FailedOperationException();

			try
			{
				if (IsDirty)
					SaveDocument();
				IsDirty = false;
				IsNew = false;
			}
			catch (Exception e)
			{
				exception.FailedDocs.Add(new FailedOperationException.FailedDocument
				{
					Path = _fileName,
					Reason = e
				});
			}

			Documents.Where(doc => doc.IsDirty).ForEach(doc =>
			{
				try
				{
					doc.Save();
				}
				catch (Exception e)
				{
					exception.FailedDocs.Add(new FailedOperationException.FailedDocument
					{
						Path = doc.RelativePath,
						Reason = e
					});
				}
			});

			if (exception.FailedDocs.Any())
				throw exception;
		}

		private void DoCreateAdd(Type docType)
		{
			IFileSystem fileSystem = Locator.Current.GetService<IFileSystem>();
			IList<string> filePaths = fileSystem.GetDocumentPaths(docType, RootPath, FileSystem.Operation.CreateOrOpen, true);
			if (filePaths == null || filePaths.Count == 0)
				return;

			FailedOperationException loadFailed = new FailedOperationException();

			foreach (string fullPath in filePaths)
			{
				string relativePath = Util.File.GetRelativePath(fullPath, RootPath);

				// Document is already in the workspace, skip it
				if (Documents.Any(doc => doc.RelativePath.Equals(relativePath)))
					continue;

				if (!fileSystem.Exists(fullPath))
				{
					BaseDocumentViewModel newDoc = CreateViewModelForDoc(relativePath);
					Documents.Add(newDoc);
					_workspace.Documents.Add(newDoc.RelativePath);
					SelectedDocument = newDoc;
					newDoc.IsDirty = true;
					IsDirty = true;
					DoOpen();

					this.Log().Info("Created document {0}", newDoc.RelativePath);
				}
				else
				{
					BaseDocumentViewModel newDoc;
					try
					{
						newDoc = CreateViewModelForDoc(relativePath);
						IsDirty = true;
					}
					catch (Exception ex)
					{
						FailedOperationException foex = ex as FailedOperationException;
						if (foex != null)
						{
							loadFailed.FailedDocs.AddRange(foex.FailedDocs);
						}
						else
						{
							loadFailed.FailedDocs.Add(new FailedOperationException.FailedDocument
							{
								Path = fullPath,
								Reason = ex
							});
						}
						continue;
					}


					Documents.Add(newDoc);
					_workspace.Documents.Add(newDoc.RelativePath);
					SelectedDocument = newDoc;
					DoOpen();

					this.Log().Info("Added document {0}", newDoc.RelativePath);
				}
			}

			if (loadFailed.FailedDocs.Any())
				throw loadFailed;
		}

		private void DoRemove()
		{
			string path = SelectedDocument.RelativePath;
			try
			{
				SelectedDocument.Dispose();

				_workspace.Documents.Remove(SelectedDocument.RelativePath);
				if (OpenDocs.Contains(SelectedDocument))
					OpenDocs.Remove(SelectedDocument);
				Documents.Remove(SelectedDocument);

				SelectedDocument = null;
				IsDirty = true;

				this.Log().Info("Removed document {0}", path);
			}
			catch (Exception ex)
			{
				FailedOperationException removeFailed = new FailedOperationException();
				removeFailed.FailedDocs.Add(new FailedOperationException.FailedDocument
				{
					Path = path,
					Reason = ex
				});
				throw removeFailed;
			}
		}

		private void DoOpen()
		{
			if (!OpenDocs.Contains(SelectedDocument))
				OpenDocs.Add(SelectedDocument);
			
			SelectedOpenDoc = SelectedDocument;
		}

		private void DoClose()
		{
			BaseDocumentViewModel temp = SelectedOpenDoc;
			SelectedOpenDoc = null;
			OpenDocs.Remove(temp);
		}

		private void DoDelete()
		{
			string path = SelectedDocument.RelativePath;
			try
			{
				BaseDocumentViewModel docToDelete = SelectedDocument;
				DoRemove();

				IFileSystem fileSystem = Locator.Current.GetService<IFileSystem>();
				fileSystem.DeleteLocalFile(Util.File.GetFullPath(RootPath, docToDelete.RelativePath));
			}
			catch (Exception ex)
			{
				FailedOperationException deleteFailed = new FailedOperationException();
				deleteFailed.FailedDocs.Add(new FailedOperationException.FailedDocument
				{
					Path = path,
					Reason = ex
				});
				throw deleteFailed;
			}
		}

		private BaseDocumentViewModel CreateViewModelForDoc(string relativePath)
		{
			Type docType = Util.File.GetDocumentTypeFromFilePath(relativePath);

			if (docType == typeof(DataSet))
			{
				return new DataSetViewModel(this, relativePath);
			}
			if (docType == typeof(GlyphSet))
			{
				return new GlyphSetViewModel(this, relativePath);
			}
			if (docType == typeof(Documents.Layout))
			{
				return new LayoutViewModel(this, relativePath);
			}
			if (docType == typeof(Documents.DeckPrintSet))
			{
				return new DeckPrintSetViewModel(this, relativePath);
			}
			
			throw new InvalidOperationException(@"Unknown document type");
		}
		
		private void LoadDocument(string fullPath)
		{
			try
			{
				IFileSystem fileSystem = Locator.Current.GetService<IFileSystem>();
				Stream inputStream = fileSystem.ReadLocalFile(fullPath);
				TextReader reader = new StreamReader(inputStream);
				string input = reader.ReadToEnd();
				_workspace = (Workspace)JsonConvert.DeserializeObject(input, typeof(Workspace),
					new JsonSerializerSettings
					{
						TypeNameHandling = TypeNameHandling.Auto
					});
			}
			catch (Exception ex)
			{
				FailedOperationException loadFailed = new FailedOperationException();
				loadFailed.FailedDocs.Add(new FailedOperationException.FailedDocument
				{
					Path = _fileName,
					Reason = ex
				});
				throw loadFailed;
			}
		}

		private void SaveDocument()
		{
			// don't catch exceptions here, they are caught further upstream
			string fileName = FullPath;
			IFileSystem fileSystem = Locator.Current.GetService<IFileSystem>();
			Stream stream = fileSystem.WriteLocalFile(fileName, !fileSystem.Exists(fileName));

			byte[] info = new UTF8Encoding(true).GetBytes(
				JsonConvert.SerializeObject(_workspace,
					new JsonSerializerSettings
					{
						TypeNameHandling = TypeNameHandling.Auto
					}));
			stream.Write(info, 0, info.Length);
			stream.Flush();
			stream.Dispose();
		}

		#endregion
	}
}
