﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Core.Documents;
using Core.Drawing;
using Core.View_Models.Layout.Commands;
using ReactiveUI;

namespace Core.View_Models.Layout
{
	/// <summary>
	/// ViewModel that wraps a Layout's ColorElement
	/// </summary>
	public class ColorElementViewModel : ElementViewModel
	{
		private readonly ColorElement _colorElement;

		#region Constructor

		/// <summary>
		/// Constructs a ColorElementViewModel
		/// </summary>
		/// <param name="element">The wrapped element</param>
		/// <param name="parent">The parent LayoutViewModel</param>
		public ColorElementViewModel(ColorElement element, LayoutViewModel parent) : base(element, parent)
		{
			_colorElement = element;
		}

		#endregion

		#region Properties

		public static readonly Color BoundColor = Drawing.Color.FromArgb(100, 255, 0, 255); // show transparent magenta for bound color elements

		/// <summary>
		/// The fill color of the element. If bound to an element, show a default "bound color" instead
		/// </summary>
		public Color? Color
		{
			get => _colorElement.Color ?? BoundColor;
			set
			{
				if (value == _colorElement.Color)
					return;

				if (DoesGenerateCommands)
					Parent.AddNewCommand(new SetColor(this, value));

				_colorElement.Color = value;
				this.RaisePropertyChanged();
			}
		}

		#endregion
	}
}
