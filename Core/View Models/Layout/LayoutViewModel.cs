﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reactive.Linq;
using Core.Drawing;
using Core.Util;
using Core.View_Models.Layout.Commands;
using ReactiveUI;
using Math = System.Math;
using ReactiveCommand = ReactiveUI.ReactiveCommand;

namespace Core.View_Models.Layout
{
	/// <summary>
	/// ViewModel that handles layouts
	/// </summary>
	public class LayoutViewModel : BaseDocumentViewModel
	{
		#region Private Members

		private readonly Documents.Layout _layout;
		private ElementViewModel _selectedElement;
		private int _selectedElementIndex = -1;
		private readonly List<IDisposable> _disposables = new List<IDisposable>();
		private int _undoStackSize = 100;

		#endregion

		#region Command Stack

		/// <summary>
		/// Undo stack
		/// </summary>
		public readonly ReactiveList<LayoutCommandBase> UndoStack = new ReactiveList<LayoutCommandBase>();

		/// <summary>
		/// Redo stack
		/// </summary>
		public readonly ReactiveList<LayoutCommandBase> RedoStack = new ReactiveList<LayoutCommandBase>();

		/// <summary>
		/// Adds a new command to the UndoStack and clears the RedoStack
		/// </summary>
		/// <param name="command">The command to perform</param>
		public void AddNewCommand(LayoutCommandBase command)
		{
			UndoStack.Add(command);
			RedoStack.Clear();
		}

		/// <summary>
		/// Maximum size of the undo stack. Adding commands beyond this size will cause the earliest commands to be forgotten.
		/// </summary>
		/// <remarks>If set to a smaller value than the current size of the UndoStack, commands will be removed from the front until
		/// it reaches appropriate size</remarks>
		public int UndoStackSize
		{
			set
			{
				if (value <= 0)
					throw new ArgumentOutOfRangeException(nameof(value), "Undo stack size must be a positive value");

				_undoStackSize = value;

				if (value < UndoStack.Count)
					UndoStack.RemoveRange(0, UndoStack.Count - value);
			}
		}

		#endregion

				
		#region Public Properties

		/// <summary>
		/// The document's type, as a helpful property
		/// </summary>
		public override Type DocType => typeof(Documents.Layout);

		/// <summary>
		/// The size of the wrapped layout in inches
		/// </summary>
		/// <remarks>Must have a positive Width and Height</remarks>
		public Size Size
		{
			get => 
_layout.Size;
			set
			{
				if (value.Width <= 0.0f || value.Height <= 0.0f)
					throw new ArgumentOutOfRangeException(nameof(value));

				_layout.Size = value;
				this.RaisePropertyChanged();
				this.RaisePropertyChanged(nameof(FullSize));

				IsDirty = true;
			}
		}

		/// <summary>
		/// The bleed thickness of the wrapped layout in inches
		/// </summary>
		/// <remarks>Must have a non-negative Width and Height</remarks>
		public Size Bleed
		{
			get => _layout.Bleed;
			set
			{
				if (value.Width < 0.0f || value.Height < 0.0f)
					throw new ArgumentOutOfRangeException(nameof(value));

				_layout.Bleed = value;
				this.RaisePropertyChanged();
				this.RaisePropertyChanged(nameof(FullSize));

				IsDirty = true;
			}
		}

		/// <summary>
		/// The full size of the layout in inches, including bleed regions
		/// </summary>
		public Size FullSize => _layout.Size + _layout.Bleed * 2;

		/// <summary>
		/// The elements contained in the Layout, in render order
		/// </summary>
		public ReactiveList<ElementViewModel> Elements { get; } = new ReactiveList<ElementViewModel>();

		/// <summary>
		/// The currently selected element
		/// </summary>
		public ElementViewModel SelectedElement
		{
			get => _selectedElement;
			set
			{
				if (value != _selectedElement)
				{
					_selectedElement = value;
					_selectedElementIndex = Elements.IndexOf(value);
					this.RaisePropertyChanged();
					SetElementsSelected(value);
				}
			}
		}

		/// <summary>
		/// Dimensions of the displayed safe zone in inches
		/// </summary>
		public Size SafeZone
		{
			get => _layout.SafeZone;
			set
			{
				if (value.Width < 0 || value.Height < 0)
					throw new ArgumentOutOfRangeException(nameof(value), "Safe zone must be non-negative");

				_layout.SafeZone = value;
				this.RaisePropertyChanged();

				IsDirty = true;
			}
		}

		/// <summary>
		/// Whether this layout is a landscape layout
		/// </summary>
		public bool IsLandscape => Size.Width > Size.Height;

		/// <summary>
		/// Whether this layout has any elements bound to card fields
		/// </summary>
		public bool HasBindings => Elements.Any(elem => elem.IsBoundToField);

		#endregion

		#region Public Methods

		/// <summary>
		/// Checks if another LayoutViewModel is rotationally equivalent in size to this one, including bleed
		/// </summary>
		/// <param name="rhs">The other LayoutViewModel to compare</param>
		/// <returns>True if the two LayoutViewModels are the same size when ignoring rotation, false otherwise</returns>
		public bool IsRotationallyEquivalentSize(LayoutViewModel rhs)
		{
			Size.RotationalEquivalence equivalence = Size.AreRotationEquivalent(Size, rhs.Size);
			if (equivalence == Size.RotationalEquivalence.NotEqual)
				return false;

			Size.RotationalEquivalence bleedEquivalence = Size.AreRotationEquivalent(Bleed, rhs.Bleed);
			if (Bleed.IsSquare)
				return bleedEquivalence == Size.RotationalEquivalence.Equal;

			return (bleedEquivalence == equivalence);
		}

		#endregion

		#region Commands

		/// <summary>
		/// Adds a new element to the layout. Takes in the type of element to add.
		/// </summary>
		public ReactiveCommand AddElement { get; protected set; }

		/// <summary>
		/// Removes the selected element from the layout
		/// </summary>
		public ReactiveCommand RemoveSelectedElement { get; protected set; }

		/// <summary>
		/// Moves the selected element back one place in render order (which is towards the front of the Elements list)
		/// </summary>
		public ReactiveCommand MoveSelectedElementBack { get; protected set; }

		/// <summary>
		/// Moves the selected element forward one place in render order (which is towards the back of the Elements list)
		/// </summary>
		public ReactiveCommand MoveSelectedElementForward { get; protected set; }

		/// <summary>
		/// Moves the selected element to the back of the render order (front of the Elements list)
		/// </summary>
		public ReactiveCommand MoveSelectedElementToBack { get; protected set; }

		/// <summary>
		/// Moves the selected element to the front of the render order (back of the Elements list)
		/// </summary>
		public ReactiveCommand MoveSelectedElementToFront { get; protected set; }

		/// <summary>
		/// Undoes the last command
		/// </summary>
		public ReactiveCommand Undo { get; protected set; }

		/// <summary>
		/// Redoes the last undone command
		/// </summary>
		public ReactiveCommand Redo { get; protected set; }

		#endregion

		#region Constructor

		/// <summary>
		/// Constructs a LayoutViewModel
		/// </summary>
		/// <param name="workspace">The containing WorkspaceViewModel</param>
		/// <param name="relativePath">The path of the Glyph Set relative to rootpath</param>
		public LayoutViewModel(WorkspaceViewModel workspace, string relativePath) : base(workspace, relativePath, typeof(Documents.Layout))
		{
			// we want an exception to be thrown here if the referenced document is incorrect
			_layout = (Documents.Layout) Document;
			Initialize();
			CreateCommands();
		}

		#endregion

		#region Overrides of BaseDocumentViewModel

		protected override void DoDispose(bool finalize)
		{
			if (_isDisposed)
				return;

			_isDisposed = true;

			Elements.ForEach(elem => elem.Dispose());
			Workspace.Layouts.Remove(this);
			_disposables.ForEach(_ => _.Dispose());
		}

		#endregion

		#region Private Methods

		private void Initialize()
		{
			using (Elements.SuppressChangeNotifications())
			{
				Elements.AddRange(_layout.Elements.Select(elem => ElementFactory.CreateViewModel(elem, this)));
			}
			Elements.ChangeTrackingEnabled = true;
			_disposables.Add(Elements.ItemChanged.Subscribe(args =>
			{
				if (args.PropertyName == "IsSelected")
				{
					if (args.Sender.IsSelected)
						SelectedElement = args.Sender;
				}
				else if (args.PropertyName != "IsEditorVisible")
					IsDirty = true;
			}));
			Elements.CollectionChanged += (sender, args) =>
			{
				bool assignSelectedElement = false;

				switch (args.Action)
				{
					case NotifyCollectionChangedAction.Add:
						for (int i = 0; i < args.NewItems.Count; i++)
						{
							ElementViewModel newVm = args.NewItems[i] as ElementViewModel;
							_layout.Elements.Insert(args.NewStartingIndex + i, newVm.Element);
						}
						break;

					case NotifyCollectionChangedAction.Remove:
						args.OldItems.Cast<ElementViewModel>().ForEach(oldElem =>
						{
							_layout.Elements.Remove(oldElem.Element);
							if (oldElem == SelectedElement)
								assignSelectedElement = true;
							oldElem.Dispose();
						});
						break;

					case NotifyCollectionChangedAction.Replace:
						args.OldItems.Cast<ElementViewModel>().ForEach(oldElem =>
						{
							_layout.Elements.Remove(oldElem.Element);
							if (oldElem == SelectedElement)
								assignSelectedElement = true;
							oldElem.Dispose();
						});
						for (int i = 0; i < args.NewItems.Count; i++)
						{
							ElementViewModel newVm = args.NewItems[i] as ElementViewModel;
							_layout.Elements.Insert(args.NewStartingIndex + i, newVm.Element);
						}
						break;
				}

				if (assignSelectedElement)
				{
					SelectedElement = Elements.Count > 0 ? 
							Elements[Math.Min(_selectedElementIndex, Elements.Count - 1)] : 
							null;
				}
				IsDirty = true;
			};
		}

		private void CreateCommands()
		{
			AddElement = ReactiveCommand.Create<Type>(DoAdd);
			_disposables.Add(AddElement.ThrownExceptions.Subscribe(ex =>
			{
				throw ex;
			}));

			RemoveSelectedElement = ReactiveCommand.Create(DoRemove,
				this.WhenAnyValue(vm => vm.SelectedElement).Select(elem => elem != null));

			MoveSelectedElementBack = ReactiveCommand.Create(DoMoveBackOne,
				this.WhenAnyValue(vm => vm.SelectedElement).Select(elem => elem != null && Elements.IndexOf(elem) > 0));

			MoveSelectedElementForward = ReactiveCommand.Create(DoMoveForwardOne,
				this.WhenAnyValue(vm => vm.SelectedElement).Select(elem => elem != null && Elements.IndexOf(elem) < Elements.Count - 1));

			MoveSelectedElementToBack = ReactiveCommand.Create(DoMoveToBack,
				this.WhenAnyValue(vm => vm.SelectedElement).Select(elem => elem != null && Elements.IndexOf(elem) > 0));

			MoveSelectedElementToFront = ReactiveCommand.Create(DoMoveToFront,
				this.WhenAnyValue(vm => vm.SelectedElement).Select(elem => elem != null && Elements.IndexOf(elem) < Elements.Count - 1));

			Undo = ReactiveCommand.Create(DoUndo,
				UndoStack.CountChanged.Select(count => count != 0));

			Redo = ReactiveCommand.Create(DoRedo,
				RedoStack.CountChanged.Select(count => count != 0));
		}

		private void DoAdd(Type elemType)
		{
			LayoutCommandBase command = new AddElement(this, elemType);
			command.Do();
			AddNewCommand(command);
		}

		private void DoRemove()
		{
			LayoutCommandBase command = new RemoveElement(this, SelectedElement.Element);
			command.Do();
			AddNewCommand(command);
		}

		private void DoMoveBackOne()
		{
			int index = Elements.IndexOf(SelectedElement);
			LayoutCommandBase command = new ReorderElement(SelectedElement, index - 1);
			command.Do();
			AddNewCommand(command);

			SelectedElement = Elements[index - 1];
		}

		private void DoMoveForwardOne()
		{
			int index = Elements.IndexOf(SelectedElement);
			LayoutCommandBase command = new ReorderElement(SelectedElement, index + 1);
			command.Do();
			AddNewCommand(command);

			SelectedElement = Elements[index + 1];
		}

		private void DoMoveToBack()
		{
			LayoutCommandBase command = new ReorderElement(SelectedElement, 0);
			command.Do();
			AddNewCommand(command);

			SelectedElement = Elements[0];
		}

		private void DoMoveToFront()
		{
			LayoutCommandBase command = new ReorderElement(SelectedElement, Elements.Count - 1);
			command.Do();
			AddNewCommand(command);

			SelectedElement = Elements[Elements.Count - 1];
		}

		private void DoUndo()
		{
			if (UndoStack.Count == 0)
				return;

			LayoutCommandBase command = UndoStack[UndoStack.Count - 1];
			command.Undo();
			UndoStack.RemoveAt(UndoStack.Count - 1);
			RedoStack.Add(command);
		}

		private void DoRedo()
		{
			if (RedoStack.Count == 0)
				return;

			LayoutCommandBase command = RedoStack[RedoStack.Count - 1];
			command.Do();
			RedoStack.RemoveAt(RedoStack.Count - 1);
			UndoStack.Add(command);
		}

		private void SetElementsSelected(ElementViewModel selected)
		{
			foreach (ElementViewModel viewModel in Elements)
			{
				viewModel.IsSelected = viewModel == selected;
			}
		}

		#endregion
	}
}
