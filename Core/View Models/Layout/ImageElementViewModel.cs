﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.IO;
using System.Reactive.Linq;
using Core.Documents;
using Core.Drawing;
using Core.Util;
using Core.View_Models.Layout.Commands;
using ReactiveUI;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using Splat;
using ReactiveCommand = ReactiveUI.ReactiveCommand;

namespace Core.View_Models.Layout
{
	public class ImageElementViewModel : ElementViewModel
	{
		private readonly ImageElement _imageElement;
		private readonly string _root;
		private readonly ObservableAsPropertyHelper<Image<Bgra32>> _image;

		#region Constructor

		/// <summary>
		/// Constructs an ImageElementViewModel
		/// </summary>
		/// <param name="element">The wrapped element</param>
		/// <param name="parent">The containing LayoutViewModel</param>
		/// <param name="workspace">The containing WorkspaceViewModel</param>
		public ImageElementViewModel(ImageElement element, LayoutViewModel parent, WorkspaceViewModel workspace) : base(element, parent)
		{
			_imageElement = element;
			_root = workspace.RootPath;

			this.WhenAnyValue(vm => vm.Path)
				.Select(path =>
				{
					if (string.IsNullOrEmpty(path))
						return null;

					Image<Bgra32> image = null;
					try
					{
						string imgPath = Util.File.GetFullPath(_root, path);
						image = SixLabors.ImageSharp.Image.Load<Bgra32>(imgPath);
					}
					// we might expect the user to type in an incorrect filename, in which case we should do nothing
					catch (FileNotFoundException)
					{
						this.Log().Warn("Layout {0}, Image element {1}: Could not locate image file {2}",
							parent.DisplayName, Name, path);
					}
					catch (Exception ex)
					{
						this.Log().Error("Layout {0}, Image element {1}: Failed to load image file {2}:\n{3}",
							parent.DisplayName, Name, path, ex);
					}

					return image;
				})
				.ToProperty(this, vm => vm.Image, out _image);

			Path = element.Path;
			SelectImage = ReactiveCommand.Create(DoSelectImage);
		}

		#endregion

		#region Public Properties

		/// <summary>
		/// The image's filepath relative to the root
		/// </summary>
		public string Path
		{
			get => _imageElement.Path;
			set
			{
				if (value == _imageElement.Path)
					return;

				if (DoesGenerateCommands)
					Parent.AddNewCommand(new SetImagePath(this, value));

				_imageElement.Path = value;
				this.RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Whether the image stretches to fill the available space
		/// </summary>
		public bool Stretch
		{
			get => _imageElement.Stretch;
			set
			{
				if (value == _imageElement.Stretch)
					return;

				if (DoesGenerateCommands)
					Parent.AddNewCommand(new SetImageStretch(this, value));

				_imageElement.Stretch = value;
				this.RaisePropertyChanged();
			}
		}

		/// <summary>
		/// The image's horizontal alignment
		/// </summary>
		public HorizontalAlignment HorizontalAlignment
		{
			get => _imageElement.HorizontalAlignment;
			set
			{
				if (value == _imageElement.HorizontalAlignment)
					return;

				if (DoesGenerateCommands)
					Parent.AddNewCommand(new SetImageAlignment(this, value, VerticalAlignment));

				_imageElement.HorizontalAlignment = value;
				this.RaisePropertyChanged();
			}
		}

		/// <summary>
		/// The image's vertical alignment
		/// </summary>
		public VerticalAlignment VerticalAlignment
		{
			get => _imageElement.VerticalAlignment;
			set
			{
				if (value == _imageElement.VerticalAlignment)
					return;

				if (DoesGenerateCommands)
					Parent.AddNewCommand(new SetImageAlignment(this, HorizontalAlignment, value));

				_imageElement.VerticalAlignment = value;
				this.RaisePropertyChanged();
			}
		}

		/// <summary>
		/// The element's image
		/// </summary>
		public Image<Bgra32> Image => _image.Value;

		/// <summary>
		/// Allows the user to select the image file
		/// </summary>
		public ReactiveCommand SelectImage { get; protected set; }

		#endregion

		#region Private Methods

		private void DoSelectImage()
		{
			IFileSystem system = Locator.Current.GetService<IFileSystem>();
			string fileName;
			if (system.GetExistingImage(_root + Path, out fileName))
				Path = Util.File.GetRelativePath(fileName, _root);
		}

		#endregion
	}
}
