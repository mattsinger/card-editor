﻿using System;
using Core.Documents;

namespace Core.View_Models.Layout
{
	/// <summary>
	/// Factory methods for Elements
	/// </summary>
	public static class ElementFactory
	{
		/// <summary>
		/// Creates an ElementViewModel for a given Element
		/// </summary>
		/// <param name="element">The Element to wrap</param>
		/// <param name="layout">The LayoutViewModel where the ElementViewModel will live</param>
		/// <returns>An ElementViewModel that appropriately wraps the element</returns>
		public static ElementViewModel CreateViewModel(Element element, LayoutViewModel layout)
		{
			TextElement text = element as TextElement;
			if (text != null)
				return new TextElementViewModel(text, layout, layout.Workspace);

			ImageElement image = element as ImageElement;
			if (image != null)
				return new ImageElementViewModel(image, layout, layout.Workspace);

			ColorElement color = element as ColorElement;
			if (color != null)
				return new ColorElementViewModel(color, layout);

			throw new InvalidCastException("Unknown Element type");
		}
	}
}
