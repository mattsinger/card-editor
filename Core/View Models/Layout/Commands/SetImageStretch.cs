﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;

namespace Core.View_Models.Layout.Commands
{
	/// <summary>
	/// Command that sets the stretch value of an Image Element
	/// </summary>
	public class SetImageStretch : TypedElementCommandBase<ImageElementViewModel>
	{
		private readonly bool _newStretched;

		/// <summary>
		/// Constructs a SetImageStretch
		/// </summary>
		/// <param name="element">The affected ElementViewModel</param>
		/// <param name="isStretched">The final stretch state of the image</param>
		public SetImageStretch(ImageElementViewModel element, bool isStretched) : base(element)
		{
			_newStretched = isStretched;
		}

		#region Overrides of LayoutCommandBase

		/// <summary>
		/// Redoes the command
		/// </summary>
		public override void Do()
		{
			ImageElementViewModel target = ViewModel;
			target.DoesGenerateCommands = false;

			target.Stretch = _newStretched;

			target.DoesGenerateCommands = true;
		}

		/// <summary>
		/// Undoes the command
		/// </summary>
		public override void Undo()
		{

			ImageElementViewModel target = ViewModel;
			target.DoesGenerateCommands = false;

			target.Stretch = !_newStretched;

			target.DoesGenerateCommands = true;
		}

		#endregion
	}
}
