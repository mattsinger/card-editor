﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Core.View_Models.Layout.Commands
{
	/// <summary>
	/// Command that changes an Element's RoundedCorners
	/// </summary>
	public class ChangeRoundedCorners : ElementCommandBase
	{
		private readonly float _originalRadius;
		private readonly ElementViewModel.RoundedCornersFlags _originalFlags;

		private readonly float _finalRadius;
		private readonly ElementViewModel.RoundedCornersFlags _finalFlags;

		/// <summary>
		/// Constructs a ChangeRoundedCorners
		/// </summary>
		/// <param name="element">The ElementViewModel whose RoundedCorners are being changed</param>
		/// <param name="targetRadius">The target radius</param>
		/// <param name="targetFlags">The target flags for Rounded Corners</param>
		public ChangeRoundedCorners(ElementViewModel element, 
			float targetRadius, ElementViewModel.RoundedCornersFlags targetFlags) : base(element)
		{
			_originalRadius = element.CornerRadius;
			_originalFlags = element.RoundedCorners;

			_finalRadius = targetRadius;
			_finalFlags = targetFlags;
		}

		#region Overrides of LayoutCommand

		/// <summary>
		/// Redoes the command
		/// </summary>
		public override void Do()
		{
			ElementViewModel target = ViewModel;
			target.DoesGenerateCommands = false;

			target.CornerRadius = _finalRadius;
			target.RoundedCorners = _finalFlags;

			target.DoesGenerateCommands = true;
		}

		/// <summary>
		/// Undoes the command
		/// </summary>
		public override void Undo()
		{
			ElementViewModel target = ViewModel;
			target.DoesGenerateCommands = false;

			target.CornerRadius = _originalRadius;
			target.RoundedCorners = _originalFlags;

			target.DoesGenerateCommands = true;
		}

		#endregion
	}
}
