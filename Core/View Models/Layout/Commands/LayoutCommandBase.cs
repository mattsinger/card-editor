﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;

namespace Core.View_Models.Layout.Commands
{
	/// <summary>
	/// Base class for Commands that affect Layouts
	/// </summary>
	public abstract class LayoutCommandBase : ICommand
	{
		protected readonly LayoutViewModel Layout;

		protected LayoutCommandBase(LayoutViewModel layout)
		{
			if (layout == null)
				throw new ArgumentNullException(nameof(layout));

			Layout = layout;
		}

		#region Implementation of ICommand

		/// <summary>
		/// Redoes the command
		/// </summary>
		public abstract void Do();

		/// <summary>
		/// Undoes the command
		/// </summary>
		public abstract void Undo();

		#endregion
	}
}
