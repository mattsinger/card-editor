﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using Core.Documents;

namespace Core.View_Models.Layout.Commands
{
	/// <summary>
	/// Command that reorders an element
	/// </summary>
	public class ReorderElement : ElementCommandBase
	{
		private readonly int _oldIndex;
		private readonly int _newIndex;

		/// <summary>
		/// Constructs a ReorderElement command
		/// </summary>
		/// <param name="elemVm">The moved Element</param>
		/// <param name="newIndex">The Element's ending index</param>
		public ReorderElement(ElementViewModel elemVm, int newIndex) : base(elemVm)
		{
			_oldIndex = Layout.Elements.IndexOf(elemVm);
			_newIndex = newIndex;
		}

		#region Implementation of ICommand

		/// <summary>
		/// Does the command
		/// </summary>
		public override void Do()
		{
			ElementViewModel elemVm = Layout.Elements[_oldIndex];
			Element elem = elemVm.Element;

			if (elem.Id != Id)
				throw new InvalidOperationException($"Element {Id} is not in the expected position");

			Layout.Elements.RemoveAt(_oldIndex);
			Layout.Elements.Insert(_newIndex, ElementFactory.CreateViewModel(elem, Layout));
		}

		/// <summary>
		/// Undoes the command
		/// </summary>
		public override void Undo()
		{
			ElementViewModel elemVm = Layout.Elements[_newIndex];
			Element elem = elemVm.Element;

			if (elem.Id != Id)
				throw new InvalidOperationException($"Element {Id} is not in the expected position");

			Layout.Elements.RemoveAt(_newIndex);
			Layout.Elements.Insert(_oldIndex, ElementFactory.CreateViewModel(elem, Layout));
		}

		#endregion
	}
}
