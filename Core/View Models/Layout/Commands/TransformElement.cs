﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Core.Drawing;

namespace Core.View_Models.Layout.Commands
{
	/// <summary>
	/// Command that transforms an Element
	/// </summary>
	public class TransformElement : ElementCommandBase
	{
		public Size OriginalSize { get; }
		public Point OriginalOrigin { get; }
		public float OriginalOrientation { get; }

		private Size _finalSize;
		private Point _finalOrigin;
		private float _finalOrientation;

		/// <summary>
		/// Sets the final transform values
		/// </summary>
		/// <param name="size">The final Element Size</param>
		/// <param name="origin">The final Element Origin</param>
		/// <param name="orient">The final Element Orientation</param>
		public void SetFinalTransform(Size size, Point origin, float orient)
		{
			_finalSize = size;
			_finalOrigin = origin;
			_finalOrientation = orient;
		}

		/// <summary>
		/// Constructs a TransformElement command
		/// </summary>
		/// <param name="element">The Element being transformed</param>
		public TransformElement(ElementViewModel element) : base(element)
		{
			OriginalSize = element.Size;
			OriginalOrigin = element.Origin;
			OriginalOrientation = element.Orientation;
		}

		#region Overrides of LayoutCommand

		/// <summary>
		/// Redoes the command
		/// </summary>
		public override void Do()
		{
			ElementViewModel target = ViewModel;
			target.Origin = _finalOrigin;
			target.Size = _finalSize;
			target.Orientation = _finalOrientation;
		}

		/// <summary>
		/// Undoes the command
		/// </summary>
		public override void Undo()
		{
			ElementViewModel target = ViewModel;
			target.Origin = OriginalOrigin;
			target.Size = OriginalSize;
			target.Orientation = OriginalOrientation;
		}

		#endregion
	}
}
