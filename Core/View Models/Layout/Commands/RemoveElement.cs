﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Linq;
using Core.Documents;

namespace Core.View_Models.Layout.Commands
{
	/// <summary>
	/// Command that removes an Element
	/// </summary>
	public class RemoveElement : LayoutCommandBase
	{
		private readonly Element _element;

		/// <summary>
		/// Constructs a RemoveElement command
		/// </summary>
		/// <param name="layout">The Layout where we removed an Element</param>
		/// <param name="element">The element to remove</param>
		public RemoveElement(LayoutViewModel layout, Element element) : base(layout)
		{
			_element = element;
		}

		#region Overrides of LayoutCommand

		/// <summary>
		/// Redoes the command
		/// </summary>
		public override void Do()
		{
			ElementViewModel elemVm = Layout.Elements.FirstOrDefault(vm => vm.Element == _element);
			if (elemVm == null)
				throw new InvalidOperationException($"Could not locate ViewModel for Element {_element.Id}");

			Layout.Elements.Remove(elemVm);
		}

		/// <summary>
		/// Undoes the command
		/// </summary>
		public override void Undo()
		{
			ElementViewModel newVm = ElementFactory.CreateViewModel(_element, Layout);
			Layout.Elements.Add(newVm);
			Layout.SelectedElement = newVm;

			Layout.IsDirty = true;
		}

		#endregion
	}
}
