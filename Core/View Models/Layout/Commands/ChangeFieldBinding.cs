﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Core.View_Models.Layout.Commands
{
	/// <summary>
	/// Command that changes an Element's binding to a Field
	/// </summary>
	public class ChangeFieldBinding : ElementCommandBase
	{
		private readonly string _originalField;
		private readonly string _finalField;

		/// <summary>
		/// Constructs a ChangeFieldBinding
		/// </summary>
		/// <param name="element">The affected ElementViewModel</param>
		/// <param name="targetField">The target Field to bind to. If null or empty, then we are clearing the binding</param>
		public ChangeFieldBinding(ElementViewModel element, string targetField) : base(element)
		{
			_originalField = element.FieldName;
			_finalField = targetField;
		}

		#region Overrides of LayoutCommandBase

		/// <summary>
		/// Redoes the command
		/// </summary>
		public override void Do()
		{
			ElementViewModel target = ViewModel;
			target.DoesGenerateCommands = false;

			target.FieldName = _finalField;

			target.DoesGenerateCommands = true;
		}

		/// <summary>
		/// Undoes the command
		/// </summary>
		public override void Undo()
		{
			ElementViewModel target = ViewModel;
			target.DoesGenerateCommands = false;

			target.FieldName = _originalField;

			target.DoesGenerateCommands = true;
		}

		#endregion
	}
}
