﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Core.View_Models.Layout.Commands
{
	public class ChangeVisibility : ElementCommandBase
	{
		private readonly bool _newVisibility;

		/// <summary>
		/// Constructs a ChangeVisibility
		/// </summary>
		/// <param name="element">The affected ElementViewModel</param>
		/// <param name="visible">Whether the Element is becoming visible or not</param>
		public ChangeVisibility(ElementViewModel element, bool visible) : base(element)
		{
			_newVisibility = visible;
		}

		#region Overrides of LayoutCommandBase

		/// <summary>
		/// Redoes the command
		/// </summary>
		public override void Do()
		{
			ElementViewModel target = ViewModel;
			target.DoesGenerateCommands = false;

			target.IsEditorVisible = _newVisibility;

			target.DoesGenerateCommands = true;
		}

		/// <summary>
		/// Undoes the command
		/// </summary>
		public override void Undo()
		{
			ElementViewModel target = ViewModel;
			target.DoesGenerateCommands = false;

			target.IsEditorVisible = !_newVisibility;

			target.DoesGenerateCommands = true;
		}

		#endregion
	}
}
