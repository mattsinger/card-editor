﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Core.Drawing;

namespace Core.View_Models.Layout.Commands
{
	/// <summary>
	/// Command that sets the alignment of an ImageElementViewModel
	/// </summary>
	public class SetImageAlignment : TypedElementCommandBase<ImageElementViewModel>
	{
		private readonly HorizontalAlignment _originalHorizontalAlignment;
		private readonly VerticalAlignment _originalVerticalAlignment;

		private readonly HorizontalAlignment _finalHorizontalAlignment;
		private readonly VerticalAlignment _finalVerticalAlignment;

		/// <summary>
		/// Constructs a SetImageAlignment
		/// </summary>
		/// <param name="element">The affected ElementViewModel</param>
		/// <param name="hAlign">The final horizontal alignment</param>
		/// <param name="vAlign">The final vertical alignment</param>
		public SetImageAlignment(ImageElementViewModel element, HorizontalAlignment hAlign, VerticalAlignment vAlign) : base(element)
		{
			_originalHorizontalAlignment = element.HorizontalAlignment;
			_originalVerticalAlignment = element.VerticalAlignment;

			_finalHorizontalAlignment = hAlign;
			_finalVerticalAlignment = vAlign;
		}

		#region Overrides of LayoutCommandBase

		/// <summary>
		/// Redoes the command
		/// </summary>
		public override void Do()
		{
			ImageElementViewModel target = ViewModel;
			target.DoesGenerateCommands = false;

			target.HorizontalAlignment = _finalHorizontalAlignment;
			target.VerticalAlignment = _finalVerticalAlignment;

			target.DoesGenerateCommands = true;
		}

		/// <summary>
		/// Undoes the command
		/// </summary>
		public override void Undo()
		{
			ImageElementViewModel target = ViewModel;
			target.DoesGenerateCommands = false;

			target.HorizontalAlignment = _originalHorizontalAlignment;
			target.VerticalAlignment = _originalVerticalAlignment;

			target.DoesGenerateCommands = true;
		}

		#endregion
	}
}
