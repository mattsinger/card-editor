﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Linq;

namespace Core.View_Models.Layout.Commands
{
	/// <summary>
	/// Base class for Commands that affect Elements in Layouts
	/// </summary>
	public abstract class ElementCommandBase : LayoutCommandBase
	{
		/// <summary>
		/// The Id of the affected ElementViewModel
		/// </summary>
		/// <remarks>We use this rather than store a reference to the ViewModel because we cannot guarantee that
		/// the reference will remain valid, if it removed and re-added</remarks>
		protected readonly Guid Id;

		/// <summary>
		/// Constructs an ElementCommandBase
		/// </summary>
		/// <param name="element">The affected ElementViewModel</param>
		protected ElementCommandBase(ElementViewModel element) : base(element.Parent)
		{
			Id = element.Id;
		}

		/// <summary>
		/// Gets the related ElementViewModel from the parent Layout
		/// </summary>
		protected ElementViewModel ViewModel
		{
			get
			{
				ElementViewModel elemVm = Layout.Elements.FirstOrDefault(vm => vm.Id == Id);
				if (elemVm == null)
					throw new InvalidOperationException($"Could not locate Element {Id}");

				return elemVm;
			}
		}
	}
}
