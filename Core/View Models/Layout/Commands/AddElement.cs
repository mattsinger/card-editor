﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Linq;
using Core.Documents;

namespace Core.View_Models.Layout.Commands
{
	/// <summary>
	/// Command that adds an Element
	/// </summary>
	public class AddElement : LayoutCommandBase
	{
		/// <summary>
		/// The type of added Element
		/// </summary>
		public Type Type { get; }

		/// <summary>
		/// The added element's Id
		/// </summary>
		public Guid Id { get; } = Guid.NewGuid();

		/// <summary>
		/// Constructs an AddElement command
		/// </summary>
		/// <param name="layout">The Layout where we added an Element</param>
		/// <param name="elementType">The type of Element that is added</param>
		public AddElement(LayoutViewModel layout, Type elementType) : base(layout)
		{
			if (elementType == null)
				throw new ArgumentNullException(nameof(elementType));

			Type = elementType;
		}

		#region Implementation of ICommand

		/// <summary>
		/// Redoes the command
		/// </summary>
		public override void Do()
		{
			Element newElement = Activator.CreateInstance(Type) as Element;
			if (newElement == null)
				throw new ArgumentOutOfRangeException(nameof(Type), @"AddElement only accepts Types of Elements");
			newElement.Name = $"{Type.Name}#{Layout.Elements.Count + 1}";
			newElement.Id = Id;

			ElementViewModel newVm = ElementFactory.CreateViewModel(newElement, Layout);
			Layout.Elements.Add(newVm);
			Layout.SelectedElement = newVm;

			Layout.IsDirty = true;
		}

		/// <summary>
		/// Undoes the command
		/// </summary>
		public override void Undo()
		{
			ElementViewModel elemVm = Layout.Elements.FirstOrDefault(elem => elem.Id == Id);
			if (elemVm == null)
				throw new InvalidOperationException($"Could not locate element {Id}");
			if (elemVm.Element.GetType() != Type)
				throw new InvalidOperationException($"{elemVm.Element.GetType().Name} {Id} does not match expected type {Type.Name}");

			Layout.Elements.Remove(elemVm);

			Layout.IsDirty = true;
		}

		#endregion
	}
}
