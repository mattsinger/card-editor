﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;

namespace Core.View_Models.Layout.Commands
{
	/// <summary>
	/// Base class for Element commands that operate on a specific type of Element
	/// </summary>
	/// <typeparam name="T">The Type of ElementViewModel affected by the command</typeparam>
	public abstract class TypedElementCommandBase<T> : ElementCommandBase where T : ElementViewModel
	{
		/// <summary>
		/// Gets the related ElementViewModel from the parent Layout
		/// </summary>
		/// <remarks>This version automatically casts to the expected type</remarks>
		protected new T ViewModel
		{
			get
			{
				T typedResult = base.ViewModel as T;
				if (typedResult == null)
					throw new InvalidCastException($"Element {Id} is not of the expected type");

				return typedResult;
			}
		}

		/// <summary>
		/// Constructs a TypedElementCommandBase
		/// </summary>
		/// <param name="element">The affected ElementViewModel</param>
		protected TypedElementCommandBase(T element) : base(element)
		{
		}
	}
}
