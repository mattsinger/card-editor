﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Core.Drawing;

namespace Core.View_Models.Layout.Commands
{
	/// <summary>
	/// Command that sets an Element's BorderColor
	/// </summary>
	public class SetBorderColor : ElementCommandBase
	{
		private readonly Color? _originalColor;
		private readonly Color? _targetColor;

		/// <summary>
		/// Constructs a SetBorderColor
		/// </summary>
		/// <param name="element">The ElementViewModel whose border color we are changing</param>
		/// <param name="targetColor">The color we are changing to</param>
		public SetBorderColor(ElementViewModel element, Color? targetColor) : base(element)
		{
			_originalColor = element.BorderColor;
			_targetColor = targetColor;
		}

		/// <summary>
		/// Redoes the command
		/// </summary>
		public override void Do()
		{
			ElementViewModel target = ViewModel;
			target.DoesGenerateCommands = false;
			target.BorderColor = _targetColor;
			target.DoesGenerateCommands = true;
		}

		/// <summary>
		/// Undoes the command
		/// </summary>
		public override void Undo()
		{
			ElementViewModel target = ViewModel;
			target.DoesGenerateCommands = false;
			target.BorderColor = _originalColor;
			target.DoesGenerateCommands = true;
		}
	}
}
