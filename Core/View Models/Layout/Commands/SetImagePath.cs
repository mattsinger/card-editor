﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Core.View_Models.Layout.Commands
{
	/// <summary>
	/// Command that sets the image path of an Image Element
	/// </summary>
	public class SetImagePath : TypedElementCommandBase<ImageElementViewModel>
	{
		private readonly string _originalPath;
		private readonly string _finalPath;

		/// <summary>
		/// Constructs a SetImagePath
		/// </summary>
		/// <param name="element">The affected ElementViewModel</param>
		/// <param name="newPath">The new image path</param>
		public SetImagePath(ImageElementViewModel element, string newPath) : base(element)
		{
			_originalPath = element.Path;
			_finalPath = newPath;
		}

		#region Overrides of LayoutCommandBase

		/// <summary>
		/// Redoes the command
		/// </summary>
		public override void Do()
		{
			ImageElementViewModel target = ViewModel;
			target.DoesGenerateCommands = false;

			target.Path = _finalPath;

			target.DoesGenerateCommands = true;
		}

		/// <summary>
		/// Undoes the command
		/// </summary>
		public override void Undo()
		{
			ImageElementViewModel target = ViewModel;
			target.DoesGenerateCommands = false;

			target.Path = _originalPath;

			target.DoesGenerateCommands = true;
		}

		#endregion
	}
}
