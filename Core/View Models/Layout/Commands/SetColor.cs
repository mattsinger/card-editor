﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using Core.Drawing;

namespace Core.View_Models.Layout.Commands
{
	/// <summary>
	/// Command for changing the fill color of a ColorElement
	/// </summary>
	public class SetColor : TypedElementCommandBase<ColorElementViewModel>
	{
		private readonly Color? _originalColor;
		private readonly Color? _finalColor;

		/// <summary>
		/// Constructs a SetColor
		/// </summary>
		/// <param name="element">The affected ColorElementViewModel</param>
		/// <param name="targetColor">The color that the Element is being changed to</param>
		public SetColor(ColorElementViewModel element, Color? targetColor) : base(element)
		{
			_originalColor = element.Color;
			_finalColor = targetColor;
		}

		#region Overrides of LayoutCommandBase

		/// <summary>
		/// Redoes the command
		/// </summary>
		public override void Do()
		{
			ColorElementViewModel target = ViewModel;
			target.DoesGenerateCommands = false;

			target.Color = _finalColor;

			target.DoesGenerateCommands = true;
		}

		/// <summary>
		/// Undoes the command
		/// </summary>
		public override void Undo()
		{
			ColorElementViewModel target = ViewModel;
			target.DoesGenerateCommands = false;

			target.Color = _originalColor;

			target.DoesGenerateCommands = true;
		}

		#endregion
	}
}
