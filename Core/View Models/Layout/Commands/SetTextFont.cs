﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Core.Drawing;

namespace Core.View_Models.Layout.Commands
{
	/// <summary>
	/// Command to set the Font of a Text Element
	/// </summary>
	public class SetTextFont : TypedElementCommandBase<TextElementViewModel>
	{
		private readonly Font _originalFont;
		private readonly Font _newFont;
		 
		/// <summary>
		/// Constructs a SetTextFont
		/// </summary>
		/// <param name="element">The affected ElementViewModel</param>
		/// <param name="newFont">The Element's new Font</param>
		public SetTextFont(TextElementViewModel element, Font newFont) : base(element)
		{
			_originalFont = new Font(element.Font);
			_newFont = new Font(newFont);
		}

		#region Overrides of LayoutCommandBase

		/// <summary>
		/// Redoes the command
		/// </summary>
		public override void Do()
		{
			TextElementViewModel target = ViewModel;
			target.DoesGenerateCommands = false;

			target.Font = _newFont;

			target.DoesGenerateCommands = true;
		}

		/// <summary>
		/// Undoes the command
		/// </summary>
		public override void Undo()
		{

			TextElementViewModel target = ViewModel;
			target.DoesGenerateCommands = false;

			target.Font = _originalFont;

			target.DoesGenerateCommands = true;
		}

		#endregion
	}
}
