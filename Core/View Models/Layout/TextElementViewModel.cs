﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using Core.Documents;
using Core.Drawing;
using Core.Text;
using Core.View_Models.Layout.Commands;
using ReactiveUI;

namespace Core.View_Models.Layout
{
	/// <summary>
	/// ViewModel that wraps a Layout's TextElement
	/// </summary>
	public class TextElementViewModel : ElementViewModel
	{
		private readonly TextElement _textElement;

		#region Constructor

		/// <summary>
		/// Constructs a TextElementViewModel
		/// </summary>
		/// <param name="element">The wrapped element</param>
		/// <param name="parent">The containing LayoutViewModel</param>
		/// <param name="workspace">The containing WorkspaceViewModel</param>
		public TextElementViewModel(TextElement element, LayoutViewModel parent, WorkspaceViewModel workspace) : base(element, parent)
		{
			_textElement = element;

			ParseTree = new Tree(workspace);

			if (!IsBoundToField)
				ParseTree.Parse(element.Text);

		    this.WhenAnyValue(vm => vm.FontName, 
                            vm => vm.FontSize, 
                            vm => vm.FontBold, 
                            vm => vm.FontItalics)
		        .Subscribe(_ =>
		        {
		            this.RaisePropertyChanged(nameof(Font));
		        });
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets the text content of the element
		/// </summary>
		public string Text
		{
			get => _textElement.Text;
			set
			{
				if (value == _textElement.Text)
					return;

				if (DoesGenerateCommands)
					Parent.AddNewCommand(new SetText(this, value));

				_textElement.Text = value;
				ParseTree.Parse(value);
				this.RaisePropertyChanged();
			}
		}

		/// <summary>
		/// The Text as a parsed tree
		/// </summary>
		/// <remarks>
		/// Remember that Tree fires its Reset event when Tree.Parse is called, so any View for this VM
		/// should subscribe on ShouldReset
		/// </remarks>
		public Tree ParseTree { get; }

		/// <summary>
		/// The base font used to render the text
		/// </summary>
		public Font Font
		{
			get => _textElement.Font;
			set
			{
				if (value == null)
					throw new ArgumentNullException(nameof(value), "Must provide a valid Font");

				if (DoesGenerateCommands)
					Parent.AddNewCommand(new SetTextFont(this, value));

				_textElement.Font.CopyFrom(value);
				this.RaisePropertyChanged();
				this.RaisePropertyChanged(nameof(FontName));
				this.RaisePropertyChanged(nameof(FontSize));
				this.RaisePropertyChanged(nameof(FontBold));
				this.RaisePropertyChanged(nameof(FontItalics));
			}
		}

		/// <summary>
		/// The name of the base font used to render this element
		/// </summary>
		public string FontName
		{
		    get => _textElement.Font.Name;
			set
			{
				if (string.IsNullOrEmpty(value))
					throw new ArgumentOutOfRangeException(nameof(value), @"FontName must be a valid string");

				if (value == _textElement.Font.Name)
					return;

				if (DoesGenerateCommands)
					Parent.AddNewCommand(new SetTextFont(this, new Font(Font)
					{
						Name = value
					}));

				_textElement.Font.Name = value;
				this.RaisePropertyChanged();
            }
		}

		/// <summary>
		/// The size of the base font used to render this element, in inches
		/// </summary>
		public float FontSize
		{
			get => _textElement.Font.Size ?? Font.DefaultSize;
			set
			{
				if (value <= 0)
					throw new ArgumentOutOfRangeException(nameof(value), @"FontSize must be a positive value");

				if (Util.Math.AreEquivalent(value, _textElement.Font.Size ?? Font.DefaultSize))
					return;

				if (DoesGenerateCommands)
					Parent.AddNewCommand(new SetTextFont(this, new Font(Font)
					{
						Size = value
					}));

				_textElement.Font.Size = value;
				this.RaisePropertyChanged();
			}
		}

        /// <summary>
        /// Whether or not the font should be rendered bold
        /// </summary>
	    public bool? FontBold
	    {
	        get => _textElement.Font.Bold;
	        set
	        {
	            if (value == _textElement.Font.Bold)
	                return;

				if (DoesGenerateCommands)
					Parent.AddNewCommand(new SetTextFont(this, new Font(Font)
					{
						Bold = value
					}));

				_textElement.Font.Bold = value;
                this.RaisePropertyChanged();
	        }
	    }

        /// <summary>
        /// Whether or not the font should be rendered in italics
        /// </summary>
	    public bool? FontItalics
	    {
	        get => _textElement.Font.Italics;
	        set
            {
                if (value == _textElement.Font.Italics)
                    return;

				if (DoesGenerateCommands)
					Parent.AddNewCommand(new SetTextFont(this, new Font(Font)
					{
						Italics = value
					}));

				_textElement.Font.Italics = value;
                this.RaisePropertyChanged();
            }
        }

		/// <summary>
		/// The text's horizontal alignment
		/// </summary>
		public HorizontalAlignment HorizontalAlignment
		{
			get => _textElement.HorizontalAlignment;
			set
			{
				if (value == _textElement.HorizontalAlignment)
					return;

				if (DoesGenerateCommands)
					Parent.AddNewCommand(new SetTextAlignment(this, value, VerticalAlignment));

				_textElement.HorizontalAlignment = value;
				this.RaisePropertyChanged();
			}
		}

		/// <summary>
		/// The text's vertical alignment
		/// </summary>
		public VerticalAlignment VerticalAlignment
		{
			get => _textElement.VerticalAlignment;
			set
			{
				if (value == _textElement.VerticalAlignment)
					return;

				if (DoesGenerateCommands)
					Parent.AddNewCommand(new SetTextAlignment(this, HorizontalAlignment, value));

				_textElement.VerticalAlignment = value;
				this.RaisePropertyChanged();
			}
		}

		/// <summary>
		/// The text color
		/// </summary>
		public Color Color
		{
			get => _textElement.Color;
			set
			{
				if (value == _textElement.Color)
					return;

				if (DoesGenerateCommands)
					Parent.AddNewCommand(new SetTextColor(this, value));

				_textElement.Color = value;
				this.RaisePropertyChanged();
			}
		}

		#endregion

		#region Overrides of ElementViewModel
		
		protected override void ScaleData(float xScale, float yScale)
		{
			base.ScaleData(xScale, yScale);
			FontSize = FontSize * Math.Min(xScale, yScale);
		}

		protected override void DoDispose(bool finalize)
		{
			if (_disposed)
				return;

			base.DoDispose(finalize);
			ParseTree.Dispose(finalize);
		}

		#endregion
	}
}
