﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using Core.Documents;
using Core.Drawing;
using Core.Util;
using Core.View_Models.Layout.Commands;
using ReactiveUI;

namespace Core.View_Models.Layout
{
	/// <summary>
	/// ViewModel that wraps a layout element
	/// </summary>
	public abstract class ElementViewModel : ReactiveObject, IDisposable
	{
		private bool _selected;
		private bool _isEditorVisible = true;
		protected bool _disposed;
		protected readonly List<IDisposable> _disposables = new List<IDisposable>();

		private TransformElement _pendingTransform;

		/// <summary>
		/// Enum defining which corners of the Element should be rounded
		/// </summary>
		/// <remarks>Duplicates Element.RoundedCornerFlag to abstract model data from views</remarks>
		[Flags]
		public enum RoundedCornersFlags
		{
			/// <summary>
			/// No corners
			/// </summary>
			None = 0,
			/// <summary>
			/// The top-left corner of the element
			/// </summary>
			TopLeft = Element.RoundedCornersFlags.TopLeft,
			/// <summary>
			/// The top-right corner of the element
			/// </summary>
			TopRight = Element.RoundedCornersFlags.TopRight,
			/// <summary>
			/// The bottom-left corner of the element
			/// </summary>
			BottomLeft = Element.RoundedCornersFlags.BottomLeft,
			/// <summary>
			/// The bottom-right corner of the element
			/// </summary>
			BottomRight = Element.RoundedCornersFlags.BottomRight,
			/// <summary>
			/// All corners
			/// </summary>
			All = TopLeft | TopRight | BottomLeft | BottomRight
		}

		/// <summary>
		/// Constructs an ElementViewModel
		/// </summary>
		/// <param name="element">The wrapped element</param>
		/// <param name="layoutVm">The parent LayoutViewModel containing this ElementViewMdodel</param>
		protected ElementViewModel(Element element, LayoutViewModel layoutVm)
		{
			Parent = layoutVm ?? throw new ArgumentNullException(nameof(layoutVm));
			Element = element;

			_disposables.Add(layoutVm.WhenAnyValue(layout => layout.Size)
				.Buffer(2, 1)
				.Select(sizes => new { oldSize = sizes[0], newSize = sizes[1] })
				.Subscribe(change =>
				{
					float xScaling = change.newSize.Width / change.oldSize.Width;
					float yScaling = change.newSize.Height / change.oldSize.Height;
					ScaleData(xScaling, yScaling);
				}));
			_disposables.Add(layoutVm.WhenAnyValue(layout => layout.Bleed)
				.Subscribe(bleed =>
				{
					Origin = new Point(
						Origin.X.Clamp(-bleed.Width, 
										Parent.Size.Width + bleed.Width - Size.Width),
						Origin.Y.Clamp(-bleed.Height, 
										Parent.Size.Height + bleed.Height - Size.Height));
				}));
		}

		/// <summary>
		/// Finalizer
		/// </summary>
		~ElementViewModel()
		{
			DoDispose(true);
		}

		#region Properties

		/// <summary>
		/// The containing LayoutViewModel
		/// </summary>
		public LayoutViewModel Parent { get; }

		/// <summary>
		/// The Element's Id
		/// </summary>
		/// <remarks>Does not raise a property changed event. Should not be set except on creation. </remarks>
		public Guid Id => Element.Id;

		/// <summary>
		/// The element's origin, in millimeters
		/// </summary>
		/// <remarks>Does not generate commands when set. Use BeginTransform() and EndTransform() to generate
		/// commands to change this value.</remarks>
		public Point Origin
		{
			get => Element.Origin;
			set
			{
				if (Util.Math.AreEquivalent(value.X, Element.Origin.X) &&
					Util.Math.AreEquivalent(value.Y, Element.Origin.Y))
					return;

				Element.Origin = value;
				this.RaisePropertyChanged();
			}
		}

		/// <summary>
		/// The element's size, in millimeters
		/// </summary>
		/// <remarks>Does not generate commands when set. Use BeginTransform() and EndTransform() to generate
		/// commands to change this value.</remarks>
		public Size Size
		{
			get => Element.Size;
			set
			{
				if (Util.Math.AreEquivalent(value.Width, Element.Size.Width) &&
					Util.Math.AreEquivalent(value.Height, Element.Size.Height))
					return;

				Element.Size = value;
				this.RaisePropertyChanged();
			}
		}

		/// <summary>
		/// The element's orientation, in degrees clockwise
		/// </summary>
		/// <remarks>Does not generate commands when set. Use BeginTransform() and EndTransform() to generate
		/// commands to change this value.</remarks>
		public float Orientation
		{
			get => Element.Orientation;
			set
			{
				while (value > 360.0f)
					value -= 360.0f;

				while (value < 0)
					value += 360.0f;

				if (Util.Math.AreEquivalent(value, Element.Orientation))
					return;

				Element.Orientation = value;
				this.RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Whether a transform command is currently pending
		/// </summary>
		public bool IsTransformPending => _pendingTransform != null;

		/// <summary>
		/// The radius of the element's corners, in millimeters
		/// </summary>
		public float CornerRadius
		{
			get => Element.CornerRadius;
			set
			{
				if (value < 0.0f)
					throw new ArgumentOutOfRangeException(nameof(value), "CornerRadius must be a non-negative value");

				if (Util.Math.AreEquivalent(value, Element.CornerRadius))
					return;

				if (DoesGenerateCommands)
					Parent.AddNewCommand(new ChangeRoundedCorners(this, value, RoundedCorners));

				Element.CornerRadius = value;
				this.RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Which corners of the element should be rounded
		/// </summary>
		public RoundedCornersFlags RoundedCorners
		{
			get => Element.RoundedCorners.ToVm();
			set
			{
				Element.RoundedCornersFlags newFlags = value.ToModel();
				if (Element.RoundedCorners == newFlags)
					return;

				if (DoesGenerateCommands)
					Parent.AddNewCommand(new ChangeRoundedCorners(this, CornerRadius, value));

				Element.RoundedCorners = newFlags;
				this.RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Thickness of the border, in inches
		/// </summary>
		public float BorderThickness
		{
			get => Element.BorderThickness;
			set
			{
				if (value < 0.0f)
					throw new ArgumentOutOfRangeException(nameof(value), "BorderThickenss must be a non-negaitve value");

				if (Util.Math.AreEquivalent(value, Element.BorderThickness))
					return;

				if (DoesGenerateCommands)
					Parent.AddNewCommand(new SetBorderThickness(this, value));

				Element.BorderThickness = value;
				this.RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Color of the border
		/// </summary>
		public Color? BorderColor
		{
			get => Element.BorderColor;
			set
			{
				if (value == Element.BorderColor)
					return;

				if (DoesGenerateCommands)
					Parent.AddNewCommand(new SetBorderColor(this, value));

				Element.BorderColor = value;
				this.RaisePropertyChanged();
			}
		}

		/// <summary>
		/// The element's name
		/// </summary>
		/// <exception cref="ArgumentNullException">Thrown if value is null</exception>
		public string Name
		{
			get => Element.Name;
			set
			{
				if (value == null)
					throw new ArgumentNullException(nameof(value));

				if (value == Element.Name)
					return;

				if (DoesGenerateCommands)
					Parent.AddNewCommand(new ChangeName(this, value));

				Element.Name = value;
				this.RaisePropertyChanged();
			}
		}

		/// <summary>
		/// The element's associated card data field
		/// </summary>
		public string FieldName
		{
			get => Element.FieldName;
			set
			{
				if (value == Element.FieldName || 
						(string.IsNullOrEmpty(value) && 
						string.IsNullOrEmpty(Element.FieldName)))
					return;

				if (DoesGenerateCommands)
					Parent.AddNewCommand(new ChangeFieldBinding(this, value));
				
				Element.FieldName = value;
				this.RaisePropertyChanged();
				this.RaisePropertyChanged(nameof(IsBoundToField));
			}
		}

		/// <summary>
		/// Whether the element should use a Field's value or its own value
		/// </summary>
		public bool IsBoundToField => !string.IsNullOrEmpty(FieldName);

		/// <summary>
		/// Whether the element is selected or not
		/// </summary>
		public bool IsSelected
		{
			get => _selected;
			set => this.RaiseAndSetIfChanged(ref _selected, value);
		}

		/// <summary>
		/// Whether the element is visible within the layout editor
		/// </summary>
		public bool IsEditorVisible
		{
			get => _isEditorVisible;
			set
			{
				if (value == _isEditorVisible)
					return;

				if (DoesGenerateCommands)
					Parent.AddNewCommand(new ChangeVisibility(this, value));

				_isEditorVisible = value;
				this.RaisePropertyChanged();
			}
		}

		/// <summary>
		/// The wrapped element
		/// </summary>
		public Element Element { get; }

		/// <summary>
		/// The element's type
		/// </summary>
		public Type ElementType => Element.GetType();

		/// <summary>
		/// Flag that determines whether this Element should generate commands when properties are changed.
		/// </summary>
		/// <remarks>Typically used by commands to prevent cyclical command generation on undo/redo.</remarks>
		public bool DoesGenerateCommands { get; set; } = true;

		#endregion

		#region Public Methods

		/// <summary>
		/// Marks the beginning of an element transform. Call before peforming any changes to Size, Origin, or Orientation
		/// to generate a command for undo/redo.
		/// </summary>
		/// <exception cref="InvalidOperationException">Thrown if a transform is already pending</exception>
		public void BeginTransform()
		{
			if (_pendingTransform != null)
				throw new InvalidOperationException("Must end the current pending transform before beginning a new one");

			_pendingTransform = new TransformElement(this);
		}

		/// <summary>
		/// Marks the end of an element transform. Call this after ending any changes to Size, Origin, or Orientation to
		/// add the command to the Parent.
		/// </summary>
		/// <exception cref="InvalidOperationException">Thrown if a transform is not pending</exception>
		public void EndTransform()
		{
			if (_pendingTransform == null)
				throw new InvalidOperationException("No pending transform to end");

			if (Size != _pendingTransform.OriginalSize ||
			    Origin != _pendingTransform.OriginalOrigin ||
			    !Util.Math.AreEquivalent(Orientation, _pendingTransform.OriginalOrientation))
			{
				_pendingTransform.SetFinalTransform(Size, Origin, Orientation);
				Parent.AddNewCommand(_pendingTransform);
			}
			_pendingTransform = null;
		}

		#endregion

		#region Protected Methods

		protected virtual void ScaleData(float xScale, float yScale)
		{
			Origin = new Point(Origin.X * xScale, Origin.Y * yScale);
			Size = new Size(Size.Width * xScale, Size.Height * yScale);
		}

		protected virtual void DoDispose(bool finalize)
		{
			_disposables.ForEach(_ => _.Dispose());
			_disposables.Clear();

			_disposed = true;
		}

		#endregion

		#region Implementation of IDisposable

		/// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
		public void Dispose()
		{
			DoDispose(false);
		}

		#endregion
	}

	/// <summary>
	/// Utility class to define extension methods for rounded corner enums
	/// </summary>
	public static class RoundedCornerUtils
	{
		/// <summary>
		/// Converts an ElementViewModel.RoundedCornersFlags to an Element.RoundedCornersFlags
		/// </summary>
		/// <param name="flags">The ElementViewModel flags</param>
		/// <returns>The corresponding Element flags</returns>
		public static Element.RoundedCornersFlags ToModel(this ElementViewModel.RoundedCornersFlags flags)
		{
			Element.RoundedCornersFlags result = Element.RoundedCornersFlags.None;
			if ((flags & ElementViewModel.RoundedCornersFlags.TopLeft) != ElementViewModel.RoundedCornersFlags.None)
				result |= Element.RoundedCornersFlags.TopLeft;

			if ((flags & ElementViewModel.RoundedCornersFlags.TopRight) != ElementViewModel.RoundedCornersFlags.None)
				result |= Element.RoundedCornersFlags.TopRight;

			if ((flags & ElementViewModel.RoundedCornersFlags.BottomLeft) != ElementViewModel.RoundedCornersFlags.None)
				result |= Element.RoundedCornersFlags.BottomLeft;

			if ((flags & ElementViewModel.RoundedCornersFlags.BottomRight) != ElementViewModel.RoundedCornersFlags.None)
				result |= Element.RoundedCornersFlags.BottomRight;

			return result;
		}

		/// <summary>
		/// Converts an ElementViewModel.RoundedCornersFlags to an Element.RoundedCornersFlags
		/// </summary>
		/// <param name="flags">The ElementViewModel flags</param>
		/// <returns>The corresponding Element flags</returns>
		public static ElementViewModel.RoundedCornersFlags ToVm(this Element.RoundedCornersFlags flags)
		{
			ElementViewModel.RoundedCornersFlags result = ElementViewModel.RoundedCornersFlags.None;
			if ((flags & Element.RoundedCornersFlags.TopLeft) != Element.RoundedCornersFlags.None)
				result |= ElementViewModel.RoundedCornersFlags.TopLeft;

			if ((flags & Element.RoundedCornersFlags.TopRight) != Element.RoundedCornersFlags.None)
				result |= ElementViewModel.RoundedCornersFlags.TopRight;

			if ((flags & Element.RoundedCornersFlags.BottomLeft) != Element.RoundedCornersFlags.None)
				result |= ElementViewModel.RoundedCornersFlags.BottomLeft;

			if ((flags & Element.RoundedCornersFlags.BottomRight) != Element.RoundedCornersFlags.None)
				result |= ElementViewModel.RoundedCornersFlags.BottomRight;

			return result;
		}
	}
}
