﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.IO;
using System.Reflection;
using System.Text;
using Core.Documents;
using Core.Util;
using Newtonsoft.Json;
using ReactiveUI;
using Splat;

namespace Core.View_Models
{
	/// <summary>
	/// Common base class for ViewModels that wrap referenceable documents
	/// </summary>
	public abstract class BaseDocumentViewModel : ReactiveObject, IDisposable
	{
		#region Private Memebers

		private bool _isDirty;
		protected bool _isDisposed;

		#endregion

		#region Properties

		public WorkspaceViewModel Workspace { get; }

		/// <summary>
		/// The path of the document relative to the root
		/// </summary>
		public string RelativePath { get; }

		/// <summary>
		/// The UI-friendly display name of the document
		/// </summary>
		public string DisplayName { get; }

		/// <summary>
		/// The document's type, as a helpful property
		/// </summary>
		public abstract Type DocType { get; }

		/// <summary>
		/// The contained document
		/// </summary>
		public IDocument Document { get; protected set; }

		/// <summary>
		/// The grouping name of the wrapped document
		/// </summary>
		public string DocGroupName
		{
			get
			{
				TypeInfo docInfo = DocType.GetTypeInfo();
				DocumentAttribute attr = docInfo.GetCustomAttribute<DocumentAttribute>();
				return attr.GroupingHeader;
			}
		}

		/// <summary>
		/// The document's status
		/// </summary>
		public enum Status
		{
			/// <summary>
			/// We don't know the document's status
			/// </summary>
			Unknown,

			/// <summary>
			/// The document has been found and is ready to be opened
			/// </summary>
			Available,

			/// <summary>
			/// The document could not be found or could not load
			/// </summary>
			Unavailable,

			/// <summary>
			/// The document is read-only and cannot be written over
			/// </summary>
			ReadOnly,

			/// <summary>
			/// The document is newly created, and has not been saved
			/// </summary>
			New,

			/// <summary>
			/// The document has local changes that have not been saved
			/// </summary>
			Dirty
		}

		/// <summary>
		/// The current status of the document
		/// </summary>
		public Status CurrentStatus { get; protected set; }

		/// <summary>
		/// Can the document be opened
		/// </summary>
		public bool CanOpen => CurrentStatus != Status.Unknown &&
							   CurrentStatus != Status.Unavailable;

		/// <summary>
		/// Whether the document has any unsaved changes
		/// </summary>
		public bool IsDirty
		{
			get => _isDirty;
			set => this.RaiseAndSetIfChanged(ref _isDirty, value);
		}

		#endregion

		#region Constructor & Destructor

		protected BaseDocumentViewModel(WorkspaceViewModel workspace, string relativePath, Type docType)
		{
			if (string.IsNullOrEmpty(relativePath))
				throw new ArgumentOutOfRangeException(nameof(relativePath), @"Relative path must be defined");

			Workspace = workspace ?? throw new ArgumentOutOfRangeException(nameof(workspace), @"Workspace must be defined");
			RelativePath = relativePath;
			DisplayName = Path.GetFileNameWithoutExtension(relativePath);
			string fullPath = Util.File.GetFullPath(workspace.RootPath, relativePath);

			IFileSystem fileSystem = Locator.Current.GetService<IFileSystem>();

			if (fileSystem != null && fileSystem.Exists(fullPath))
			{
				try
				{
					Load(docType, fullPath);
					CurrentStatus = fileSystem.IsReadOnly(fullPath) ? Status.ReadOnly : Status.Available;
					this.Log().Info("Loaded document {0}", relativePath);
				}
				catch (Exception)
				{
					this.Log().Error("Failed to load document {0}", relativePath);
					CurrentStatus = Status.Unavailable;
					throw;
				}
			}
			else
			{
				Document = Activator.CreateInstance(docType) as IDocument;
				if (Document == null)
					throw new InvalidCastException("Failed to create Document instance");

				CurrentStatus = Status.New;
				IsDirty = true;
				this.Log().Info("Created document {0}", relativePath);
			}
		}

		~BaseDocumentViewModel()
		{
			DoDispose(true);
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Saves the document to file
		/// </summary>
		public void Save()
		{
			try
			{
				string fileName = Util.File.GetFullPath(Workspace.RootPath, RelativePath);
				IFileSystem fileSystem = Locator.Current.GetService<IFileSystem>();
				Stream stream = fileSystem.WriteLocalFile(fileName, CurrentStatus == Status.New);

				byte[] info = new UTF8Encoding(true).GetBytes(
					JsonConvert.SerializeObject(Document,
						new JsonSerializerSettings
						{
							TypeNameHandling = TypeNameHandling.Auto
						}));
				stream.Write(info, 0, info.Length);
				stream.Flush();
				stream.Dispose();
			}
			catch (Exception e)
			{
				this.Log().Error("Exception hit while saving {0}:\n{1}", RelativePath, e);
				throw;
			}

			CurrentStatus = Status.Available;
			IsDirty = false;
		}
		
		/// <summary>
		/// Called when the document is no longer valid in the workspace
		/// </summary>
		public void Dispose()
		{
			DoDispose(false);
		}

		protected abstract void DoDispose(bool finalize);

		#endregion

		#region Private Methods

		private void Load(Type docType, string fullPath)
		{
			Stream inputStream = null;
			TextReader reader = null;
			try
			{
				IFileSystem fileSystem = Locator.Current.GetService<IFileSystem>();
				inputStream = fileSystem.ReadLocalFile(fullPath);
				reader = new StreamReader(inputStream);
				string input = reader.ReadToEnd();
				Document = (IDocument) JsonConvert.DeserializeObject(input, docType,
					new JsonSerializerSettings
					{
						TypeNameHandling = TypeNameHandling.Auto
					});

			}
			catch (Exception e)
			{
				this.Log().Error("Exception hit while loading {0}:\n{1}", fullPath, e);
				CurrentStatus = Status.Unavailable;
				throw;
			}
			finally
			{
				reader?.Dispose();
				inputStream?.Dispose();
			}
		}

		#endregion
	}
}
