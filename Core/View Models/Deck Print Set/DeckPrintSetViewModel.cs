﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading;
using Core.Documents;
using Core.Drawing;
using Core.Exceptions;
using Core.Util;
using Core.View_Models.Data_Set;
using Core.View_Models.Layout;
using ReactiveUI;
using Splat;

namespace Core.View_Models.Deck_Print_Set
{
	/// <summary>
	/// ViewModel that wraps a DeckPrintSet
	/// </summary>
	public class DeckPrintSetViewModel : BaseDocumentViewModel
	{
		#region Private Members

		private readonly DeckPrintSet _deckPrintSet;
		private readonly Subject<Unit> _cardCountChanged = new Subject<Unit>();
		private readonly List<IDisposable> _disposables = new List<IDisposable>();
		private CardCountViewModel _selectedCardCountViewModel;

		private CanPrintError _printError = CanPrintError.None;
		private Size _cardLayoutsize;
		private int _xCardCount;
		private int _yCardCount;
		private float _printProgress = -1.0f;
		private CancellationTokenSource _cancelSource;

		/// <summary>
		/// Provides a mapping between a DataSetRef in the document and the corresponding DataSetViewModel in the workspace
		/// </summary>
		public class DataSetMapping : IDisposable
		{
			private bool _isDisposed;
			private readonly List<IDisposable> _subscriptions = new List<IDisposable>();

			/// <summary>
			/// A DataSetRef in the document
			/// </summary>
			public DeckPrintSet.DataSetRef DataSetRef { get; }

			/// <summary>
			/// A DataSetViewModel in the Workspace
			/// </summary>
			public DataSetViewModel ViewModel { get; }

			/// <summary>
			/// Constructs a DataSetMapping
			/// </summary>
			/// <param name="parent">The parent DeckPrintSetViewModel that contains this mapping</param>
			/// <param name="vm">The DataSetViewModel in the workspace</param>
			/// <param name="dsRef">The DataSetRef in the original document</param>
			public DataSetMapping(DeckPrintSetViewModel parent, DataSetViewModel vm, DeckPrintSet.DataSetRef dsRef)
			{
				DataSetRef = dsRef;
				ViewModel = vm;

				_subscriptions.Add(vm.Cards.ItemsAdded.Subscribe(newCard =>
				{
					int newCardIndex = vm.Cards.IndexOf(newCard);
					DeckPrintSet.CardCount newCardCount = new DeckPrintSet.CardCount
					{
						Id = newCard.Id,
						Count = 1
					};
					DataSetRef.Cards.Insert(newCardIndex, newCardCount);

					int indexOffset = 0;
					foreach (DeckPrintSet.DataSetRef dataSetRef in parent._deckPrintSet.DataSets)
					{
						if (dataSetRef == DataSetRef)
						{
							CardCountViewModel ccvm = new CardCountViewModel(newCardCount, this, parent._cardCountChanged);
							parent.CardCounts.Insert(indexOffset + newCardIndex, ccvm);
							return;
						}

						indexOffset += dataSetRef.Cards.Count;
					}
				}));
				_subscriptions.Add(vm.Cards.BeforeItemsRemoved.Subscribe(oldCard =>
				{
					int oldCardIndex = vm.Cards.IndexOf(oldCard);
					DataSetRef.Cards.RemoveAt(oldCardIndex);

					int indexOffset = 0;
					foreach (DeckPrintSet.DataSetRef dataSetRef in parent._deckPrintSet.DataSets)
					{
						if (dataSetRef == DataSetRef)
						{
							parent.CardCounts.RemoveAt(indexOffset + oldCardIndex);
							return;
						}

						indexOffset += dataSetRef.Cards.Count;
					}
				}));
				_subscriptions.Add(
					vm.WhenAnyValue(_ => _.FrontLayout, _ => _.BackLayout)
						.Throttle(TimeSpan.FromSeconds(0.25))
						.Subscribe(_ =>
							{
								parent.UpdateCardLayoutSize();
								parent.UpdateCanPrint();
							}));
				_subscriptions.Add(
					vm.WhenAnyValue(_ => _.FrontLayout.FullSize, _ => _.BackLayout.FullSize)
						.Throttle(TimeSpan.FromSeconds(0.25))
						.Subscribe(_ =>
							{
								parent.UpdateCardLayoutSize();
							}));
			}

			~DataSetMapping() { DoDispose(true); }

			public void Dispose()
			{
				DoDispose(false);
			}

			private void DoDispose(bool finalize)
			{
				if (_isDisposed)
					return;

				_isDisposed = true;
				_subscriptions.ForEach(d => d.Dispose());
				_subscriptions.Clear();
			}
		}

		private readonly List<DataSetMapping> _dataSetMap = new List<DataSetMapping>();

		#endregion

		#region Constructors & Finalizers

		/// <summary>
		/// Constructs a DeckPrintSetViewModel
		/// </summary>
		/// <param name="workspace">The parent workspace containing the document</param>
		/// <param name="relativePath">The path of the file relative to the workspace root folder</param>
		public DeckPrintSetViewModel(WorkspaceViewModel workspace, string relativePath) : base(workspace, relativePath, typeof(DeckPrintSet))
		{
			_deckPrintSet = (DeckPrintSet) Document;
			CreateCommands();
			_disposables.Add(_cardCountChanged.Subscribe(_ =>
			{
				IsDirty = true;
			}));

			Workspace.Initialized += (sender, args) =>
			{
				IEnumerable<DataSetViewModel> dataSetViewModels = Workspace.Documents.OfType<DataSetViewModel>();
				foreach (DeckPrintSet.DataSetRef dataSetRef in _deckPrintSet.DataSets)
				{
					DataSetViewModel dsvm = dataSetViewModels.FirstOrDefault(vm => vm.RelativePath == dataSetRef.Path);
					if (dsvm == null)
					{
						args.FailedDocuments.FailedDocs.Add(new FailedOperationException.FailedDocument
						{
							Path = RelativePath,
							Reason = new DocumentNotFoundException(dataSetRef.Path)
						});
					}
					else
					{
						_dataSetMap.Add(new DataSetMapping(this, dsvm, dataSetRef));
					}
				}

				InitCardCounts();
				UpdateCanPrint();
			};
		}

		#endregion

		#region Overrides of BaseDocumentViewModel

		/// <summary>
		/// The document's type, as a helpful property
		/// </summary>
		public override Type DocType => typeof(DeckPrintSet);

		/// <summary>
		/// Called when the document is no longer part of the workspace
		/// </summary>
		/// <param name="finalize">Whether this is called from the finalizer or not</param>
		protected override void DoDispose(bool finalize)
		{
			if (_isDisposed)
				return;

			_isDisposed = true;
			_disposables.ForEach(d => d.Dispose());
			_disposables.Clear();

			_dataSetMap.ForEach(mapping => mapping.Dispose());
			_dataSetMap.Clear();
		}

		#endregion

		#region Properties

		/// <summary>
		/// The list of all CardCountViewModels
		/// </summary>
		public ReactiveList<CardCountViewModel> CardCounts { get; } = new ReactiveList<CardCountViewModel>();

		/// <summary>
		/// The currently selected CardCountViewModel
		/// </summary>
		public CardCountViewModel SelectedCardCount
		{
			get => _selectedCardCountViewModel;
			set => this.RaiseAndSetIfChanged(ref _selectedCardCountViewModel, value);
		}

		/// <summary>
		/// The width of the page, in inches
		/// </summary>
		public float PageWidth
		{
			get => _deckPrintSet.PrintOptions.PaperSize.Width;
			set
			{
				if (value <= 0)
					throw new ArgumentOutOfRangeException(nameof(value), "PageWidth must be a positive value");

				_deckPrintSet.PrintOptions.PaperSize = new Size(value, PageHeight);
				this.RaisePropertyChanged();
				UpdateCardCounts();
				IsDirty = true;
			}
		}

		/// <summary>
		/// The height of the page, in inches
		/// </summary>
		public float PageHeight
		{
			get => _deckPrintSet.PrintOptions.PaperSize.Height;
			set
			{
				if (value <= 0)
					throw new ArgumentOutOfRangeException(nameof(value), "PageHeight must be a positive value");

				_deckPrintSet.PrintOptions.PaperSize = new Size(PageWidth, value);
				this.RaisePropertyChanged();
				UpdateCardCounts();
				IsDirty = true;
			}
		}

		/// <summary>
		/// The amount of horizontal space between cards, in inches
		/// </summary>
		public float HSpacing
		{
			get => _deckPrintSet.PrintOptions.Spacing.Width;
			set
			{
				if (value < 0)
					throw new ArgumentOutOfRangeException(nameof(value), "HSpacing must be a non-negative value");

				_deckPrintSet.PrintOptions.Spacing = new Size(value, VSpacing);
				this.RaisePropertyChanged();
				UpdateCardCounts();
				IsDirty = true;
			}
		}

		/// <summary>
		/// The amount of vertical space between cards, in inches
		/// </summary>
		public float VSpacing
		{
			get => _deckPrintSet.PrintOptions.Spacing.Height;
			set
			{
				if (value < 0)
					throw new ArgumentOutOfRangeException(nameof(value), "VSpacing must be a non-negative value");

				_deckPrintSet.PrintOptions.Spacing = new Size(HSpacing, value);
				this.RaisePropertyChanged();
				UpdateCardCounts();
				IsDirty = true;
			}
		}

		/// <summary>
		/// The amount of space from the left and right edges of the paper, in inches
		/// </summary>
		public float HMargin
		{
			get => _deckPrintSet.PrintOptions.Margins.Width;
			set
			{
				if (value < 0)
					throw new ArgumentOutOfRangeException(nameof(value), "HMargin must be a non-negative value");

				_deckPrintSet.PrintOptions.Margins = new Size(value, VMargin);
				this.RaisePropertyChanged();
				UpdateCardCounts();
				IsDirty = true;
			}
		}

		/// <summary>
		/// The amount of space from the top and bottom edges of the paper, in inches
		/// </summary>
		public float VMargin
		{
			get => _deckPrintSet.PrintOptions.Margins.Height;
			set
			{
				if (value < 0)
					throw new ArgumentOutOfRangeException(nameof(value), "VMargin must be a non-negative value");

				_deckPrintSet.PrintOptions.Margins = new Size(HMargin, value);
				this.RaisePropertyChanged();
				UpdateCardCounts();
				IsDirty = true;
			}
		}

		/// <summary>
		/// Whether we should include the bleed region when printing cards
		/// </summary>
		public bool IncludeBleed
		{
			get => _deckPrintSet.PrintOptions.IncludeBleed;
			set
			{
				_deckPrintSet.PrintOptions.IncludeBleed = value;
				this.RaisePropertyChanged();
				UpdateCardLayoutSize();
				IsDirty = true;
			}
		}

		/// <summary>
		/// Whether we should print the cards in landscape mode (horizontally) or portrait mode (vertically)
		/// </summary>
		public bool UseLandscape
		{
			get => _deckPrintSet.PrintOptions.Landscape;
			set
			{
				_deckPrintSet.PrintOptions.Landscape = value;
				this.RaisePropertyChanged();
				UpdateCardLayoutSize();
				IsDirty = true;
			}
		}

		/// <summary>
		/// Errors codes for whether user can print cards or why not
		/// </summary>
		public enum CanPrintError
		{
			/// <summary>
			/// No problem, user can print cards
			/// </summary>
			None,

			/// <summary>
			/// The Deck Print Set has no cards to print
			/// </summary>
			NoCards,

			/// <summary>
			/// A Data Set is missing a Layout
			/// </summary>
			MissingLayout,

			/// <summary>
			/// One or more Layouts do no match the size of other Layouts
			/// </summary>
			LayoutMismatch
		}

		/// <summary>
		/// Whether we can print the cards or not, requires all 
		/// </summary>
		public CanPrintError PrintError
		{
			get => _printError;
			set => this.RaiseAndSetIfChanged(ref _printError, value);
		}

		/// <summary>
		/// Size of the cards, based on first available layout
		/// </summary>
		public Size CardLayoutSize
		{
			get => _cardLayoutsize;
			set => this.RaiseAndSetIfChanged(ref _cardLayoutsize, value);
		}

		/// <summary>
		/// Number of cards that fit on the page horizontally
		/// </summary>
		public int XCardCount
		{
			get => _xCardCount;
			set => this.RaiseAndSetIfChanged(ref _xCardCount, value);
		}

		/// <summary>
		/// Number of cards that fit on the page vertically
		/// </summary>
		public int YCardCount
		{
			get => _yCardCount;
			set => this.RaiseAndSetIfChanged(ref _yCardCount, value);
		}

		/// <summary>
		/// The amount of progress made while printing. Resets to 0 when finished.
		/// </summary>
		public float PrintProgress
		{
			get => _printProgress;
			set => this.RaiseAndSetIfChanged(ref _printProgress, value);
		}

		#endregion

		#region Commands & Interactions

		/// <summary>
		/// Adds a DataSet to the DeckPrintSet
		/// </summary>
		public ReactiveCommand AddDataSet { get; set; }

		/// <summary>
		/// Removes the selected DataSet from the DeckPrintSet
		/// </summary>
		public ReactiveCommand RemoveSelectedDataSet { get; set; }

		/// <summary>
		/// Prints the cards
		/// </summary>
		public ReactiveCommand Print { get; set; }

		/// <summary>
		/// Cancels printing cards
		/// </summary>
		public ReactiveCommand CancelPrint { get; set; }

		#endregion

		#region Private Methods

		private void InitCardCounts()
		{
			using (CardCounts.SuppressChangeNotifications())
			{
				foreach (DataSetMapping mapping in _dataSetMap)
				{
					foreach (DeckPrintSet.CardCount cardCount in mapping.DataSetRef.Cards)
					{
						CardCounts.Add(new CardCountViewModel(cardCount, mapping, _cardCountChanged));
					}
				}
			}
		}

		private void CreateCommands()
		{
			AddDataSet = ReactiveCommand.Create(DoAddDataSet);
			RemoveSelectedDataSet = ReactiveCommand.Create(DoRemoveDataSet,
				this.WhenAnyValue(vm => vm.SelectedCardCount).Select(vm => vm != null));

			Print = ReactiveCommand.Create(DoPrint,
				this.WhenAnyValue(vm => vm.PrintError).Select(error => error == CanPrintError.None));
			CancelPrint = ReactiveCommand.Create(DoCancelPrint);
		}

		private void DoAddDataSet()
		{
			IList<BaseDocumentViewModel> validDataSets =
				Workspace.Documents
						.Where(IsDataSetValid)
						.ToList();

			IFileSystem fileSystem = Locator.Current.GetService<IFileSystem>();
			DataSetViewModel newDataSet = fileSystem?.SelectDocument(validDataSets, "Select a Data Set") as DataSetViewModel;
			if (newDataSet != null)
			{
				DeckPrintSet.DataSetRef newRef = new DeckPrintSet.DataSetRef
				{
					Path = newDataSet.RelativePath
				};
				_deckPrintSet.DataSets.Add(newRef);
				DataSetMapping newMapping = new DataSetMapping(this, newDataSet, newRef);
				_dataSetMap.Add(newMapping);

				using (CardCounts.SuppressChangeNotifications())
				{
					foreach (CardDataViewModel card in newDataSet.Cards)
					{
						DeckPrintSet.CardCount newCount = new DeckPrintSet.CardCount
						{
							Id = card.Id,
							Count = 1
						};
						newRef.Cards.Add(newCount);
						CardCounts.Add(new CardCountViewModel(newCount, newMapping, _cardCountChanged));
					}
				}
				UpdateCardLayoutSize();
				IsDirty = true;
			}

			//UpdateCanPrint();
		}

		private void DoRemoveDataSet()
		{
			DataSetMapping mapping = SelectedCardCount.Mapping;
			_dataSetMap.Remove(mapping);

			DeckPrintSet.DataSetRef refToRemove = mapping.DataSetRef;
			_deckPrintSet.DataSets.Remove(refToRemove);

			SelectedCardCount = null;
			using (CardCounts.SuppressChangeNotifications())
			{
				CardCounts.RemoveAll(cardCount => cardCount.Mapping == mapping);
			}

			mapping.Dispose();
			UpdateCardLayoutSize();
			UpdateCanPrint();

			IsDirty = true;
		}

		private void DoPrint()
		{
			_cancelSource = new CancellationTokenSource();
			CancellationToken token = _cancelSource.Token;
			RxApp.MainThreadScheduler.Schedule(new Unit(), (scheduler, unit) =>
			{
				PrintProgress = .00001f;

				float completedCards = 0;
				float totalCards = CardCounts.Sum(ccvm => ccvm.Count);

				ICardRenderer renderer = Locator.Current.GetService<ICardRenderer>();
				ICardPrinter printer = Locator.Current.GetService<ICardPrinter>();
				IPrinterDocument document = printer.CreateNewDocument(_deckPrintSet.PrintOptions, CardLayoutSize);
				DataSetViewModel dsvm = null;
				LayoutViewModel frontLvm = null;
				LayoutViewModel backLvm = null;
				IBitmap unboundFront = null;
				IBitmap unboundBack = null;
				OutputParams outputParams = null;
				foreach (CardCountViewModel ccvm in CardCounts)
				{
					if (token.IsCancellationRequested)
						return null;

					if (dsvm != ccvm.Mapping.ViewModel)
					{
						// moved to a new data set, check if we have unbound layouts
						dsvm = ccvm.Mapping.ViewModel;
						frontLvm = dsvm.FrontLayout;
						backLvm = dsvm.BackLayout;
						unboundFront = null;
						unboundBack = null;

						outputParams = new OutputParams
						{
							Ppi = OutputParams.PrintResolution,
							Format = Image.Format.Bmp,
							IncludeBleed = IncludeBleed,
							FrontOrientation = frontLvm.IsLandscape != UseLandscape ? 
											OutputParams.Orientation.Ninety : 
											OutputParams.Orientation.Zero
						};
						// include option in dataset ref to flip back?
						outputParams.BackOrientation = ccvm.Mapping.DataSetRef.FlipBackSide ?
							OutputParams.GetFlippedOrientation(outputParams.FrontOrientation) :
							outputParams.FrontOrientation;
					}

					CardDataViewModel cdvm = dsvm.Cards.First(card => card.Id == ccvm.Id);
					IBitmap front, back;

					if (!frontLvm.HasBindings)
					{
						if (unboundFront == null)
							unboundFront = renderer.RenderCard(cdvm, frontLvm, outputParams, true);

						front = unboundFront;
					}
					else
					{
						front = renderer.RenderCard(cdvm, frontLvm, outputParams, true);
					}

					if (!backLvm.HasBindings)
					{
						if (unboundBack == null)
							unboundBack = renderer.RenderCard(dsvm.Cards.First(), backLvm, outputParams, false);

						back = unboundBack;
					}
					else
					{
						back = renderer.RenderCard(cdvm, backLvm, outputParams, false);
					}

					if (front == null || back == null)
					{
						// report an error!
						return null;
					}

					for (int i = 0; i < ccvm.Count; i++)
						document.AddCard(front, back);

					completedCards += ccvm.Count;
					PrintProgress = completedCards / totalCards;
				}

				printer.Print(document);
				PrintProgress = 0.0f;

				return null;
			});
			
			//Thread newThread = new Thread(() =>
			//{
				
			//});
			//newThread.SetApartmentState(ApartmentState.STA);
			//newThread.Start();
		}

		private void DoCancelPrint()
		{
			if (_cancelSource == null)
				return;

			if (_cancelSource.IsCancellationRequested)
				return;

			_cancelSource.Cancel();
		}

		private bool IsDataSetValid(BaseDocumentViewModel doc)
		{
			DataSetViewModel dataSetVm = doc as DataSetViewModel;
			if (dataSetVm == null)
				return false;

			if (_deckPrintSet.DataSets.Any(dataSet => dataSet.Path == dataSetVm.RelativePath))
				return false;

			return true;
		}

		private void UpdateCanPrint()
		{
			if (!_dataSetMap.Any())
			{
				PrintError = CanPrintError.NoCards;
				return;
			}

			if (_dataSetMap.Any(mapping => mapping.ViewModel.FrontLayout == null || mapping.ViewModel.BackLayout == null))
			{
				PrintError = CanPrintError.MissingLayout;
				return;
			}

			LayoutViewModel firstLayout = _dataSetMap[0].ViewModel.FrontLayout;
			if (_dataSetMap.Any(mapping =>
				!firstLayout.IsRotationallyEquivalentSize(mapping.ViewModel.FrontLayout) ||
				!firstLayout.IsRotationallyEquivalentSize(mapping.ViewModel.BackLayout)))
			{
				PrintError = CanPrintError.LayoutMismatch;
				return;
			}

			PrintError = CanPrintError.None;
		}

		private void UpdateCardLayoutSize()
		{
			Size firstLayoutSize = new Size(0, 0);
			foreach (DataSetMapping mapping in _dataSetMap)
			{
				if (mapping.ViewModel.FrontLayout != null)
				{
					firstLayoutSize = IncludeBleed ? mapping.ViewModel.FrontLayout.FullSize : mapping.ViewModel.FrontLayout.Size;
					break;
				}

				if (mapping.ViewModel.BackLayout != null)
				{
					firstLayoutSize = IncludeBleed ? mapping.ViewModel.BackLayout.FullSize : mapping.ViewModel.BackLayout.Size;
				}
			}

			bool isLandscape = firstLayoutSize.Width > firstLayoutSize.Height;
			CardLayoutSize = UseLandscape == isLandscape ? firstLayoutSize : firstLayoutSize.Inverse();
			UpdateCardCounts();
		}

		private void UpdateCardCounts()
		{
			float availableHSpace = PageWidth - HMargin * 2;
			float availableVSpace = PageHeight - VMargin * 2;

			int xCardCount = 0;
			int yCardCount = 0;

			while (availableHSpace >= CardLayoutSize.Width)
			{
				availableHSpace -= (CardLayoutSize.Width + HSpacing);
				xCardCount++;
			}

			while (availableVSpace >= CardLayoutSize.Height)
			{
				availableVSpace -= (CardLayoutSize.Height + VSpacing);
				yCardCount++;
			}

			XCardCount = xCardCount;
			YCardCount = yCardCount;
		}

		#endregion
	}
}
