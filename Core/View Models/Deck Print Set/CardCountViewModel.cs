﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Reactive;
using Core.Documents;

namespace Core.View_Models.Deck_Print_Set
{
	/// <summary>
	/// Helper ViewModel that wraps information for a single Card within a DeckPrintSet
	/// </summary>
	/// <remarks>
	/// This is not a ReactiveObject because 1) there will be many of these, and RxUI can get very expensive
	/// and 2) the values in this "ViewModel" can't really change behind the UI's back, so there's nothing 
	/// to hook up anyway.
	/// </remarks>
	public class CardCountViewModel
	{
		#region Private Members

		private readonly DeckPrintSet.CardCount _cardCount;
		private readonly IObserver<Unit> _countChanged;

		#endregion

		#region Public Properties

		/// <summary>
		/// Name of the DataSet where this card comes from, used for grouping/searching
		/// </summary>
		public string DataSetName => Mapping.ViewModel.DisplayName;

		/// <summary>
		/// The DataSetMapping that contains the matching card
		/// </summary>
		public DeckPrintSetViewModel.DataSetMapping Mapping { get; }

		/// <summary>
		/// The Id of the wrapped card
		/// </summary>
		public string Id => _cardCount.Id;

		/// <summary>
		/// How many times the card appears in the deck
		/// </summary>
		public int Count
		{
			get => _cardCount.Count;
			set
			{
				if (value < 0)
					throw new ArgumentOutOfRangeException(nameof(value), "Count must be a non-negative value");

				if (value == _cardCount.Count)
					return;

				_cardCount.Count = value;
				_countChanged.OnNext(new Unit());
			}
		}

		#endregion

		#region Constructor

		/// <summary>
		/// Constructs a CardCountViewModel
		/// </summary>
		/// <param name="cardCount">The CardCount containing the data</param>
		/// <param name="mapping">The DataSetMapping for the related card</param>
		/// <param name="countChanged">An Observer listening to when the count has changed</param>
		public CardCountViewModel(DeckPrintSet.CardCount cardCount, DeckPrintSetViewModel.DataSetMapping mapping, IObserver<Unit> countChanged)
		{
			_cardCount = cardCount;
			Mapping = mapping;
			_countChanged = countChanged;
		}

		#endregion
	}
}
