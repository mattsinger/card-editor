﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Specialized;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using Core.Documents;
using Core.Util;
using ReactiveUI;
using ReactiveCommand = ReactiveUI.ReactiveCommand;

namespace Core.View_Models.Glyph_Set
{
	/// <summary>
	/// ViewModel that wraps a glyph set
	/// </summary>
	public class GlyphSetViewModel : BaseDocumentViewModel
	{
		#region Private Members

		private readonly GlyphSet _glyphSet;
		private GlyphViewModel _selectedGlyph;
		private IDisposable _glyphListDisposable;

		#endregion

		#region Overrides of BaseDocumentViewModel

		/// <summary>
		/// The document's type, as a helpful property
		/// </summary>
		public override Type DocType => typeof(GlyphSet);

		/// <summary>
		/// Called when the document is no longer part of the workspace
		/// </summary>
		/// <param name="finalize">Whether this is called from the finalizer or not</param>
		protected override void DoDispose(bool finalize)
		{
			if (_isDisposed)
				return;

			_isDisposed = true;
			Workspace.Glyphs.RemoveAll(GlyphList);
			_glyphListDisposable.Dispose();
		}

		#endregion

		#region Glyphs

		/// <summary>
		/// The contained Glyphs
		/// </summary>
		public ReactiveList<GlyphViewModel> GlyphList { get; } = new ReactiveList<GlyphViewModel>();

		/// <summary>
		/// The currently selected glyph
		/// </summary>
		public GlyphViewModel SelectedGlyph
		{
			get => _selectedGlyph;
			set => this.RaiseAndSetIfChanged(ref _selectedGlyph, value);
		}

		/// <summary>
		/// Add a new glyph to set
		/// </summary>
		public ReactiveCommand Add { get; protected set; }

		/// <summary>
		/// Remove the selected glyph from the set
		/// </summary>
		public ReactiveCommand Remove { get; protected set; }

        /// <summary>
        /// Renames the selected glyph
        /// </summary>
        public ReactiveCommand Rename { get; protected set; }

		/// <summary>
		/// Interaction asking the user for the name of a new glyph
		/// </summary>
		public Interaction<Unit, string> GetGlyphId { get; } = new Interaction<Unit, string>();

		/// <summary>
		/// Interaction fired when the user provides a duplicate glyph Id
		/// </summary>
		public Interaction<Unit, Unit> DuplicateGlyphId { get; } = new Interaction<Unit, Unit>();

		#endregion

		#region Constructor

		/// <summary>
		/// Constructs a GlyphSetViewModel
		/// </summary>
		/// <param name="workspaceViewModel">The containing WorkspaceViewModel</param>
		/// <param name="relativePath">The path of the Glyph Set relative to rootpath</param>
		public GlyphSetViewModel(WorkspaceViewModel workspaceViewModel, string relativePath) : base(workspaceViewModel, relativePath, typeof(GlyphSet))
		{
			GlyphList.ChangeTrackingEnabled = true;
			GlyphList.ItemChanged.Subscribe(_ => IsDirty = true);

			// we want an exception to be thrown here if the referenced document is incorrect
			_glyphSet = (GlyphSet)Document;
			InitializeData();
			CreateCommands();
		}

		#endregion

		#region Private Methods

		private void InitializeData()
		{
			using (GlyphList.SuppressChangeNotifications())
			{
				GlyphList.AddRange(
					_glyphSet.Glyphs.Select(glyph => new GlyphViewModel(glyph, Workspace.RootPath)));
				Workspace.Glyphs.AddRange(GlyphList);
			}
			_glyphListDisposable = GlyphList.ItemChanged.Subscribe(_ => IsDirty = true);
			GlyphList.CollectionChanged += (sender, args) =>
			{
				switch (args.Action)
				{
					case NotifyCollectionChangedAction.Add:
						for (int i = 0; i < args.NewItems.Count; i++)
						{
							GlyphViewModel newVm = args.NewItems[i] as GlyphViewModel;
							Workspace.Glyphs.Add(newVm);
							_glyphSet.Glyphs.Insert(args.NewStartingIndex + i, newVm.Glyph);
						}
						break;

					case NotifyCollectionChangedAction.Remove:
						args.OldItems.Cast<GlyphViewModel>().ForEach(oldGlyph =>
						{
							Workspace.Glyphs.Remove(oldGlyph);
							_glyphSet.Glyphs.Remove(oldGlyph.Glyph);
						});
						break;

					case NotifyCollectionChangedAction.Replace:
						args.OldItems.Cast<GlyphViewModel>().ForEach(oldGlyph =>
						{
							Workspace.Glyphs.Remove(oldGlyph);
							_glyphSet.Glyphs.Remove(oldGlyph.Glyph);
						});
						for (int i = 0; i < args.NewItems.Count; i++)
						{
							GlyphViewModel newVm = args.NewItems[i] as GlyphViewModel;
							Workspace.Glyphs.Add(newVm);
							_glyphSet.Glyphs.Insert(args.NewStartingIndex + i, newVm.Glyph);
						}
						break;
				}

				IsDirty = true;
			};
		}

		private void DoAdd()
		{
			GetGlyphId.Handle(new Unit()).Subscribe(glyphId =>
			{
				if (string.IsNullOrEmpty(glyphId))
					return;

				if (Workspace.Glyphs.Any(glyph => glyph.Id == glyphId))
				{
					DuplicateGlyphId.Handle(new Unit()).Subscribe();
					return;
				}

				GlyphSet.Glyph newGlyph = new GlyphSet.Glyph
				{
					Id = glyphId
				};

				GlyphViewModel newVm = new GlyphViewModel(newGlyph, Workspace.RootPath);
				GlyphList.Add(newVm);

				SelectedGlyph = newVm;
			});
		}

		private void DoRemove()
		{
			GlyphList.Remove(SelectedGlyph);
		}

	    private void DoRename()
	    {
            GetGlyphId.Handle(new Unit()).Subscribe(glyphId =>
            {
                if (string.IsNullOrEmpty(glyphId))
                    return;

                if (Workspace.Glyphs.Any(glyph => glyph.Id == glyphId && glyph != SelectedGlyph))
                {
                    DuplicateGlyphId.Handle(new Unit()).Subscribe();
                    return;
                }
                
                SelectedGlyph.Id = glyphId;
            });
        }

		private void CreateCommands()
		{
			Add = ReactiveCommand.Create(DoAdd);

			Remove = ReactiveCommand.Create(DoRemove,
				this.WhenAnyValue(vm => vm.SelectedGlyph).Select(glyph => glyph != null));

            Rename = ReactiveCommand.Create(DoRename,
                this.WhenAnyValue(vm => vm.SelectedGlyph).Select(glyph => glyph != null));
		}

		#endregion
	}
}
