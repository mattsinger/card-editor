﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.IO;
using System.Reactive.Linq;
using Core.Documents;
using Core.Util;
using ReactiveUI;
using Splat;
using ReactiveCommand = ReactiveUI.ReactiveCommand;

namespace Core.View_Models.Glyph_Set
{
	/// <summary>
	/// ViewModel that wraps a single glyph
	/// </summary>
	public class GlyphViewModel : ReactiveObject, IGlyphViewModel
	{
		private readonly string _root;
		private readonly ObservableAsPropertyHelper<IBitmap> _image;

		#region Public Properties

		public GlyphSet.Glyph Glyph { get; }

		/// <summary>
		/// Id for the Glyph
		/// </summary>
		public string Id
		{
			get { return Glyph.Id; }
			set
			{
				if (value == Glyph.Id)
					return;

				Glyph.Id = value;
				this.RaisePropertyChanged();
			}
		}

		/// <summary>
		/// The glyph's filepath relative to the root
		/// </summary>
		public string Path
		{
			get { return Glyph.RelativePath; }
			set
			{
				if (value == Glyph.RelativePath)
					return;

				Glyph.RelativePath = value;
				this.RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Allows the user to set the path to the glyph
		/// </summary>
		public ReactiveCommand SelectPath { get; protected set; }

		/// <summary>
		/// The glyph's image
		/// </summary>
		public IBitmap Image => _image.Value;

		#endregion

		#region Constructor

		/// <summary>
		/// Constructs a GlyphViewModel
		/// </summary>
		/// <param name="glyph">The wrapped Glyph</param>
		/// <param name="rootPath">The root path of the glyph</param>
		public GlyphViewModel(GlyphSet.Glyph glyph, string rootPath)
		{
			Glyph = glyph;
			_root = rootPath;

			this.WhenAnyValue(vm => vm.Path)
				.Select(path =>
				{
					if (string.IsNullOrEmpty(path))
						return null;

					string imgPath = Util.File.GetFullPath(_root, path);
					IFileSystem system = Locator.Current.GetService<IFileSystem>();
					Stream stream = Stream.Null;
					IBitmap image = null;
					try
					{
						stream = system.ReadLocalFile(imgPath);
						image = BitmapLoader.Current.Load(stream, null, null).Result;
						stream.Dispose();
					}
					// we might expect the user to type in an incorrect filename, in which case we should do nothing
					catch (FileNotFoundException)
					{
						this.Log().Warn("Glyph {0}: Could not locate image file {1}", 
							Id, path);
					}
					catch (Exception ex)
					{
						this.Log().Error("Glyph {0}: Failed to load image {1}:\n{2}", 
							Id, path, ex);
					}
					finally
					{
						stream?.Dispose();
					}

					return image;
				})
				.ToProperty(this, vm => vm.Image, out _image);

			Path = Glyph.RelativePath;
			SelectPath = ReactiveCommand.Create(DoSelectPath);
		}

		#endregion

		#region Private Methods

		private void DoSelectPath()
		{
			IFileSystem system = Locator.Current.GetService<IFileSystem>();
			string fileName;
			if (system.GetExistingImage(_root + Path, out fileName))
				Path = Util.File.GetRelativePath(fileName, _root);
		}

		#endregion
	}
}
