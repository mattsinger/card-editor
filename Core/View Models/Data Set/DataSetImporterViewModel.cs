﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using Core.Util;
using Core.Util.Importers;
using ReactiveUI;
using Splat;

namespace Core.View_Models.Data_Set
{
	/// <summary>
	/// Handles importing Data Set data from other sources
	/// </summary>
	public class DataSetImporterViewModel : ReactiveObject, IDisposable
	{
		#region Private Members

		private string _filePath;
		private string _subsection = string.Empty;
		private string _idColumn = string.Empty;
		private bool _firstRowHeaders = true;
		private bool _overwriteExistingData;
		private bool _canImport;
		private bool _isLoadingFile;
		private IDataSetImporter _importer;

		private bool _disposed;

		private readonly string _rootPath;

		#endregion

		#region Properties & Nested Classes

		/// <summary>
		/// The path to the file from which we import card data
		/// </summary>
		public string FilePath
		{
			get => _filePath;
			set => this.RaiseAndSetIfChanged(ref _filePath, value);
		}

		/// <summary>
		/// The subsection of the file to read headers from
		/// </summary>
		public string Subsection
		{
			get => _subsection;
			set => this.RaiseAndSetIfChanged(ref _subsection, value);
		}

		/// <summary>
		/// The list of subsections which can be read from
		/// </summary>
		public ReactiveList<string> Subsections { get; } = new ReactiveList<string>();

		/// <summary>
		/// Whether the file has any subsections
		/// </summary>
		public bool HasSubsections => Subsections.Count > 0;

		/// <summary>
		/// Whether or not the first row are headers, which get translated to Field names
		/// </summary>
		public bool UseFirstRowHeaders
		{
			get => _firstRowHeaders;
			set => this.RaiseAndSetIfChanged(ref _firstRowHeaders, value);
		}

		/// <summary>
		/// Whether the imported data should overwrite existing data or append to it.
		/// </summary>
		/// <remarks>When appending, any new fields are added at the end of the list and duplicate field names are ignored.
		/// New cards are added at the end of the list and duplicate cards are ignored. If new fields are added, existing cards
		/// will be updated to have the new field, but with default data.</remarks>
		public bool OverwriteExistingData
		{
			get => _overwriteExistingData;
			set => this.RaiseAndSetIfChanged(ref _overwriteExistingData, value);
		}

		/// <summary>
		/// Name of the column to use for card Ids. If null or empty, then Ids will be automatically generated.
		/// </summary>
		/// <remarks>Automatically updates the IsId field for all Headers</remarks>
		public string IdColumn
		{
			get => _idColumn;
			set
			{
				Headers.ForEach(header => header.IsId = string.CompareOrdinal(header.Key, value) == 0);
				this.RaiseAndSetIfChanged(ref _idColumn, value);
			}
		}

		/// <summary>
		/// Gets the full list of viable Id Column entries. Includes an empty string to represent auto-generated Ids.
		/// </summary>
		public ReactiveList<string> ViableIdColumns { get; } = new ReactiveList<string>();

		/// <summary>
		/// The importer used to read file data
		/// </summary>
		internal IDataSetImporter Importer
		{
			get => _importer;
			private set
			{
				_importer?.Dispose();
				_importer = value;
			}
		}

		/// <summary>
		/// The list of imported headers
		/// </summary>
		public ReactiveList<FieldImporterViewModel> Headers { get; } = new ReactiveList<FieldImporterViewModel>
		{
			ChangeTrackingEnabled = true
		};

		/// <summary>
		/// Whether or not we are ready to import data
		/// </summary>
		public bool CanImport
		{
			get => _canImport;
			set => this.RaiseAndSetIfChanged(ref _canImport, value);
		}

		/// <summary>
		/// Whether we are loading data from file
		/// </summary>
		public bool IsLoadingFile
		{
			get => _isLoadingFile;
			set => this.RaiseAndSetIfChanged(ref _isLoadingFile, value);
		}

		#endregion

		#region Commands & Interactions

		/// <summary>
		/// Command to select the import file
		/// </summary>
		public ReactiveCommand SelectFile { get; protected set; }

		#endregion

		#region Constructor

		/// <summary>
		/// Constructs a DataSetImporterViewModel
		/// </summary>
		/// <param name="rootPath">The root path to begin searching for import files</param>
		public DataSetImporterViewModel(string rootPath)
		{
			_rootPath = rootPath;

			CreateCommands();

			this.WhenAnyValue(vm => vm.FilePath)
				.Subscribe(_ =>
				{
					CreateImporter();
					LoadFileData();
				});
			this.WhenAnyValue(vm => vm.UseFirstRowHeaders)
				.Subscribe(_ =>
				{
					LoadHeaders();
				});
			this.WhenAnyValue(vm => vm.Subsection)
				.Where(_ => !IsLoadingFile)
				.Subscribe(_ =>
				{
					LoadHeaders();
				});
			Subsections.Changed
				.Subscribe(_ =>
				{
					this.RaisePropertyChanged(nameof(HasSubsections));
				});
			Headers.ItemChanged
				.Where(args => 
					args.PropertyName == nameof(FieldImporterViewModel.Key) ||
					args.PropertyName == nameof(FieldImporterViewModel.CanBeId))
				.Subscribe(_ => ResetIdColumn());
		}

		#endregion

		#region Dispose and Finalizer

		~DataSetImporterViewModel()
		{
			DoDispose(true);
		}

		public void Dispose()
		{
			DoDispose(false);
		}

		#endregion

		#region Private Methods

		private void CreateCommands()
		{
			SelectFile = ReactiveCommand.Create(DoSelectFile);
		}

		private void CreateImporter()
		{
			if (string.IsNullOrEmpty(FilePath))
			{
				Importer = null;
				return;
			}

			string fileExt = Path.GetExtension(FilePath);
			Importer = DataSetImportHelpers.GetImporterForExtension(fileExt);
		}

		private void DoSelectFile()
		{
			IFileSystem fileSystem = Locator.Current.GetService<IFileSystem>();
			string importFile;

			if (fileSystem.GetDataSetImportFile(_rootPath, out importFile))
			{
				FilePath = importFile;
			}
		}

		private void ClearData()
		{
			Headers.Clear();
			Subsections.Clear();
			Subsection = string.Empty;
		}

		private void LoadFileData()
		{
			IsLoadingFile = true;

			ClearData();

			if (string.IsNullOrEmpty(FilePath))
			{
				IsLoadingFile = false;
				CanImport = false;
				return;
			}

			IFileSystem fileSystem = Locator.Current.GetService<IFileSystem>();
			if (!fileSystem.Exists(FilePath))
			{
				IsLoadingFile = false;
				CanImport = false;
				return;
			}

			List<string> subsections = Importer != null ? Importer.GetSubsections(FilePath) : new List<string>();
			foreach (string subsection in subsections)
			{
				Subsections.Add(subsection);
			}
			if (Subsections.Any())
				Subsection = Subsections.First();

			LoadHeaders();

			CanImport = true;
			IsLoadingFile = false;
		}

		private void LoadHeaders()
		{
			using (Headers.SuppressChangeNotifications())
			{
				Headers.Clear();
				List<FieldImporterViewModel> importedHeaders = Importer?.LoadHeaders(FilePath, Subsection, UseFirstRowHeaders);
				importedHeaders?.ForEach(newHeader =>
				{
					if (Headers.All(header => header.Key != newHeader.Key))
					{
						Headers.Add(newHeader);
					}
				});
			}
			Headers.Reset();

			ResetIdColumn();
		}

		private void ResetIdColumn()
		{
			using (ViableIdColumns.SuppressChangeNotifications())
			{
				ViableIdColumns.Clear();
				ViableIdColumns.Add(string.Empty);
				Headers.Where(header => header.CanBeId)
					.ForEach(header => ViableIdColumns.Add(header.Key));
			}
			ViableIdColumns.Reset();
			if (string.IsNullOrEmpty(IdColumn))
			{
				string idHeader = ViableIdColumns.FirstOrDefault(id => string.Compare(id, "id", StringComparison.OrdinalIgnoreCase) == 0);
				if (!string.IsNullOrEmpty(idHeader))
					IdColumn = idHeader;
			}
			else
			{
				if (!ViableIdColumns.Contains(IdColumn))
					IdColumn = string.Empty;
			}
		}

		private void DoDispose(bool finalize)
		{
			if (_disposed)
				return;

			_disposed = true;
			Importer?.Dispose();
			Importer = null;
		}

		#endregion
	}
}
