﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Core.Documents;
using Core.Util;
using Splat;
using ReactiveCommand = ReactiveUI.ReactiveCommand;

namespace Core.View_Models.Data_Set
{
	/// <summary>
	/// ViewModel for Image fields in card data
	/// </summary>
	public class ImageFieldViewModel : FieldViewModel
	{
		/// <summary>
		/// Constructs a FieldViewModel
		/// </summary>
		/// <param name="field">The wrapped card field</param>
		/// <param name="root">The workspace root path</param>
		public ImageFieldViewModel(CardField field, string root) : base(field)
		{
			Root = root;

			SelectPath = ReactiveCommand.Create(DoSelectPath);
		}

		#region Public Properties

		/// <summary>
		/// Selects an image file
		/// </summary>
		public ReactiveCommand SelectPath { get; protected set; }

		/// <summary>
		/// The field's file path, relative to the root
		/// </summary>
		public string Path
		{
			get => _field.Value;
			set
			{
				if (value == _field.Value)
					return;

				_field.Value = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// The field's value as seen by external systems, useful for import/export
		/// </summary>
		/// <remarks>For an ImageFieldViewModel, this is the full path to the image</remarks>
		public override string ExternalValue
		{
			get => File.GetFullPath(Root, Path);
			set => Path = File.GetRelativePath(value, Root);
		}

		/// <summary>
		/// The root path
		/// </summary>
		public string Root { get; }

		#endregion

		#region Private Methods

		private void DoSelectPath()
		{
			IFileSystem system = Locator.Current.GetService<IFileSystem>();
			string fileName;
			if (system.GetExistingImage(File.GetFullPath(Root, Path), out fileName))
				Path = File.GetRelativePath(fileName, Root);
		}

		#endregion
	}
}
