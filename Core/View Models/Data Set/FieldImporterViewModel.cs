﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Core.Documents;
using ReactiveUI;

namespace Core.View_Models.Data_Set
{
	/// <summary>
	/// ViewModel to handle options when importing a field header
	/// </summary>
	public class FieldImporterViewModel : ReactiveObject
	{
		private string _key;
		private FieldType _fieldType;
		private bool _include = true;
		private bool _isId;

		/// <summary>
		/// The name of the field
		/// </summary>
		public string Key
		{
			get => _key;
			set => this.RaiseAndSetIfChanged(ref _key, value);
		}

		/// <summary>
		/// The data type of the field
		/// </summary>
		public FieldType FieldType
		{
			get => _fieldType;
			set => this.RaiseAndSetIfChanged(ref _fieldType, value);
		}

		/// <summary>
		/// Whether this field should be included in the import
		/// </summary>
		/// <remarks>If set to false, then IsId is also set to false</remarks>
		public bool Include
		{
			get => _include;
			set
			{
				this.RaiseAndSetIfChanged(ref _include, value);
				this.RaisePropertyChanged(nameof(CanBeId));
				if (!value)
					IsId = false;
			}
		}

		/// <summary>
		/// Whether this field is actually the card id. Implies Include is true.
		/// </summary>
		public bool IsId
		{
			get => _isId;
			set => this.RaiseAndSetIfChanged(ref _isId, value);
		}

		/// <summary>
		/// Whether the field can be an Id, i.e. it is included in the import
		/// </summary>
		public bool CanBeId => Include;
	}
}
