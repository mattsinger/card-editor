﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019  Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Linq;
using Core.Documents;
using Core.Exceptions;
using ReactiveUI;

namespace Core.View_Models.Data_Set
{
	/// <summary>
	/// ViewModel that wraps a single CardData
	/// </summary>
	public class CardDataViewModel : ReactiveObject
	{
		private readonly string _root;

		/// <summary>
		/// The wrapped CardData
		/// </summary>
		public CardData Data { get; }

		/// <summary>
		/// The DataSetViewModel that contains this card
		/// </summary>
		public DataSetViewModel Parent { get; }

		/// <summary>
		/// The id of the card
		/// </summary>
		public string Id
		{
			get => Data.Id;
			set
			{
				if (value == Data.Id)
					return;

				Data.Id = value;
				this.RaisePropertyChanged();
			}
		}

		/// <summary>
		/// The individual fields of the card data, does not include Id
		/// </summary>
		public ReactiveList<FieldViewModel> Fields { get; } = new ReactiveList<FieldViewModel>
		{
			ChangeTrackingEnabled = true
		};

		/// <summary>
		/// Constructs a CardDataViewModel
		/// </summary>
		/// <param name="data">The wrapped CardData</param>
		/// <param name="parent">The DataSetViewModel that contains this card</param>
		/// <param name="root">The workspace root path</param>
		public CardDataViewModel(CardData data, DataSetViewModel parent, string root)
		{
			_root = root;
			Data = data;
			Parent = parent;

			foreach (CardField field in data.Fields)
			{
				DataSetViewModel.FieldHeader header = parent.FieldHeaders.FirstOrDefault(desc => desc.Key.Equals(field.Key));
				if (header == null)
					throw new FieldNotFoundException(field.Key);
				Fields.Add(CreateFieldVm(field, header.FieldType));
			}
			Fields.ItemChanged.Subscribe(_ =>
			{
				// this just bubbles up the change so higher-level observers pick up the change
				this.RaisePropertyChanged();
			});
		}

		/// <summary>
		/// Creates a new field, ignores duplicates
		/// </summary>
		/// <param name="fieldName">Name of the field</param>
		/// <param name="fieldType">Data type of the field</param>
		public void AddField(string fieldName, FieldType fieldType)
		{
			int fieldIndex = Data.Fields.FindIndex(pair => pair.Key == fieldName);
			if (fieldIndex != -1)
				return;

			CardField newField = new CardField
			{
				Key = fieldName
			};
			Data.Fields.Add(newField);
			Fields.Add(CreateFieldVm(newField, fieldType));
		}

		/// <summary>
		/// Removes a field by its index
		/// </summary>
		/// <param name="index">Index of the field</param>
		public void RemoveField(int index)
		{
			if (index < 0)
				throw new ArgumentOutOfRangeException(nameof(index));

			Data.Fields.RemoveAt(index);
			Fields.RemoveAt(index);
		}

		private FieldViewModel CreateFieldVm(CardField field, FieldType fieldType)
		{
			switch (fieldType)
			{
				case FieldType.Text:
					return new TextFieldViewModel(field);

				case FieldType.Image:
					return new ImageFieldViewModel(field, _root);

				case FieldType.Color:
					return new ColorFieldViewModel(field);

				default:
					throw new InvalidOperationException("Cannot create ViewModel for unknown field type");
			}
		}
	}
}
