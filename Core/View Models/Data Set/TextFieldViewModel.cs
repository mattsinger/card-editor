﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Core.Documents;

namespace Core.View_Models.Data_Set
{
	/// <summary>
	/// ViewModel for text fields in card data
	/// </summary>
	public class TextFieldViewModel : FieldViewModel
	{
		/// <summary>
		/// Constructs a FieldViewModel
		/// </summary>
		/// <param name="field">The wrapped card field</param>
		public TextFieldViewModel(CardField field) : base(field)
		{
		}

		/// <summary>
		/// The field's text
		/// </summary>
		public string Text
		{
			get => _field.Value;
			set
			{
				if (value == _field.Value)
					return;

				_field.Value = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// The field's value as seen by external systems, useful for import/export
		/// </summary>
		/// <remarks>For TextFieldViewModels, this is just Text</remarks>
		public override string ExternalValue
		{
			get => Text;
			set => Text = value;
		}
	}
}
