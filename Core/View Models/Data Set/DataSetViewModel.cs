﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using Core.Documents;
using Core.Drawing;
using Core.Exceptions;
using Core.Util;
using Core.Util.Exporters;
using Core.View_Models.Card_Preview;
using Core.View_Models.Layout;
using ReactiveUI;
using Splat;
using Math = System.Math;
using ReactiveCommand = ReactiveUI.ReactiveCommand;

namespace Core.View_Models.Data_Set
{
	/// <summary>
	/// ViewModel that wraps a DataSet
	/// </summary>
	public class DataSetViewModel : BaseDocumentViewModel
	{
		#region Private Members

		private readonly DataSet _dataSet;
		private int _selectedFieldIndex = -1;
		private FieldViewModel _selectedField;
		private CardDataViewModel _selectedCard;
		private readonly CardPreviewViewModel _frontPreview;
		private readonly CardPreviewViewModel _backPreview;

		private bool _initializingLayouts;
		private readonly List<IDisposable> _disposables = new List<IDisposable>();

		private LayoutError _frontError = LayoutError.None;
		private LayoutError _backError = LayoutError.None;
		private IDisposable _frontSizeDisposable;
		private IDisposable _backSizeDisposable;

		#endregion

		#region Public Properties

		/// <summary>
		/// The document's type, as a helpful property
		/// </summary>
		public override Type DocType => typeof(DataSet);

		/// <summary>
		/// Helper struct that encompasses the metadata for a card field
		/// </summary>
		public class FieldHeader : ReactiveObject
		{
		    private string _key;
			private FieldType _type;

            /// <summary>
            /// The name of the Field's key
            /// </summary>
		    public string Key
		    {
		        get => _key;
	            set => this.RaiseAndSetIfChanged(ref _key, value);
            }

			/// <summary>
			/// The Field's type
			/// </summary>
			public FieldType FieldType
			{
				get => _type;
				set => this.RaiseAndSetIfChanged(ref _type, value);
			}
		}

		/// <summary>
		/// The field headers for the data set, not including Ids
		/// </summary>
		public ReactiveList<FieldHeader> FieldHeaders { get; } = new ReactiveList<FieldHeader>
		{
			ChangeTrackingEnabled = true
		};

		/// <summary>
		/// The current set of data
		/// </summary>
		public ReactiveList<CardDataViewModel> Cards { get; } = new ReactiveList<CardDataViewModel>();

		/// <summary>
		/// The CardDataViewModel wrapping the currently selected card
		/// </summary>
		public CardDataViewModel SelectedCard
		{
			get => _selectedCard;
			set => this.RaiseAndSetIfChanged(ref _selectedCard, value);
		}

		/// <summary>
		/// The currently selected field index
		/// </summary>
		public int SelectedFieldIndex
		{
			get => _selectedFieldIndex;
			set => this.RaiseAndSetIfChanged(ref _selectedFieldIndex, value);
		}

		/// <summary>
		/// The currently selected field of the SelectedCard
		/// </summary>
		public FieldViewModel SelectedField
		{
			get => _selectedField;
			protected set
			{
				if (_selectedField != null)
					_selectedField.PropertyChanged -= SelectedFieldChanged;

				_selectedField =  value;

				if (_selectedField != null)
					_selectedField.PropertyChanged += SelectedFieldChanged;
			}
		}

		/// <summary>
		/// Error codes for problems that may occur with the front and back layouts
		/// </summary>
		public enum LayoutError
		{
			None,
			NoLayoutSelected,
			LayoutSizeMismatch
		}

		/// <summary>
		/// The LayoutViewModel for the card front layout
		/// </summary>
		public LayoutViewModel FrontLayout
		{
			get => _frontPreview.LayoutViewModel;
			set
			{
				if (_frontPreview.LayoutViewModel == value)
					return;

				if (_frontPreview.LayoutViewModel != null)
					_frontSizeDisposable.Dispose();

				if (!_initializingLayouts &&
					value != null && 
					(!Workspace.Documents.Contains(value) ||
					!IsLayoutValid(value, BackLayout)))
					throw new ArgumentOutOfRangeException(nameof(value), "FrontLayout must be a ValidFrontLayout in the Workspace or null");
				
				_dataSet.FrontLayout = value?.RelativePath;
				_frontPreview.LayoutViewModel = value;
				this.RaisePropertyChanged();

				if (value != null)
					_frontSizeDisposable = _frontPreview.LayoutViewModel
						.WhenAnyValue(layout => layout.FullSize)
						.Subscribe(_ => UpdateLayoutErrors());

				if (!_initializingLayouts)
					IsDirty = true;
			}
		}

		/// <summary>
		/// The preview view model of the card's front
		/// </summary>
		public CardPreviewViewModel FrontPreview => _frontPreview;

		/// <summary>
		/// The error in the FrontLayout
		/// </summary>
		public LayoutError FrontLayoutError
		{
			get => _frontError;
			set => this.RaiseAndSetIfChanged(ref _frontError, value);
		}

		/// <summary>
		/// The LayoutViewModel for the card back layout
		/// </summary>
		public LayoutViewModel BackLayout
		{
			get => _backPreview.LayoutViewModel;
			set
			{
				if (_backPreview.LayoutViewModel == value)
					return;

				if (_backPreview.LayoutViewModel != null)
					_backSizeDisposable.Dispose();

				if (!_initializingLayouts &&
					value != null && 
					(!Workspace.Documents.Contains(value) || 
					!IsLayoutValid(value, BackLayout)))
					throw new ArgumentOutOfRangeException(nameof(value), "BackLayout must be a ValidBackLayout in the Workspace or null");

				_dataSet.BackLayout = value?.RelativePath;
				_backPreview.LayoutViewModel = value;
				this.RaisePropertyChanged();

				if (value != null)
					_backSizeDisposable = _backPreview.LayoutViewModel
						.WhenAnyValue(layout => layout.FullSize)
						.Subscribe(_ => UpdateLayoutErrors());

				if (!_initializingLayouts)
					IsDirty = true;
			}
		}

		/// <summary>
		/// The preview view model of the card's back
		/// </summary>
		public CardPreviewViewModel BackPreview => _backPreview;

		/// <summary>
		/// The error in the BackLayout
		/// </summary>
		public LayoutError BackLayoutError
		{
			get => _backError;
			set => this.RaiseAndSetIfChanged(ref _backError, value);
		}

		/// <summary>
		/// Recently used output parameters
		/// </summary>
		public OutputParams RecentOutputParams { get; set; }

		#endregion

		#region Public Methods

		/// <summary>
		/// Called when the data set is being removed from the workspace
		/// </summary>
		protected override void DoDispose(bool finalize)
		{
			if (_isDisposed)
				return;

			_isDisposed = true;
			Workspace.CardDatas.RemoveAll(Cards);
			_disposables.ForEach(_ => _.Dispose());
			_disposables.Clear();
		}

		#endregion

		#region Commands & Interactions

		/// <summary>
		/// Adds a new card to the data set
		/// </summary>
		public ReactiveCommand AddCard { get; protected set; }

		/// <summary>
		/// Removes the SelectedCard from the data set
		/// </summary>
		public ReactiveCommand RemoveSelectedCard { get; protected set; }

        /// <summary>
        /// Renames the SelectedCard
        /// </summary>
        public ReactiveCommand RenameSelectedCard { get; protected set; }

		/// <summary>
		/// Adds a new field to the data set. Takes in a FieldHeader for the new field.
		/// </summary>
		public ReactiveCommand AddField { get; protected set; }

		/// <summary>
		/// Removes the SelectedField from the data set
		/// </summary>
		public ReactiveCommand RemoveSelectedField { get; protected set; }

        /// <summary>
        /// Renames the SelectedField
        /// </summary>
        public ReactiveCommand RenameSelectedField { get; protected set; }

		/// <summary>
		/// Imports Card Data from an external file
		/// </summary>
		public ReactiveCommand Import { get; protected set; }

		/// <summary>
		/// Exports Card Data to a file
		/// </summary>
		public ReactiveCommand Export { get; protected set; }

		/// <summary>
		/// Selects the Front Layout
		/// </summary>
		public ReactiveCommand SelectFrontLayout { get; protected set; }

		/// <summary>
		/// Clears the Front Layout
		/// </summary>
		public ReactiveCommand ClearFrontLayout { get; protected set; }

		/// <summary>
		/// Outputs the SelectedCard to an image file, using the FrontLayout. Requires OutputParams as input.
		/// </summary>
		public ReactiveCommand OutputCardFrontToFile { get; protected set; }

		/// <summary>
		/// Selects the Back Layout
		/// </summary>
		public ReactiveCommand SelectBackLayout { get; protected set; }

		/// <summary>
		/// Clears the Back Layout
		/// </summary>
		public ReactiveCommand ClearBackLayout { get; protected set; }

		/// <summary>
		/// Outputs the SelectedCard to an image file, using the BackLayout. Requires OutputParams as input.
		/// </summary>
		public ReactiveCommand OutputCardBackToFile { get; protected set; }

		/// <summary>
		/// Outputs all cards to images, stored in one zip file. Requires OutputParams as input.
		/// </summary>
		/// <remarks>
		/// If either FrontLayout or BackLayout does not have any bound Elements, it will only output the layout once.
		/// </remarks>
		public ReactiveCommand BatchOutputCardsToFile { get; protected set; }

		/// <summary>
		/// Forces the preview view models to refresh, because the selected card has updated
		/// </summary>
		public ReactiveCommand RefreshPreviews { get; protected set; }

		/// <summary>
		/// Interaction asking the user for the name of a new card
		/// </summary>
		public Interaction<Unit, string> GetCardId { get; } = new Interaction<Unit, string>();

		/// <summary>
		/// Interaction fired when the user provides a duplicate card Id
		/// </summary>
		public Interaction<Unit, Unit> DuplicateCardId { get; } = new Interaction<Unit, Unit>();

		/// <summary>
		/// Interaction asking the user for the name of a field
		/// </summary>
		public Interaction<Unit, string> GetFieldName { get; } = new Interaction<Unit, string>();

		/// <summary>
		/// Interaction fired when the user provides a duplicate field name
		/// </summary>
		public Interaction<Unit, Unit> DuplicateFieldName { get; } = new Interaction<Unit, Unit>();

		/// <summary>
		/// Interaction fired when the ViewModel is unable to output a card to file
		/// </summary>
		public Interaction<Unit, Unit> FailedToOutputCard { get; } = new Interaction<Unit, Unit>();

		/// <summary>
		/// Orientations required when getting OutputParams
		/// </summary>
		[Flags]
		public enum RequiredOrientations
		{
			None,	// prevents Front flag from being equivalent to an empty mask
			Front,
			Back,
			Both = Front | Back
		};

		/// <summary>
		/// Interaction fired when asking for card output parameters
		/// </summary>
		/// <remarks>
		/// Input is a tuple of the required orientations and the default output parameters to use.
		/// Output is the selected output parameters.
		/// </remarks>
		public Interaction<Tuple<RequiredOrientations, OutputParams>, OutputParams> GetOutputParams { get; } = new Interaction<Tuple<RequiredOrientations, OutputParams>, OutputParams>();

		/// <summary>
		/// Interaction fired when asking for import settings
		/// </summary>
		/// <remarks>
		/// Input is a DataSetImporterViewModel that can read files.
		/// Output is whether the user completed the import set-up (true) or cancelled (false).
		/// </remarks>
		public Interaction<DataSetImporterViewModel, bool> GetImportData { get; } = new Interaction<DataSetImporterViewModel, bool>();

		#endregion

		#region Constructor

		/// <summary>
		/// Constructs a DataSetViewModel
		/// </summary>
		/// <param name="workspace">The parent workspace</param>
		/// <param name="relativePath">The data set's path relative to rootPath</param>
		public DataSetViewModel(WorkspaceViewModel workspace, string relativePath) : base(workspace, relativePath, typeof(DataSet))
		{
			_dataSet = (DataSet) Document;
			_frontPreview = new CardPreviewViewModel(workspace);
			_backPreview = new CardPreviewViewModel(workspace);
			InitializeData();
			InitializeBinding();
			CreateCommands();

			Workspace.Initialized += (sender, args) =>
			{
				InitializeLayouts(args);
			};
		}

		#endregion

		#region Private Methods

		private void InitializeData()
		{
			FieldHeaders.AddRange(_dataSet.FieldDescriptors.Select(pair => new FieldHeader
			{
				Key = pair.Key,
				FieldType = pair.Type
			}));
            _disposables.Add(FieldHeaders.ItemChanged.Subscribe(args =>
            {
	            FieldHeader changedHeader = args.Sender;
	            int headerIndex = FieldHeaders.IndexOf(changedHeader);
	            _dataSet.FieldDescriptors[headerIndex].Key = changedHeader.Key;
                IsDirty = true;
            }));
		    FieldHeaders.CollectionChanged += (sender, args) => IsDirty = true;

            using (Cards.SuppressChangeNotifications())
			{
				Cards.AddRange(_dataSet.Data.Select(data => new CardDataViewModel(data, this, Workspace.RootPath)));
				Workspace.CardDatas.AddRange(Cards);
			}
			Cards.CollectionChanged += (sender, args) =>
			{
				switch (args.Action)
				{
					case NotifyCollectionChangedAction.Add:
						for (int i = 0; i < args.NewItems.Count; i++)
						{
							CardDataViewModel newVm = args.NewItems[i] as CardDataViewModel;
							Workspace.CardDatas.Add(newVm);
							_dataSet.Data.Insert(args.NewStartingIndex + i, newVm.Data);
						}
						break;

					case NotifyCollectionChangedAction.Remove:
						args.OldItems.Cast<CardDataViewModel>().ForEach(oldCard =>
						{
							Workspace.CardDatas.Remove(oldCard);
							_dataSet.Data.Remove(oldCard.Data);
						});
						break;

					case NotifyCollectionChangedAction.Replace:
						args.OldItems.Cast<CardDataViewModel>().ForEach(oldCard =>
						{
							Workspace.CardDatas.Remove(oldCard);
							_dataSet.Data.Remove(oldCard.Data);
						});
						for (int i = 0; i < args.NewItems.Count; i++)
						{
							CardDataViewModel newVm = args.NewItems[i] as CardDataViewModel;
							Workspace.CardDatas.Add(newVm);
							_dataSet.Data.Insert(args.NewStartingIndex + i, newVm.Data);
						}
						break;
				}

				IsDirty = true;
			};

			// if we are waiting on the workspace to initialize, hold off on locating layouts until we are finished
			if (!Workspace.IsInitializing)
			{
				WorkspaceViewModel.InitializationEventArgs args = new WorkspaceViewModel.InitializationEventArgs();
				InitializeLayouts(args);

				if (args.FailedDocuments.FailedDocs.Any())
					throw args.FailedDocuments;
			}

			_disposables.Add(Cards.ItemChanged.Subscribe(_ =>
			{
				IsDirty = true;
			}));
		}

		private void InitializeLayouts(WorkspaceViewModel.InitializationEventArgs initArgs)
		{
			_initializingLayouts = true;

			if (!string.IsNullOrEmpty(_dataSet.FrontLayout))
			{
				FrontLayout = Workspace.Documents.FirstOrDefault(vm =>
					vm.RelativePath == _dataSet.FrontLayout &&
					vm is LayoutViewModel) as LayoutViewModel;

				if (FrontLayout == null)
					initArgs.FailedDocuments.FailedDocs.Add(new FailedOperationException.FailedDocument
					{
						Path = RelativePath,
						Reason = new DocumentNotFoundException(_dataSet.FrontLayout)
					});
			}

			if (!string.IsNullOrEmpty(_dataSet.BackLayout))
			{
				BackLayout = Workspace.Documents.FirstOrDefault(vm =>
					vm.RelativePath == _dataSet.BackLayout &&
					vm is LayoutViewModel) as LayoutViewModel;

				if (BackLayout == null)
					initArgs.FailedDocuments.FailedDocs.Add(new FailedOperationException.FailedDocument
					{
						Path = RelativePath,
						Reason = new DocumentNotFoundException(_dataSet.BackLayout)
					});
			}

			UpdateLayoutErrors();

			_disposables.Add(Workspace.Layouts.Changed.Subscribe(args =>
			{
				switch (args.Action)
				{
					case NotifyCollectionChangedAction.Remove:
					case NotifyCollectionChangedAction.Replace:
						if (args.OldItems.Contains(FrontLayout))
							FrontLayout = null;
						if (args.OldItems.Contains(BackLayout))
							BackLayout = null;
						break;

					case NotifyCollectionChangedAction.Reset:
						if (!Workspace.Layouts.Contains(FrontLayout))
							FrontLayout = null;
						if (!Workspace.Layouts.Contains(BackLayout))
							BackLayout = null;
						break;
				}
			}));

			_initializingLayouts = false;
		}

		private void UpdateLayoutErrors()
		{
			if (FrontLayout != null && BackLayout != null && 
				!IsLayoutValid(FrontLayout, BackLayout) && 
				FrontLayout.CanOpen)	// catching edge case in IsLayoutValid
			{
				FrontLayoutError = LayoutError.LayoutSizeMismatch;
				BackLayoutError = LayoutError.LayoutSizeMismatch;
				return;
			}
			
			FrontLayoutError = FrontLayout == null ? LayoutError.NoLayoutSelected : LayoutError.None;
			BackLayoutError = BackLayout == null ? LayoutError.NoLayoutSelected : LayoutError.None;
		}

		private void InitializeBinding()
		{
			_disposables.Add(this.WhenAnyValue(vm => vm.SelectedCard)
				.Subscribe(card =>
				{
					FrontPreview.CardDataViewModel = card;
					BackPreview.CardDataViewModel = card;
				}));

			_disposables.Add(this.WhenAnyValue(vm => vm.SelectedCard, vm => vm.SelectedFieldIndex)
				.Subscribe(cardIndexTuple =>
				{
					if (cardIndexTuple.Item1 != null &&
						cardIndexTuple.Item2 >= 0 &&
						cardIndexTuple.Item2 < cardIndexTuple.Item1.Fields.Count)
					{
						SelectedField = cardIndexTuple.Item1.Fields[cardIndexTuple.Item2];
					}
					else
					{
						SelectedField = null;
					}
				}));
		}

		private void DoAddCard()
		{
			GetCardId.Handle(new Unit()).Subscribe(cardId =>
			{
				if (string.IsNullOrEmpty(cardId))
					return;

				if (Workspace.CardDatas.Any(card => card.Id == cardId))
				{
					DuplicateCardId.Handle(new Unit()).Subscribe();
					return;
				}

				// create new empty card data, create viewmodel to wrap it, then add all fields
				CardData newData = new CardData
				{
					Id = cardId
				};
				CardDataViewModel newVm = new CardDataViewModel(newData, this, Workspace.RootPath);
				foreach (FieldHeader header in FieldHeaders)
				{
					newVm.AddField(header.Key, header.FieldType);
				}

				Cards.Add(newVm);

				SelectedCard = newVm;
			});
		}

		private void DoRemoveSelectedCard()
		{
			int index = Cards.IndexOf(SelectedCard);
			_dataSet.Data.Remove(SelectedCard.Data);
			Cards.Remove(SelectedCard);
			Workspace.CardDatas.Remove(SelectedCard);

			SelectedCard = Cards.Count > 0 ? Cards[Math.Min(index, Cards.Count - 1)] : null;
		}

	    private void DoRenameSelectedCard()
	    {
            GetCardId.Handle(new Unit()).Subscribe(cardId =>
            {
                if (string.IsNullOrEmpty(cardId))
                    return;

                if (Workspace.CardDatas.Any(card => card.Id == cardId && card != SelectedCard))
                {
                    DuplicateCardId.Handle(new Unit()).Subscribe();
                    return;
                }
                
                SelectedCard.Id = cardId;
            });
        }

		private void DoAddField(FieldType type)
		{
			GetFieldName.Handle(new Unit()).Subscribe(key =>
			{
				if (string.IsNullOrEmpty(key))
					return;

				if (FieldHeaders.Any(h => h.Key.Equals(key)))
				{
					DuplicateFieldName.Handle(new Unit()).Subscribe();
					return;
				}

				AddNewField(new FieldHeader
				{
					Key = key,
					FieldType = type
				});
			});
		}

		private void AddNewField(FieldHeader newHeader)
		{
			_dataSet.FieldDescriptors.Add(new DataSet.FieldDescriptor
			{
				Key = newHeader.Key,
				Type = newHeader.FieldType
			});
			foreach (CardDataViewModel cardDataVm in Cards)
			{
				cardDataVm.AddField(newHeader.Key, newHeader.FieldType);
			}
			FieldHeaders.Add(newHeader);
		}

		private void DoRemoveSelectedField()
		{
			int indexToRemove = SelectedFieldIndex;
			FieldHeaders.RemoveAt(indexToRemove);
			_dataSet.FieldDescriptors.RemoveAt(indexToRemove);
			foreach (CardDataViewModel cardDataVm in Cards)
			{
				cardDataVm.RemoveField(indexToRemove);
			}
			SelectedFieldIndex = -1;
		}

	    private void DoRenameSelectedField()
	    {
            GetFieldName.Handle(new Unit()).Subscribe(newKey =>
            {
                if (string.IsNullOrEmpty(newKey))
                    return;

                FieldHeader curHeader = FieldHeaders[SelectedFieldIndex];
                FieldHeader namedHeader = FieldHeaders.FirstOrDefault(h => h.Key.Equals(newKey));
                if (namedHeader != null && namedHeader != curHeader)
                {
                    DuplicateFieldName.Handle(new Unit()).Subscribe();
                    return;
                }

                foreach (CardDataViewModel card in Cards)
                    card.Fields[SelectedFieldIndex].FieldName = newKey;
                curHeader.Key = newKey;
            });
        }

		private void DoImport()
		{
			DataSetImporterViewModel importerVm = new DataSetImporterViewModel(Workspace.RootPath);
			GetImportData
				.Handle(importerVm)
				.Subscribe(accepted =>
				{
					if (!accepted)
					{
						importerVm.Dispose();
						return;
					}

					using (FieldHeaders.SuppressChangeNotifications())
					{
						using (Cards.SuppressChangeNotifications())
						{
							if (importerVm.OverwriteExistingData)
							{
								// need to do this manually because we are suppressing change notifications
								// in theory, we could do this on the Reset call at the bottom, but that would require us to
								// also track the old cards since the event doesn't include them
								Workspace.CardDatas.RemoveAll(Cards);   
								_dataSet.Data.Clear();
								_dataSet.FieldDescriptors.Clear();

								Cards.Clear();
								FieldHeaders.Clear();
							}

							// create new FieldHeader for each imported field that has a unique key and is not the Id column
							importerVm.Headers
								.Where(header => FieldHeaders.All(field => field.Key != header.Key) && !header.IsId && header.Include)
								.ForEach(header =>
								{
									AddNewField(new FieldHeader
									{
										Key = header.Key,
										FieldType = header.FieldType
									});
								});

							List<CardData> importedCards = importerVm.Importer.LoadCards(
								importerVm.FilePath,
								importerVm.Subsection,
								importerVm.UseFirstRowHeaders,
								importerVm.Headers.ToList());		// use original headers taken from file, not our existing list

							importedCards.ForEach(importedCard =>
							{
								if (Workspace.CardDatas.Any(card => card.Id == importedCard.Id))
								{
									return;
								}

								// create card data to match import
								CardData newData = new CardData
								{
									Id = importedCard.Id
								};
								// create view model and add fields
								CardDataViewModel newVm = new CardDataViewModel(newData, this, Workspace.RootPath);
								foreach (FieldHeader header in FieldHeaders)
								{
									newVm.AddField(header.Key, header.FieldType);
								}

								// copy fields from imported card into new data
								foreach (CardField importField in importedCard.Fields)
								{
									FieldViewModel fieldVm = newVm.Fields.FirstOrDefault(field => field.FieldName == importField.Key);
									if (fieldVm != null)
									{
										fieldVm.ExternalValue = importField.Value;
									}
								}

								_dataSet.Data.Add(newData);
								Cards.Add(newVm);
								Workspace.CardDatas.Add(newVm); // need to do this manually because we are suppressing change notifications
							});
						}
					}
					FieldHeaders.Reset();
					Cards.Reset();

					importerVm.Dispose();

					IsDirty = true;
				});
		}

		private void DoExport()
		{
			//throw new NotImplementedException();
			string filePath;
			IFileSystem fileSystem = Locator.Current.GetService<IFileSystem>();
			if (!fileSystem.GetDataSetExportFile(Workspace.RootPath, out filePath))
				return;

			// should we thrown an exception on a null exporter?
			IDataSetExporter exporter = DataSetExportHelpers.GetImporterForExtension(Path.GetExtension(filePath));
			exporter?.WriteCards(filePath, _dataSet);
		}

		private void DoSelectFrontLayout()
		{
			IList<BaseDocumentViewModel> validLayouts = Workspace.Documents
				.Where(doc => IsLayoutValid(doc as LayoutViewModel, BackLayout))
				.ToList();

			IFileSystem fileSystem = Locator.Current.GetService<IFileSystem>();
			LayoutViewModel newFront = fileSystem?.SelectDocument(validLayouts, "Select the Front Layout") as LayoutViewModel;
			if (newFront != null)
				FrontLayout = newFront;

			UpdateLayoutErrors();
		}

		private void DoSelectBackLayout()
		{
			IList<BaseDocumentViewModel> validLayouts = Workspace.Documents
				.Where(doc => IsLayoutValid(doc as LayoutViewModel, FrontLayout))
				.ToList();

			IFileSystem fileSystem = Locator.Current.GetService<IFileSystem>();
			LayoutViewModel newBack = fileSystem?.SelectDocument(validLayouts, "Select the Back Layout") as LayoutViewModel;
			if (newBack != null)
				BackLayout = newBack;

			UpdateLayoutErrors();
		}

		private void DoOutputCard(CardDataViewModel card, LayoutViewModel layout, bool useFrontOrient, string suffix)
		{
			GetOutputParams
				.Handle(
					new Tuple<RequiredOrientations, OutputParams>(
						useFrontOrient ? RequiredOrientations.Front : RequiredOrientations.Back,
						RecentOutputParams))
				.Subscribe(outputParams =>
				{
					if (outputParams == null)
						return;

					RecentOutputParams = outputParams;

					IFileSystem fileSystem = Locator.Current.GetService<IFileSystem>();
					string startFile = card != null ? $"{card.Id}{suffix}" : $"{DisplayName}{suffix}";
					string imgPath;
					if (!fileSystem.GetOutputImage(Workspace.RootPath, startFile, outputParams.Format, out imgPath))
						return;

					ICardRenderer renderer = Locator.Current.GetService<ICardRenderer>();
					IBitmap renderedCard = renderer.RenderCard(card, layout, outputParams, useFrontOrient);

					if (renderedCard == null)
					{
						FailedToOutputCard.Handle(new Unit()).Subscribe();
						return;
					}

					fileSystem.SaveImageToFile(imgPath, outputParams.Format, renderedCard);
				});
		}

		private void DoBatchOutputCards()
		{
			GetOutputParams
				.Handle(
					new Tuple<RequiredOrientations, OutputParams>(
						RequiredOrientations.Front | RequiredOrientations.Back,
						RecentOutputParams))
				.Subscribe(outputParams =>
				{
					if (outputParams == null)
						return;

					RecentOutputParams = outputParams;

					IFileSystem fileSystem = Locator.Current.GetService<IFileSystem>();
					string outputPath;
					if (!fileSystem.GetDirectory(Workspace.RootPath, out outputPath))
						return;

					BatchOutputLayout(outputPath, FrontLayout, outputParams, true, "_front");
					BatchOutputLayout(outputPath, BackLayout, outputParams, false, "_back");
				});
		}

		private void BatchOutputLayout(string folderPath, LayoutViewModel layout, OutputParams outputParams, bool useFrontOrient, string suffix)
		{
			ICardRenderer renderer = Locator.Current.GetService<ICardRenderer>();
			IFileSystem fileSystem = Locator.Current.GetService<IFileSystem>();
			IBitmap renderedCard;

			// if layout has no bound elements, only output once. Use the data set name rather than card id
			if (!layout.HasBindings)
			{
				string imgPath = $"{folderPath}\\{DisplayName}{suffix}{outputParams.Format.GetExtension()}";
				renderedCard = renderer.RenderCard(null, layout, outputParams, useFrontOrient);
				if (renderedCard == null)
					FailedToOutputCard.Handle(new Unit()).Subscribe();

				fileSystem.SaveImageToFile(imgPath, outputParams.Format, renderedCard);
			}
			else
			{
				foreach (CardDataViewModel card in Cards)
				{
					string imgPath = $"{folderPath}\\{card.Id}{suffix}{outputParams.Format.GetExtension()}";
					renderedCard = renderer.RenderCard(card, layout, outputParams, useFrontOrient);
					if (renderedCard == null)
						FailedToOutputCard.Handle(new Unit()).Subscribe();

					fileSystem.SaveImageToFile(imgPath, outputParams.Format, renderedCard);
				}
			}
		}

		private void DoRefreshPreviews()
		{
			FrontPreview.Refresh();
			BackPreview.Refresh();
		}

		private void CreateCommands()
		{
			AddCard = ReactiveCommand.Create(DoAddCard);
			RemoveSelectedCard = ReactiveCommand.Create(DoRemoveSelectedCard, 
				this.WhenAnyValue(vm => vm.SelectedCard).Select(card => card != null));
            RenameSelectedCard = ReactiveCommand.Create(DoRenameSelectedCard,
                this.WhenAnyValue(vm => vm.SelectedCard).Select(card => card != null));
            AddField = ReactiveCommand.Create<FieldType>(DoAddField);
			RemoveSelectedField = ReactiveCommand.Create(DoRemoveSelectedField,
				this.WhenAnyValue(vm => vm.SelectedFieldIndex).Select(index => index >= 0));
            RenameSelectedField = ReactiveCommand.Create(DoRenameSelectedField,
                this.WhenAnyValue(vm => vm.SelectedFieldIndex).Select(index => index >= 0));

			Import = ReactiveCommand.Create(DoImport);
			Export = ReactiveCommand.Create(DoExport);

            SelectFrontLayout = ReactiveCommand.Create(DoSelectFrontLayout);
			ClearFrontLayout = ReactiveCommand.Create(() =>
				{
					FrontLayout = null;
					UpdateLayoutErrors();
				}, 
				this.WhenAnyValue(vm => vm.FrontLayout).Select(layout => layout != null));
			OutputCardFrontToFile = ReactiveCommand.Create(
				() => DoOutputCard(SelectedCard, FrontLayout, true, "_front"),
				this.WhenAnyValue(vm => vm.FrontLayout, vm => vm.SelectedCard)
					.Select(tuple => 
						tuple.Item1 != null && 
							(!tuple.Item1.HasBindings || tuple.Item2 != null)));	// command is viable when there is a layout and it is unbound or we have selected a card
			SelectBackLayout = ReactiveCommand.Create(DoSelectBackLayout);
			ClearBackLayout = ReactiveCommand.Create(() =>
				{
					BackLayout = null;
					UpdateLayoutErrors();
				},
				this.WhenAnyValue(vm => vm.BackLayout).Select(layout => layout != null));
			OutputCardBackToFile = ReactiveCommand.Create(
				() => DoOutputCard(SelectedCard, BackLayout, false, "_back"),
				this.WhenAnyValue(vm => vm.BackLayout, vm => vm.SelectedCard)
					.Select(tuple =>
						tuple.Item1 != null &&
							(!tuple.Item1.HasBindings || tuple.Item2 != null)));    // command is viable when there is a layout and it is unbound or we have selected a card

			BatchOutputCardsToFile = ReactiveCommand.Create(
				DoBatchOutputCards,
				this.WhenAnyValue(vm => vm.FrontLayoutError, vm => vm.BackLayoutError)
					.Select(tuple => tuple.Item1 == LayoutError.None && tuple.Item2 == LayoutError.None));

			RefreshPreviews = ReactiveCommand.Create(DoRefreshPreviews);
		}

		private bool IsLayoutValid(LayoutViewModel layout, LayoutViewModel comparitor)
		{
			if (layout == null || !layout.CanOpen)
				return false;

			if (comparitor != null && layout.Size != comparitor.Size)
				return false;

			if (comparitor != null && layout.Bleed != comparitor.Bleed)
				return false;

			return true;
		}

		private void SelectedFieldChanged(object sender, PropertyChangedEventArgs e)
		{
			FrontPreview?.Refresh();
			BackPreview?.Refresh();

			IsDirty = true;
		}

		#endregion
	}
}
