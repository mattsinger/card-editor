﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Core.Annotations;
using Core.Documents;

namespace Core.View_Models.Data_Set
{
	/// <summary>
	/// Base wrapper for an individual field. Notice that this is NOT a ReactiveObject, and not really a 
	/// ViewModel per se. It was previously, but this turns out to be prohibitively expensive when you 
	/// have a large number of cards and fields. Instead, the parent DataSetViewModel should be told to
	/// refresh the currently selected card.
	/// </summary>
	public abstract class FieldViewModel : INotifyPropertyChanged
	{
		protected readonly CardField _field;

	    /// <summary>
	    /// The name of the wrapped field
	    /// </summary>
	    public string FieldName
	    {
	        get => _field.Key;
		    set
	        {
	            if (_field.Key == value)
	                return;

	            _field.Key = value;
	        }
	    }

		/// <summary>
		/// The field's value as seen by external systems, useful for import/export
		/// </summary>
		public abstract string ExternalValue { get; set; }

		/// <summary>
		/// Constructs a FieldViewModel
		/// </summary>
		/// <param name="field">The wrapped card field</param>
		/// <exception cref="ArgumentNullException">Thrown if field or valueChanged is null</exception>
		/// <exception cref="ArgumentException">Thrown if the field does not have a valid name</exception>
		protected FieldViewModel(CardField field)
		{
			if (field == null)
				throw new ArgumentNullException(nameof(field));

			if (string.IsNullOrEmpty(field.Key))
				throw new ArgumentException("Field must have a valid Key");

			_field = field;
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
