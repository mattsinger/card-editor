﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using Core.Documents;
using Core.Drawing;

namespace Core.View_Models.Data_Set
{
	/// <summary>
	/// ViewModel for Color fields in card data
	/// </summary>
	public class ColorFieldViewModel : FieldViewModel
	{
		/// <summary>
		/// Default color for ColorFieldViewModels (Transparent)
		/// </summary>
		public static Color DefaultColor = Color.Transparent;

		private Color _color;

		/// <summary>
		/// Constructs a FieldViewModel
		/// </summary>
		/// <param name="field">The wrapped card field</param>
		/// <exception cref="ArgumentNullException">Thrown if field is null</exception>
		/// <exception cref="ArgumentException">Thrown if the field does not have a valid name</exception>
		public ColorFieldViewModel(CardField field) : base(field)
		{
			_color = Color.Parse(field.Value) ?? DefaultColor;
		}

		/// <summary>
		/// The field's color
		/// </summary>
		public Color Color
		{
			get => _color;
			set
			{
				if (value == _color)
					return;

				_color = value;
				_field.Value = value.ToString();
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// The field's value as seen by external systems, useful for import/export
		/// </summary>
		/// <remarks>For ColorFieldViewModel, this is the string representation of the Color</remarks>
		public override string ExternalValue
		{
			get => Color.ToString();
			set => Color = Color.Parse(value) ?? DefaultColor;
		}
	}
}
