﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Advanced;
using SixLabors.ImageSharp.PixelFormats;

namespace CardEditor.Converters
{
	/// <summary>
	/// Converts a ImageSharp.Image to something usable by WPF's Image
	/// </summary>
	public class ImageSharpImageConverter : IValueConverter
	{
		/// <summary>
		/// Converts a SixLabors.Image to a WPF-usable image
		/// </summary>
		/// <param name="value">The value produced by the binding source.</param>
		/// <param name="targetType">The type of the binding target property.</param>
		/// <param name="parameter">The converter parameter to use.</param>
		/// <param name="culture">The culture to use in the converter.</param>
		/// <returns>A WPF-usable image</returns>
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			Image<Bgra32> slImage = value as Image<Bgra32>;
			if (slImage == null)
				return null;
			
			unsafe
			{
				fixed (void* p = &slImage.GetPixelSpan()[0])
				{
					int length = slImage.GetPixelSpan().Length * (slImage.PixelType.BitsPerPixel/8);
					int stride = length / slImage.Height;
					IntPtr ptr = new IntPtr(p);
					BitmapSource source = BitmapSource.Create(slImage.Width, slImage.Height, 
						96, 96, PixelFormats.Bgra32, null, ptr,
						length, stride);

					return source;
				}
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
