﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Globalization;
using System.Windows.Data;
using Core.View_Models.Deck_Print_Set;

namespace CardEditor.Converters
{
	/// <summary>
	/// Converter that converts a CardCountViewModel to its appropriate grouping header text
	/// </summary>
    public class CardCountGroupingConverter : IValueConverter
    {
		/// <summary>
		/// Converts a CardCountViewModel to the appropriate grouping header text
		/// </summary>
		/// <param name="value">The CardCountViewModel to convert</param>
		/// <param name="targetType">String</param>
		/// <param name="parameter">unused</param>
		/// <param name="culture">unused</param>
		/// <returns>A string matching the text for grouping the CardCountViewModel</returns>
	    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			CardCountViewModel ccvm = value as CardCountViewModel;

			return ccvm?.DataSetName;
		}

		/// <summary>
		/// Unused function
		/// </summary>
		/// <param name="value">unused</param>
		/// <param name="targetType">unused</param>
		/// <param name="parameter">unused</param>
		/// <param name="culture">unused</param>
		/// <returns>unused</returns>
	    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
	    {
		    throw new NotImplementedException();
	    }
    }
}
