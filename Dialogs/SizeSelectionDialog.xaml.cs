﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Windows;
using System.Windows.Controls;
using Core.Util;
using Size = Core.Drawing.Size;

namespace CardEditor.Dialogs
{
	/// <summary>
	/// Interaction logic for SizeSelectionWindow.xaml
	/// </summary>
	public partial class SizeSelectionDialog : Window
	{
		#region Static Members

		public static readonly Size MicroSize			= new Size(1.25f,	1.75f);
		public static readonly Size MiniSize			= new Size(1.75f,	2.5f);
		public static readonly Size BusinessSize		= new Size(2.0f,	3.0f);
		public static readonly Size BridgeSize			= new Size(2.25f,	3.5f);
		public static readonly Size PokerSize			= new Size(2.5f,	3.5f);
		public static readonly Size TarotSize			= new Size(2.75f,	4.75f);
		public static readonly Size JumboSize			= new Size(3.5f,	5.5f);

		public static readonly Size NoBleedSize			= new Size(0,		0);
		public static readonly Size EightInchBleedSize	= new Size(0.125f,	0.125f);

		public static readonly Size NoSafeZone			= new Size(0, 0);
		public static readonly Size EightInchSafeZone	= new Size(0.125f,	0.125f);
		public static readonly Size QuarterInchSafeZone = new Size(0.25f,	0.25f);

		#endregion

		#region Private Members

		private Size? _initSize;
		private Size? _initBleed;

		private Size? _selectedSize;
		private Size? _selectedBleed;

		private Size _selectedSafeZone = NoSafeZone;

		#endregion

		#region Public Properties

		/// <summary>
		/// The selected size
		/// </summary>
		public Size? Size { get; protected set; }

		/// <summary>
		/// The selected bleed size
		/// </summary>
		public Size? Bleed { get; protected set; }

		/// <summary>
		/// The selected safe zone
		/// </summary>
		public Size SafeZone
		{
			get { return _selectedSafeZone; }
			set
			{
				if (value.Width < 0 || value.Height < 0)
					throw new ArgumentOutOfRangeException(nameof(value));

				_selectedSafeZone = value;
				InitSafeZone();
			}
		}

		/// <summary>
		/// The initial selected card size
		/// </summary>
		public Size? InitialSize
		{
			get { return _initSize; }
			set
			{
				_initSize = value;
				InitCardSize();
			}
		}

		/// <summary>
		/// The initial selected card bleed
		/// </summary>
		public Size? InitialBleed
		{
			get { return _initBleed; }
			set
			{
				_initBleed = value;
				InitBleedSize();
			}
		}

		#endregion

		/// <summary>
		/// Constructs a SizeSelectionWindow
		/// </summary>
		public SizeSelectionDialog()
		{
			InitializeComponent();
		}

		#region WPF Handlers

		private void Micro_OnChecked(object sender, RoutedEventArgs e)
		{
			SetSize(MicroSize);
		}

		private void Mini_OnChecked(object sender, RoutedEventArgs e)
		{
			SetSize(MiniSize);
		}

		private void Poker_OnChecked(object sender, RoutedEventArgs e)
		{
			SetSize(PokerSize);
		}

		private void BusinessCard_OnChecked(object sender, RoutedEventArgs e)
		{
			SetSize(BusinessSize);
		}

		private void Bridge_OnChecked(object sender, RoutedEventArgs e)
		{
			SetSize(BridgeSize);
		}

		private void Tarot_OnChecked(object sender, RoutedEventArgs e)
		{
			SetSize(TarotSize);
		}

		private void Jumbo_OnChecked(object sender, RoutedEventArgs e)
		{
			SetSize(JumboSize);
		}

		private void CustomSize_OnChecked(object sender, RoutedEventArgs e)
		{
			SetSize(CustomSizeWidth.Text.GetFloat(), CustomSizeHeight.Text.GetFloat());
		}

		private void Landscape_OnChecked(object sender, RoutedEventArgs e)
		{
			if (Size.HasValue)
				SetSize(Size.Value);
		}

		private void NoBleed_OnChecked(object sender, RoutedEventArgs e)
		{
			SetBleed(NoBleedSize);
		}

		private void OneEigthBleed_OnChecked(object sender, RoutedEventArgs e)
		{
			SetBleed(EightInchBleedSize);
		}

		private void CustomBleed_OnChecked(object sender, RoutedEventArgs e)
		{
			SetBleed(CustomBleedWidth.Text.GetFloat(), CustomBleedHeight.Text.GetFloat());
		}

		private void OkButton_OnClick(object sender, RoutedEventArgs e)
		{
			if (_selectedSize == null || _selectedBleed == null)
				throw new NullReferenceException();

			bool useInverse = Landscape.IsChecked.HasValue && Landscape.IsChecked.Value;

			Size = useInverse ? _selectedSize.Value.Inverse() : _selectedSize;
			Bleed = _selectedBleed;

			DialogResult = true;
		}

		private void CustomSize_OnTextChanged(object sender, TextChangedEventArgs e)
		{
			SetSize(CustomSizeWidth.Text.GetFloat(), CustomSizeHeight.Text.GetFloat());
		}

		private void CustomBleed_OnTextChanged(object sender, TextChangedEventArgs e)
		{
			SetBleed(CustomBleedWidth.Text.GetFloat(), CustomBleedHeight.Text.GetFloat());
		}

		private void NoSz_OnChecked(object sender, RoutedEventArgs e)
		{
			SetSafeZone(NoSafeZone);
		}

		private void OneEighthSz_OnChecked(object sender, RoutedEventArgs e)
		{
			SetSafeZone(EightInchSafeZone);
		}

		private void OneQuarterSz_OnChecked(object sender, RoutedEventArgs e)
		{
			SetSafeZone(QuarterInchSafeZone);
		}

		private void CustomSafeZone_OnChecked(object sender, RoutedEventArgs e)
		{
			SetSafeZone(CustomSzWidth.Text.GetFloat(), CustomSzHeight.Text.GetFloat());
		}

		private void CustomSafeZone_OnTextChanged(object sender, TextChangedEventArgs e)
		{
			SetSafeZone(CustomSzWidth.Text.GetFloat(), CustomSzHeight.Text.GetFloat());
		}

		#endregion

		#region Private Methods

		private void InitCardSize()
		{
			if (!InitialSize.HasValue)
			{
				Mini.IsChecked = false;
				Poker.IsChecked = false;
				BusinessCard.IsChecked = false;
				Tarot.IsChecked = false;
				Jumbo.IsChecked = false;
				CustomSize.IsChecked = false;
				CustomSizeWidth.Text = string.Empty;
				CustomSizeHeight.Text = string.Empty;

				Landscape.IsChecked = false;

				return;
			}

			if (MatchInitialSize(InitialSize.Value, MicroSize, Micro)) { }
			else if (MatchInitialSize(InitialSize.Value, MiniSize, Mini)) { }
			else if (MatchInitialSize(InitialSize.Value, PokerSize, Poker)) { }
			else if (MatchInitialSize(InitialSize.Value, BusinessSize, BusinessCard)) { }
			else if (MatchInitialSize(InitialSize.Value, BridgeSize, Bridge)) { }
			else if (MatchInitialSize(InitialSize.Value, TarotSize, Tarot)) { }
			else if (MatchInitialSize(InitialSize.Value, JumboSize, Jumbo)) { }
			else
			{
				if (InitialSize.Value.Width > InitialSize.Value.Height)
				{
					InitialSize.Value.Inverse();
					Landscape.IsChecked = true;
				}
				else
					Landscape.IsChecked = false;

				CustomSize.IsChecked = true;
				CustomSizeWidth.Text = $"{InitialSize.Value.Width:F3}";
				CustomSizeHeight.Text = $"{InitialSize.Value.Height:F3}";
			}
		}

		private bool MatchInitialSize(Size initialSize, Size compareSize, RadioButton button)
		{
			if (initialSize == compareSize)
			{
				button.IsChecked = true;
				return true;
			}

			if (initialSize.Inverse() == compareSize)
			{
				button.IsChecked = true;
				Landscape.IsChecked = true;
				return true;
			}

			button.IsChecked = false;
			return false;
		}

		private void InitBleedSize()
		{
			if (!InitialBleed.HasValue)
			{
				NoBleed.IsChecked = false;
				EighthInchBleed.IsChecked = false;
				CustomBleed.IsChecked = false;
				CustomBleedWidth.Text = string.Empty;
				CustomBleedHeight.Text = string.Empty;

				return;
			}

			if (InitialBleed.Value == NoBleedSize)
				NoBleed.IsChecked = true;
			else if (InitialBleed.Value == EightInchBleedSize)
				EighthInchBleed.IsChecked = true;
			else
			{
				CustomBleed.IsChecked = true;
				CustomBleedWidth.Text = $"{InitialBleed.Value.Width:F3}";
				CustomBleedHeight.Text = $"{InitialBleed.Value.Height:F3}";
			}
		}

		private void InitSafeZone()
		{
			if (_selectedSafeZone == NoSafeZone)
				NoSz.IsChecked = true;
			else if (_selectedSafeZone == EightInchSafeZone)
				EighthInchSz.IsChecked = true;
			else if (_selectedSafeZone == QuarterInchSafeZone)
				QuarterInchSz.IsChecked = true;
			else
			{
				CustomSafeZone.IsChecked = true;
				CustomSzWidth.Text = $"{_selectedSafeZone.Width:F3}";
				CustomSzHeight.Text = $"{_selectedSafeZone.Height:F3}";
			}
		}

		private Size? ParseSize(float? width, float? height)
		{
			if (width.HasValue && height.HasValue)
				return new Size(width.Value, height.Value);

			return null;
		}

		private void SetSize(Size size)
		{
			_selectedSize = size;
			UpdateOkButton();
		}

		private void SetSize(float? width, float? height)
		{
			_selectedSize = ParseSize(width, height);

			UpdateOkButton();
		}

		private void SetBleed(Size bleed)
		{
			_selectedBleed = bleed;
			UpdateOkButton();
		}

		private void SetBleed(float? width, float? height)
		{
			_selectedBleed = ParseSize(width, height);

			UpdateOkButton();
		}

		private void SetSafeZone(Size sz)
		{
			SafeZone = sz;
		}

		private void SetSafeZone(float? width, float? height)
		{
			Size? szSize = ParseSize(CustomSzWidth.Text.GetFloat(), CustomSzHeight.Text.GetFloat());
			if (szSize.HasValue)
				SafeZone = szSize.Value;
		}

		private void UpdateOkButton()
		{
			OkButton.IsEnabled = _selectedSize.HasValue && _selectedBleed.HasValue;
		}

		#endregion
	}
}
