﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using Core.Drawing;
using Core.Util;
using Core.View_Models.Data_Set;
using Splat;
using Image = Core.Drawing.Image;

namespace CardEditor.Dialogs
{
	/// <summary>
	/// Interaction logic for OutputParamsDialog.xaml
	/// </summary>
	public partial class OutputParamsDialog : Window, IEnableLogger
	{
		private DataSetViewModel.RequiredOrientations _requiredOrientations = DataSetViewModel.RequiredOrientations.None;
		private OutputParams _outputParams;

		/// <summary>
		/// The selected output parameters. Setting this will update the appropriate controls to match.
		/// </summary>
		/// <exception cref="ArgumentNullException">Thrown if set to a null value</exception>
		public OutputParams OutputParams
		{
			get { return _outputParams; }
			set
			{
				if (value == null)
					throw new ArgumentNullException(nameof(value));

				_outputParams = value;
				if (IsLoaded)
					UpdateControls();
			}
		}

		/// <summary>
		/// Whether the back orientation is enabled
		/// </summary>
		public DataSetViewModel.RequiredOrientations RequiredOrientations
		{
			get { return _requiredOrientations; }
			set
			{
				_requiredOrientations = value;
				FlipBackLayout.IsEnabled = value == DataSetViewModel.RequiredOrientations.Both;
				FlipBackLayout.Visibility = value == DataSetViewModel.RequiredOrientations.Both ? Visibility.Visible : Visibility.Collapsed;
			}
		}

		/// <summary>
		/// The root path to start searching for ICC files
		/// </summary>
		public string StartPath { get; set; }

		/// <summary>
		/// Constructs an OutputParamsDialog
		/// </summary>
		/// <param name="initialParams">The initial parameters to use</param>
		/// <exception cref="ArgumentNullException">Thrown if initialParams is null</exception>
		public OutputParamsDialog(OutputParams initialParams)
		{
			if (initialParams == null)
				throw new ArgumentNullException(nameof(initialParams));

			InitializeComponent();

			_outputParams = initialParams;
		}

		#region WPF Handlers

		private void ScreenRez_CheckedHandler(object sender, RoutedEventArgs e)
		{
			if (OutputParams == null)
				return;

			OutputParams.Ppi = OutputParams.ScreenResolution;
			if (OkButton != null)
				OkButton.IsEnabled = true;
		}

		private void PrintRez_CheckedHandler(object sender, RoutedEventArgs e)
		{
			if (OutputParams == null)
				return;

			OutputParams.Ppi = OutputParams.PrintResolution;
			if (OkButton != null)
				OkButton.IsEnabled = true;
		}

		private void CustomRez_CheckedHandler(object sender, RoutedEventArgs e)
		{
			if (OutputParams == null)
				return;

			SetPpi(CustomRezValue.Text.GetFloat());
		}

		private void CustomRezValue_TextChangedHandler(object sender, TextChangedEventArgs args)
		{
			if (OutputParams == null)
				return;

			SetPpi(CustomRezValue.Text.GetFloat());
		}

		private void OrientationChangedHandler(object sender, RoutedEventArgs e)
		{
			if (OutputParams == null)
				return;

			UpdateOrientations();
		}

		private void OkButton_ClickHandler(object sender, RoutedEventArgs e)
		{
			DialogResult = true;
		}

		private void IncludeBleed_ClickHandler(object sender, RoutedEventArgs e)
		{
			if (OutputParams == null)
				return;

			OutputParams.IncludeBleed = IncludeBleed.IsChecked.IsValidAndTrue();
		}

		private void LoadedHandler(object sender, RoutedEventArgs e)
		{
			Image.OutputImageFormats.ForEach(format =>
			{
				ComboBoxItem newItem = new ComboBoxItem
				{
					Tag = format,
					Content = Enum.GetName(typeof(Image.Format), format)
				};
				FormatComboBox.Items.Add(newItem);
			});
			
			UpdateControls();
		}

		private void FormatComboBox_SelectionChangedHandler(object sender, SelectionChangedEventArgs e)
		{
			if (OutputParams == null)
				return;

			ComboBoxItem selectedItem = FormatComboBox.SelectedItem as ComboBoxItem;
			OutputParams.Format = (Image.Format)selectedItem.Tag;
		}

		#endregion

		#region Private Methods

		private void SetPpi(float? ppi)
		{
			if (ppi.HasValue)
			{
				OkButton.IsEnabled = true;
				OutputParams.Ppi = ppi.Value;
			}
			else
			{
				OkButton.IsEnabled = false;
			}
		}

		private void UpdateOrientations()
		{
			if (_requiredOrientations == DataSetViewModel.RequiredOrientations.Back)
				UpdateBaseOrientation(ref OutputParams.BackOrientation);
			else
			{
				UpdateBaseOrientation(ref OutputParams.FrontOrientation);
				UpdateBackOrientation();
			}
		}

		private void UpdateBaseOrientation(ref OutputParams.Orientation orientation)
		{
			if (Orient0.IsChecked.IsValidAndTrue())
			{
				orientation = OutputParams.Orientation.Zero;
			}
			else if (Orient90.IsChecked.IsValidAndTrue())
			{
				orientation = OutputParams.Orientation.Ninety;
			}
			else if (Orient180.IsChecked.IsValidAndTrue())
			{
				orientation = OutputParams.Orientation.OneEighty;
			}
			else if (Orient270.IsChecked.IsValidAndTrue())
			{
				orientation = OutputParams.Orientation.TwoSeventy;
			}
		}

		private void UpdateBackOrientation()
		{
			OutputParams.BackOrientation = FlipBackLayout != null && FlipBackLayout.IsChecked.IsValidAndTrue() ?
				OutputParams.GetFlippedOrientation(OutputParams.FrontOrientation) :
				OutputParams.FrontOrientation;
		}

		private void UpdateControls()
		{
			if (Core.Util.Math.AreEquivalent(OutputParams.Ppi, OutputParams.ScreenResolution))
			{
				ScreenRez.IsChecked = true;
			}
			else if (Core.Util.Math.AreEquivalent(OutputParams.Ppi, OutputParams.PrintResolution))
			{
				PrintRez.IsChecked = true;
			}
			else
			{
				CustomRez.IsChecked = true;
				CustomRezValue.Text = OutputParams.Ppi.ToString(CultureInfo.InvariantCulture);
			}

			IncludeBleed.IsChecked = OutputParams.IncludeBleed;

			bool backBaseOrientation = _requiredOrientations == DataSetViewModel.RequiredOrientations.Back;

			OutputParams.Orientation baseOrientation;
			if (backBaseOrientation)
			{
				baseOrientation = OutputParams.BackOrientation;
			}
			else
			{
				baseOrientation = OutputParams.FrontOrientation;
				FlipBackLayout.IsChecked = OutputParams.IsFlippedOrientation(OutputParams.FrontOrientation, OutputParams.BackOrientation);
			}

			switch (baseOrientation)
			{
				case OutputParams.Orientation.Zero:
					Orient0.IsChecked = true;
					break;

				case OutputParams.Orientation.Ninety:
					Orient90.IsChecked = true;
					break;

				case OutputParams.Orientation.OneEighty:
					Orient180.IsChecked = true;
					break;

				case OutputParams.Orientation.TwoSeventy:
					Orient270.IsChecked = true;
					break;
			}

			foreach (ComboBoxItem item in FormatComboBox.Items)
			{
				Image.Format format = (Image.Format) item.Tag;
				if (format == OutputParams.Format)
				{
					FormatComboBox.SelectedItem = item;
					break;
				}
			}
		}

		#endregion
	}
}
