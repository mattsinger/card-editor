﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using Core.Util;
using Microsoft.Win32;
using Application = System.Windows.Application;

namespace CardEditor.Dialogs
{
	/// <summary>
	/// Interaction logic for SettingsWindow.xaml
	/// </summary>
	public partial class SettingsWindow : Window, INotifyPropertyChanged
	{
		private string _cmykString;

		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		/// <summary>
		/// Accessor for CMYK profile value
		/// </summary>
		public string CmykString
		{
			get { return _cmykString; }
			set
			{
				if (value != _cmykString)
				{
					_cmykString = value;
					OnPropertyChanged();
				}
			}
		}

		public SettingsWindow()
		{
			InitializeComponent();

			InitGeneral();
			InitLayout();
		}

		private void InitGeneral()
		{
			CmykPath.DataContext = this;
			CmykString = Properties.Settings.Default.CmykConverter?.OriginalString;
			CmykPath.TextChanged += CmykPathTextChangedHandler;
		}

		private void InitLayout()
		{
			SnapSensitivity.Value = Properties.Settings.Default.SnapSensitivity;
			SnapSensitivity.ValueChanged += SnapSensitivityValueChangedHandler;

			UndoStackSize.Value = Properties.Settings.Default.UndoStackSize;
		}

		#region WPF Handlers

		private void CmykPathTextChangedHandler(object sender, TextChangedEventArgs args)
		{
			UpdateCmyk(CmykPath.Text);
		}

		private void CmykPathSelectClickedHandler(object sender, RoutedEventArgs args)
		{
			OpenFileDialog openFileDialog = new OpenFileDialog
			{
				CheckFileExists = true,
				Multiselect = false,
				Filter = @"Color profiles (*.icm;*.icc)|*.icm;*.icc",
				RestoreDirectory = true
			};

			if (openFileDialog.ShowDialog(Application.Current.MainWindow).IsValidAndTrue())
			{
				CmykString = openFileDialog.FileName;
			}
		}

		private void SnapSensitivityValueChangedHandler(object sender, RoutedPropertyChangedEventArgs<double> args)
		{
			Properties.Settings.Default.SnapSensitivity = (int)args.NewValue;
		}

		private void ClosedHandler(object sender, EventArgs e)
		{
			Properties.Settings.Default.UndoStackSize = UndoStackSize.Value ?? 100;
			Properties.Settings.Default.Save();
		}

		#endregion

		#region Private Methods

		private void UpdateCmyk(string uri)
		{
			CmykString = uri;
			if (string.IsNullOrEmpty(uri))
				Properties.Settings.Default.CmykConverter = null;
			else
			{
				Uri cmykUri;
				Properties.Settings.Default.CmykConverter = Uri.TryCreate(uri, UriKind.Absolute, out cmykUri) ? cmykUri : null;
			}
		}

		#endregion
	}
}
