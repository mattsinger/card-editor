﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using Core.View_Models;
using ReactiveUI;

namespace CardEditor.Dialogs
{
	/// <summary>
	/// Interaction logic for DocumentSelectWindow.xaml
	/// </summary>
	public partial class DocumentSelectDialog : Window
	{
		public DocumentSelectDialog(IList<BaseDocumentViewModel> documents)
		{
			InitializeComponent();

			DocList.ItemsSource = documents;

			this.WhenAnyValue(_ => _.DocList.SelectedItem)
				.Subscribe(item =>
				{
					SelectedDocument = item as BaseDocumentViewModel;
					OkButton.IsEnabled = SelectedDocument != null;
				});
		}

		public BaseDocumentViewModel SelectedDocument { get; set; }

		private void DocListDoubleClickedHandler(object sender, MouseButtonEventArgs e)
		{
			SelectedDocument = DocList.SelectedItem as BaseDocumentViewModel;
			DialogResult = true;
		}

		private void OkButtonClickedHandler(object sender, RoutedEventArgs e)
		{
			DialogResult = true;
		}
	}
}
