﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Windows;
using Core.View_Models.Data_Set;
using ReactiveUI;

namespace CardEditor.Dialogs
{
	/// <summary>
	/// Interaction logic for ImportDataSetDialog.xaml
	/// </summary>
	public partial class ImportDataSetDialog : Window, IViewFor<DataSetImporterViewModel>
	{
		#region Dependency Properties

		/// <summary>
		/// Dependency property
		/// </summary>
		public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
			typeof(DataSetImporterViewModel),
			typeof(ImportDataSetDialog));

		#endregion

		/// <summary>
		/// Constructs an ImportDataSetDialog
		/// </summary>
		public ImportDataSetDialog()
		{
			InitializeComponent();

			this.WhenAnyValue(_ => _.ViewModel).BindTo(this, _ => _.DataContext);
			this.OneWayBind(ViewModel, vm => vm.Headers, v => v.ImportedFields.ItemsSource);
			this.OneWayBind(ViewModel, vm => vm.ViableIdColumns, v => v.IdColumn.ItemsSource);
			this.OneWayBind(ViewModel, vm => vm.Subsections, v => v.Subsections.ItemsSource);
			this.Bind(ViewModel, vm => vm.IdColumn, v => v.IdColumn.SelectedValue);
			this.Bind(ViewModel, vm => vm.Subsection, v => v.Subsections.SelectedValue);
			this.Bind(ViewModel, vm => vm.FilePath, v => v.ImportFile.Text);
			this.Bind(ViewModel, vm => vm.UseFirstRowHeaders, v => v.UseFirstRowHeaders.IsChecked);
			this.Bind(ViewModel, vm => vm.OverwriteExistingData, v => v.OverwriteExistingData.IsChecked);
			this.Bind(ViewModel, vm => vm.CanImport, v => v.Import.IsEnabled);

			this.BindCommand(ViewModel, vm => vm.SelectFile, v => v.SelectFile);
		}

		#region Implementation of IViewFor

		object IViewFor.ViewModel
		{
			get { return ViewModel; }
			set { ViewModel = (DataSetImporterViewModel)value; }
		}

		#endregion

		#region Implementation of IViewFor<DataSetImporterViewModel>

		/// <summary>
		/// ViewModel
		/// </summary>
		public DataSetImporterViewModel ViewModel
		{
			get { return (DataSetImporterViewModel) GetValue(ViewModelProperty); }
			set
			{
				SetValue(ViewModelProperty, value);
			}
		}

		#endregion

		#region WPF Handlers

		private void ImportClickedHandler(object sender, RoutedEventArgs e)
		{
			DialogResult = true;
		}

		#endregion
	}
}
