﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Windows;
using System.Windows.Input;

namespace CardEditor.Dialogs
{
	/// <summary>
	/// Interaction logic for TextEntryDialog.xaml
	/// </summary>
	public partial class TextEntryDialog : Window
	{

		/// <summary>
		/// The user input
		/// </summary>
		public string Text { get; private set; }

		/// <summary>
		/// Constructor
		/// </summary>
		public TextEntryDialog()
		{
			InitializeComponent();

			TextEntryBox.Focus();
			TextEntryBox.KeyUp += (sender, args) =>
			{
				switch (args.Key)
				{
					case Key.Enter:
						Finish();
						break;

					case Key.Escape:
						Close();
						break;
				}
			};
		}

		/// <summary>
		/// Finishes the dialog
		/// </summary>
		public void Finish()
		{
			Text = TextEntryBox.Text;
			DialogResult = true;
		}

		/// <summary>
		/// Cancels the dialog
		/// </summary>
		public void Cancel()
		{
			DialogResult = false;
		}
	}
}
