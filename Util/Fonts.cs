﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Windows.Media;
using Core.Drawing;

namespace CardEditor.Util
{
	/// <summary>
	/// Contains helper functions for Windows fonts
	/// </summary>
	public static class Fonts
	{
		/// <summary>
		/// Gets the corresponding Windows FontFamily for a Font
		/// </summary>
		/// <param name="font">The Font to evaluate</param>
		/// <returns>A FontFamily matching the font, or null if not defined</returns>
		public static FontFamily GetFontFamily(Font font)
		{
			FontFamilyConverter ffc = new FontFamilyConverter();
			try
			{
				FontFamily family = string.IsNullOrEmpty(font?.Name) ? null :
				(FontFamily)ffc.ConvertFromString(font.Name);

				return family;
			}
			catch (NotSupportedException)	// thrown if font.Name cannot be converted
			{
				return null;
			}
		}

		/// <summary>
		/// Gets the corresponding Windows FontFamily for a Font
		/// </summary>
		/// <param name="font">The Font to evaluate</param>
		/// <returns>A FontFamily matching the font, or the generic sans serif font if not defined</returns>
		public static FontFamily GetFontFamilyOrDefault(Font font)
		{
			FontFamily converted = GetFontFamily(font);
			if (converted != null)
				return converted;

			FontFamilyConverter ffc = new FontFamilyConverter();
			return (FontFamily) ffc.ConvertFromString(System.Drawing.FontFamily.GenericSansSerif.Name);
		}

		/// <summary>
		/// Gets the corresponding font size for a Font
		/// </summary>
		/// <param name="font">The Font to evaluate</param>
		/// <returns>The font's Font.Size, or the default size if not defined</returns>
		public static float GetFontSizeOrDefault(Font font)
		{
			return font?.Size ?? Font.DefaultSize;
		}
	}
}
