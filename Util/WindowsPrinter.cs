﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Core.Documents;
using Core.Drawing;
using Core.Util;
using Splat;
using Image = System.Windows.Controls.Image;
using Size = System.Windows.Size;
using CoreSize = Core.Drawing.Size;
using HorizontalAlignment = System.Windows.HorizontalAlignment;

namespace CardEditor.Util
{
	/// <summary>
	/// Windows implementation of a print service
	/// </summary>
	public class WindowsPrinter : ICardPrinter
	{
		public IPrinterDocument CreateNewDocument(DeckPrintSet.PrintOptionValues printOptions, CoreSize cardSize)
		{
			return new WindowsPrintDoc(printOptions, cardSize);
		}

		public void Print(IPrinterDocument doc)
		{
			WindowsPrintDoc windowsDoc = (WindowsPrintDoc) doc;

			// force each page to perform its layout
			foreach (PageContent pageContent in windowsDoc.Document.Pages)
			{
				FixedPage page = pageContent.GetPageRoot(false);
				page.Measure(windowsDoc._pageSize);
				page.Arrange(new Rect(windowsDoc._pageSize));
				page.UpdateLayout();
			}

			PrintDialog dialog = new PrintDialog
			{
				SelectedPagesEnabled = false
			};
			if (dialog.ShowDialog().IsValidAndTrue())
			{
				IDocumentPaginatorSource source = windowsDoc.Document;
				dialog.PrintDocument(source.DocumentPaginator, "Card Editor deck print");
			}
		}
	}

	public class WindowsPrintDoc : IPrinterDocument
	{
		public readonly Size _pageSize;
		public readonly Size _pageMargins;
		public readonly Size _cardMargins;
		public readonly Size _cardSize;
		private Grid _currentFrontGrid;
		private Grid _currentBackGrid;

		private readonly int _numCardsX, _numCardsY;
		private int _curCardX, _curCardY;

		// we expect to re-use some images (often the back image)
		private readonly Dictionary<IBitmap, BitmapSource> _imageStorage = new Dictionary<IBitmap, BitmapSource>();

		public FixedDocument Document { get; } = new FixedDocument();

		public WindowsPrintDoc(DeckPrintSet.PrintOptionValues printOptions, CoreSize cardSize)
		{
			CoreSize printPpi = new CoreSize(OutputParams.PrintResolution, OutputParams.PrintResolution);
			_pageSize = Core.Util.Math.GetDisplaySizeFromInches(printOptions.PaperSize, printPpi).ToUISize();
			Document.DocumentPaginator.PageSize = _pageSize;

			_pageMargins = Core.Util.Math.GetDisplaySizeFromInches(printOptions.Margins, printPpi).ToUISize();
			_cardMargins = Core.Util.Math.GetDisplaySizeFromInches(printOptions.Spacing, printPpi).ToUISize();
			_cardSize = Core.Util.Math.GetDisplaySizeFromInches(cardSize, printPpi).ToUISize();

			double availableX = _pageSize.Width - 2 * _pageMargins.Width;
			while (availableX >= _cardSize.Width)
			{
				_numCardsX++;
				availableX -= (_cardSize.Width + _cardMargins.Width);
			}

			double availableY = _pageSize.Height - 2 * _pageMargins.Height;
			while (availableY >= _cardSize.Height)
			{
				_numCardsY++;
				availableY -= (_cardSize.Height + _cardMargins.Height);
			}

			AddPages();
		}

		public void AddCard(IBitmap front, IBitmap back)
		{
			if (!Core.Util.Math.AreEquivalent(front.Width, (float)_cardSize.Width) ||
			    !Core.Util.Math.AreEquivalent(front.Height, (float)_cardSize.Height))
			{
				throw new ArgumentOutOfRangeException(nameof(front), @"Front image must match expected dimensions");
			}

			if (!Core.Util.Math.AreEquivalent(back.Width, (float)_cardSize.Width) ||
			    !Core.Util.Math.AreEquivalent(back.Height, (float)_cardSize.Height))
			{
				throw new ArgumentOutOfRangeException(nameof(back), @"Back image must match expected dimensions");
			}

			if (_curCardX >= _numCardsX)
			{
				_curCardX = 0;
				_curCardY++;
			}

			if (_curCardY >= _numCardsY)
			{
				_curCardY = 0;
				AddPages();
			}

			if (!_imageStorage.ContainsKey(front))
				_imageStorage.Add(front, front.ToNative());

			if (!_imageStorage.ContainsKey(back))
				_imageStorage.Add(back, back.ToNative());

			Image frontImage = new Image
			{
				Source = _imageStorage[front]
			};
			Image backImage = new Image
			{
				Source = _imageStorage[back]
			};

			// the actual page content is stored in a grid, where every other row/column is empty padding, so we convert
			// current card x/y to grid x/y by doubling (since its 0-indexed)
			int gridX = _curCardX * 2;
			int gridY = _curCardY * 2;
			Grid.SetColumn(frontImage, gridX);
			Grid.SetRow(frontImage, gridY);
			_currentFrontGrid.Children.Add(frontImage);

			// back grid flows right to left, so we actually need to inverse the column
			// we have (2 * (X - 1)) total columns in the grid
			int backGridX = (2 * (_numCardsX - 1)) - gridX;
			Grid.SetColumn(backImage, backGridX);
			Grid.SetRow(backImage, gridY);
			_currentBackGrid.Children.Add(backImage);

			_curCardX++;
		}

		private Grid CreateGrid(bool isFrontPage)
		{
			Grid grid = new Grid();

			double gridWidth = 0;

			for (int x = 0; x < _numCardsX; x++)
			{
				grid.ColumnDefinitions.Add(new ColumnDefinition
				{
					MinWidth = _cardSize.Width,
					MaxWidth = _cardSize.Width
				});
				gridWidth += _cardSize.Width;

				// add extra column for padding between each card
				if (x < _numCardsX - 1)
				{
					grid.ColumnDefinitions.Add(new ColumnDefinition
					{
						MinWidth = _cardMargins.Width,
						MaxWidth = _cardMargins.Width
					});
					gridWidth += _cardMargins.Width;
				}
			}

			for (int y = 0; y < _numCardsY; y++)
			{
				grid.RowDefinitions.Add(new RowDefinition
				{
					MinHeight = _cardSize.Height,
					MaxHeight = _cardSize.Height
				});

				// add extra row for padding between each card
				if (y < _numCardsY - 1)
					grid.RowDefinitions.Add(new RowDefinition
					{
						MinHeight = _cardMargins.Height,
						MaxHeight = _cardMargins.Height
					});
			}

			double leftSideMargin = _pageMargins.Width;
			double rightSideMargin = _pageSize.Width - (gridWidth + _pageMargins.Width);

			grid.Margin = new Thickness(
				isFrontPage ? leftSideMargin : rightSideMargin, 
				_pageMargins.Height, 
				isFrontPage ? rightSideMargin : leftSideMargin, 
				_pageMargins.Height);

			return grid;
		}

		private void AddPages()
		{
			Grid frontGrid = CreateGrid(true);

			FixedPage frontPage = new FixedPage
			{
				Width = _pageSize.Width,
				Height = _pageSize.Height
			};
			frontPage.Children.Add(frontGrid);

			PageContent frontPageContent = new PageContent();
			((IAddChild)frontPageContent).AddChild(frontPage);
			Document.Pages.Add(frontPageContent);

			Grid backGrid = CreateGrid(false);

			FixedPage backPage = new FixedPage
			{
				Width = _pageSize.Width,
				Height = _pageSize.Height
			};
			backPage.Children.Add(backGrid);

			PageContent backPageContent = new PageContent();
			((IAddChild)backPageContent).AddChild(backPage);
			Document.Pages.Add(backPageContent);

			_currentFrontGrid = frontGrid;
			_currentBackGrid = backGrid;
		}
	}
}
