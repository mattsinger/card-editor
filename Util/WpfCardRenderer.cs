﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using CardEditor.Views.Card_Preview;
using Core.Drawing;
using Core.Util;
using Core.View_Models.Card_Preview;
using Core.View_Models.Data_Set;
using Core.View_Models.Layout;
using Splat;
using Size = Core.Drawing.Size;

namespace CardEditor.Util
{
	/// <summary>
	/// WPF implementation for rendering cards for output
	/// </summary>
	public class WpfCardRenderer : ICardRenderer, IEnableLogger
	{
		#region Implementation of ICardRenderer

		/// <summary>
		/// Renders a card to an image file
		/// </summary>
		/// <param name="card">The card to be rendered, may be null</param>
		/// <param name="layout">The layout dictating how it should be rendered</param>
		/// <param name="outputParams">The parameters for output</param>
		/// <param name="useFrontOrient">Whether the card should be rendered using the front orientation or the back orientation</param>
		/// <returns>A bitmap containing the rendered card or null if it failed to render</returns>
		public IBitmap RenderCard(CardDataViewModel card, LayoutViewModel layout, OutputParams outputParams, bool useFrontOrient)
		{
			Size layoutSize = outputParams.IncludeBleed ? layout.FullSize : layout.Size;
			Size ppi = new Size(outputParams.Ppi, outputParams.Ppi);
			Size renderSize = Core.Util.Math.GetDisplaySizeFromInches(layoutSize, ppi);
			OutputParams.Orientation orientation = useFrontOrient ? outputParams.FrontOrientation : outputParams.BackOrientation;
			
			if (orientation == OutputParams.Orientation.Ninety || orientation == OutputParams.Orientation.TwoSeventy)
				renderSize = renderSize.Inverse();
			
			try
			{
				CardPreviewViewModel previewViewModel = new CardPreviewViewModel(layout.Workspace)
				{
					CardDataViewModel = card,
					LayoutViewModel = layout,
					IncludeBleed = outputParams.IncludeBleed,
					Ppi = ppi
				};

				// for output, we don't want to use CMYK conversion
				CardPreviewView preview = new CardPreviewView
				{
					ViewModel = previewViewModel
				};
				preview.ForceRender();

				RotateTransform renderTransform = new RotateTransform
				{
					CenterX = renderSize.Width/2,
					CenterY = renderSize.Height/2
				};
				switch (orientation)
				{
					case OutputParams.Orientation.Zero:
						renderTransform.Angle = 0;
						break;
					case OutputParams.Orientation.Ninety:
						renderTransform.Angle = 90;
						break;
					case OutputParams.Orientation.OneEighty:
						renderTransform.Angle = 180;
						break;
					case OutputParams.Orientation.TwoSeventy:
						renderTransform.Angle = 270;
						break;
					default:
						throw new InvalidOperationException("Unknown Orientation type");
				}
				preview.LayoutTransform = renderTransform;

				System.Windows.Size viewSize = renderSize.ToUISize();
				preview.Measure(viewSize);
				preview.Arrange(new Rect(viewSize));
				preview.UpdateLayout();

				int targetWidth = (int) (renderSize.Width + .5f);
				int targetHeight = (int) (renderSize.Height + .5f);
				// CardPreviewView and other PreviewViews handle sizing with respect to PPI/DPI manually, so we want to render at normal screen DPI
				RenderTargetBitmap target = new RenderTargetBitmap(targetWidth, targetHeight, 96, 96, PixelFormats.Default);
				target.Render(preview);
				target.Freeze();

				previewViewModel.Dispose();

				IBitmap bitmap = target.FromNative();
				return bitmap;
			}
			catch (Exception ex)
			{
				this.Log().Error($"Could not output card \"{card?.Id}\", layout \"{layout.DisplayName}\" to file:\n{ex}");
				return null;
			}
		}

		#endregion
	}
}
