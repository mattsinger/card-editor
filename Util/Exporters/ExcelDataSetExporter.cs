﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using Core.Documents;
using Core.Util;
using Core.Util.Exporters;
using Splat;
using Excel = Microsoft.Office.Interop.Excel;

namespace CardEditor.Util.Exporters
{
	[DataSetExporter(".xlsx", "Microsoft Excel OpenXML Spreadsheet")]
	public class ExcelDataSetExporter : IDataSetExporter
	{
		/// <summary>
		/// Writes cards to the first Worksheet page of an Excel Spreadsheet, or adds one if it doesn't exist
		/// </summary>
		/// <param name="filepath">The path of the file to write</param>
		/// <param name="cards"></param>
		public void WriteCards(string filepath, DataSet cards)
		{
			// we should have already confirmed the overwrite, so don't let Excel do it again
			Excel.Application app = new Excel.Application
			{
				DisplayAlerts = false
			};
			Excel.Workbook workbook = null;
			Excel.Worksheet worksheet = null;
			try
			{
				bool exists = Locator.Current.GetService<IFileSystem>().Exists(filepath);
				workbook = exists ? 
					app.Workbooks.Open(filepath, Local: true, ReadOnly: false, Editable: true) : 
					app.Workbooks.Add();
				worksheet = workbook.Worksheets.OfType<Excel.Worksheet>().FirstOrDefault() ??
				            (Excel.Worksheet) workbook.Worksheets.Add();

				// Excel cells are 1-indexed. First row is for headers and first column is for
				// ids, so everything else is 0-index + 2
				worksheet.Cells[1, 1].Value2 = "Id";
				for (int i = 0; i < cards.FieldDescriptors.Count; i++)
				{
					worksheet.Cells[1, i + 2].Value2 = cards.FieldDescriptors[i].Key;
				}

				for (int c = 0; c < cards.Data.Count; c++)
				{
					CardData data = cards.Data[c];
					worksheet.Cells[c + 2, 1].Value = data.Id;
					for (int i = 0; i < cards.FieldDescriptors.Count; i++)
					{
						worksheet.Cells[c + 2, i + 2].Value2 = data.Fields[i].Value;
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				throw;
			}
			finally
			{
				if (worksheet != null)
					Marshal.ReleaseComObject(worksheet);
				if (workbook != null)
				{
					workbook.Close(true, filepath);
					Marshal.ReleaseComObject(workbook);
				}

				app.Quit();
				Marshal.ReleaseComObject(app);
			}
		}
	}
}
