﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Text;
using Splat;

namespace CardEditor.Util
{
	/// <summary>
	/// Implementation for logging
	/// </summary>
	public class Logger : ILogger, IDisposable
	{
		#region Private Members

		private bool _disposed;
		private readonly List<string> _sessionLogs = new List<string>();

		#endregion

		#region Finalizer

		public Logger()
		{
			_disposed = false;
		}

		~Logger()
		{
			DoDispose(true);
		}

		#endregion

		#region Public Properties

		/// <summary>
		/// Gets all logs from the current session
		/// </summary>
		public IReadOnlyList<string> SessionLogs => _sessionLogs.AsReadOnly();

		#endregion

		#region Implementation of ILogger

		/// <summary>
		/// Writes a message to the log with a given level
		/// </summary>
		/// <param name="message">The message to write</param>
		/// <param name="logLevel">The level of the log</param>
		public void Write(string message, LogLevel logLevel)
		{
			if (logLevel < Level)
				return;

			StringBuilder builder = new StringBuilder(DateTime.UtcNow.ToString("O"));
			builder.Append($" {Enum.GetName(typeof(LogLevel), logLevel)}: ");
			builder.AppendLine(message);

			_sessionLogs.Add(builder.ToString());
		}

		/// <summary>
		/// The minimum level to output to logs
		/// </summary>
		public LogLevel Level { get; set; }

		#endregion

		#region Implementation of IDisposable

		/// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
		public void Dispose()
		{
			DoDispose(false);
		}

		#endregion

		#region Private Methods

		private void DoDispose(bool finalizing)
		{
			if (_disposed)
				return;

			//using (FileStream fs = new FileStream(Environment.SpecialFolder.ApplicationData + "/Log.txt", FileMode.Create))
			//{
			//	foreach (string log in _sessionLogs)
			//	{
			//		byte[] logBytes = Encoding.UTF8.GetBytes(log);
			//		fs.Write(logBytes, 0, logBytes.Length);
			//	}
			//}

			_disposed = true;
		}

		#endregion
	}
}
