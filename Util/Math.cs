﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Windows;

namespace CardEditor.Util
{
	/// <summary>
	/// Utility functions for Windows-specific math functions
	/// </summary>
	public static class Math
	{
		/// <summary>
		/// Converts a Core.Drawing.Size to a System.Windows.Size
		/// </summary>
		/// <param name="coreSize">The original Core.Drawing.Size</param>
		/// <returns>An equivalent System.Windows.Size</returns>
		public static Size ToUISize(this Core.Drawing.Size coreSize)
		{
			return new Size(coreSize.Width, coreSize.Height);
		}

		/// <summary>
		/// Converts a System.Windows.Size to a Core.Drawing.Size
		/// </summary>
		/// <param name="windowsSize">The original System.Windows.Size</param>
		/// <returns>An equivalent Core.Drawing.Size</returns>
		public static Core.Drawing.Size ToCoreSize(this System.Windows.Size windowsSize)
		{
			return new Core.Drawing.Size((float)windowsSize.Width, (float)windowsSize.Height);
		}
	}
}
