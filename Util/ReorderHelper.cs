﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using CardEditor.Controls;

namespace CardEditor.Util
{
	/// <summary>
	/// A helper class to assist in drag-and-drop reordering
	/// </summary>
	/// <remarks>
	/// Derived from sample code found online, but original source unknown
	/// </remarks>
	internal class ReorderHelper
	{
		#region Private Members

		private readonly ItemsControl _reorderTarget;

		private enum DragState
		{
			NotDragging,
			MouseDown,
			Dragging
		}
		private DragState _dragState = DragState.NotDragging;
		private Point _mousePosition;
		private object _draggedData;
		private readonly DataFormat _format;
		private InsertAdorner _insertAdorner;
		private FrameworkElement _dropTarget;
		private int _removeIndex = -1;
		private int _insertIndex = -1;
		private bool _inTopHalf;
		private bool _enabled = true;

		#endregion

		#region Properties

		/// <summary>
		/// Whether the reorder helper is enabled or not
		/// </summary>
		public bool Enabled
		{
			get => _enabled;
			set
			{
				_enabled = value;
				if (!_enabled && _dragState == DragState.Dragging)
				{
					EndDragging();
				}
			}
		}

		#endregion

		#region Constructor

		/// <summary>
		/// Constructs a ReorderHelper
		/// </summary>
		/// <param name="reorderTarget">The target container that is </param>
		public ReorderHelper(ItemsControl reorderTarget)
		{
			_format = DataFormats.GetDataFormat($"{reorderTarget.GetHashCode()}_DragContents");

			_reorderTarget = reorderTarget;
			reorderTarget.PreviewMouseLeftButtonDown += ReorderTarget_PreviewMouseLeftButtonDownHandler;
			reorderTarget.PreviewMouseMove += ReorderTarget_PreviewMouseMoveHandler;
			reorderTarget.PreviewMouseLeftButtonUp += ReorderTarget_PreviewMouseLeftButtonUpHandler;
		}

		#endregion

		#region Events

		/// <summary>
		/// Event args used to notify that an item has been re-ordered
		/// </summary>
		public class ReorderedEventArgs : EventArgs
		{
			/// <summary>
			/// The item that was re-ordered
			/// </summary>
			public object Item { get; set; }
		}

		/// <summary>
		/// Event handler that fires when an item has been re-ordered
		/// </summary>
		public EventHandler<ReorderedEventArgs> ItemReordered;

		#endregion

		#region WPF Handlers

		private void ReorderTarget_PreviewMouseLeftButtonDownHandler(object sender, MouseButtonEventArgs args)
		{
			if (!Enabled)
			{
				return;
			}

			Visual sourceVisual = args.OriginalSource as Visual;
			FrameworkElement sourceElement = _reorderTarget.ContainerFromElement(sourceVisual) as FrameworkElement;
			if (sourceElement != null)
			{
				_dragState = DragState.MouseDown;
				_mousePosition = args.GetPosition(Application.Current.MainWindow);
				_draggedData = sourceElement.DataContext;
				_removeIndex = _reorderTarget.Items.IndexOf(_draggedData);
			}
		}

		private void ReorderTarget_PreviewMouseMoveHandler(object sender, MouseEventArgs args)
		{
			if (!Enabled)
			{
				return;
			}

			if (_dragState == DragState.NotDragging)
				return;

			if (_dragState != DragState.Dragging)
			{
				IInputElement inputElement = args.OriginalSource as IInputElement;
				if (inputElement != null && !inputElement.IsKeyboardFocused && 
					MouseHasDragged(_mousePosition, args.GetPosition(Application.Current.MainWindow)))
				{
					_dragState = DragState.Dragging;
					BeginDragging();

					args.Handled = true;
				}
			}
		}

		private void ReorderTarget_PreviewMouseLeftButtonUpHandler(object sender, MouseButtonEventArgs args)
		{
			if (_dragState == DragState.NotDragging)
				return;

			if (_dragState == DragState.Dragging)
			{
				EndDragging();
				args.Handled = true;
			}

			_dragState = DragState.NotDragging;
			_draggedData = null;
		}

		private void ReorderTarget_PreviewDragOverHandler(object sender, DragEventArgs args)
		{
			GetDropTarget(args);
			UpdateInsertAdorner();
			args.Handled = true;
		}

		private void ReorderTarget_PreviewDropHandler(object sender, DragEventArgs args)
		{
			RemoveInsertAdorner();
			MoveData();

			args.Handled = true;
		}

		private void ReorderTarget_PreviewDragEnterHandler(object sender, DragEventArgs args)
		{
			GetDropTarget(args);
			AddInsertAdorner();
			args.Handled = true;
		}

		private void ReorderTarget_PreviewDragLeaveHandler(object sender, DragEventArgs args)
		{
			RemoveInsertAdorner();

			args.Handled = true;
		}

		#endregion

		#region Private Methods

		private void BeginDragging()
		{
			_reorderTarget.AllowDrop = true;
			_reorderTarget.PreviewDragEnter += ReorderTarget_PreviewDragEnterHandler;
			_reorderTarget.PreviewDragOver += ReorderTarget_PreviewDragOverHandler;
			_reorderTarget.PreviewDragLeave += ReorderTarget_PreviewDragLeaveHandler;
			_reorderTarget.PreviewDrop += ReorderTarget_PreviewDropHandler;

			DataObject data = new DataObject(_format.Name, _draggedData);
			DragDropEffects effect = DragDrop.DoDragDrop(_reorderTarget, data, DragDropEffects.Move);
		}

		private void EndDragging()
		{
			_reorderTarget.AllowDrop = false;
			_reorderTarget.PreviewDragEnter -= ReorderTarget_PreviewDragEnterHandler;
			_reorderTarget.PreviewDragOver -= ReorderTarget_PreviewDragOverHandler;
			_reorderTarget.PreviewDragLeave -= ReorderTarget_PreviewDragLeaveHandler;
			_reorderTarget.PreviewDrop -= ReorderTarget_PreviewDropHandler;
		}

		private bool MouseHasDragged(Point initial, Point current)
		{
			// should be SystemParameters.MinimumHorizontalDragDistance and MinimumVerticalDragDistance,
			// but those are rather large and often result in the mouse exiting and highlighting other elements.
			return System.Math.Abs(initial.X - current.X) >= 1 &&
				   System.Math.Abs(initial.Y - current.Y) >= 1;
		}

		private void GetDropTarget(DragEventArgs args)
		{
			int totalItems = _reorderTarget.Items.Count;
			_dropTarget = _reorderTarget.ContainerFromElement((DependencyObject)args.OriginalSource) as FrameworkElement;

			if (_dropTarget != null)
			{
				Point positionRelativeToItemContainer = args.GetPosition(_dropTarget);
				_inTopHalf = positionRelativeToItemContainer.Y < _dropTarget.RenderSize.Height/2;
				_insertIndex = _reorderTarget.ItemContainerGenerator.IndexFromContainer(_dropTarget);

				if (!_inTopHalf)
				{
					_insertIndex++;
				}
			}
			else
			{
				_dropTarget = _reorderTarget.ItemContainerGenerator.ContainerFromIndex(totalItems - 1) as FrameworkElement;
				_inTopHalf = false;
				_insertIndex = totalItems;
			}
		}

		private void AddInsertAdorner()
		{
			if (_dropTarget != null)
			{
				_insertAdorner = new InsertAdorner(_dropTarget, AdornerLayer.GetAdornerLayer(_dropTarget))
				{
					InTopHalf = _inTopHalf
				};
			}
		}

		private void UpdateInsertAdorner()
		{
			if (_insertAdorner != null)
			{
				_insertAdorner.InTopHalf = _inTopHalf;
				_insertAdorner.InvalidateVisual();
			}
		}

		private void RemoveInsertAdorner()
		{
			_insertAdorner?.Dispose();
			_insertAdorner = null;
		}

		private void MoveData()
		{
			if (_removeIndex != _insertIndex)
			{
				if (_removeIndex < _insertIndex)
					_insertIndex--;

				IList itemList = _reorderTarget.ItemsSource as IList;
				itemList.RemoveAt(_removeIndex);
				itemList.Insert(_insertIndex, _draggedData);

				ItemReordered?.Invoke(this, new ReorderedEventArgs
				{
					Item = _draggedData
				});
			}
		}

		#endregion
	}
}
