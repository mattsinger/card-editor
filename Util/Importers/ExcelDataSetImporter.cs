﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using Core.Documents;
using Core.Exceptions;
using Core.Util;
using Core.Util.Importers;
using Core.View_Models.Data_Set;
using Splat;
using Excel = Microsoft.Office.Interop.Excel;

namespace CardEditor.Util.Importers
{
	/// <summary>
	/// A Data Det importer that can read data from an Excel spreadsheet
	/// </summary>
	[DataSetImporter(".xls", ".xlsx")]
	public class ExcelDataSetImporter : IDataSetImporter
	{
		private readonly Excel.Application _xlApp = new Excel.Application();

		/// <summary>
		/// Gets the list of subsections within the import data. The exact meaning of this is 
		/// specific to the importer
		/// </summary>
		/// <returns>The names of the worksheets in the spreadsheet</returns>
		public List<string> GetSubsections(string filepath)
		{
			List<string> sheetNames = new List<string>();
			Excel.Workbook workbook = _xlApp.Workbooks.Open(filepath, ReadOnly: true, Local: true);

			for (int i = 0; i < workbook.Worksheets.OfType<Excel.Worksheet>().Count(); i++)
			{
				Excel.Worksheet worksheet = workbook.Worksheets.OfType<Excel.Worksheet>().ElementAt(i);

				sheetNames.Add(worksheet.Name);

				Marshal.ReleaseComObject(worksheet);
			}
			
			Marshal.ReleaseComObject(workbook);
			return sheetNames;
		}

		/// <summary>
		/// Retrieves the headers from the first worksheet in an Excel spreadsheet
		/// </summary>
		/// <param name="filepath">The absolute path to the file</param>
		/// <param name="subsection">The worksheet to read from</param>
		/// <param name="useHeaderRow">Whether it should use the first row as headers</param>
		/// <returns>A list of field headers, auto-populated with keys and types</returns>
		/// <remarks>Assumes card data is stored in a contiguous block of cells</remarks>
		public List<FieldImporterViewModel> LoadHeaders(string filepath, string subsection, bool useHeaderRow)
		{
			List<FieldImporterViewModel> headers = new List<FieldImporterViewModel>();

			IFileSystem fileSystem = Locator.Current.GetService<IFileSystem>();
			if (!fileSystem.Exists(filepath))
				return headers;

			Excel.Workbook workbook = _xlApp.Workbooks.Open(filepath, ReadOnly:true, Local:true);
			Excel.Worksheet worksheet = workbook.Worksheets.OfType<Excel.Worksheet>().First(xlws => xlws.Name == subsection);
			Excel.Range usedRange = worksheet.UsedRange;
			
			// for some reason cells are 1-indexed in Excel
			for (int column = 1; column <= usedRange.Columns.Count; column++)  // note indices, Excel uses 1-indexed row/columns
			{
				string header;
				FieldType fieldType;

				if (useHeaderRow)
				{
					dynamic headerValue = worksheet.Cells[1, column].Value2;
					if (headerValue == null)
						header = $"Field{column}";
					else
					{
						header = headerValue.ToString();
						if (string.IsNullOrEmpty(header))
							header = $"Field{column}";
					}

					dynamic fieldValue = worksheet.Cells[2, column].Value2;
					fieldType = fieldValue == null ? 
						FieldType.Text : 
						DataSetImportHelpers.GetFieldTypeFromData(fieldValue.ToString());
				}
				else
				{
					header = $"Field{column}";
					dynamic fieldValue = worksheet.Cells[1, column].Value2;
					fieldType = fieldValue == null ? 
						FieldType.Text : 
						DataSetImportHelpers.GetFieldTypeFromData(fieldValue.ToString());
				}

				headers.Add(new FieldImporterViewModel
				{
					Key = header,
					FieldType = fieldType
				});
			}

			Marshal.ReleaseComObject(worksheet);
			Marshal.ReleaseComObject(workbook);

			return headers;
		}

		/// <summary>
		/// Retrieves the set of cards from the first page in an Excel spreadsheet
		/// </summary>
		/// <param name="filepath">The absolute path to the file</param>
		/// <param name="subsection">The worksheet to load from</param>
		/// <param name="useHeaderRow">Whether it should use the first row as headers</param>
		/// <param name="fields">The card field descriptors in column order, including the id column if present</param>
		/// <returns>A list of cards</returns>
		public List<CardData> LoadCards(string filepath, string subsection, bool useHeaderRow, List<FieldImporterViewModel> fields)
		{
			List<CardData> cards = new List<CardData>();

			IFileSystem fileSystem = Locator.Current.GetService<IFileSystem>();
			if (!fileSystem.Exists(filepath))
				return cards;

			string fileName = Path.GetFileNameWithoutExtension(filepath);

			Excel.Workbook workbook = _xlApp.Workbooks.Open(filepath, ReadOnly: true, Local: true);
			Excel.Worksheet worksheet = workbook.Worksheets.OfType<Excel.Worksheet>().First(xlws => xlws.Name == subsection);
			Excel.Range usedRange = worksheet.UsedRange;

			int cardCount = 1;
			for (int row = useHeaderRow ? 2 : 1; row <= usedRange.Rows.Count; row++, cardCount++)
			{
				CardData cardData = new CardData
				{
					Id = $"{fileName}{cardCount:D3}"
				};

				for (int col = 1; col <= usedRange.Columns.Count; col++)
				{
					if (!fields[col - 1].Include)
						continue;

					dynamic fieldValue = worksheet.Cells[row, col].Value2;
					if (fields[col - 1].IsId)
					{
						if (fieldValue == null)
							throw new FieldNotFoundException("Id");
						cardData.Id = fieldValue.ToString();
					}
					else
						cardData.Fields.Add(new CardField
						{
							Key = fields[col - 1].Key,
							Value = fieldValue?.ToString() ?? string.Empty
						});
				}

				cards.Add(cardData);
			}

			Marshal.ReleaseComObject(worksheet);
			Marshal.ReleaseComObject(workbook);

			return cards;
		}

		public void Dispose()
		{
			Marshal.ReleaseComObject(_xlApp);
		}
	}
}
