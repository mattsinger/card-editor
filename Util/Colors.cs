﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Core.Drawing;

namespace CardEditor.Util
{
	/// <summary>
	/// Helper methods for converting between various C#/WPF Colors and Core.Drawing.Color
	/// </summary>
	public static class Colors
	{
		/// <summary>
		/// Converts a Core.Drawing.Color to a System.Windows.Media.Color
		/// </summary>
		/// <param name="color">The color to convert</param>
		/// <returns>An appropriate System.Windows.Media.Color</returns>
		public static System.Windows.Media.Color ToMediaColor(this Color color)
		{
			return System.Windows.Media.Color.FromArgb(color.A, color.R, color.G, color.B);
		}

		/// <summary>
		/// Converts a System.Windows.Media.Color to a Core.Drawing.Color
		/// </summary>
		/// <param name="color">The color to convert</param>
		/// <returns>An appropriate Core.Drawing.Color</returns>
		public static Color ToCoreColor(this System.Windows.Media.Color color)
		{
			return Color.FromArgb(color.A, color.R, color.G, color.B);
		}
	}
}
