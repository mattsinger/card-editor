﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using Core.Documents;
using Core.Drawing;
using Core.Util;
using Core.Util.Exporters;
using Core.View_Models;
using Splat;
using Application = System.Windows.Application;
using DocumentSelectDialog = CardEditor.Dialogs.DocumentSelectDialog;
using File = System.IO.File;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;
using SaveFileDialog = Microsoft.Win32.SaveFileDialog;

namespace CardEditor.Util
{
	/// <summary>
	/// Windows-specific implementation of IFileSystem
	/// </summary>
	public class WindowsFileSystem : IFileSystem, IEnableLogger
	{
		#region Images

		/// <summary>
		/// Requests the path to an existing image file
		/// </summary>
		/// <param name="startPath">The start path to begin looking for files</param>
		/// <param name="imgPath">The path to the selected image file</param>
		/// <returns>True if an image file was selected, false otherwise</returns>
		public bool GetExistingImage(string startPath, out string imgPath)
		{
			OpenFileDialog openFile = new OpenFileDialog
			{
				Multiselect = false,
				Filter = ImageFileFilter,
				RestoreDirectory = true
			};
			// make sure we have an absolute path to the folder
			string directoryPath = Path.GetDirectoryName(startPath);
			if (directoryPath != null)
			{
				directoryPath = Path.GetFullPath(directoryPath);
				openFile.InitialDirectory = directoryPath;
			}

			Window activeWindow = GetActiveWindow();
			bool? dialogResult = openFile.ShowDialog(activeWindow);
			bool result = dialogResult.IsValidAndTrue();
			imgPath = result ? openFile.FileName : null;

			return result;
		}

		/// <summary>
		/// Requests the path to an image file to create for output, expected to be a PNG (for now)
		/// </summary>
		/// <param name="startDirectory">The start path to begin looking for locations</param>
		/// <param name="startFile">The suggested filename for the image file</param>
		/// <param name="format">The file format for the output image</param>
		/// <param name="imgPath">The resulting path to the output file</param>
		/// <returns>True if an image file was selected, false otherwise</returns>
		public bool GetOutputImage(string startDirectory, string startFile, Image.Format format, out string imgPath)
		{

			SaveFileDialog saveFile = new SaveFileDialog
			{
				AddExtension = true,
				Filter = $"Card Image (*{format.GetExtension()})|*{format.GetExtension()}",
				InitialDirectory = startDirectory,
				FileName = startFile,
				OverwritePrompt = true,
				RestoreDirectory = true
			};
			if (saveFile.ShowDialog(GetActiveWindow()).IsValidAndTrue())
			{
				imgPath = saveFile.FileName;
				return true;
			}

			imgPath = null;
			return false;
		}

		/// <summary>
		/// Saves a bitmap to a file
		/// </summary>
		/// <param name="imgPath">The path of the image file</param>
		/// <param name="format">The format of the image</param>
		/// <param name="image">A bitmap containing the rendered card</param>
		/// <returns>True if the file saved successfully, false otherwise</returns>
		public bool SaveImageToFile(string imgPath, Image.Format format, IBitmap image)
		{
			using (Stream outputStream = WriteLocalFile(imgPath, true))
			{
				try
				{
					switch (format)
					{
						case Image.Format.Bmp:
						{
							BmpBitmapEncoder encoder = new BmpBitmapEncoder();
							encoder.Frames.Add(BitmapFrame.Create(image.ToNative()));
							encoder.Save(outputStream);
						}
						break;

						case Image.Format.Jpg:
						{
							JpegBitmapEncoder encoder = new JpegBitmapEncoder
							{
								QualityLevel = 100
							};
							encoder.Frames.Add(BitmapFrame.Create(image.ToNative()));
							encoder.Save(outputStream);
						}
						break;

						case Image.Format.Png:
						{
							PngBitmapEncoder encoder = new PngBitmapEncoder();
							encoder.Frames.Add(BitmapFrame.Create(image.ToNative()));
							encoder.Save(outputStream);
						}
						break;

						case Image.Format.Tiff:
						{
							TiffBitmapEncoder encoder = new TiffBitmapEncoder();
							encoder.Frames.Add(BitmapFrame.Create(image.ToNative()));
							encoder.Save(outputStream);
						}
						break;
					}
				}
				catch (Exception ex)
				{
					this.Log().ErrorException("Failed to save card to file", ex);
					return false;
				}
			}

			return true;
		}

		#endregion

		#region Documents

		/// <summary>
		/// Requests the path to a document files
		/// </summary>
		/// <typeparam name="T">The type of document, which must implement IDocument</typeparam>
		/// <param name="startDirectory">The start directory to begin looking for files</param>
		/// <param name="operation">The file operation we want to perform</param>
		/// <param name="allowMultiple">Whether we allow multiple file paths or only one, ignored when operation is Save</param>
		/// <returns>Returns a list of filepaths to document files. Returns null if operation was cancelled</returns>
		public IList<string> GetDocumentPaths<T>(string startDirectory, FileSystem.Operation operation, bool allowMultiple) where T : IDocument
		{
			return GetDocumentPaths(typeof(T), startDirectory, operation, allowMultiple);
		}

		/// <summary>
		/// Requests the path to a single existing document file
		/// </summary>
		/// <param name="docType">The type of document, which must implement IDocument</param>
		/// <param name="startDirectory">The start directory to begin looking for files</param>
		/// <param name="operation">The file operation we want to perform</param>
		/// <param name="allowMultiple">Whether we allow multiple file paths or only one, ignored when operation is Save</param>
		/// <returns>Returns a list of filepaths to document files. Returns null if operation was cancelled</returns>
		public IList<string> GetDocumentPaths(Type docType, string startDirectory, FileSystem.Operation operation, bool allowMultiple)
		{
			Microsoft.Win32.FileDialog dialog;
			switch (operation)
			{
				case FileSystem.Operation.Create:
					dialog = new SaveFileDialog
					{
						Filter = GetDocumentFilter(docType),
						InitialDirectory = startDirectory,
						RestoreDirectory = true,
						AddExtension = true,
						CheckFileExists = false,
						Title = $"Create new {docType.GetCustomAttribute<DocumentAttribute>().Description}"
					};
					break;

				case FileSystem.Operation.Open:
					dialog = new OpenFileDialog
					{
						Multiselect = allowMultiple,
						Filter = GetDocumentFilter(docType),
						InitialDirectory = startDirectory,
						RestoreDirectory = true,
						AddExtension = true,
						CheckFileExists = true,
						Title = $"Open {docType.GetCustomAttribute<DocumentAttribute>().Description}"
					};
					break;

				case FileSystem.Operation.CreateOrOpen:
					dialog = new OpenFileDialog
					{
						Multiselect = allowMultiple,
						Filter = GetDocumentFilter(docType),
						InitialDirectory = startDirectory,
						RestoreDirectory = true,
						AddExtension = true,
						CheckFileExists = false,
						Title = $"Open or create new {docType.GetCustomAttribute<DocumentAttribute>().Description}"
					};
					break;

				case FileSystem.Operation.Save:
					dialog = new SaveFileDialog
					{
						Filter = GetDocumentFilter(docType),
						InitialDirectory = startDirectory,
						RestoreDirectory = true,
						AddExtension = true,
						CheckFileExists = true,
						Title = $"Save {docType.GetCustomAttribute<DocumentAttribute>().Description}"
					};
					break;

				default:
					throw new InvalidOperationException();
			}
			
			
			bool result = dialog.ShowDialog(GetActiveWindow()).IsValidAndTrue();
			IList<string> paths = result ? dialog.FileNames.ToList() : null;

			return paths;
		}

		/// <summary>
		/// Requests a document from a given list
		/// </summary>
		/// <param name="documents">The list of documents to select from</param>
		/// <returns>The selected document, or null</returns>
		public BaseDocumentViewModel SelectDocument(IList<BaseDocumentViewModel> documents, string title)
		{
			DocumentSelectDialog dialog = new DocumentSelectDialog(documents)
			{
				Title = title,
				Owner = GetActiveWindow()
			};
			bool? result = dialog.ShowDialog();
			if (result.HasValue && result.Value)
			{
				return dialog.SelectedDocument;
			}

			return null;
		}

		#endregion

		#region Import/Export Files

		/// <summary>
		/// Asks the user for the path to a file that can be imported to a DataSet
		/// </summary>
		/// <param name="startDirectory">The directory to start searching</param>
		/// <param name="filePath">The filepath to the chosen import file</param>
		/// <returns>True if the user selects an importable file, false otherwise</returns>
		public bool GetDataSetImportFile(string startDirectory, out string filePath)
		{
			OpenFileDialog dialog = new OpenFileDialog
			{
				AddExtension = true,
				CheckFileExists = false,
				Multiselect = false,
				InitialDirectory = startDirectory,
				RestoreDirectory = true,
				Filter = DataSetImportFileFilter
			};

			bool result = dialog.ShowDialog(GetActiveWindow()).IsValidAndTrue();
			filePath = result ? dialog.FileName : null;

			return result;
		}

		/// <summary>
		/// Asks the user for the path to a file to which a DataSet can be exported
		/// </summary>
		/// <param name="startDirectory">The directory to start searching</param>
		/// <param name="filePath">The filepath of the chosen export file</param>
		/// <returns>True if the user selects a file, false otherwise</returns>
		public bool GetDataSetExportFile(string startDirectory, out string filePath)
		{
			SaveFileDialog dialog = new SaveFileDialog
			{
				AddExtension = true,
				InitialDirectory = startDirectory,
				OverwritePrompt = true,
				RestoreDirectory = true,
				Filter = DataSetExportFileFilter
			};

			if (dialog.ShowDialog(GetActiveWindow()).IsValidAndTrue())
			{
				filePath = dialog.FileName;
				return true;
			}

			filePath = null;
			return false;
		}

		#endregion

		#region Directories

		/// <summary>
		/// Requests the path to a directory
		/// </summary>
		/// <param name="startDirectory">The directory to start searching</param>
		/// <param name="finalDirectory">The directory the user actually selected</param>
		/// <returns>True if a directory was selected, false otherwise</returns>
		public bool GetDirectory(string startDirectory, out string finalDirectory)
		{
			FolderBrowserDialog dialog = new FolderBrowserDialog
			{
				SelectedPath = startDirectory,
				ShowNewFolderButton = true
			};
			DialogResult result = dialog.ShowDialog(GetActiveWindow().GetIWin32Window());
			if (result == DialogResult.OK)
			{
				finalDirectory = dialog.SelectedPath;
				return true;
			}

			finalDirectory = null;
			return false;
		}

		#endregion

		#region Files

		/// <summary>
		/// Checks whether or not a file exists
		/// </summary>
		/// <param name="path">The full path to the file</param>
		/// <returns>True if the file exists, false otherwise</returns>
		public bool Exists(string path)
		{
			FileInfo info = new FileInfo(path);

			return info.Exists;
		}

		/// <summary>
		/// Checks whether the file is read-only or not
		/// </summary>
		/// <param name="path">The full path to the file</param>
		/// <returns>True if the file is read-only, false otherwise</returns>
		public bool IsReadOnly(string path)
		{
			FileInfo info = new FileInfo(path);

			return info.IsReadOnly;
		}

		/// <summary>
		/// Opens a local file
		/// </summary>
		/// <param name="path">The path to the file</param>
		/// <returns>A stream that references the file</returns>
		/// <remarks>File opening can be platform-specific, so FileStream is unavailable in PCLs</remarks>
		public Stream ReadLocalFile(string path)
		{
			return new FileStream(path, FileMode.Open);
		}

		/// <summary>
		/// Opens or creates a local file for writing
		/// </summary>
		/// <param name="path">The path to the file</param>
		/// <param name="create">If true, the file will be created. Otherwise the file is assumed to exist.</param>
		/// <returns>A stream suitable for writing to the defined file</returns>
		public Stream WriteLocalFile(string path, bool create)
		{
			return create ? File.Create(path) : new FileStream(path, FileMode.Truncate);
		}

		/// <summary>
		/// Deletes a file from the file system
		/// </summary>
		/// <param name="path">The path of the file to be deleted</param>
		/// <returns>True if it was successfully deleted, false otherwise</returns>
		public bool DeleteLocalFile(string path)
		{
			FileInfo info = new FileInfo(path);

			if (info.Exists)
				info.Delete();

			return false;
		}

		/// <summary>
		/// Gets the directory separating character
		/// </summary>
		/// <remarks>Path.DirectorySeparatorChar is platform-specific, so unavailable in PCLs</remarks>
		public char DirectorySeparatorChar => Path.DirectorySeparatorChar;

		/// <summary>
		/// Gets the alternate directory separating character
		/// </summary>
		/// <remarks>Path.AltDirectorySeparatorChar is platform-specific, so unavailable in PCLs</remarks>
		public char AltDirectorySeparatorChar => Path.AltDirectorySeparatorChar;

		#endregion

		#region Private Helpers

		/// <summary>
		/// Returns open dialog file filter for supported image types
		/// </summary>
		private string ImageFileFilter
		{
			get
			{
				StringBuilder filter = new StringBuilder();
				List<string> imgExts = Core.Util.File.SupportedImageExtensions.ToList();
				for (int i = 0; i < imgExts.Count; i++)
				{
					filter.Append($"*{imgExts[i]}");

					if (i < imgExts.Count - 1)
						filter.Append(";");
				}

				return $"Supported Image files ({filter})|{filter}";
			}
		}

		/// <summary>
		/// Gets open dialog file filter for supported data set import file types
		/// </summary>
		private string DataSetImportFileFilter
		{
			get
			{
				StringBuilder filter = new StringBuilder();
				List<string> dataSetImportExts = Core.Util.File.SupportedDataSetImportExtensions.ToList();
				for (int i = 0; i < dataSetImportExts.Count; i++)
				{
					filter.Append($"*{dataSetImportExts[i]}");

					if (i < dataSetImportExts.Count - 1)
						filter.Append(";");
				}

				return $"Importable Data Set files ({filter})|{filter}";
			}
		}

		private string DataSetExportFileFilter
		{
			get
			{
				StringBuilder filter = new StringBuilder();
				foreach (Tuple<TypeInfo, DataSetExporterAttribute> tuple in AttributeLocator<DataSetExporterAttribute>.GetAllInstances)
				{
					DataSetExporterAttribute attr = tuple.Item2;
					filter.Append($"{attr.Description}|*{attr.Extension}|");
				}

				return filter.ToString(0, filter.Length - 1); // exclude trailing pipe
			}
		}

		/// <summary>
		/// Returns open dialog file filter for referenceable Card Editor documents
		/// </summary>
		/// <remarks>This should be moved to a platform-specific project</remarks>
		private string ReferenceableDocumentFilter
		{
			get
			{
				StringBuilder filter = new StringBuilder();
				List<Type> docTypes = Core.Util.File.ReferenceableDocumentTypes.ToList();
				for (int i = 0; i < docTypes.Count; i++)
				{
					DocumentAttribute attr = docTypes[i].GetCustomAttribute<DocumentAttribute>();

					filter.Append($"*{attr.Extension}");

					if (i < docTypes.Count - 1)
						filter.Append(";");
				}

				return $"Card Editor documents ({filter})|{filter}";
			}
		}

		/// <summary>
		/// Returns a filter for a specific document
		/// </summary>
		/// <param name="docType">The type of document</param>
		/// <returns>A suitable filter string for the document</returns>
		private string GetDocumentFilter(Type docType)
		{
			DocumentAttribute attr = docType.GetCustomAttribute<DocumentAttribute>();

			return $"{attr.Description} (*{attr.Extension})|*{attr.Extension}";
		}

		/// <summary>
		/// Gets the currently active window
		/// </summary>
		/// <returns>The currently active window</returns>
		public static Window GetActiveWindow()
		{
			return Application.Current.Windows.OfType<Window>().First(x => x.IsActive);
		}

		#endregion
	}

	/// <summary>
	/// Extension methods and helper classes for Windows file system
	/// </summary>
	public static class WindowsExtensions
	{
		public static IWin32Window GetIWin32Window(this Window systemWindow)
		{
			System.Windows.Interop.HwndSource source = PresentationSource.FromVisual(systemWindow) as System.Windows.Interop.HwndSource;
			if (source == null)
				throw new InvalidCastException(@"Unable to get presentation source from Window");

			IWin32Window win = new Win32Wrapper(source.Handle);
			return win;
		}

		/// <summary>
		/// Helper class that wraps a window handle in a WinForms IWin32Window
		/// </summary>
		private class Win32Wrapper : IWin32Window
		{
			private readonly IntPtr _handle;

			public Win32Wrapper(IntPtr handle)
			{
				_handle = handle;
			}

			IntPtr IWin32Window.Handle { get { return _handle; } }
		}
	}
}
