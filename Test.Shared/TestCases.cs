﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections;
using System.Reflection;
using Core.Drawing;

namespace Test.Shared
{
	/// <summary>
	/// Helper class that provides common test cases
	/// </summary>
    public static class TestCases
    {
		/// <summary>
		/// String input values
		/// </summary>
		public static IEnumerable StringInputs
		{
			get
			{
				yield return null;
				yield return string.Empty;
				yield return "test";
			}
		}

		/// <summary>
		/// Point inputs
		/// </summary>
		public static IEnumerable PointInputs
		{
			get
			{
				yield return null;
				yield return new Point();
			}
		}

		/// <summary>
		/// Size inputs
		/// </summary>
		public static IEnumerable SizeInputs
		{
			get
			{
				yield return null;
				yield return new Size();
			}
		}

		/// <summary>
		/// Inputs for valid sizes
		/// </summary>
		public static IEnumerable NonNullSizes
		{
			get
			{
				yield return new Size(0, 0);
				yield return new Size(5, 5);
				yield return new Size(2, 3);
				yield return new Size(-10, -3.8f);
			}
		}

		/// <summary>
		/// Float inputs
		/// </summary>
		public static IEnumerable FloatInputs
		{
			get
			{
				yield return 0.0f;
				yield return 1.0f;
				yield return 100.0f;
				yield return -1.0f;
			}
		}

		/// <summary>
		/// Non-zero float inputs
		/// </summary>
		public static IEnumerable NonZeroFloatInputs
		{
			get
			{
				yield return 1.0f;
				yield return 100.0f;
				yield return -1.0f;
			}
		}

		/// <summary>
		/// Color inputs
		/// </summary>
		public static IEnumerable ColorInputs
		{
			get
			{
				foreach (Color namedColor in NamedColorInputs)
					yield return namedColor;
				yield return Color.FromArgb(0, 255, 255, 255);
				yield return Color.FromArgb(128, 128, 128, 128);
			}
		}

		/// <summary>
		/// Named Color inputs
		/// </summary>
		public static IEnumerable NamedColorInputs
		{
			get
			{
				Type colorType = typeof(Color);
				foreach (PropertyInfo propertyInfo in colorType.GetProperties(BindingFlags.Public | BindingFlags.Static))
				{
					if (propertyInfo.PropertyType == colorType)
					{
						yield return propertyInfo.GetValue(null, null);
					}
				}
			}
		}

		/// <summary>
		/// The names of all Named Colors
		/// </summary>
		public static IEnumerable NamedColorNames
		{
			get
			{
				Type colorType = typeof(Color);
				foreach (PropertyInfo propertyInfo in colorType.GetProperties(BindingFlags.Public | BindingFlags.Static))
				{
					if (propertyInfo.PropertyType == colorType)
					{
						yield return propertyInfo.Name;
					}
				}
			}
		}
	}
}
