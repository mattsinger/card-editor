﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Core.View_Models.Data_Set;
using ReactiveUI;

namespace CardEditor.Views.Data_Set
{
	/// <summary>
	/// Interaction logic for ColorFieldView.xaml
	/// </summary>
	public partial class ColorFieldView : UserControl, IViewFor<ColorFieldViewModel>
	{
		/// <summary>
		/// Dependency property
		/// </summary>
		public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
			typeof(ColorFieldViewModel),
			typeof(ColorFieldView));

		protected static int _openViewCount;
		public static int OpenViewCount
		{
			get => _openViewCount;
			private set
			{
				_openViewCount = value;
				OpenViewCountChanged?.Invoke(null, _openViewCount);
			}
		}

		public static EventHandler<int> OpenViewCountChanged;

		public ColorFieldView()
		{
			InitializeComponent();

			this.WhenAnyValue(_ => _.DataContext)
				.Where(context => context != null)
				.Subscribe(context =>
				{
					ViewModel = context as ColorFieldViewModel;
				});
		}

		#region Implementation of IViewFor

		/// <summary>
		/// </summary>
		object IViewFor.ViewModel
		{
			get => ViewModel;
			set => ViewModel = (ColorFieldViewModel)value;
		}

		#endregion

		#region Implementation of IViewFor<ColorFieldViewModel>

		/// <summary>
		/// The ViewModel corresponding to this specific View. This should be
		/// a DependencyProperty if you're using XAML.
		/// </summary>
		public ColorFieldViewModel ViewModel
		{
			get => (ColorFieldViewModel) GetValue(ViewModelProperty);
			set
			{
				SetValue(ViewModelProperty, value);
				if (value != null)
					BindToViewModel();
			}
		}

		#endregion

		#region Private Methods

		private void BindToViewModel()
		{
			ColorPicker.SelectedColor = Color.FromArgb(ViewModel.Color.A, ViewModel.Color.R, ViewModel.Color.G, ViewModel.Color.B);
			this.WhenAnyValue(v => v.ColorPicker.SelectedColor)
				.Where(color => color.HasValue)
				.Subscribe(color =>
				{
					if (!color.HasValue)
						throw new NullReferenceException("Somehow ended up with a null color even though we should have filtered it out");

					ViewModel.Color = Core.Drawing.Color.FromArgb(color.Value.A, color.Value.R, color.Value.G, color.Value.B);
				});
			ColorPicker.Opened += (sender, args) =>
			{
				OpenViewCount++;
			};
			ColorPicker.Closed += (sender, args) =>
			{
				OpenViewCount--;
			};
		}

		#endregion
	}
}
