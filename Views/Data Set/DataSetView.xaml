﻿<UserControl x:Class="CardEditor.Views.Data_Set.DataSetView"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:converters="clr-namespace:CardEditor.Converters"
             xmlns:cardPreview1="clr-namespace:CardEditor.Views.Card_Preview"
             xmlns:dataSet="clr-namespace:CardEditor.Views.Data_Set"
             x:Name="DataSetControl"
			 HorizontalAlignment="Stretch" VerticalAlignment="Stretch"
			 KeyDown="KeyDownHandler">
	<UserControl.Resources>
		<converters:LayoutPickerConverter x:Key="LayoutPickerConverter" />
        <converters:DataSetHeaderConverter x:Key="DataSetHeaderConverter" />
        <Style x:Key="{x:Static dataSet:DataSetView.HeaderStyleKey}" TargetType="DataGridColumnHeader">
            <EventSetter Event="Click" Handler="HeaderClickHandler"/>
            <EventSetter Event="MouseDoubleClick" Handler="HeaderDoubleClickHandler"/>
            <Style.Triggers>
                <DataTrigger Value="True">
                    <DataTrigger.Binding>
                        <MultiBinding Mode="OneWay" Converter="{StaticResource DataSetHeaderConverter}">
                            <Binding RelativeSource="{RelativeSource AncestorType=DataGrid}" Path="CurrentColumn.DisplayIndex"/>
                            <Binding RelativeSource="{RelativeSource Self}" Path="Column.DisplayIndex" />
                        </MultiBinding>
                    </DataTrigger.Binding>
                    <Setter Property="FontWeight" Value="Bold"/>
                </DataTrigger>
            </Style.Triggers>
        </Style>
	</UserControl.Resources>
    <Grid>
        <Grid.ColumnDefinitions>
            <ColumnDefinition Width="*" MinWidth="200" />
            <ColumnDefinition Width="*" MinWidth="200" />
        </Grid.ColumnDefinitions>

		<!-- Card Data -->
        <Grid Grid.Column="0">
            <Grid.RowDefinitions>
                <RowDefinition Height="Auto"/>
                <RowDefinition Height="*" />
            </Grid.RowDefinitions>
		    <StackPanel Orientation="Horizontal" Name="ButtonPanel" DockPanel.Dock="Top" Grid.Row="0">
            
                <!-- Add/Remove Cards/Fields -->
			    <Button Name="AddCard" ToolTip="Add Card">
				    <Image Source="/CardEditor;component/Resources/playing-card--plus.png"/>
			    </Button>
			    <Button Name="RemoveCard" ToolTip="Remove Card" Margin="0, 0, 2, 0">
				    <Image Source="/CardEditor;component/Resources/playing-card--minus.png"/>
			    </Button>
			    <Button Name="AddField" ToolTip="Add Field" Click="AddFieldClickHandler">
				    <Image Source="/CardEditor;component/Resources/table-insert-column.png"/>
				    <Button.ContextMenu>
					    <ContextMenu Name="AddFieldMenu">
						    <MenuItem Header="Add Text Field" Name="AddTextField"/>
						    <MenuItem Header="Add Image Field" Name="AddImageField"/>
						    <MenuItem Header="Add Color Field" Name="AddColorField"/>
					    </ContextMenu>
				    </Button.ContextMenu>
			    </Button>
			    <Button Name="RemoveField" ToolTip="Remove Field">
				    <Image Source="/CardEditor;component/Resources/table-delete-column.png"/>
			    </Button>

		        <Separator Margin="5, 0"/>
            
                <!-- Import/Export cards -->
                <Button Name="Import" ToolTip="Import Cards From External File">
                    <Image Source="/CardEditor;component/Resources/table-import.png"/>
                </Button>
                <Button Name="Export" ToolTip="Export Cards To External File">
                    <Image Source="/CardEditor;component/Resources/table-export.png"/>
                </Button>

                <Separator Margin="5, 0"/>
            
                <Button Name="OutputAll" ToolTip="Output All Cards, requires both front and back layouts">
				    <Image Source="/CardEditor;component/Resources/folder--arrow.png"/>
			    </Button>
		    </StackPanel>
		    <DataGrid Name="CardDataGrid" 
				      CanUserReorderColumns="False" CanUserSortColumns="False" 
				      CanUserAddRows="False" CanUserDeleteRows="True"
				      SelectionMode="Single" AutoGenerateColumns="False"
				      HorizontalAlignment="Stretch" VerticalAlignment="Stretch"
				      DataContext="{Binding Path=ViewModel.Cards, ElementName=DataSetControl}"
				      FrozenColumnCount="1"
                      Padding="0,0,4,0"
                      Grid.Row="1">
			    <DataGrid.Columns>
                    <DataGridTextColumn Header="Id" Binding="{Binding Id, UpdateSourceTrigger=PropertyChanged}" MinWidth="60" Width="SizeToCells" IsReadOnly="True">
                        <DataGridTextColumn.ElementStyle>
                            <Style TargetType="{x:Type TextBlock}">
                                <Setter Property="VerticalAlignment" Value="Center" />
                                <EventSetter Event="MouseLeftButtonDown" Handler="IdMouseLeftButtonDownHandler"/>
                            </Style>
                        </DataGridTextColumn.ElementStyle>
                    </DataGridTextColumn>
                </DataGrid.Columns>
		    </DataGrid>
        </Grid>

        <!-- Separator -->
		<GridSplitter Grid.Column="0" HorizontalAlignment="Right" Width="4" VerticalAlignment="Stretch" />

        <!-- Layouts -->
        <Grid Grid.Column="1">
            <Grid.RowDefinitions>
                <RowDefinition Height="Auto" />
                <RowDefinition Height="*" />
            </Grid.RowDefinitions>
            <Grid.ColumnDefinitions>
                <ColumnDefinition Width="*" />
                <ColumnDefinition Width="*" />
            </Grid.ColumnDefinitions>
		    <!-- Front Layout -->
		    <StackPanel Grid.Row="0" Grid.Column="0"
                Orientation="Horizontal" Background="SteelBlue" Height="{Binding ActualHeight, ElementName=ButtonPanel}">
			    <TextBlock Text="Front Layout" Foreground="White" Padding="5, 0, 0, 0" VerticalAlignment="Center"/>
                <Image Source="/CardEditor;component/Resources/exclamation.png" Margin="5,0,0,0" 
                        Name="FrontLayoutWarning" Visibility="Hidden" />
		    </StackPanel>
            <DockPanel Grid.Row="1" Grid.Column="0" LastChildFill="True">
                <DockPanel DockPanel.Dock="Top" Background="White" Height="20">
                    <Button x:Name="OutputFrontLayout" DockPanel.Dock="Right" Margin="5,0,0,0" ToolTip="Output To Image">
                        <Image Source="/CardEditor;component/Resources/image-import.png"/>
                    </Button>
                    <Button x:Name="ClearFrontLayout" DockPanel.Dock="Right" ToolTip="Clear Layout">
                        <Image Source="/CardEditor;component/Resources/cross-button.png"/>
                    </Button>
                    <Button x:Name="SelectFrontLayout"  DockPanel.Dock="Left" ToolTip="Select Layout" HorizontalContentAlignment="Left">
                        <DockPanel>
                            <Image Source="/CardEditor;component/Resources/layers-group.png"
							DockPanel.Dock="Left"/>
                            <TextBlock VerticalAlignment="Center"
							Background="Transparent" Text="{Binding Path=FrontLayout,Converter={StaticResource LayoutPickerConverter}}"
							DockPanel.Dock="Left" Margin="2,0,0,0"/>
                        </DockPanel>
                    </Button>
                </DockPanel>
                <ScrollViewer HorizontalScrollBarVisibility="Visible" DockPanel.Dock="Bottom" Background="{StaticResource CheckerboardBkg}">
                    <cardPreview1:CardPreviewView x:Name="FrontPreview">
                        <cardPreview1:CardPreviewView.ContextMenu>
                            <ContextMenu>
                                <MenuItem Header="Output to Image" />
                            </ContextMenu>
                        </cardPreview1:CardPreviewView.ContextMenu>
                    </cardPreview1:CardPreviewView>
                </ScrollViewer>
            </DockPanel>
            
            <!-- Back Layout -->
            <StackPanel Grid.Row="0" Grid.Column="1"
                Orientation="Horizontal" Background="SteelBlue" Height="{Binding ActualHeight, ElementName=ButtonPanel}">
                <TextBlock Text="Back Layout" Foreground="White" Padding="5, 0, 0, 0" VerticalAlignment="Center"/>
		        <Image Source="/CardEditor;component/Resources/exclamation.png" Margin="5,0,0,0" 
		                Name="BackLayoutWarning" Visibility="Hidden" />
            </StackPanel>
		    <DockPanel LastChildFill="True" Grid.Row="1" Grid.Column="1">
			    <DockPanel DockPanel.Dock="Top" Background="White" LastChildFill="True" Height="20">
				    <Button x:Name="OutputBackLayout" DockPanel.Dock="Right" Margin="5,0,0,0" ToolTip="Output To Image">
					    <Image Source="/CardEditor;component/Resources/image-import.png" />
				    </Button>
				    <Button x:Name="ClearBackLayout" DockPanel.Dock="Right" ToolTip="Clear Layout">
					    <Image Source="/CardEditor;component/Resources/cross-button.png"/>
				    </Button>
				    <Button x:Name="SelectBackLayout" DockPanel.Dock="Right" ToolTip="Select Layout" HorizontalContentAlignment="Left">
					    <DockPanel>
						    <Image Source="/CardEditor;component/Resources/layers-group.png" 
							        DockPanel.Dock="Left"/>
						    <TextBlock HorizontalAlignment="Stretch" VerticalAlignment="Center"
						           Background="Transparent" Text="{Binding Path=BackLayout,Converter={StaticResource LayoutPickerConverter}}"
								           DockPanel.Dock="Left" Margin="2,0,0,0"/>
					        </DockPanel>
				        </Button>
			        </DockPanel>
			        <ScrollViewer HorizontalScrollBarVisibility="Visible" DockPanel.Dock="Bottom" Background="{StaticResource CheckerboardBkg}">
				        <cardPreview1:CardPreviewView x:Name="BackPreview">
					        <cardPreview1:CardPreviewView.ContextMenu>
						        <ContextMenu>
							        <MenuItem Header="Output to Image"/>
						        </ContextMenu>
					        </cardPreview1:CardPreviewView.ContextMenu>
				        </cardPreview1:CardPreviewView>
			        </ScrollViewer>
		        </DockPanel>
        </Grid>
    </Grid>
</UserControl>































































