﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Core.View_Models.Data_Set;
using ReactiveUI;

namespace CardEditor.Views.Data_Set
{
	/// <summary>
	/// Interaction logic for TextFieldView.xaml
	/// </summary>
	public partial class TextFieldView : UserControl, IViewFor<TextFieldViewModel>
	{
		/// <summary>
		/// Dependency property
		/// </summary>
		public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
			typeof(TextFieldViewModel),
			typeof(TextFieldView));

		/// <summary>
		/// Constructs a TextFieldView
		/// </summary>
		public TextFieldView()
		{
			InitializeComponent();

			this.WhenAnyValue(_ => _.DataContext)
				.Subscribe(context =>
				{
					ViewModel = context as TextFieldViewModel;
				});
		}

		#region Implementation of IViewFor

		/// <summary>
		/// </summary>
		object IViewFor.ViewModel
		{
			get => ViewModel;
			set => ViewModel = (TextFieldViewModel)value;
		}

		#endregion

		#region Implementation of IViewFor<TextFieldViewModel>

		/// <summary>
		/// The ViewModel corresponding to this specific View. This should be
		/// a DependencyProperty if you're using XAML.
		/// </summary>
		public TextFieldViewModel ViewModel
		{
			get => (TextFieldViewModel) GetValue(ViewModelProperty);
			set
			{
				SetValue(ViewModelProperty, value);
				BindToViewModel();
			}
		}

		#endregion

		#region Private Methods

		private void BindToViewModel()
		{
			this.Bind(ViewModel, vm => vm.Text, v => v.TextValue.Text);
		}

		#endregion

		private void OnPreviewKeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Escape)
				Keyboard.ClearFocus();
		}
	}
}
