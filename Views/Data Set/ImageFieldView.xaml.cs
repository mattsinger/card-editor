﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Core.View_Models.Data_Set;
using ReactiveUI;

namespace CardEditor.Views.Data_Set
{
	/// <summary>
	/// Interaction logic for ImageFieldView.xaml
	/// </summary>
	public partial class ImageFieldView : UserControl, IViewFor<ImageFieldViewModel>
	{
		/// <summary>
		/// Dependency property
		/// </summary>
		public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
			typeof(ImageFieldViewModel),
			typeof(ImageFieldView));

		/// <summary>
		/// Constructs an ImageFieldView
		/// </summary>
		public ImageFieldView()
		{
			InitializeComponent();

			this.WhenAnyValue(_ => _.DataContext)
				.Where(context => context != null)
				.Subscribe(context =>
				{
					ViewModel = context as ImageFieldViewModel;
				});
		}

		#region Implementation of IViewFor

		/// <summary>
		/// </summary>
		object IViewFor.ViewModel
		{
			get => ViewModel;
			set => ViewModel = (ImageFieldViewModel)value;
		}

		#endregion

		#region Implementation of IViewFor<ImageFieldViewModel>

		/// <summary>
		/// The ViewModel corresponding to this specific View. This should be
		/// a DependencyProperty if you're using XAML.
		/// </summary>
		public ImageFieldViewModel ViewModel
		{
			get => (ImageFieldViewModel) GetValue(ViewModelProperty);
			set
			{
				SetValue(ViewModelProperty, value);
				BindToViewModel();
			}
		}

		#endregion

		#region Private Methods

		private void BindToViewModel()
		{
			this.BindCommand(ViewModel, vm => vm.SelectPath, v => v.SelectFileButton);
			this.Bind(ViewModel, vm => vm.Path, v => v.FilePathText.Text);
		}

		private void OnSelectFile(object sender, RoutedEventArgs e)
		{
			ICommand command = ViewModel?.SelectPath;
			command?.Execute(null);
		}

		#endregion

		private void OnTextChanged(object sender, TextChangedEventArgs e)
		{
			ViewModel.Path = (sender as TextBox).Text;
		}
	}
}
