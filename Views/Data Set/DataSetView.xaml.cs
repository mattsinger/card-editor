﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Reactive;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using CardEditor.Dialogs;
using CardEditor.Util;
using Core.Documents;
using Core.Drawing;
using Core.Util;
using Core.View_Models.Data_Set;
using ReactiveUI;
using Image = Core.Drawing.Image;

namespace CardEditor.Views.Data_Set
{
	/// <summary>
	/// Interaction logic for DataSetView.xaml
	/// </summary>
	public partial class DataSetView : UserControl, IViewFor<DataSetViewModel>
	{
		private readonly List<IDisposable> _subscriptions = new List<IDisposable>();
		private readonly ReorderHelper _reorderHelper;

	    public const string HeaderStyleKey = "FieldHeaderStyle";

		/// <summary>
		/// Dependency property
		/// </summary>
		public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
			typeof(DataSetViewModel),
			typeof(DataSetView));

		/// <summary>
		/// Constructs a DataSetView
		/// </summary>
		public DataSetView()
		{
			InitializeComponent();

			this.WhenAnyValue(_ => _.ViewModel).BindTo(this, _ => _.DataContext);
			this.OneWayBind(ViewModel, vm => vm.Cards, v => v.CardDataGrid.ItemsSource);
			this.Bind(ViewModel, vm => vm.SelectedCard, v => v.CardDataGrid.SelectedItem);

			this.BindCommand(ViewModel, vm => vm.AddCard, v => v.AddCard);
			this.BindCommand(ViewModel, vm => vm.RemoveSelectedCard, v => v.RemoveCard);
			this.BindCommand(ViewModel, vm => vm.RemoveSelectedField, v => v.RemoveField);

			this.BindCommand(ViewModel, vm => vm.Import, v => v.Import);
			this.BindCommand(ViewModel, vm => vm.Export, v => v.Export);

			this.BindCommand(ViewModel, 
				vm => vm.AddField, 
				v => v.AddTextField, 
				Observable.Return(FieldType.Text));
			this.BindCommand(ViewModel,
				vm => vm.AddField,
				v => v.AddImageField,
				Observable.Return(FieldType.Image));
			this.BindCommand(ViewModel,
				vm => vm.AddField,
				v => v.AddColorField,
				Observable.Return(FieldType.Color));

			this.BindCommand(ViewModel, vm => vm.SelectFrontLayout, v => v.SelectFrontLayout);
			this.BindCommand(ViewModel, vm => vm.SelectBackLayout, v => v.SelectBackLayout);
			this.BindCommand(ViewModel, vm => vm.ClearFrontLayout, v => v.ClearFrontLayout);
			this.BindCommand(ViewModel, vm => vm.ClearBackLayout, v => v.ClearBackLayout);
			this.BindCommand(ViewModel, vm => vm.OutputCardFrontToFile, v => v.OutputFrontLayout);
			this.BindCommand(ViewModel, vm => vm.OutputCardBackToFile, v => v.OutputBackLayout);
			this.BindCommand(ViewModel, vm => vm.BatchOutputCardsToFile, v => v.OutputAll);

			CardDataGrid.Loaded += CardDataGridLoadedHandler;

			_reorderHelper = new ReorderHelper(CardDataGrid);
			_reorderHelper.ItemReordered += (sender, args) =>
			{
				ViewModel.SelectedCard = args.Item as CardDataViewModel;
			};

			Loaded += (v, args) =>
			{
				ColorFieldView.OpenViewCountChanged += HandleColorFieldOpenViewCountChanged;
			};
			Unloaded += (v, args) =>
			{
				ColorFieldView.OpenViewCountChanged -= HandleColorFieldOpenViewCountChanged;
			};

		}

		#region Implementation of IViewFor

		/// <summary>
		/// </summary>
		object IViewFor.ViewModel
		{
			get => ViewModel;
			set => ViewModel = (DataSetViewModel)value;
		}

		#endregion

		#region Implementation of IViewFor<DataSetViewModel>

		/// <summary>
		/// The ViewModel corresponding to this specific View. This should be
		/// a DependencyProperty if you're using XAML.
		/// </summary>
		public DataSetViewModel ViewModel
		{
			get => (DataSetViewModel) GetValue(ViewModelProperty);
			set
			{
				SetValue(ViewModelProperty, value);
				BindToViewModel();
			}
		}

		#endregion

		#region Private Methods

		private void BindToViewModel()
		{
			_subscriptions.ForEach(_ => _.Dispose());
			_subscriptions.Clear();

			if (ViewModel == null)
				return;

			ViewModel.FrontPreview.Ppi = App.Ppi;
			ViewModel.BackPreview.Ppi = App.Ppi;
			FrontPreview.ViewModel = ViewModel.FrontPreview;
			BackPreview.ViewModel = ViewModel.BackPreview;

			_subscriptions.Add(ViewModel.WhenAnyValue(vm => vm.FrontLayoutError)
				.SubscribeOn(RxApp.MainThreadScheduler)
				.Subscribe(err =>
				{
					UpdateLayoutError(FrontLayoutWarning, err);
				}));
			_subscriptions.Add(ViewModel.WhenAnyValue(vm => vm.BackLayoutError)
				.SubscribeOn(RxApp.MainThreadScheduler)
				.Subscribe(err =>
				{
					UpdateLayoutError(BackLayoutWarning, err);
				}));

			BindCardData();
		}

		private void BindCardData()
		{
			_subscriptions.Add(ViewModel.FieldHeaders.Changed.Subscribe(_ =>
			{
				InitHeaders();
			}));
            _subscriptions.Add(ViewModel.FieldHeaders.ItemChanged.Subscribe(_ =>
            {
                InitHeaders();
            }));

			_subscriptions.Add(ViewModel.GetCardId.RegisterHandler(interaction =>
			{
				interaction.SetOutput(GetNewText("Enter new card ID"));
			}));
			_subscriptions.Add(ViewModel.DuplicateCardId.RegisterHandler(_ =>
			{
				MessageBox.Show(Application.Current.MainWindow, 
					"That card Id is already in use", "Duplicate card Id", MessageBoxButton.OK, MessageBoxImage.Warning);
				_.SetOutput(new Unit());
			}));

			_subscriptions.Add(ViewModel.GetFieldName.RegisterHandler(interaction =>
			{
				interaction.SetOutput(GetNewText("Enter new text field name"));
			}));
			_subscriptions.Add(ViewModel.DuplicateFieldName.RegisterHandler(_ =>
			{
				MessageBox.Show(Application.Current.MainWindow,
					"That field name is already in use in this Data Set", "Duplicate field name", MessageBoxButton.OK, MessageBoxImage.Warning);
				_.SetOutput(new Unit());
			}));
			_subscriptions.Add(ViewModel.FailedToOutputCard.RegisterHandler(_ =>
			{
				MessageBox.Show(Application.Current.MainWindow,
					"The card could not be output to file. See the logs for details.", "Unable to output card", MessageBoxButton.OK, MessageBoxImage.Warning);
				_.SetOutput(new Unit());
			}));
			_subscriptions.Add(ViewModel.GetOutputParams.RegisterHandler(context =>
			{
				OutputParams outputParams = context.Input.Item2;
				if (outputParams == null)
				{
					outputParams = new OutputParams
					{
						Ppi = OutputParams.PrintResolution,
						BackOrientation = OutputParams.Orientation.Zero,
						FrontOrientation = OutputParams.Orientation.Zero,
						IncludeBleed = true,
						Format = Image.Format.Png
					};
					if (ViewModel.FrontLayout != null)
					{
						outputParams.FrontOrientation = ViewModel.FrontLayout.IsLandscape ?
							OutputParams.Orientation.Ninety :
							OutputParams.Orientation.Zero;
					}

					if (ViewModel.BackLayout != null)
					{
						outputParams.BackOrientation = ViewModel.BackLayout.IsLandscape ?
							OutputParams.Orientation.Ninety :
							OutputParams.Orientation.Zero;
					}
				}

				OutputParamsDialog dialog = new OutputParamsDialog(outputParams)
				{
					RequiredOrientations = context.Input.Item1,
					StartPath = ViewModel.Workspace.RootPath,
					Owner = Application.Current.MainWindow
				};

				context.SetOutput(dialog.ShowDialog().IsValidAndTrue() ? dialog.OutputParams : null);
			}));
			_subscriptions.Add(ViewModel.GetImportData.RegisterHandler(context =>
			{
				ImportDataSetDialog dialog = new ImportDataSetDialog
				{
					ViewModel = context.Input,
					Owner = WindowsFileSystem.GetActiveWindow()
				};

				bool doImport = dialog.ShowDialog().IsValidAndTrue();
				context.SetOutput(doImport);
			}));

			// column 0 is reserved for the Card Id, fields start at column 1
			_subscriptions.Add(this.WhenAnyValue(v => v.CardDataGrid.CurrentCell.Column.DisplayIndex)
				.Select(index => index - 1)
				.BindTo(ViewModel, vm => vm.SelectedFieldIndex));

		    Application.Current.Dispatcher.InvokeAsync(InitHeaders);
		}

		private void InitHeaders()
		{
			// remove all columns except the first, which is reserved for Id
			while (CardDataGrid.Columns.Count > 1)
			{
				CardDataGrid.Columns.RemoveAt(CardDataGrid.Columns.Count - 1);
			}

			if (ViewModel == null)
				return;

			for (int i = 0; i < ViewModel.FieldHeaders.Count; i++)
			{
				DataGridTemplateColumn column = new DataGridTemplateColumn();
				DataTemplate dataTemplate = new DataTemplate();
				switch (ViewModel.FieldHeaders[i].FieldType)
				{
					case FieldType.Text:
						{
							FrameworkElementFactory factory = new FrameworkElementFactory(typeof(TextFieldView));
							factory.SetBinding(DataContextProperty,
								new Binding($"Fields[{i}]"));
							dataTemplate.VisualTree = factory;
							dataTemplate.DataType = typeof(TextFieldViewModel);
						}
						break;

					case FieldType.Image:
						{
							FrameworkElementFactory factory = new FrameworkElementFactory(typeof(ImageFieldView));
							factory.SetBinding(DataContextProperty,
								new Binding($"Fields[{i}]"));
							dataTemplate.VisualTree = factory;
							dataTemplate.DataType = typeof(ImageFieldViewModel);
						}
						break;

					case FieldType.Color:
						{
							FrameworkElementFactory factory = new FrameworkElementFactory(typeof(ColorFieldView));
							factory.SetBinding(DataContextProperty,
								new Binding($"Fields[{i}]"));
							dataTemplate.VisualTree = factory;
							dataTemplate.DataType = typeof(ColorFieldViewModel);
						}
						break;

					default:
						throw new NotSupportedException("Only text and image fields supported");
				}

				column.Header = ViewModel.FieldHeaders[i].Key;
				column.MinWidth = column.ActualWidth > 0 ? column.ActualWidth : 100;
				column.Width = new DataGridLength(1, DataGridLengthUnitType.Star);
				column.CellTemplate = dataTemplate;
                column.HeaderStyle = FindResource(HeaderStyleKey) as Style;

				CardDataGrid.Columns.Add(column);

			    if (ViewModel.SelectedFieldIndex == i + 1)
			        CardDataGrid.CurrentColumn = column;
			}
		}

		private string GetNewText(string title)
		{
			TextEntryDialog dialog = new TextEntryDialog
			{
				Title = title,
				Owner = Application.Current.MainWindow
			};
			dialog.ShowDialog();

			if (dialog.DialogResult.HasValue && dialog.DialogResult.Value)
				return dialog.Text;

			return null;
		}

		private void UpdateLayoutError(FrameworkElement element, DataSetViewModel.LayoutError error)
		{
			switch (error)
			{
				case DataSetViewModel.LayoutError.None:
					element.Visibility = Visibility.Hidden;
					break;

				case DataSetViewModel.LayoutError.NoLayoutSelected:
					element.Visibility = Visibility.Visible;
					element.ToolTip = Properties.Resources.ResourceManager.GetString("NoLayoutError");
					break;

				case DataSetViewModel.LayoutError.LayoutSizeMismatch:
					element.Visibility = Visibility.Visible;
					element.ToolTip = Properties.Resources.ResourceManager.GetString("LayoutSizeMismatch");
					break;
			}
		}

		#endregion

		#region WPF Handlers

		private void HandleColorFieldOpenViewCountChanged(object sender, int openViewCount)
		{
			_reorderHelper.Enabled = openViewCount <= 0;
		}

		private void AddFieldClickHandler(object sender, RoutedEventArgs e)
		{
			AddFieldMenu.IsOpen = true;
		}

		private void CardDataGridLoadedHandler(object sender, RoutedEventArgs e)
		{
			foreach (DataGridColumn column in CardDataGrid.Columns)
			{
				column.Width = new DataGridLength(CardDataGrid.ActualWidth / CardDataGrid.Columns.Count, DataGridLengthUnitType.SizeToCells);
			}
		}

		private void KeyDownHandler(object sender, KeyEventArgs args)
		{
			if (args.Key == Key.Delete && ViewModel.SelectedCard != null)
			{
				ICommand command = ViewModel.RemoveSelectedCard;
				command.Execute(null);
				args.Handled = true;
			}
        }

        private void HeaderClickHandler(object sender, RoutedEventArgs e)
        {
            DataGridColumnHeader clickedHeader = sender as DataGridColumnHeader;
            if (clickedHeader == null)
                throw new NullReferenceException("We expected a header to be clicked, but it was not a header!");

            ViewModel.SelectedFieldIndex = clickedHeader.Column.DisplayIndex - 1;   // adjusting for Id column
            CardDataGrid.CurrentColumn = clickedHeader.Column;
        }

        private void HeaderDoubleClickHandler(object sender, MouseButtonEventArgs e)
        {
            ICommand command = ViewModel.RenameSelectedField;
            command.Execute(null);
        }

        private void IdMouseLeftButtonDownHandler(object sender, MouseButtonEventArgs args)
        {
            if (args.ClickCount != 2)
                return;

            ICommand command = ViewModel.RenameSelectedCard;
            command.Execute(null);
		}

		#endregion
	}
}
