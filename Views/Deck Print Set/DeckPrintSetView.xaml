﻿<UserControl x:Class="CardEditor.Views.Deck_Print_Set.DeckPrintSetView"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:xctk="http://schemas.xceed.com/wpf/xaml/toolkit"
             xmlns:converters="clr-namespace:CardEditor.Converters"
             x:Name="DeckPrintSetControl">
    <UserControl.Resources>
        <converters:CardCountGroupingConverter x:Key="GroupingConverter" />
        <converters:CardCountFlipBackSideConverter x:Key="FlipBackSideConverter"/>
        <Style x:Key="DataSetHeaderStyle" TargetType="{x:Type GroupItem}">
            <Setter Property="Template">
                <Setter.Value>
                    <ControlTemplate TargetType="{x:Type GroupItem}">
                        <Expander IsExpanded="True" Foreground="Black" Header="{Binding}" HorizontalAlignment="Stretch" HorizontalContentAlignment="Stretch">
                            <Expander.HeaderTemplate>
                                <DataTemplate>
                                    <DockPanel LastChildFill="True" HorizontalAlignment="{Binding Path=HorizontalAlignment, RelativeSource={RelativeSource FindAncestor, AncestorType={x:Type ContentPresenter}}, Mode=OneWayToSource}">
                                        <CheckBox Checked="FlipBackSide_OnChecked" Unchecked="FlipBackSide_OnChecked"  DataContext="{Binding Path=Items[0]}"
                                                  DockPanel.Dock="Right" 
                                                  IsChecked="{Binding Converter={StaticResource FlipBackSideConverter}, Mode=OneWay}" />
                                        <TextBlock Text="Flip back side " DockPanel.Dock="Right" />
                                        <TextBlock FontWeight="Bold" 
                                                   Text="{Binding Path=Items[0], Converter={StaticResource GroupingConverter}}"
                                                   HorizontalAlignment="Left" />
                                    </DockPanel>
                                </DataTemplate>
                            </Expander.HeaderTemplate>
                            <Expander.Content>
                                <ItemsPresenter />
                            </Expander.Content>
                        </Expander>
                    </ControlTemplate>
                </Setter.Value>
            </Setter>
        </Style>
    </UserControl.Resources>
    <Grid>
        <Grid.ColumnDefinitions>
            <ColumnDefinition Width="*" MinWidth="100"/>
            <ColumnDefinition Width="4" />
            <ColumnDefinition Width="3*" />
        </Grid.ColumnDefinitions>
        
        <!-- Card Counts -->
        <Grid Grid.Column="0">
            <Grid.RowDefinitions>
                <RowDefinition Height="Auto" />
                <RowDefinition Height="*"/>
            </Grid.RowDefinitions>
            <StackPanel Grid.Row="0" Orientation="Horizontal">
                <Button Name="AddDataSet" ToolTip="Add Data Set">
                    <Image Source="/CardEditor;component/Resources/table--plus.png"/>
                </Button>
                <Button Name="RemoveSelectedDataSet" ToolTip="Remove Selected Data Set">
                    <Image Source="/CardEditor;component/Resources/table--minus.png"/>
                </Button>
                <Rectangle Width="4"/>
                <Button Name="Print" Click="Print_OnClick">
                    <Image Name="PrintImage" Source="/CardEditor;component/Resources/printer.png" />
                </Button>
            </StackPanel>
            <DataGrid Name="CardCounts" Grid.Row="1" HorizontalAlignment="Stretch"
                      CanUserAddRows="False" CanUserDeleteRows="False" CanUserReorderColumns="False"
                      AutoGenerateColumns="False"
                      DataContext="{Binding ViewModel.CardCounts, ElementName=DeckPrintSetControl}">
                <DataGrid.GroupStyle>
                    <GroupStyle ContainerStyle="{StaticResource DataSetHeaderStyle}" />
                </DataGrid.GroupStyle>
                <DataGrid.Columns>
                    <DataGridTextColumn Header="Card" Binding="{Binding Id}" MinWidth="60" Width="SizeToCells" IsReadOnly="True" />
                    <DataGridTemplateColumn Header="Count" Width="SizeToCells" MinWidth="60">
                        <DataGridTemplateColumn.CellTemplate>
                            <DataTemplate>
                                <xctk:IntegerUpDown Minimum="0" Value="{Binding Count}" ValueChanged="OnCountChanged"/>
                            </DataTemplate>
                        </DataGridTemplateColumn.CellTemplate>
                    </DataGridTemplateColumn>
                </DataGrid.Columns>
            </DataGrid>
        </Grid>

        <!-- Separator -->
        <GridSplitter Grid.Column="1" HorizontalAlignment="Stretch" VerticalAlignment="Stretch" />
        
        <!-- Print Layout -->
        <DockPanel Grid.Column="2" LastChildFill="True">
            <StackPanel DockPanel.Dock="Left" Background="LightGray">
                <TextBlock Background="SteelBlue" Foreground="White" Text="Print Layout" Margin="2 0 0 0"/>
                <Grid>
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="Auto" />
                        <ColumnDefinition Width="*" />
                    </Grid.ColumnDefinitions>
                    <Grid.RowDefinitions>
                        <RowDefinition Height="Auto" />
                        <RowDefinition Height="Auto" />
                        <RowDefinition Height="Auto" />
                        <RowDefinition Height="Auto" />
                        <RowDefinition Height="Auto" />
                    </Grid.RowDefinitions>
                    
                    <!-- Page Size -->
                    <TextBlock Grid.Row="0" Grid.Column="0" Margin="2" Text="Page Size"/>
                    <StackPanel Grid.Row="0" Grid.Column="1" VerticalAlignment="Stretch" HorizontalAlignment="Left" Orientation="Horizontal">
                        <xctk:DoubleUpDown ShowButtonSpinner="False" Width="50" Minimum="1" VerticalContentAlignment="Center" VerticalAlignment="Stretch"
                                           Name="PageWidth"/>
                        <TextBlock Text="&quot; x " />
                        <xctk:DoubleUpDown ShowButtonSpinner="False" Width="50" Minimum="1" VerticalContentAlignment="Center" VerticalAlignment="Stretch"
                                           Name="PageHeight"/>
                        <TextBlock Text="&quot; " />
                    </StackPanel>

                    <!-- Spacing -->
                    <TextBlock Grid.Row="1" Grid.Column="0" Margin="2" Text="Spacing"/>
                    <StackPanel Grid.Row="1" Grid.Column="1" VerticalAlignment="Stretch" HorizontalAlignment="Left" Orientation="Horizontal">
                        <xctk:DoubleUpDown ShowButtonSpinner="False" Width="50" Minimum="0" VerticalContentAlignment="Center" VerticalAlignment="Stretch"
                                           Name="HSpacing"/>
                        <TextBlock Text="&quot; x " />
                        <xctk:DoubleUpDown ShowButtonSpinner="False" Width="50" Minimum="0" VerticalContentAlignment="Center" VerticalAlignment="Stretch"
                                           Name="VSpacing"/>
                        <TextBlock Text="&quot; " />
                    </StackPanel>

                    <!-- Margins -->
                    <TextBlock Grid.Row="2" Grid.Column="0" Margin="2" Text="Margin"/>
                    <StackPanel Grid.Row="2" Grid.Column="1" VerticalAlignment="Stretch" HorizontalAlignment="Left" Orientation="Horizontal">
                        <xctk:DoubleUpDown ShowButtonSpinner="False" Width="50" Minimum="0" VerticalContentAlignment="Center" VerticalAlignment="Stretch"
                                           Name="HMargin"/>
                        <TextBlock Text="&quot; x " />
                        <xctk:DoubleUpDown ShowButtonSpinner="False" Width="50" Minimum="0" VerticalContentAlignment="Center" VerticalAlignment="Stretch"
                                           Name="VMargin"/>
                        <TextBlock Text="&quot; " />
                    </StackPanel>

                    <!-- Include Bleed -->
                    <TextBlock Grid.Row="3" Grid.Column="0" Margin="2" Text="Include Bleed"/>
                    <CheckBox Grid.Row="3" Grid.Column="1" VerticalAlignment="Center" Name="IncludeBleed"/>

                    <!-- Landscape -->
                    <TextBlock Grid.Row="4" Grid.Column="0" Margin="2" Text="Landscape"/>
                    <CheckBox Grid.Row="4" Grid.Column="1" VerticalAlignment="Center" Name="UseLandscape"/>
                </Grid>
            </StackPanel>
            <ScrollViewer DockPanel.Dock="Right" HorizontalScrollBarVisibility="Visible" Focusable="True"
                          Background="{StaticResource CheckerboardBkg}">
                <Canvas Name="PageLayout" Background="White" />
            </ScrollViewer>
        </DockPanel>
    </Grid>
</UserControl>
