﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.ComponentModel;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Util;
using Core.View_Models.Deck_Print_Set;
using ReactiveUI;

namespace CardEditor.Views.Deck_Print_Set
{
	/// <summary>
	/// Interaction logic for DeckPrintSetView.xaml
	/// </summary>
	public partial class DeckPrintSetView : UserControl, IViewFor<DeckPrintSetViewModel>
	{
		#region Dependency Properties

		public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
			typeof(DeckPrintSetViewModel),
			typeof(DeckPrintSetView));

		#endregion

		#region Constructors & Finalizer

		public DeckPrintSetView()
		{
			InitializeComponent();

			ToolTipService.SetShowOnDisabled(Print, true);

			this.WhenActivated(d =>
			{
				d.Add(this.WhenAnyValue(_ => _.ViewModel).BindTo(this, _ => _.DataContext));

				d.Add(this.BindCommand(ViewModel, vm => vm.AddDataSet, v => v.AddDataSet));
				d.Add(this.BindCommand(ViewModel, vm => vm.RemoveSelectedDataSet, v => v.RemoveSelectedDataSet));
				d.Add(ViewModel.Print.CanExecute
								.ObserveOn(RxApp.MainThreadScheduler)
								.Subscribe(canExecute => Print.IsEnabled = canExecute));
				d.Add(ViewModel.Print.IsExecuting
								.Where(isExecuting => isExecuting == false)
								.ObserveOn(RxApp.MainThreadScheduler)
								.Subscribe(_ => Mouse.OverrideCursor = null));
				
				d.Add(this.OneWayBind(ViewModel, vm => vm.PrintError, v => v.Print.ToolTip,
					error =>
					{
						ToolTip toolTip = new ToolTip();

						switch (error)
						{
							case DeckPrintSetViewModel.CanPrintError.None:
								toolTip.Content = "Print cards";
								break;

							case DeckPrintSetViewModel.CanPrintError.NoCards:
								toolTip.Content = "No cards to print";
								break;

							case DeckPrintSetViewModel.CanPrintError.MissingLayout:
								toolTip.Content = "All referenced Data Sets must define a Front and Back Layout";
								break;

							case DeckPrintSetViewModel.CanPrintError.LayoutMismatch:
								toolTip.Content = "All referenced Data Sets must match card and bleed sizes";
								break;

							default:
								throw new InvalidOperationException("Unknown print error code");
						}

						return toolTip;
					}));
				d.Add(this.OneWayBind(ViewModel, vm => vm.PrintError, v => v.PrintImage.Source,
					error =>
					{
						Uri uriSource = error == DeckPrintSetViewModel.CanPrintError.None ? 
							new Uri(@"/CardEditor;component/Resources/printer.png", UriKind.Relative) : 
							new Uri(@"/CardEditor;component/Resources/printer--exclamation.png", UriKind.Relative);

						return new BitmapImage(uriSource);
					}));

				d.Add(this.OneWayBind(ViewModel, vm => vm.CardCounts, v => v.CardCounts.ItemsSource));
				d.Add(this.Bind(ViewModel, vm => vm.SelectedCardCount, v => v.CardCounts.SelectedItem));

				ICollectionView cv = CollectionViewSource.GetDefaultView(CardCounts.ItemsSource);
				cv.GroupDescriptions.Clear();
				cv.GroupDescriptions.Add(new PropertyGroupDescription("DataSetName"));

				d.Add(this.Bind(ViewModel, vm => vm.PageWidth, v => v.PageWidth.Value, 
					(vmInput) => vmInput,
					(viewInput) => (float)(viewInput ?? 1.0f)));
				d.Add(this.Bind(ViewModel, vm => vm.PageHeight, v => v.PageHeight.Value,
					(vmInput) => vmInput,
					(viewInput) => (float)(viewInput ?? 1.0f)));
				d.Add(this.Bind(ViewModel, vm => vm.HSpacing, v => v.HSpacing.Value,
					(vmInput) => vmInput,
					(viewInput) => (float)(viewInput ?? 0.0f)));
				d.Add(this.Bind(ViewModel, vm => vm.VSpacing, v => v.VSpacing.Value,
					(vmInput) => vmInput,
					(viewInput) => (float)(viewInput ?? 0.0f)));
				d.Add(this.Bind(ViewModel, vm => vm.HMargin, v => v.HMargin.Value,
					(vmInput) => vmInput,
					(viewInput) => (float)(viewInput ?? 0.0f)));
				d.Add(this.Bind(ViewModel, vm => vm.VMargin, v => v.VMargin.Value,
					(vmInput) => vmInput,
					(viewInput) => (float)(viewInput ?? 0.0f)));
				d.Add(this.Bind(ViewModel, vm => vm.IncludeBleed, v => v.IncludeBleed.IsChecked));
				d.Add(this.Bind(ViewModel, vm => vm.UseLandscape, v => v.UseLandscape.IsChecked));

				// WhenAnyValue only supports up to 8 simultaneous observables, so we need to split it up
				d.Add(
					ViewModel.WhenAnyValue(
						vm => vm.PageWidth, 
						vm => vm.PageHeight, 
						vm => vm.HSpacing, 
						vm => vm.VSpacing,
						vm => vm.HMargin,
						vm => vm.VMargin)
					.ObserveOn(RxApp.MainThreadScheduler)
					.Subscribe(_ => UpdatePrintPreview()));
				d.Add(
					ViewModel.WhenAnyValue(
						vm => vm.CardLayoutSize,
						vm => vm.XCardCount,
						vm => vm.YCardCount)
					.ObserveOn(RxApp.MainThreadScheduler)
					.Subscribe(_ => UpdatePrintPreview()));
				
			});
		}

		#endregion

		#region Implementation of IViewFor

		object IViewFor.ViewModel
		{
			get => ViewModel;
			set => ViewModel = (DeckPrintSetViewModel)value;
		}

		#endregion

		#region Implementation of IViewFor<DeckPrintSetViewModel>

		public DeckPrintSetViewModel ViewModel
		{
			get => (DeckPrintSetViewModel) GetValue(ViewModelProperty);
			set => SetValue(ViewModelProperty, value);
		}

		#endregion

		#region WPF Handlers

		private void OnCountChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			FrameworkElement fe = sender as FrameworkElement;
			CardCountViewModel ccvm = fe?.DataContext as CardCountViewModel;
			if (ccvm == null)
				return;

			ccvm.Count = (int) e.NewValue;
		}

		// because we are using the checkbox in the header and the header is based on items bound to ItemSource,
		// we can't do two-way binding. Therefore we have to manually update the ViewModel and set its
		// IsDirty flag
		private void FlipBackSide_OnChecked(object sender, RoutedEventArgs e)
		{
			CheckBox chkBox = sender as CheckBox;
			CardCountViewModel ccvm = chkBox.DataContext as CardCountViewModel;

			bool isChecked = chkBox.IsChecked.IsValidAndTrue();
			if (ccvm.Mapping.DataSetRef.FlipBackSide != isChecked)
			{
				ccvm.Mapping.DataSetRef.FlipBackSide = isChecked;
				ViewModel.IsDirty = true;
			}
		}

		#endregion

		#region Private Members

		private void UpdatePrintPreview()
		{
			PageLayout.Children.Clear();

			// arbitrarily chosen scale
			const float ViewScale = 40;
			PageLayout.Width = ViewModel.PageWidth * ViewScale;
			PageLayout.Height = ViewModel.PageHeight * ViewScale;

			float yPos = ViewModel.VMargin * ViewScale;
			for (int y = 0; y < ViewModel.YCardCount; y++)
			{
				float xPos = ViewModel.HMargin * ViewScale;
				for (int x = 0; x < ViewModel.XCardCount; x++)
				{
					Rectangle rect = new Rectangle
					{
						Width = ViewModel.CardLayoutSize.Width * ViewScale,
						Height = ViewModel.CardLayoutSize.Height * ViewScale,
						Stroke = new SolidColorBrush(Colors.Gray),
					};

					PageLayout.Children.Add(rect);
					Canvas.SetLeft(rect, xPos);
					Canvas.SetTop(rect, yPos);

					xPos += (ViewModel.CardLayoutSize.Width + ViewModel.HSpacing) * ViewScale;
				}

				yPos += (ViewModel.CardLayoutSize.Height + ViewModel.VSpacing) * ViewScale;
			}
		}

		#endregion

		private void Print_OnClick(object sender, RoutedEventArgs e)
		{
			ICommand printCommand = ViewModel.Print;
			if (printCommand.CanExecute(null))
			{
				Mouse.OverrideCursor = Cursors.Wait;
				printCommand.Execute(null);
			}
		}
	}
}
