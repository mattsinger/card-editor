﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Core.View_Models.Card_Preview;
using ReactiveUI;
using Math = Core.Util.Math;
using Size = Core.Drawing.Size;

namespace CardEditor.Views.Card_Preview
{
	/// <summary>
	/// Interaction logic for CardPreviewView.xaml
	/// </summary>
	public partial class CardPreviewView : UserControl, IViewFor<CardPreviewViewModel>
	{
		#region Dependency Properties

		/// <summary>
		/// ViewModel Dependency Property
		/// </summary>
		public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
			typeof(CardPreviewViewModel),
			typeof(CardPreviewView));

		#endregion

		#region Implementation of IViewFor

		object IViewFor.ViewModel
		{
			get => ViewModel;
			set => ViewModel = (CardPreviewViewModel)value;
		}

		#endregion

		#region Implementation of IViewFor<CardPreviewViewModel>

		public CardPreviewViewModel ViewModel
		{
			get => (CardPreviewViewModel) GetValue(ViewModelProperty);
			set
			{
				SetValue(ViewModelProperty, value);
				BindToViewModel();
			}
		}

		#endregion

		#region Properties

		public Uri CmykConverter
		{
			get => _cmykConverter;
			set
			{
				if (value == _cmykConverter)
					return;

				_cmykConverter = value;
				RenderCard();
			}
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Forces the card to render immediately
		/// </summary>
		public void ForceRender()
		{
			SetCanvasSize();
			RenderCard();
		}

		#endregion

		#region Private Members

		private readonly List<UIElement> _elementViews = new List<UIElement>();
		private readonly List<IDisposable> _disposables = new List<IDisposable>();
		private Uri _cmykConverter;

		// used purely for rendering the preview
		private readonly Canvas _cardCanvas = new Canvas
		{
			Background = new SolidColorBrush(Colors.White),
			Width = 0,
			Height = 0,
			ClipToBounds = true
		};

		#endregion

		#region Constructor

		public CardPreviewView()
		{
			InitializeComponent();
			
			Loaded += LoadedHandler;
			Unloaded += UnloadedHandler;
		}

		#endregion

		#region Event Handlers

		private void LoadedHandler(object sender, RoutedEventArgs e)
		{
			CmykConverter = Properties.Settings.Default.CmykConverter;
			Properties.Settings.Default.PropertyChanged += DefaultPropertyChangedHandler;
		}

		private void UnloadedHandler(object sender, RoutedEventArgs e)
		{
			Properties.Settings.Default.PropertyChanged -= DefaultPropertyChangedHandler;

			_disposables.ForEach(_ => _.Dispose());
			_disposables.Clear();
		}

		private void DefaultPropertyChangedHandler(object sender, PropertyChangedEventArgs args)
		{
			if (args.PropertyName == nameof(Properties.Settings.Default.CmykConverter))
				CmykConverter = Properties.Settings.Default.CmykConverter;
		}

		#endregion

		#region Private Methods

		private void BindToViewModel()
		{
			_disposables.ForEach(_ => _.Dispose());
			_disposables.Clear();

			_disposables.Add(ViewModel.WhenAnyValue(vm => vm.DisplaySize, vm => vm.Ppi, vm => vm.LayoutViewModel)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(_ =>
				{
					SetCanvasSize();
					RenderCard();
				}));

			AddPreviewViews(ViewModel.PreviewFields.ToList(), 0);
			_disposables.Add(ViewModel.PreviewFields.Changed.ObserveOn(RxApp.MainThreadScheduler).Subscribe(args =>
			{
				switch (args.Action)
				{
					case NotifyCollectionChangedAction.Add:
						AddPreviewViews(args.NewItems, args.NewStartingIndex);
						break;

					case NotifyCollectionChangedAction.Remove:
						RemovePreviewViews(args.OldStartingIndex, args.OldItems.Count);
						break;

					case NotifyCollectionChangedAction.Reset:
						RemovePreviewViews(0, _elementViews.Count);
						AddPreviewViews(ViewModel.PreviewFields.ToList(), 0);
						break;

					default:
						throw new InvalidOperationException($"Unhandled operation type: {args.Action}");
				}
				RenderCard();
			}));

			_disposables.Add(ViewModel.PreviewFields.ItemChanged
				.Throttle(TimeSpan.FromSeconds(.75))
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(_ => RenderCard()));
		}

		private void SetCanvasSize()
		{
			Size renderSize = Math.GetDisplaySizeFromInches(ViewModel.DisplaySize, ViewModel.Ppi);
			CardDisplay.Width = renderSize.Width;
			CardDisplay.Height = renderSize.Height;
			_cardCanvas.Width = renderSize.Width;
			_cardCanvas.Height = renderSize.Height;
		}

		private void AddPreviewViews(IList elements, int startIndex)
		{
			int index = startIndex;
			foreach (BaseFieldPreviewViewModel preview in elements)
			{
				UIElement newView = CreatePreviewView(preview);
				Panel.SetZIndex(newView, index);
				_elementViews.Insert(index, newView);
				_cardCanvas.Children.Insert(index, newView);

				index++;
			}

			for (; index < _elementViews.Count; index++)
				Panel.SetZIndex(_elementViews[index], index);
		}

		private void RemovePreviewViews(int startIndex, int count)
		{
			_elementViews.RemoveRange(startIndex, count);
			_cardCanvas.Children.RemoveRange(startIndex, count);
			for (int i = startIndex; i < _elementViews.Count; i++)
				Panel.SetZIndex(_elementViews[i], i);
		}

		private UIElement CreatePreviewView(BaseFieldPreviewViewModel vm)
		{
			TextFieldPreviewViewModel textVm = vm as TextFieldPreviewViewModel;
			if (textVm != null)
				return new TextFieldPreviewView
				{
					ViewModel = textVm
				};

			ImageFieldPreviewViewModel imageVm = vm as ImageFieldPreviewViewModel;
			if (imageVm != null)
				return new ImageFieldPreviewView
				{
					ViewModel = imageVm
				};

			ColorFieldPreviewViewModel colorVm = vm as ColorFieldPreviewViewModel;
			if (colorVm != null)
				return new ColorFieldPreviewView
				{
					ViewModel = colorVm
				};

			throw new InvalidOperationException("Unknown preview type");
		}

		private void RenderCard()
		{
			int imageWidth = (int) (CardDisplay.Width + .5f);
			int imageHeight = (int) (CardDisplay.Height + .5f);

			if (imageWidth <= 0 || imageHeight <= 0)
				return;

			// manually force a layout pass on the card canvas to update the preview elements
			System.Windows.Size viewSize = new System.Windows.Size(_cardCanvas.Width, _cardCanvas.Height);
			_cardCanvas.Measure(viewSize);
			_cardCanvas.Arrange(new Rect(viewSize));
			_cardCanvas.UpdateLayout();

			// We've already set image/canvas size and preview views to match desired PPI, 
			// so we render at normal screen PPI here
			RenderTargetBitmap target = new RenderTargetBitmap(
				imageWidth, 
				imageHeight, 
				96.0,               // NOTE: we must ALWAYS use 96 dpi when rendering the image out
				96.0,
				PixelFormats.Default);
			target.Render(_cardCanvas);
			target.Freeze();

			BitmapSource result;
			if (CmykConverter != null)
			{
				ColorContext rgbContext = new ColorContext(target.Format);
				ColorContext cmykContext = new ColorContext(CmykConverter);
				ColorConvertedBitmap ccb = new ColorConvertedBitmap();
				ccb.BeginInit();
				ccb.SourceColorContext = rgbContext;
				ccb.Source = target;
				ccb.DestinationColorContext = cmykContext;
				ccb.DestinationFormat = PixelFormats.Cmyk32;
				ccb.EndInit();

				result = ccb;
			}
			else
				result = target;

			CardDisplay.Source = result;
		}

		#endregion
	}
}
