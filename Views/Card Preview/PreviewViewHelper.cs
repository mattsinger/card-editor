﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Reactive.Linq;
using System.Windows.Controls;
using System.Windows.Media;
using Core.View_Models.Card_Preview;
using ReactiveUI;
using Math = Core.Util.Math;
using Size = Core.Drawing.Size;

namespace CardEditor.Views.Card_Preview
{
	/// <summary>
	/// Helper class for PreviewViews to handle common functionality
	/// </summary>
	internal class PreviewViewHelper
	{
		#region Private Members

		private BaseFieldPreviewViewModel _viewModel;
		private readonly Control _view;
		private readonly TranslateTransform _bleedOffset = new TranslateTransform();
		private readonly RotateTransform _orientation = new RotateTransform();

		#endregion

		#region Public Properties

		public BaseFieldPreviewViewModel ViewModel
		{
			get { return _viewModel; }
			set
			{
				_viewModel = value;
				BindToViewModel();
			}
		}

		#endregion

		#region Constructor

		/// <summary>
		/// Constructs a PreviewViewHelper
		/// </summary>
		/// <param name="view">The helped view</param>
		public PreviewViewHelper(Control view)
		{
			_view = view;

			TransformGroup renderTransforms = new TransformGroup();
			renderTransforms.Children.Add(_orientation);
			renderTransforms.Children.Add(_bleedOffset);
			_view.RenderTransform = renderTransforms;
		}

		#endregion

		#region Private Methods

		private void BindToViewModel()
		{
			ViewModel.Parent.WhenAnyValue(preview => preview.IncludeBleed)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(_ => UpdateOrigin());

			ViewModel.Parent.WhenAnyValue(preview => preview.Ppi)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(_ =>
				{
					UpdateOrigin();
					UpdateSize();
				});

			UpdateOrigin();
			ViewModel.WhenAnyValue(vm => vm.Origin)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(_ => UpdateOrigin());

			UpdateSize();
			ViewModel.WhenAnyValue(vm => vm.Size)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(_ => UpdateSize());

			UpdateOrientation();
			ViewModel.WhenAnyValue(vm => vm.Orientation)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(_ => UpdateOrientation());
		}

		private void UpdateOrigin()
		{
			Size cardPos = new Size(ViewModel.Origin) + ViewModel.Parent.LayoutViewModel.Bleed;
			Size displayPos = Math.GetDisplaySizeFromInches(cardPos, ViewModel.Parent.Ppi);
			Canvas.SetLeft(_view, displayPos.Width);
			Canvas.SetTop(_view, displayPos.Height);
			
			if (!ViewModel.Parent.IncludeBleed)
			{
				Size bleedSize = Math.GetDisplaySizeFromInches(ViewModel.Parent.LayoutViewModel.Bleed, ViewModel.Parent.Ppi);
				_bleedOffset.X = -bleedSize.Width;
				_bleedOffset.Y = -bleedSize.Height;
			}
			else
			{
				_bleedOffset.X = 0;
				_bleedOffset.Y = 0;
			}
		}

		private void UpdateSize()
		{
			Size displaySize = Math.GetDisplaySizeFromInches(ViewModel.Size, ViewModel.Parent.Ppi);
			_view.Width = displaySize.Width;
			_view.Height = displaySize.Height;

			_orientation.CenterX = displaySize.Width / 2;
			_orientation.CenterY = displaySize.Height / 2;
		}

		private void UpdateOrientation()
		{
			_orientation.Angle = ViewModel.Orientation;
		}

		#endregion
	}
}
