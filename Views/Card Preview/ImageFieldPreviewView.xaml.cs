﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using CardEditor.Util;
using Core.View_Models.Card_Preview;
using Core.View_Models.Layout;
using ReactiveUI;
using Math = Core.Util.Math;

namespace CardEditor.Views.Card_Preview
{
	/// <summary>
	/// Interaction logic for ImageFieldPreviewView.xaml
	/// </summary>
	public partial class ImageFieldPreviewView : UserControl, IViewFor<ImageFieldPreviewViewModel>
	{
		#region Dependency Property

		/// <summary>
		/// Dependency property
		/// </summary>
		public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
			typeof(ImageFieldPreviewViewModel),
			typeof(ImageFieldPreviewView));

		#endregion

		#region Private Members

		private readonly PreviewViewHelper _helper;

		#endregion

		#region Constructor

		/// <summary>
		/// Constructs an ImageFieldPreviewView
		/// </summary>
		public ImageFieldPreviewView()
		{
			InitializeComponent();
			this.WhenAnyValue(_ => _.ViewModel).BindTo(this, _ => _.DataContext);

			_helper = new PreviewViewHelper(this);
		}

		#endregion

		#region Implementation of IViewFor

		object IViewFor.ViewModel
		{
			get => ViewModel;
			set => ViewModel = (ImageFieldPreviewViewModel)value;
		}

		#endregion

		#region Implementation of IViewFor<ImageFieldPreviewViewModel>

		public ImageFieldPreviewViewModel ViewModel
		{
			get => (ImageFieldPreviewViewModel) GetValue(ViewModelProperty);
			set
			{
				SetValue(ViewModelProperty, value);
				BindToViewModel();
			}
		}

		#endregion

		#region Private Methods

		private void BindToViewModel()
		{
			_helper.ViewModel = ViewModel;

			UpdateCornerRadius();
			ViewModel.WhenAnyValue(vm => vm.CornerRadius, vm => vm.RoundedCorners)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(_ => UpdateCornerRadius());

			ViewModel.WhenAnyValue(vm => vm.BorderThickness)
				.Subscribe(thickness =>
				{
					float pixelThickness = Math.GetDisplayLengthFromInches(thickness, App.Ppi.Width);
					ImageRegion.BorderThickness = new Thickness(pixelThickness);
				});
			ViewModel.WhenAnyValue(vm => vm.BorderColor)
				.Select(color => color ?? Core.Drawing.Color.Transparent)
				.Subscribe(color =>
				{
					ImageRegion.BorderBrush = new SolidColorBrush(color.ToMediaColor());
				});

			SetStretchMode(ViewModel.Stretch);
			ViewModel.WhenAnyValue(vm => vm.Stretch)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(SetStretchMode);

			UpdateViewHorizontalAlignment();
			ViewModel.WhenAnyValue(vm => vm.HorizontalAlignment).Subscribe(_ => UpdateViewHorizontalAlignment());

			UpdateViewVerticalAlignment();
			ViewModel.WhenAnyValue(vm => vm.VerticalAlignment).Subscribe(_ => UpdateViewVerticalAlignment());
		}

		private void UpdateCornerRadius()
		{
			float radius = Math.GetDisplayLengthFromInches(ViewModel.CornerRadius, App.Ppi.Height);
			ImageRegion.CornerRadius = new CornerRadius
			{
				TopLeft = (ViewModel.RoundedCorners & ElementViewModel.RoundedCornersFlags.TopLeft) != ElementViewModel.RoundedCornersFlags.None ? radius : 0,
				TopRight = (ViewModel.RoundedCorners & ElementViewModel.RoundedCornersFlags.TopRight) != ElementViewModel.RoundedCornersFlags.None ? radius : 0,
				BottomLeft = (ViewModel.RoundedCorners & ElementViewModel.RoundedCornersFlags.BottomLeft) != ElementViewModel.RoundedCornersFlags.None ? radius : 0,
				BottomRight = (ViewModel.RoundedCorners & ElementViewModel.RoundedCornersFlags.BottomRight) != ElementViewModel.RoundedCornersFlags.None ? radius : 0,
			};
		}

		private void SetStretchMode(bool stretch)
		{
			Image.Stretch = stretch ? Stretch.Uniform : Stretch.None;
		}

		private void UpdateViewHorizontalAlignment()
		{
			switch (ViewModel.HorizontalAlignment)
			{
				case Core.Drawing.HorizontalAlignment.Left:
					Image.AlignmentX = AlignmentX.Left;
					break;

				case Core.Drawing.HorizontalAlignment.Center:
					Image.AlignmentX = AlignmentX.Center;
					break;

				case Core.Drawing.HorizontalAlignment.Right:
					Image.AlignmentX = AlignmentX.Right;
					break;
			}
		}

		private void UpdateViewVerticalAlignment()
		{
			switch (ViewModel.VerticalAlignment)
			{
				case Core.Drawing.VerticalAlignment.Top:
					Image.AlignmentY = AlignmentY.Top;
					break;

				case Core.Drawing.VerticalAlignment.Center:
					Image.AlignmentY = AlignmentY.Center;
					break;

				case Core.Drawing.VerticalAlignment.Bottom:
					Image.AlignmentY = AlignmentY.Bottom;
					break;
			}
		}

		#endregion
	}
}
