﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Windows;
using System.Windows.Controls;
using Core.Text;
using ReactiveUI;

namespace CardEditor.Views.Card_Preview
{
	/// <summary>
	/// Interaction logic for GlyphNodeView.xaml
	/// </summary>
	public partial class GlyphNodeView : UserControl, IViewFor<GlyphNode>
	{
		#region Dependency Properties

		/// <summary>
		/// Dependency property
		/// </summary>
		public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
			typeof(GlyphNode),
			typeof(GlyphNodeView));

		#endregion

		#region Constructor

		/// <summary>
		/// Constructs a GlyphNodeView
		/// </summary>
		public GlyphNodeView()
		{
			InitializeComponent();

			this.WhenAnyValue(_ => _.ViewModel).BindTo(this, _ => _.DataContext);
		}

		#endregion

		#region Implementation of IViewFor

		/// <summary>
		/// The wrapped ViewModel
		/// </summary>
		object IViewFor.ViewModel
		{
			get => ViewModel;
			set => ViewModel = (GlyphNode)value;
		}

		#endregion

		#region Implementation of IViewFor<GlyphNode>

		/// <summary>
		/// The ViewModel corresponding to this specific View. This should be
		/// a DependencyProperty if you're using XAML.
		/// </summary>
		public GlyphNode ViewModel
		{
			get => (GlyphNode) GetValue(ViewModelProperty);
			set => SetValue(ViewModelProperty, value);
		}

		#endregion
	}
}
