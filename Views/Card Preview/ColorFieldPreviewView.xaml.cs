﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using CardEditor.Util;
using Core.View_Models.Card_Preview;
using Core.View_Models.Layout;
using ReactiveUI;
using Math = Core.Util.Math;

namespace CardEditor.Views.Card_Preview
{
	/// <summary>
	/// Interaction logic for ColorFieldPreviewView.xaml
	/// </summary>
	public partial class ColorFieldPreviewView : UserControl, IViewFor<ColorFieldPreviewViewModel>
	{
		#region Dependency Properties

		/// <summary>
		/// Dependency property
		/// </summary>
		public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
			typeof(ColorFieldPreviewViewModel),
			typeof(ColorFieldPreviewView));

		#endregion

		#region Private Memebers

		private readonly PreviewViewHelper _helper;

		#endregion

		#region Constructor

		public ColorFieldPreviewView()
		{
			InitializeComponent();

			_helper = new PreviewViewHelper(this);
		}

		#endregion

		#region Implementation of IViewFor

		/// <summary>
		/// </summary>
		object IViewFor.ViewModel
		{
			get => ViewModel;
			set => ViewModel = (ColorFieldPreviewViewModel)value;
		}

		#endregion

		#region Implementation of IViewFor<ColorFieldPreviewViewModel>

		/// <summary>
		/// The ViewModel corresponding to this specific View. This should be
		/// a DependencyProperty if you're using XAML.
		/// </summary>
		public ColorFieldPreviewViewModel ViewModel
		{
			get => (ColorFieldPreviewViewModel) GetValue(ViewModelProperty);
			set
			{
				SetValue(ViewModelProperty, value);
				BindToViewModel();
			}
		}

		#endregion

		#region Private Methods

		private void BindToViewModel()
		{
			_helper.ViewModel = ViewModel;

			UpdateCornerRadius();
			ViewModel.WhenAnyValue(vm => vm.CornerRadius, vm => vm.RoundedCorners)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(_ => UpdateCornerRadius());

			ViewModel.Parent.WhenAnyValue(parent => parent.Ppi)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(_ => UpdateCornerRadius());

			UpdateViewColor();
			ViewModel.WhenAnyValue(vm => vm.Color)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(_ => UpdateViewColor());

			ViewModel.WhenAnyValue(vm => vm.BorderThickness)
				.Subscribe(thickness =>
				{
					float pixelThickness = Math.GetDisplayLengthFromInches(thickness, App.Ppi.Width);
					ColorRegion.BorderThickness = new Thickness(pixelThickness);
				});
			ViewModel.WhenAnyValue(vm => vm.BorderColor)
				.Select(color => color ?? Core.Drawing.Color.Transparent)
				.Subscribe(color =>
				{
					ColorRegion.BorderBrush = new SolidColorBrush(color.ToMediaColor());
				});
		}

		private void UpdateCornerRadius()
		{
			float radius = Math.GetDisplayLengthFromInches(ViewModel.CornerRadius, App.Ppi.Height);
			ColorRegion.CornerRadius = new CornerRadius
			{
				TopLeft = (ViewModel.RoundedCorners & ElementViewModel.RoundedCornersFlags.TopLeft) != ElementViewModel.RoundedCornersFlags.None ? radius : 0,
				TopRight = (ViewModel.RoundedCorners & ElementViewModel.RoundedCornersFlags.TopRight) != ElementViewModel.RoundedCornersFlags.None ? radius : 0,
				BottomLeft = (ViewModel.RoundedCorners & ElementViewModel.RoundedCornersFlags.BottomLeft) != ElementViewModel.RoundedCornersFlags.None ? radius : 0,
				BottomRight = (ViewModel.RoundedCorners & ElementViewModel.RoundedCornersFlags.BottomRight) != ElementViewModel.RoundedCornersFlags.None ? radius : 0,
			};
		}

		private void UpdateViewColor()
		{
			Core.Drawing.Color vmColor = ViewModel.Color;
			SolidColorBrush brush = new SolidColorBrush(Color.FromArgb(vmColor.A, vmColor.R, vmColor.G, vmColor.B));
			ColorRegion.Background = brush;
		}

		#endregion
	}
}
