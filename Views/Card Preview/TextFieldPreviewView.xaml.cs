﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using CardEditor.Util;
using Core.View_Models.Card_Preview;
using Core.View_Models.Layout;
using ReactiveUI;
using Math = Core.Util.Math;

namespace CardEditor.Views.Card_Preview
{
	/// <summary>
	/// Interaction logic for TextPreviewView.xaml
	/// </summary>
	public partial class TextFieldPreviewView : UserControl, IViewFor<TextFieldPreviewViewModel>
	{
		#region Dependency Properties

		/// <summary>
		/// Dependency property
		/// </summary>
		public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
			typeof(TextFieldPreviewViewModel),
			typeof(TextFieldPreviewView));

		#endregion

		#region Private Members
		
		private readonly PreviewViewHelper _helper;

		#endregion

		#region Constructor

		/// <summary>
		/// Constructs a TextFieldPreviewView
		/// </summary>
		public TextFieldPreviewView()
		{
			InitializeComponent();

			_helper = new PreviewViewHelper(this);
		}

		#endregion

		#region Implementation of IViewFor

		object IViewFor.ViewModel
		{
			get => ViewModel;
			set => ViewModel = (TextFieldPreviewViewModel) value;
		}

		#endregion

		#region Implementation of IViewFor<TextFieldPreviewViewModel>

		public TextFieldPreviewViewModel ViewModel
		{
			get => (TextFieldPreviewViewModel) GetValue(ViewModelProperty);
			set
			{
				SetValue(ViewModelProperty, value);
				BindToViewModel();
			}
		}

		#endregion

		#region Private Methods

		private void BindToViewModel()
		{
			_helper.ViewModel = ViewModel;

			UpdateCornerRadius();
			ViewModel.WhenAnyValue(vm => vm.CornerRadius, vm => vm.RoundedCorners)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(_ => UpdateCornerRadius());

			ViewModel.WhenAnyValue(vm => vm.BorderThickness)
				.Subscribe(thickness =>
				{
					float pixelThickness = Math.GetDisplayLengthFromInches(thickness, App.Ppi.Width);
					ActualBorder.BorderThickness = new Thickness(pixelThickness);
				});
			ViewModel.WhenAnyValue(vm => vm.BorderColor)
				.Select(color => color ?? Core.Drawing.Color.Transparent)
				.Subscribe(color =>
				{
					ActualBorder.BorderBrush = new SolidColorBrush(color.ToMediaColor());
				});

			UpdateViewFont();
			ViewModel.WhenAnyValue(vm => vm.Font)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(_ => UpdateViewFont());

			UpdateViewHorizontalAlignment();
			ViewModel.WhenAnyValue(vm => vm.HorizontalAlignment)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(_ => UpdateViewHorizontalAlignment());

			UpdateViewVerticalAlignment();
			ViewModel.WhenAnyValue(vm => vm.VerticalAlignment)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(_ => UpdateViewVerticalAlignment());

			UpdateViewColor();
			ViewModel.WhenAnyValue(vm => vm.Color)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(_ => UpdateViewColor());

			UpdateViewScale();
			ViewModel.Parent.WhenAnyValue(parent => parent.Ppi)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(_ => UpdateViewScale());
			
			PreviewControl.ParseTree = ViewModel.ParseTree;
		}

		private void UpdateCornerRadius()
		{
			float radius = Math.GetDisplayLengthFromInches(ViewModel.CornerRadius, App.Ppi.Height);
			ActualBorder.CornerRadius = new CornerRadius
			{
				TopLeft = (ViewModel.RoundedCorners & ElementViewModel.RoundedCornersFlags.TopLeft) != ElementViewModel.RoundedCornersFlags.None ? radius : 0,
				TopRight = (ViewModel.RoundedCorners & ElementViewModel.RoundedCornersFlags.TopRight) != ElementViewModel.RoundedCornersFlags.None ? radius : 0,
				BottomLeft = (ViewModel.RoundedCorners & ElementViewModel.RoundedCornersFlags.BottomLeft) != ElementViewModel.RoundedCornersFlags.None ? radius : 0,
				BottomRight = (ViewModel.RoundedCorners & ElementViewModel.RoundedCornersFlags.BottomRight) != ElementViewModel.RoundedCornersFlags.None ? radius : 0,
			};

			// actually use transparent border thickness to push text to correct location to fit corner radius
			float displayRadius = Math.GetDisplayLengthFromInches(ViewModel.CornerRadius * .25f, ViewModel.Parent.Ppi.Width);
			RoundedCorners.BorderThickness = new Thickness
			{
				Top = (ViewModel.RoundedCorners &
					(ElementViewModel.RoundedCornersFlags.TopLeft | ElementViewModel.RoundedCornersFlags.TopRight))
						!= ElementViewModel.RoundedCornersFlags.None ? displayRadius : 0,
				Left = (ViewModel.RoundedCorners &
					(ElementViewModel.RoundedCornersFlags.TopLeft | ElementViewModel.RoundedCornersFlags.BottomLeft))
						!= ElementViewModel.RoundedCornersFlags.None ? displayRadius : 0,
				Right = (ViewModel.RoundedCorners &
					(ElementViewModel.RoundedCornersFlags.TopRight | ElementViewModel.RoundedCornersFlags.BottomRight))
						!= ElementViewModel.RoundedCornersFlags.None ? displayRadius : 0,
				Bottom = (ViewModel.RoundedCorners &
					(ElementViewModel.RoundedCornersFlags.BottomLeft | ElementViewModel.RoundedCornersFlags.BottomRight))
						!= ElementViewModel.RoundedCornersFlags.None ? displayRadius : 0,
			};
		}

		private void UpdateViewFont()
		{
			PreviewControl.Font = ViewModel.Font;
		}

		private void UpdateViewHorizontalAlignment()
		{
			PreviewControl.HorizontalAlignment = ViewModel.HorizontalAlignment;
		}

		private void UpdateViewVerticalAlignment()
		{
			PreviewControl.VerticalAlignment = ViewModel.VerticalAlignment;
		}

		private void UpdateViewColor()
		{
			PreviewControl.Color = ViewModel.Color;
		}

		private void UpdateViewScale()
		{
			PreviewControl.Ppi = ViewModel.Parent.Ppi.Height;
		}

		#endregion
	}
}
