﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using Core.View_Models.Layout;

namespace CardEditor.Views.Layout
{
	/// <summary>
	/// Common interface for all Views that wrap ElementViewModels. 
	/// </summary>
	/// <remarks>
	/// I would rather this be a base class that holds common functionality, but WPF doesn't allow views to inherit XAML. 
	/// This means we end up with a bunch of boilerplate, because the same stuff has to be implemented in each view. 
	/// Using this interface means that at least LayoutView can treat all Element views the same way and reduce some
	/// boilerplate code there.
	/// </remarks>
	public interface IElementView : IDisposable
	{
		/// <summary>
		/// The contained view model
		/// </summary>
		ElementViewModel ViewModel { get; }

		/// <summary>
		/// The scale of the parent view, setter only
		/// </summary>
		float Scale { set; }

		/// <summary>
		/// The snap tolerance in pixels
		/// </summary>
		float SnapTolerance { set; }
	}
}
