﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using CardEditor.Controls;
using Core.Util;
using Core.View_Models.Layout;
using ReactiveUI;
using Math = Core.Util.Math;
using Point = Core.Drawing.Point;
using Size = Core.Drawing.Size;

namespace CardEditor.Views.Layout
{
	/// <summary>
	/// Interaction logic for ElementTransformView.xaml
	/// </summary>
	public partial class ElementTransformView : UserControl, IViewFor<ElementViewModel>, IElementView
	{
		#region Private Members

		private readonly LayoutView _layoutView;
		private readonly List<IDisposable> _subscriptions = new List<IDisposable>();
		private bool _disposed;

		#endregion

		#region Dependency Property

		public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
			typeof(ElementViewModel),
			typeof(ElementTransformView));

		#endregion

		#region Public Properties

		/// <summary>
		/// The Reposition Control, for convenience
		/// </summary>
		public RepositionControl RepositionControl => Reposition;

		/// <summary>
		/// The Resize Control, for conveneince
		/// </summary>
		public ResizeControl ResizeControl => Resize;

		/// <summary>
		/// Whether the shape is being edited
		/// </summary>
		public bool IsEditingShape => Reposition.IsRepositioning ||
			Resize.IsResizing ||
			Orient.IsOrienting;

		#endregion

		#region Constructor and Finalizer

		/// <summary>
		/// Constructs a new ElementLayoutView
		/// </summary>
		public ElementTransformView(LayoutView layoutView)
		{
			InitializeComponent();

			_layoutView = layoutView;

			Reposition.RepositionTarget = this;
			Reposition.HorizontalSnapTolerance = Properties.Settings.Default.SnapSensitivity;
			Reposition.VerticalSnapTolerance = Properties.Settings.Default.SnapSensitivity;
			Reposition.IsRepositioningChanged += (sender, args) =>
			{
				if (Reposition.IsRepositioning)
					ViewModel.BeginTransform();
				else
					ViewModel.EndTransform();
			};

			Resize.ResizeTarget = this;
			Resize.HorizontalSnapTolerance = Properties.Settings.Default.SnapSensitivity;
			Resize.VerticalSnapTolerance = Properties.Settings.Default.SnapSensitivity;
			Resize.IsResizingChanged += (sender, args) =>
			{
				if (Resize.IsResizing)
					ViewModel.BeginTransform();
				else
					ViewModel.EndTransform();
			};

			Orient.OrientationTarget = this;
			Orient.SnapValues = new List<float>
			{
				0, 360, 90, 180, -90, -180		// need to catch values in both positive and negative ranges
			};
			Orient.SnapRange = 7.5f;
			Orient.IsOrientingChanged += (sender, args) =>
			{
				if (Orient.IsOrienting)
					ViewModel.BeginTransform();
				else
					ViewModel.EndTransform();
			};

			_subscriptions.Add(
				this.WhenAnyValue(_ => _.ViewModel)
					.BindTo(this, _ => _.DataContext));
			_subscriptions.Add(
				this.WhenAnyValue(v => v.Width, v => v.Height)
					.Where(tuple => !double.IsNaN(tuple.Item1) && !double.IsNaN(tuple.Item2))
					.Where(_ => IsEditingShape)
					.Subscribe(_ =>
					{
						UpdateVmSize();
					}));

			DependencyPropertyDescriptor.FromProperty(Canvas.TopProperty, GetType())
										.AddValueChanged(this, TopChangedHandler);
			DependencyPropertyDescriptor.FromProperty(Canvas.LeftProperty, GetType())
										.AddValueChanged(this, LeftChangedHandler);
			DependencyPropertyDescriptor.FromProperty(OrientationControl.OrientationProperty, Orient.GetType())
										.AddValueChanged(Orient, OrientationChangedHandler);

			MouseDown += MouseDownHandler;
			LostFocus += LostFocusHandler;
		}

		/// <summary>
		/// Finalizer
		/// </summary>
		~ElementTransformView()
		{
			DoDispose(true);
		}

		#endregion

		#region Implementation of IViewFor

		/// <summary>
		/// </summary>
		object IViewFor.ViewModel
		{
			get => ViewModel;
			set => ViewModel = (ElementViewModel)value;
		}

		/// <summary>
		/// The scale of the parent view, setter only
		/// </summary>
		public float Scale
		{
			set
			{
				Resize.Scale = value;
				Reposition.Scale = value;
			}
		}

		/// <summary>
		/// The snap tolerance in pixels
		/// </summary>
		public float SnapTolerance
		{
			set
			{
				Resize.HorizontalSnapTolerance = value;
				Resize.VerticalSnapTolerance = value;
				Reposition.HorizontalSnapTolerance = value;
				Reposition.VerticalSnapTolerance = value;
			}
		}

		#endregion

		#region Implementation of IViewFor<ElementViewModel>

		/// <summary>
		/// The ViewModel corresponding to this specific View. This should be
		/// a DependencyProperty if you're using XAML.
		/// </summary>
		public ElementViewModel ViewModel
		{
			get { return (ElementViewModel)GetValue(ViewModelProperty); }
			set
			{
				SetValue(ViewModelProperty, value);
				BindToViewModel();
			}
		}

		#endregion

		#region Implementation of IDisposable

		/// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
		public void Dispose()
		{
			DoDispose(false);
		}

		#endregion

		#region WPF Handlers

		private void TopChangedHandler(object sender, EventArgs e)
		{
			if (!IsEditingShape)
				return;

			UpdateVmOrigin();
		}

		private void LeftChangedHandler(object sender, EventArgs e)
		{
			if (!IsEditingShape)
				return;

			UpdateVmOrigin();
		}

		private void OrientationChangedHandler(object sender, EventArgs e)
		{
			if (!IsEditingShape)
				return;

			UpdateVmOrientation();
			Reposition.Orientation = Orient.Orientation;
			Resize.Orientation = Orient.Orientation;
		}

		private void MouseDownHandler(object sender, MouseButtonEventArgs e)
		{
			ViewModel.IsSelected = true;
		}

		private void LostFocusHandler(object sender, RoutedEventArgs args)
		{
			FocusLost();
		}

		#endregion

		#region Private Methods

		private void BindToViewModel()
		{
			_subscriptions.Add(ViewModel.Parent.WhenAnyValue(layout => layout.FullSize)
				.Subscribe(size =>
				{
					UpdateViewOrigin();
					UpdateViewSize();
				}));

			UpdateViewOrigin();
			_subscriptions.Add(ViewModel.WhenAnyValue(vm => vm.Origin)
				.Where(_ => !IsEditingShape)
				.Subscribe(_ =>
				{
					UpdateViewOrigin();
				}));

			UpdateViewSize();
			_subscriptions.Add(ViewModel.WhenAnyValue(vm => vm.Size)
				.Where(_ => !IsEditingShape)
				.Subscribe(_ =>
				{
					UpdateViewSize();
				}));

			UpdateViewOrientation();
			_subscriptions.Add(ViewModel.WhenAnyValue(vm => vm.Orientation)
				.Where(_ => !IsEditingShape)
				.Subscribe(_ =>
				{
					UpdateViewOrientation();
				}));

			_subscriptions.Add(ViewModel.WhenAnyValue(vm => vm.IsSelected)
				.Subscribe(isSelected =>
				{
					Resize.Visibility = isSelected ? Visibility.Visible : Visibility.Hidden;
					Reposition.Opacity = isSelected ? 1.0f : .25f;
					Orient.Visibility = isSelected ? Visibility.Visible : Visibility.Hidden;

					if (!isSelected)
					{
						FocusLost();
						Reposition.HorizontalSnapPoints = null;
						Reposition.VerticalSnapPoints = null;
						Resize.HorizontalSnapPoints = null;
						Resize.VerticalSnapPoints = null;
					}
					else
					{
						RecalculateSnapPoints();
					}
				}));

			_subscriptions.Add(ViewModel.WhenAnyValue(vm => vm.IsEditorVisible)
				.Subscribe(isVisible =>
				{
					Visibility = isVisible ? Visibility.Visible : Visibility.Hidden;
				}));

			_subscriptions.Add(ViewModel.Parent.WhenAnyValue(
				layout => layout.Size,
				layout => layout.Bleed,
				layout => layout.SafeZone)
				.Subscribe(_ =>
				{
					if (ViewModel.IsSelected)
						RecalculateSnapPoints();
				}));

			_subscriptions.Add(this.OneWayBind(ViewModel, vm => vm.Name, v => v.ElementName.Text));
		}

		private void FocusLost()
		{
			ViewModel.IsSelected = false;
		}

		private void UpdateVmOrigin()
		{
			Size displayDiff = new Size((float)Canvas.GetLeft(this), (float)Canvas.GetTop(this));
			Size mmDiff = Math.GetInchesFromDisplaySize(displayDiff, App.Ppi) - ViewModel.Parent.Bleed;
			ViewModel.Origin = new Point(mmDiff.Width, mmDiff.Height);
		}

		private void UpdateVmSize()
		{
			Size displayDiff = new Size((float)Width, (float)Height);
			Size mmDiff = Math.GetInchesFromDisplaySize(displayDiff, App.Ppi);
			ViewModel.Size = mmDiff;
		}

		private void UpdateVmOrientation()
		{
			ViewModel.Orientation = Orient.Orientation;
		}

		private void UpdateViewOrigin()
		{
			Size displayPos = Math.GetDisplaySizeFromInches(new Size(ViewModel.Origin) + ViewModel.Parent.Bleed, App.Ppi);
			Canvas.SetLeft(this, displayPos.Width);
			Canvas.SetTop(this, displayPos.Height);
		}

		private void UpdateViewSize()
		{
			Size displaySize = Math.GetDisplaySizeFromInches(ViewModel.Size, App.Ppi);
			Width = displaySize.Width;
			Height = displaySize.Height;
		}

		private void UpdateViewOrientation()
		{
			Orient.Orientation = ViewModel.Orientation;

			Reposition.Orientation = Orient.Orientation.ToRadians();
			Resize.Orientation = Orient.Orientation;
		}

		private void RecalculateSnapPoints()
		{
			List<HorizontalSnapPoint> hMoveSnapPts = new List<HorizontalSnapPoint>();
			List<VerticalSnapPoint> vMoveSnapPts = new List<VerticalSnapPoint>();

			_layoutView.GetSnapPointsForElement(this, ref hMoveSnapPts, ref vMoveSnapPts);

			Reposition.HorizontalSnapPoints = hMoveSnapPts;
			Reposition.VerticalSnapPoints = vMoveSnapPts;

			Resize.HorizontalSnapPoints = hMoveSnapPts;
			Resize.VerticalSnapPoints = vMoveSnapPts;
		}

		private void DoDispose(bool finalize)
		{
			if (_disposed)
				return;

			_subscriptions.ForEach(_ => _.Dispose());
			_subscriptions.Clear();
			_disposed = true;
		}

		#endregion
	}
}
