﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using CardEditor.Util;
using Core.View_Models.Layout;
using ReactiveUI;
using Math = Core.Util.Math;

namespace CardEditor.Views.Layout
{
	/// <summary>
	/// Interaction logic for ColorElementView.xaml
	/// </summary>
	public partial class ColorElementView : UserControl, IViewFor<ColorElementViewModel>
	{
		private readonly List<IDisposable> _subscriptions = new List<IDisposable>();
		private readonly ElementViewHelper _helper;
		private bool _disposed;

		#region Dependency Property

		public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
			typeof(ColorElementViewModel),
			typeof(ColorElementView));

		#endregion

		#region Consturctor & Finalizer

		/// <summary>
		/// Constructs a ColorElementView
		/// </summary>
		public ColorElementView()
		{
			InitializeComponent();

			this.WhenAnyValue(_ => _.ViewModel).BindTo(this, _ => _.DataContext);

			_helper = new ElementViewHelper(this);
		}

		~ColorElementView()
		{
			DoDispose(true);
		}

		#endregion

		#region Implementation of IViewFor

		/// <summary>
		/// </summary>
		object IViewFor.ViewModel
		{
			get => ViewModel;
			set => ViewModel = (ColorElementViewModel)value;
		}

		#endregion

		#region Implementation of IViewFor<ColorElementViewModel>

		/// <summary>
		/// The ViewModel corresponding to this specific View. This should be
		/// a DependencyProperty if you're using XAML.
		/// </summary>
		public ColorElementViewModel ViewModel
		{
			get => (ColorElementViewModel) GetValue(ViewModelProperty);
			set
			{
				SetValue(ViewModelProperty, value);
				BindToViewModel();
			}
		}

		#endregion

		#region Implementation of IDisposable

		/// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
		public void Dispose()
		{
			DoDispose(false);
		}

		#endregion

		#region Private Methods

		private void BindToViewModel()
		{
			_subscriptions.Add(ViewModel.WhenAnyValue(vm => vm.CornerRadius, vm => vm.RoundedCorners)
				.Subscribe(_ => UpdateViewCornerRadius()));
			_subscriptions.Add(
				ViewModel.WhenAnyValue(vm => vm.Color, vm => vm.IsBoundToField)
						.Subscribe(_ => UpdateViewColor()));
			_subscriptions.Add(
				ViewModel.WhenAnyValue(vm => vm.BorderThickness)
						.Subscribe(thickness =>
						{
							float pixelThickness = Math.GetDisplayLengthFromInches(thickness, App.Ppi.Width);
							ColorRegion.BorderThickness = new Thickness(pixelThickness);
						}));
			_subscriptions.Add(
				ViewModel.WhenAnyValue(vm => vm.BorderColor)
						.Select(color => color ?? Core.Drawing.Color.Transparent)
						.Subscribe(color =>
						{
							ColorRegion.BorderBrush = new SolidColorBrush(color.ToMediaColor());
						}));

			_helper.ViewModel = ViewModel;
		}

		private void UpdateViewCornerRadius()
		{
			float radius = Math.GetDisplayLengthFromInches(ViewModel.CornerRadius, App.Ppi.Height);
			ColorRegion.CornerRadius = new CornerRadius
			{
				TopLeft =		(ViewModel.RoundedCorners & ElementViewModel.RoundedCornersFlags.TopLeft) != ElementViewModel.RoundedCornersFlags.None ? radius : 0,
				TopRight =		(ViewModel.RoundedCorners & ElementViewModel.RoundedCornersFlags.TopRight) != ElementViewModel.RoundedCornersFlags.None ? radius : 0,
				BottomLeft =	(ViewModel.RoundedCorners & ElementViewModel.RoundedCornersFlags.BottomLeft) != ElementViewModel.RoundedCornersFlags.None ? radius : 0,
				BottomRight =	(ViewModel.RoundedCorners & ElementViewModel.RoundedCornersFlags.BottomRight) != ElementViewModel.RoundedCornersFlags.None ? radius : 0,
			};
		}

		private void UpdateViewColor()
		{
			Core.Drawing.Color vmColor = ColorElementViewModel.BoundColor;
			if (!ViewModel.IsBoundToField && ViewModel.Color != null)
				vmColor = ViewModel.Color.Value;
			SolidColorBrush brush = new SolidColorBrush(vmColor.ToMediaColor());
			ColorRegion.Background = brush;
		}

		private void DoDispose(bool finalize)
		{
			if (_disposed)
				return;

			_subscriptions.ForEach(sub => sub.Dispose());
			_subscriptions.Clear();
			_helper.Dispose();
			_disposed = true;
		}

		#endregion
	}
}
