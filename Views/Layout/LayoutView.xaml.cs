﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using CardEditor.Controls;
using CardEditor.Util;
using Core.Documents;
using Core.Util;
using Core.View_Models.Layout;
using ReactiveUI;
using Xceed.Wpf.AvalonDock.Controls;
using Brushes = System.Windows.Media.Brushes;
using Colors = System.Windows.Media.Colors;
using Size = Core.Drawing.Size;

namespace CardEditor.Views.Layout
{
	/// <summary>
	/// Interaction logic for LayoutView.xaml
	/// </summary>
	public partial class LayoutView : UserControl, IViewFor<LayoutViewModel>
	{
		#region Private Members

		/// <summary>
		/// Helper class to manage all of the UIs for an ElementViewModel
		/// </summary>
		private class ElementUi : IDisposable
		{
			private readonly LayoutView _layoutView;
			private bool _disposed;

			/// <summary>
			/// The managed ViewModel
			/// </summary>
			public ElementViewModel ViewModel { get; }

			/// <summary>
			/// The Content view (text/image/color/etc.)
			/// </summary>
			public FrameworkElement View { get; }

			/// <summary>
			/// The ElementTransformView for placement
			/// </summary>
			public ElementTransformView ElementTransform { get; }

			/// <summary>
			/// Constructs an ElementUi
			/// </summary>
			/// <param name="vm">The ElementViewModel to wrap</param>
			/// <param name="layout">The containing LayoutView</param>
			public ElementUi(ElementViewModel vm, LayoutView layout)
			{
				_layoutView = layout;
				ViewModel = vm;
				View = CreateElementView(vm);
				ElementTransform = new ElementTransformView(layout)
				{
					ViewModel = vm,
					SnapTolerance = Properties.Settings.Default.SnapSensitivity
				};

				layout._previewCanvas.Children.Add(View);
				layout.LayoutCanvas.Children.Add(ElementTransform);
			}

			/// <summary>
			/// Finalizer
			/// </summary>
			~ElementUi()
			{
				DoDispose(true);
			}

			/// <summary>
			/// Sets the Z-indices for the views
			/// </summary>
			/// <param name="index">The element's Z-index</param>
			public void SetZIndex(int index)
			{
				Panel.SetZIndex(View, index);
				Panel.SetZIndex(ElementTransform, index);
			}

			/// <summary>
			/// Disposes the views
			/// </summary>
			public void Dispose()
			{
				DoDispose(false);
			}

			private FrameworkElement CreateElementView(ElementViewModel vm)
			{
				TextElementViewModel textVm = vm as TextElementViewModel;
				if (textVm != null)
				{
					return new TextElementView
					{
						ViewModel = textVm
					};
				}

				ImageElementViewModel imageVm = vm as ImageElementViewModel;
				if (imageVm != null)
				{
					return new ImageElementView
					{
						ViewModel = imageVm
					};
				}

				ColorElementViewModel colorVm = vm as ColorElementViewModel;
				if (colorVm != null)
				{
					return new ColorElementView
					{
						ViewModel = colorVm
					};
				}

				throw new InvalidOperationException("Unknown element type");
			}

			private void DoDispose(bool finalize)
			{
				if (_disposed)
					return;

				(View as IDisposable)?.Dispose();
				ElementTransform.Dispose();

				_layoutView.Dispatcher.InvokeAsync(() =>
				{
					_layoutView._previewCanvas.Children.Remove(View);
					_layoutView.LayoutCanvas.Children.Remove(ElementTransform);
				});

				_disposed = true;
			}
		}
		
		private readonly List<ElementUi> _elementUis = new List<ElementUi>();
		private readonly List<IDisposable> _subscriptions = new List<IDisposable>();
		private readonly ReorderHelper _reorderHelper;
		private IPropertiesView _propertiesView;
		private float _scale = 1.0f;
		private RepositionControl _selectedReposition;
		private ResizeControl _selectedResize;

		// this canvas stores the actual Element views for preview rendering
		private readonly Canvas _previewCanvas = new Canvas
		{
			Background = new SolidColorBrush(Colors.White)
		};
		private Uri _cmykConverter;

		#endregion

		#region Dependency Properties

		/// <summary>
		/// Dependency property
		/// </summary>
		public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
			typeof(LayoutViewModel),
			typeof(LayoutView));

		#endregion

		#region Properties

		public Uri CmykConverter
		{
			get { return _cmykConverter; }
			set
			{
				if (value == _cmykConverter)
					return;

				_cmykConverter = value;
				RenderPreview();
			}
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Calculates all valid snap points for an element
		/// </summary>
		/// <param name="snappedElem">The element we want snap points for</param>
		/// <param name="hSnapPts">The valid horizontal snap points</param>
		/// <param name="vSnapPts">The valid vertical snap points</param>
		public void GetSnapPointsForElement(Control snappedElem, ref List<HorizontalSnapPoint> hSnapPts, ref List<VerticalSnapPoint> vSnapPts)
		{
			// snap top/left edges to top/left edges of the canvas (includes bleed)
			hSnapPts.Add(new HorizontalSnapPoint
			{
				Alignment = HorizontalAlignment.Left,
				Value = 0,
				Element = LayoutCanvas
			});
			vSnapPts.Add(new VerticalSnapPoint
			{
				Alignment = VerticalAlignment.Top,
				Value = 0,
				Element = LayoutCanvas
			});

			Size fullSize = Core.Util.Math.GetDisplaySizeFromInches(ViewModel.FullSize, App.Ppi);

			// snap bottom/right edges to bottom/right edges of the canvas (includes bleed)
			hSnapPts.Add(new HorizontalSnapPoint
			{
				Alignment = HorizontalAlignment.Right,
				Value = fullSize.Width,
				Element = LayoutCanvas
			});
			vSnapPts.Add(new VerticalSnapPoint
			{
				Alignment = VerticalAlignment.Bottom,
				Value = fullSize.Height,
				Element = LayoutCanvas
			});

			// if bleed is defined
			Size bleedTopLeft = Core.Util.Math.GetDisplaySizeFromInches(ViewModel.Bleed, App.Ppi);
			Size bleedBottomRight = fullSize - bleedTopLeft;
			if (ViewModel.Bleed.Width > 0 || ViewModel.Bleed.Height > 0)
			{
				// snap top/left edges to top/left edges of the layout region (excludes bleed)
				hSnapPts.Add(new HorizontalSnapPoint
				{
					Alignment = HorizontalAlignment.Left,
					Value = bleedTopLeft.Width,
					Element = BleedBoundary
				});
				vSnapPts.Add(new VerticalSnapPoint
				{
					Alignment = VerticalAlignment.Top,
					Value = bleedTopLeft.Height,
					Element = BleedBoundary
				});

				// snap bottom/right edges to bottom/right edges of the layout region (excludes bleed)
				hSnapPts.Add(new HorizontalSnapPoint
				{
					Alignment = HorizontalAlignment.Right,
					Value = bleedBottomRight.Width,
					Element = BleedBoundary
				});
				vSnapPts.Add(new VerticalSnapPoint
				{
					Alignment = VerticalAlignment.Bottom,
					Value = bleedBottomRight.Height,
					Element = BleedBoundary
				});
			}

			// if safe zone is defined
			if (ViewModel.SafeZone.Width > 0 || ViewModel.SafeZone.Height > 0)
			{
				Size szSize = Core.Util.Math.GetDisplaySizeFromInches(ViewModel.SafeZone, App.Ppi);
				Size szTopLeft = bleedTopLeft + szSize;
				Size szBottomRight = bleedBottomRight - szSize;

				// snap top/left edges to top/left edges of the safe zone
				hSnapPts.Add(new HorizontalSnapPoint
				{
					Alignment = HorizontalAlignment.Left,
					Value = szTopLeft.Width,
					Element = SafeZoneBoundary
				});
				vSnapPts.Add(new VerticalSnapPoint
				{
					Alignment = VerticalAlignment.Top,
					Value = szTopLeft.Height,
					Element = SafeZoneBoundary
				});

				// snap bottom/right edges to bottom/right edges of the safe zone
				hSnapPts.Add(new HorizontalSnapPoint
				{
					Alignment = HorizontalAlignment.Right,
					Value = szBottomRight.Width,
					Element = SafeZoneBoundary
				});
				vSnapPts.Add(new VerticalSnapPoint
				{
					Alignment = VerticalAlignment.Bottom,
					Value = szBottomRight.Height,
					Element = SafeZoneBoundary
				});
			}

			// snap to align element with middle of the layout
			hSnapPts.Add(new HorizontalSnapPoint
			{
				Alignment = HorizontalAlignment.Center,
				Value = fullSize.Width / 2,
				Element = LayoutCanvas
			});
			vSnapPts.Add(new VerticalSnapPoint
			{
				Alignment = VerticalAlignment.Center,
				Value = fullSize.Height / 2,
				Element = LayoutCanvas
			});

			// for all elements that are not the target element
			foreach (ElementUi ui in _elementUis)
			{
				if (ReferenceEquals(ui.ElementTransform, snappedElem))
					continue;

				ElementViewModel elemVm = ui.ViewModel;

				Size elemTopLeft = bleedTopLeft + Core.Util.Math.GetDisplaySizeFromInches(elemVm.Origin.ToSize(), App.Ppi);
				Size elemBottomRight = elemTopLeft + Core.Util.Math.GetDisplaySizeFromInches(elemVm.Size, App.Ppi);
				Size elemMidPoint = (elemTopLeft + elemBottomRight) / 2;

				// snap top/left edge to top/left edges of other elements
				hSnapPts.Add(new HorizontalSnapPoint
				{
					Alignment = HorizontalAlignment.Left,
					Value = elemTopLeft.Width,
					Element = ui.ElementTransform
				});
				vSnapPts.Add(new VerticalSnapPoint
				{
					Alignment = VerticalAlignment.Top,
					Value = elemTopLeft.Height,
					Element = ui.ElementTransform
				});

				// snap top/left edge to bottom/right edges of other elements
				hSnapPts.Add(new HorizontalSnapPoint
				{
					Alignment = HorizontalAlignment.Left,
					Value = elemBottomRight.Width,
					Element = ui.ElementTransform
				});
				vSnapPts.Add(new VerticalSnapPoint
				{
					Alignment = VerticalAlignment.Top,
					Value = elemBottomRight.Height,
					Element = ui.ElementTransform
				});

				// snap bottom/right edge to top/left edges of other elements
				hSnapPts.Add(new HorizontalSnapPoint
				{
					Alignment = HorizontalAlignment.Right,
					Value = elemTopLeft.Width,
					Element = ui.ElementTransform
				});
				vSnapPts.Add(new VerticalSnapPoint
				{
					Alignment = VerticalAlignment.Bottom,
					Value = elemTopLeft.Height,
					Element = ui.ElementTransform
				});

				// snap bottom/right edge to bottom/right edges of other elements
				hSnapPts.Add(new HorizontalSnapPoint
				{
					Alignment = HorizontalAlignment.Right,
					Value = elemBottomRight.Width,
					Element = ui.ElementTransform
				});
				vSnapPts.Add(new VerticalSnapPoint
				{
					Alignment = VerticalAlignment.Bottom,
					Value = elemBottomRight.Height,
					Element = ui.ElementTransform
				});

				// snap center to center of other elements
				hSnapPts.Add(new HorizontalSnapPoint
				{
					Alignment = HorizontalAlignment.Center,
					Value = elemMidPoint.Width,
					Element = ui.ElementTransform
				});
				vSnapPts.Add(new VerticalSnapPoint
				{
					Alignment = VerticalAlignment.Center,
					Value = elemMidPoint.Height,
					Element = ui.ElementTransform
				});
			}
		}

		#endregion

		#region Constructor

		/// <summary>
		/// Constructs a LayoutView
		/// </summary>
		public LayoutView()
		{
			InitializeComponent();

			this.WhenAnyValue(_ => _.ViewModel).BindTo(this, _ => _.DataContext);
			this.OneWayBind(ViewModel, vm => vm.Elements, v => v.ElementList.ItemsSource);
			this.Bind(ViewModel, vm => vm.SelectedElement, v => v.ElementList.SelectedItem);

			this.BindCommand(ViewModel, vm => vm.AddElement, v => v.NewImage, Observable.Return(typeof(ImageElement)));
			this.BindCommand(ViewModel, vm => vm.AddElement, v => v.NewText, Observable.Return(typeof(TextElement)));
			this.BindCommand(ViewModel, vm => vm.AddElement, v => v.NewColor, Observable.Return(typeof(ColorElement)));
			this.BindCommand(ViewModel, vm => vm.RemoveSelectedElement, v => v.RemoveElementButton);
			this.BindCommand(ViewModel, vm => vm.MoveSelectedElementBack, v => v.PushBack);
			this.BindCommand(ViewModel, vm => vm.MoveSelectedElementForward, v => v.PushForward);
			this.BindCommand(ViewModel, vm => vm.MoveSelectedElementToBack, v => v.MoveToBack);
			this.BindCommand(ViewModel, vm => vm.MoveSelectedElementToFront, v => v.MoveToFront);
			this.BindCommand(ViewModel, vm => vm.Undo, v => v.Undo);
			this.BindCommand(ViewModel, vm => vm.Redo, v => v.Redo);

			_subscriptions.Add(
				this.WhenAnyValue(v => v.ScaleSelector.Text)
					.Throttle(TimeSpan.FromSeconds(.25))
					.ObserveOn(RxApp.MainThreadScheduler)
					.Select(text =>
					{
						string scaleText = text.Trim().Trim('%');
						float scale;
						bool scaleValid = float.TryParse(scaleText, out scale);
						if (scaleValid)
							_scale = scale * .01f;

						return scaleValid;
					})
					.Where(validScale => validScale)
					.Subscribe(_ =>
					{
						ScaleTransform transform = new ScaleTransform(_scale, _scale);
						LayoutCanvas.LayoutTransform = transform;
						_elementUis.Select(ui => ui.ElementTransform).ForEach(view => view.Scale = _scale);
					}));

			_reorderHelper = new ReorderHelper(ElementList);
			_reorderHelper.ItemReordered += (sender, args) =>
			{
				ViewModel.SelectedElement = args.Item as ElementViewModel;
			};

			Loaded += LoadedHandler;
			Unloaded += UnloadedHandler;
		}

		#endregion

		#region Implementation of IViewFor

		/// <summary>
		/// </summary>
		object IViewFor.ViewModel
		{
			get => ViewModel;
			set => ViewModel = (LayoutViewModel)value;
		}

		#endregion

		#region Implementation of IViewFor<LayoutViewModel>

		/// <summary>
		/// The ViewModel corresponding to this specific View. This should be
		/// a DependencyProperty if you're using XAML.
		/// </summary>
		public LayoutViewModel ViewModel
		{
			get { return (LayoutViewModel) GetValue(ViewModelProperty); }
			set
			{
				SetValue(ViewModelProperty, value);
				BindToViewModel();
			}
		}

		#endregion

		#region WPF Event Handlers

		private void LoadedHandler(object sender, RoutedEventArgs e)
		{
			CmykConverter = Properties.Settings.Default.CmykConverter;
			Properties.Settings.Default.PropertyChanged += DefaultPropertyChangedHandler;
		}

		private void UnloadedHandler(object sender, RoutedEventArgs e)
		{
			Properties.Settings.Default.PropertyChanged -= DefaultPropertyChangedHandler;

			_subscriptions.ForEach(_ => _.Dispose());
			_subscriptions.Clear();
		}

		private void ResizeClickHandler(object sender, RoutedEventArgs e)
		{
			SelectSize(true);
		}

		private void NamePreviewKeyDownHandler(object sender, KeyEventArgs args)
		{
			if (args.Key == Key.Escape || args.Key == Key.Enter)
			{
				TextBox textBox = sender as TextBox;
				if (textBox == null)
					throw new InvalidCastException("Name control is expected to be a TextBox");

				BindingExpression exp = textBox.GetBindingExpression(TextBox.TextProperty);
				if (exp == null)
					throw new NullReferenceException("Failed to binding expression for TextBox's Text property");

				exp.UpdateSource();
				Keyboard.ClearFocus();

				textBox.Cursor = Cursors.Arrow;
				textBox.Background = Brushes.Transparent;
				textBox.Focusable = false;

				args.Handled = true;
			}
		}

		private void NameMouseDoubleClickHandler(object sender, MouseButtonEventArgs args)
		{
			TextBox textBox = sender as TextBox;
			if (textBox == null)
				throw new InvalidCastException("Name control is expected to be a TextBox");

			textBox.Focusable = true;
			textBox.Focus();
			textBox.Cursor = Cursors.IBeam;
			textBox.Background = Brushes.White;

			args.Handled = true;
		}

		private void NameLostFocusHandler(object sender, RoutedEventArgs args)
		{
			TextBox textBox = sender as TextBox;
			if (textBox == null)
				throw new InvalidCastException("Name control is expected to be a TextBox");

			textBox.Focusable = false;
			textBox.Background = Brushes.Transparent;
			textBox.Cursor = Cursors.Arrow;
		}

		private void NewElementButton_OnClick(object sender, RoutedEventArgs args)
		{
			NewElementMenu.IsOpen = true;
		}

		private void ElementList_MouseDownHandler(object sender, MouseButtonEventArgs args)
		{
			ElementList.SelectedItem = null;
		}

		private void ElementList_PreviewKeyDownHandler(object sender, KeyEventArgs args)
		{
			switch (args.Key)
			{
				case Key.Down:
					if (ElementList.SelectedIndex == -1)
					{
						if (ElementList.Items.Count > 0)
							ElementList.SelectedIndex = 0;
					}
					else
						ElementList.SelectedIndex = System.Math.Min(ElementList.SelectedIndex + 1, ElementList.Items.Count);
					args.Handled = true;
					break;

				case Key.Up:
					if (ElementList.SelectedIndex == -1)
					{
						if (ElementList.Items.Count > 0)
							ElementList.SelectedIndex = ElementList.Items.Count - 1;
					}
					else
						ElementList.SelectedIndex = System.Math.Max(ElementList.SelectedIndex - 1, 0);
					args.Handled = true;
					break;

				default:
					KeyDownHandler(sender, args);	// try to perform other input handling, since ListView swallows it otherwise
					break;
			}
		}

		private void KeyDownHandler(object sender, KeyEventArgs args)
		{
			ICommand command = null;
			switch (args.Key)
			{
				case Key.Delete:
					if (ViewModel.SelectedElement != null)
						command = ViewModel.RemoveSelectedElement;
					break;

				case Key.Z:
					if (Keyboard.Modifiers == ModifierKeys.Control)
						command = ViewModel.Undo;
					break;

				case Key.Y:
					if (Keyboard.Modifiers == ModifierKeys.Control)
						command = ViewModel.Redo;
					break;
			}

			if (command != null)
			{
				command.Execute(null);
				args.Handled = true;
			}
		}

		private void SelectedElementViewLeftChangedHandler(object sender, EventArgs e)
		{
			RecalcVerticalSnapLine();
		}

		private void SelectedElementViewTopChangedHandler(object sender, EventArgs e)
		{
			RecalcHoriztonalSnapLine();
		}

		#endregion

		#region Private Methods

		private void BindToViewModel()
		{
			ViewModel.UndoStackSize = Properties.Settings.Default.UndoStackSize;

			BindCanvasSize();
			AddElementViews(ViewModel.Elements, 0);

			_subscriptions.Add(
				ViewModel.Elements.Changed.ObserveOn(RxApp.MainThreadScheduler).Subscribe(args =>
				{
					switch (args.Action)
					{
						case NotifyCollectionChangedAction.Add:
							AddElementViews(args.NewItems, args.NewStartingIndex);
							break;

						case NotifyCollectionChangedAction.Remove:
							RemoveElementViews(args.OldItems, args.OldStartingIndex);
							break;

						case NotifyCollectionChangedAction.Reset:
							RemoveElementViews(ViewModel.Elements, ViewModel.Elements.Count);
							AddElementViews(ViewModel.Elements, ViewModel.Elements.Count);
							break;

						case NotifyCollectionChangedAction.Replace:
							RemoveElementViews(args.OldItems, args.OldStartingIndex);
							AddElementViews(args.NewItems, args.NewStartingIndex);
							break;

						default:
							// TODO: handle move
							throw new InvalidOperationException($"Unknown operation {args.Action}");
					}

					RenderPreview();
				}));
			_subscriptions.Add(
				ViewModel.Elements.ItemChanged
						.Throttle(TimeSpan.FromSeconds(.25))
						.ObserveOn(RxApp.MainThreadScheduler)
						.Subscribe(_ =>
						{
							RenderPreview();
						}));
			if (ViewModel.SelectedElement != null)
				PopulateProperties();

			_subscriptions.Add(
				ViewModel.WhenAnyValue(vm => vm.SelectedElement)
						.Buffer(2, 1)
						.Where(elems => elems[0] != elems[1])
						.Select(elems => new { prev = elems[0], next = elems[1] })
						.Subscribe(change =>
						{
							UnsubscribeFromElementView(change.prev);
							SubscribeToElementView(change.next);
							PopulateProperties();
						}));

			if (ViewModel.Size.Width <= 0.0f || ViewModel.Size.Height <= 0.0f)
				SelectSize(false);

			// just force a render once to make sure everything gets updated
			Dispatcher.InvokeAsync((Action) RenderPreview);
		}

		private void BindCanvasSize()
		{
			UpdateCanvasSize();
			_subscriptions.Add(
				ViewModel.WhenAnyValue(vm => vm.Size, vm => vm.Bleed, vm => vm.SafeZone)
					.ObserveOn(RxApp.MainThreadScheduler)
					.Subscribe(newSize =>
					{
						UpdateCanvasSize();
						RenderPreview();
					}));
		}

		private void UpdateCanvasSize()
		{
			Size fullRenderSize = Core.Util.Math.GetDisplaySizeFromInches(ViewModel.FullSize, App.Ppi);
			LayoutCanvas.Width = fullRenderSize.Width;
			LayoutCanvas.Height = fullRenderSize.Height;
			LayoutPreview.Width = fullRenderSize.Width;
			LayoutPreview.Height = fullRenderSize.Height;

			Size cardRenderSize = Core.Util.Math.GetDisplaySizeFromInches(ViewModel.Size, App.Ppi);
			Size bleedRenderPos = Core.Util.Math.GetDisplaySizeFromInches(ViewModel.Bleed, App.Ppi);
			BleedBoundary.Width = cardRenderSize.Width;
			BleedBoundary.Height = cardRenderSize.Height;
			Canvas.SetLeft(BleedBoundary, bleedRenderPos.Width);
			Canvas.SetTop(BleedBoundary, bleedRenderPos.Height);

			bool haveSafeZone = ViewModel.SafeZone.Width > 0 || ViewModel.SafeZone.Height > 0;
			string safeZoneText;
			if (haveSafeZone)
			{
				SafeZoneBoundary.Visibility = Visibility.Visible;
				Size safeZoneSize = Core.Util.Math.GetDisplaySizeFromInches(ViewModel.Size - ViewModel.SafeZone * 2, App.Ppi);
				Size safeZonePos = Core.Util.Math.GetDisplaySizeFromInches(ViewModel.Bleed + ViewModel.SafeZone, App.Ppi);
				SafeZoneBoundary.Width = safeZoneSize.Width;
				SafeZoneBoundary.Height = safeZoneSize.Height;
				Canvas.SetLeft(SafeZoneBoundary, safeZonePos.Width);
				Canvas.SetTop(SafeZoneBoundary, safeZonePos.Height);

				safeZoneText = $"{ViewModel.SafeZone.Width:0.###}\" x {ViewModel.SafeZone.Height:0.###}\" safe zone";
			}
			else
			{
				SafeZoneBoundary.Visibility = Visibility.Hidden;
				safeZoneText = "No safe zone";
			}

			SizeText.Text = $"{ViewModel.Size.Width:0.###}\" x {ViewModel.Size.Height:0.###}\" size + " +
			                $"{ViewModel.Bleed.Width:0.###}\" x {ViewModel.Bleed.Height:0.###}\" bleed, " +
							safeZoneText;
		}

		private void AddElementViews(IList elements, int startIndex)
		{
			int index = startIndex;
			foreach (ElementViewModel element in elements)
			{
				ElementUi newUi = new ElementUi(element, this);
				_elementUis.Insert(index, newUi);
				newUi.SetZIndex(index);

				index++;

				if (element == ViewModel.SelectedElement)
					newUi.ElementTransform.Loaded += SelectedElementLoadedHandler;
			}

			for (; index < _elementUis.Count; index++)
				_elementUis[index].SetZIndex(index);
		}

		private void RemoveElementViews(IList elements, int startIndex)
		{
			foreach (ElementViewModel element in elements)
			{
				if (element == ViewModel.SelectedElement)
					UnsubscribeFromElementView(element);

				ElementUi elemUi = _elementUis.FirstOrDefault(ui => ui.ViewModel == element);
				if (elemUi == null)
					throw new NullReferenceException("Could not locate ElementUi for ViewModel");

				elemUi.Dispose();
				_elementUis.Remove(elemUi);
			}
			
			for (int i = startIndex; i < _elementUis.Count; i++)
				_elementUis[i].SetZIndex(i);
		}

		private void SelectedElementLoadedHandler(object sender, RoutedEventArgs e)
		{
			SubscribeToElementView(ViewModel.SelectedElement);

			// unsubscribing from within the handler is unsafe, let's schedule it to happen later on the main thread
			FrameworkElement elemView = sender as FrameworkElement;
			if (elemView == null)
				throw new InvalidCastException();
			elemView.Dispatcher.Invoke(() => elemView.Loaded -= SelectedElementLoadedHandler);
		}

		private void SelectSize(bool setInitials)
		{
			Dialogs.SizeSelectionDialog dialog = new Dialogs.SizeSelectionDialog
			{
				Owner = Application.Current.MainWindow
			};
			if (setInitials)
			{
				dialog.InitialSize = ViewModel.Size;
				dialog.InitialBleed = ViewModel.Bleed;
				dialog.SafeZone = ViewModel.SafeZone;
			}

			if (dialog.ShowDialog() == true)
			{
				if (dialog.Size == null)
					throw new NullReferenceException("Size selection dialog returned a null Size");
				if (dialog.Bleed == null)
					throw new NullReferenceException("Size selection dialog returned a null Bleed");

				ViewModel.Size = dialog.Size.Value;
				ViewModel.Bleed = dialog.Bleed.Value;
				ViewModel.SafeZone = dialog.SafeZone;
			}
		}

		private void PopulateProperties()
		{
			if (ViewModel.SelectedElement != null)
				this.Bind(ViewModel, vm => vm.SelectedElement.Name, v => v.PropertiesElementName.Text);
			else
				PropertiesElementName.Text = string.Empty;

			_propertiesView?.Dispose();

			TextElementViewModel textVm = ViewModel.SelectedElement as TextElementViewModel;
			if (textVm != null)
			{
				TextElementPropertiesView view = new TextElementPropertiesView();
				_propertiesView = view;
				PropertiesContainer.Content = view;
				view.ViewModel = textVm;
				return;
			}

			ImageElementViewModel imgVm = ViewModel.SelectedElement as ImageElementViewModel;
			if (imgVm != null)
			{
				ImageElementPropertiesView view = new ImageElementPropertiesView();
				_propertiesView = view;
				PropertiesContainer.Content = view;
				view.ViewModel = imgVm;
				return;
			}

			ColorElementViewModel colorVm = ViewModel.SelectedElement as ColorElementViewModel;
			if (colorVm != null)
			{
				ColorElementPropertiesView view = new ColorElementPropertiesView();
				_propertiesView = view;
				PropertiesContainer.Content = view;
				view.ViewModel = colorVm;
				return;
			}

			_propertiesView = null;
			PropertiesContainer.Content = null;
		}

		private void SubscribeToElementView(ElementViewModel element)
		{
			if (element == null)
				return;

			ElementUi ui = _elementUis.FirstOrDefault(u => u.ViewModel == element);
			if (ui == null)
				return;

			RepositionControl reposition = ui.ElementTransform.RepositionControl;
			if (_selectedReposition != null)
				if (ReferenceEquals(reposition, _selectedReposition))
					return;
				else
					throw new ArgumentException(@"Subscribing to element when we already have a selected element", nameof(element));

			_selectedReposition = reposition;

			_selectedReposition.IsRepositioningChanged += IsRepositioningChangedHandler;
			_selectedReposition.SnappedHorizontalPointsChanged += SnappedHorizontalPointsChangedHandler;
			_selectedReposition.SnappedVerticalPointsChanged += SnappedVerticalPointsChangedHandler;

			ResizeControl resize = ui.ElementTransform.ResizeControl;
			if (_selectedResize != null)
				if (ReferenceEquals(resize, _selectedResize))
					return;
				else
					throw new ArgumentException(@"Subscribing to element when we already have a selected element", nameof(element));

			_selectedResize = resize;
			_selectedResize.IsResizingChanged += IsResizingChangedHandler;
			_selectedResize.SnappedHorizontalPointsChanged += SnappedHorizontalPointsChangedHandler;
			_selectedResize.SnappedVerticalPointsChanged += SnappedVerticalPointsChangedHandler;

			DependencyPropertyDescriptor.FromProperty(Canvas.TopProperty, ui.ElementTransform.GetType())
							.AddValueChanged(ui.ElementTransform, SelectedElementViewTopChangedHandler);
			DependencyPropertyDescriptor.FromProperty(Canvas.LeftProperty, ui.ElementTransform.GetType())
										.AddValueChanged(ui.ElementTransform, SelectedElementViewLeftChangedHandler);
		}

		private void UnsubscribeFromElementView(ElementViewModel element)
		{
			if (element == null)
				return;

			ElementUi ui = _elementUis.FirstOrDefault(u => u.ViewModel == element);
			if (ui == null)
				return;

			if (_selectedReposition != null)
			{
				RepositionControl reposition = ui.ElementTransform.FindLogicalChildren<RepositionControl>().First();
				if (!ReferenceEquals(reposition, _selectedReposition))
					throw new ArgumentException(@"Unsubscribing from element that is not the selected element", nameof(element));

				_selectedReposition.IsRepositioningChanged -= IsRepositioningChangedHandler;
				_selectedReposition.SnappedHorizontalPointsChanged -= SnappedHorizontalPointsChangedHandler;
				_selectedReposition.SnappedVerticalPointsChanged -= SnappedVerticalPointsChangedHandler;
				_selectedReposition = null;
			}

			if (_selectedResize != null)
			{
				ResizeControl resize = ui.ElementTransform.FindLogicalChildren<ResizeControl>().First();
				if (!ReferenceEquals(resize, _selectedResize))
					throw new ArgumentException(@"Unsubscribing from element that is not the selected element", nameof(element));

				_selectedResize.IsResizingChanged -= IsResizingChangedHandler;
				_selectedResize.SnappedHorizontalPointsChanged -= SnappedHorizontalPointsChangedHandler;
				_selectedResize.SnappedVerticalPointsChanged -= SnappedVerticalPointsChangedHandler;
				_selectedResize = null;
			}

			DependencyPropertyDescriptor.FromProperty(Canvas.TopProperty, ui.ElementTransform.GetType())
							.RemoveValueChanged(ui.ElementTransform, SelectedElementViewTopChangedHandler);
			DependencyPropertyDescriptor.FromProperty(Canvas.LeftProperty, ui.ElementTransform.GetType())
										.RemoveValueChanged(ui.ElementTransform, SelectedElementViewLeftChangedHandler);

		}

		private void RecalcHoriztonalSnapLine()
		{
			List<HorizontalSnapPoint> hSnaps = null;
			FrameworkElement snappingElement = null;

			if (_selectedReposition.IsRepositioning && _selectedReposition.SnappedHorizontalPoints.Any())
			{
				hSnaps = _selectedReposition.SnappedHorizontalPoints;
				snappingElement = _selectedReposition.RepositionTarget;
			}
			else if (_selectedResize.IsResizing && _selectedResize.SnappedHorizontalPoints.Any())
			{
				hSnaps = _selectedResize.SnappedHorizontalPoints;
				snappingElement = _selectedResize.ResizeTarget;
			}

			if (hSnaps != null && hSnaps.Any())
			{
				double minY = Canvas.GetTop(snappingElement);
				double maxY = minY + snappingElement.Height;
				foreach (HorizontalSnapPoint snapPoint in hSnaps)
				{
					double elemMinY = ReferenceEquals(snapPoint.Element, LayoutCanvas) ?
						0 :
						Canvas.GetTop(snapPoint.Element);
					double elemMaxY = elemMinY + snapPoint.Element.Height;

					minY = System.Math.Min(minY, elemMinY);
					maxY = System.Math.Max(maxY, elemMaxY);
				}
				HorizontalSnapLine.Visibility = Visibility.Visible;
				HorizontalSnapLine.X1 = hSnaps.First().Value;
				HorizontalSnapLine.X2 = hSnaps.First().Value;
				HorizontalSnapLine.Y1 = minY - 10;
				HorizontalSnapLine.Y2 = maxY + 10;
			}
			else
			{
				HorizontalSnapLine.Visibility = Visibility.Collapsed;
			}
		}

		private void RecalcVerticalSnapLine()
		{
			List<VerticalSnapPoint> vSnaps = null;
			FrameworkElement snappingElement = null;

			if (_selectedReposition.IsRepositioning && _selectedReposition.SnappedVerticalPoints.Any())
			{
				vSnaps = _selectedReposition.SnappedVerticalPoints;
				snappingElement = _selectedReposition.RepositionTarget;
			}
			else if (_selectedResize.IsResizing && _selectedResize.SnappedVerticalPoints.Any())
			{
				vSnaps = _selectedResize.SnappedVerticalPoints;
				snappingElement = _selectedResize.ResizeTarget;
			}

			if (vSnaps != null && vSnaps.Any())
			{
				double minX = Canvas.GetLeft(snappingElement);
				double maxX = minX + snappingElement.Width;
				foreach (VerticalSnapPoint snapPoint in vSnaps)
				{
					double elemMinX = ReferenceEquals(snapPoint.Element, LayoutCanvas) ?
						0 :
						Canvas.GetLeft(snapPoint.Element);
					double elemMaxX = elemMinX + snapPoint.Element.Width;

					minX = System.Math.Min(minX, elemMinX);
					maxX = System.Math.Max(maxX, elemMaxX);
				}
				VerticalSnapLine.Visibility = Visibility.Visible;
				VerticalSnapLine.Y1 = vSnaps.First().Value;
				VerticalSnapLine.Y2 = vSnaps.First().Value;
				VerticalSnapLine.X1 = minX - 10;
				VerticalSnapLine.X2 = maxX + 10;
			}
			else
			{
				VerticalSnapLine.Visibility = Visibility.Collapsed;
			}
		}

		private void RenderPreview()
		{
			int imageWidth = (int)(LayoutPreview.Width + .5f);
			int imageHeight = (int)(LayoutPreview.Height + .5f);

			if (imageWidth <= 0 || imageHeight <= 0)
				return;

			// manually force a layout pass on the card canvas to update the preview elements
			System.Windows.Size viewSize = new System.Windows.Size(LayoutCanvas.Width, LayoutCanvas.Height);
			_previewCanvas.Measure(viewSize);
			_previewCanvas.Arrange(new Rect(viewSize));
			_previewCanvas.UpdateLayout();

			RenderTargetBitmap target = new RenderTargetBitmap(
				imageWidth,
				imageHeight,
				96.0,				// NOTE: we must ALWAYS use 96 dpi when rendering the image out
				96.0,
				PixelFormats.Default);
			target.Render(_previewCanvas);
			target.Freeze();

			BitmapSource result;
			if (CmykConverter != null)
			{
				ColorContext rgbContext = new ColorContext(target.Format);
				ColorContext cmykContext = new ColorContext(CmykConverter);
				ColorConvertedBitmap ccb = new ColorConvertedBitmap();
				ccb.BeginInit();
				ccb.SourceColorContext = rgbContext;
				ccb.Source = target;
				ccb.DestinationColorContext = cmykContext;
				ccb.DestinationFormat = PixelFormats.Cmyk32;
				ccb.EndInit();

				result = ccb;
			}
			else
				result = target;

			LayoutPreview.Source = result;
		}

		#endregion

		#region Event Handlers

		private void DefaultPropertyChangedHandler(object sender, PropertyChangedEventArgs args)
		{
			if (args.PropertyName == nameof(Properties.Settings.Default.SnapSensitivity))
				_elementUis.ForEach(ui => ui.ElementTransform.SnapTolerance = Properties.Settings.Default.SnapSensitivity);
			else if (args.PropertyName == nameof(Properties.Settings.Default.CmykConverter))
				CmykConverter = Properties.Settings.Default.CmykConverter;
			else if (args.PropertyName == nameof(Properties.Settings.Default.UndoStackSize))
				ViewModel.UndoStackSize = Properties.Settings.Default.UndoStackSize;
		}

		private void IsRepositioningChangedHandler(object sender, EventArgs eventArgs)
		{
			if (!ReferenceEquals(_selectedReposition, sender))
				throw new InvalidOperationException();

			if (_selectedReposition.IsRepositioning)
			{
				if (_selectedReposition.SnappedHorizontalPoints.Any())
					HorizontalSnapLine.Visibility = Visibility.Visible;
				if (_selectedReposition.SnappedVerticalPoints.Any())
					VerticalSnapLine.Visibility = Visibility.Visible;
			}
			else
			{
				HorizontalSnapLine.Visibility = Visibility.Collapsed;
				VerticalSnapLine.Visibility = Visibility.Collapsed;
			}
		}

		private void IsResizingChangedHandler(object sender, EventArgs eventArgs)
		{
			if (!ReferenceEquals(_selectedResize, sender))
				throw new InvalidOperationException();

			if (_selectedResize.IsResizing)
			{
				if (_selectedResize.SnappedHorizontalPoints.Any())
					HorizontalSnapLine.Visibility = Visibility.Visible;
				if (_selectedResize.SnappedVerticalPoints.Any())
					VerticalSnapLine.Visibility = Visibility.Visible;
			}
			else
			{
				HorizontalSnapLine.Visibility = Visibility.Collapsed;
				VerticalSnapLine.Visibility = Visibility.Collapsed;
			}
		}

		private void SnappedHorizontalPointsChangedHandler(object sender, EventArgs eventArgs)
		{
			if (!ReferenceEquals(_selectedReposition, sender) &&
				!ReferenceEquals(_selectedResize, sender))
				throw new InvalidOperationException();

			RecalcHoriztonalSnapLine();
		}

		private void SnappedVerticalPointsChangedHandler(object sender, EventArgs eventArgs)
		{
			if (!ReferenceEquals(_selectedReposition, sender) &&
				!ReferenceEquals(_selectedResize, sender))
				throw new InvalidOperationException();

			RecalcVerticalSnapLine();
		}

		#endregion
	}
}
