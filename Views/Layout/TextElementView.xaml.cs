﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Media;
using CardEditor.Util;
using Core.Drawing;
using Core.Util;
using Core.View_Models.Layout;
using ReactiveUI;
using Color = System.Windows.Media.Color;
using Math = Core.Util.Math;
using UserControl = System.Windows.Controls.UserControl;

namespace CardEditor.Views.Layout
{
	/// <summary>
	/// Interaction logic for TextElementView.xaml
	/// </summary>
	public partial class TextElementView : UserControl, IViewFor<TextElementViewModel>
	{
		private readonly List<IDisposable> _subscriptions = new List<IDisposable>();
		private readonly ElementViewHelper _helper;
		private bool _disposed;

		#region Dependency Properties

		/// <summary>
		/// Dependency property
		/// </summary>
		public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
			typeof(TextElementViewModel),
			typeof(TextElementView));

		#endregion

		#region Implementation of IViewFor

		/// <summary>
		/// </summary>
		object IViewFor.ViewModel
		{
			get { return ViewModel; }
			set { ViewModel = (TextElementViewModel)value; }
		}

		#endregion

		#region Implementation of IViewFor<TextElementViewModel>

		/// <summary>
		/// The ViewModel corresponding to this specific View. This should be
		/// a DependencyProperty if you're using XAML.
		/// </summary>
		public TextElementViewModel ViewModel
		{
			get => (TextElementViewModel) GetValue(ViewModelProperty);
			set
			{
				SetValue(ViewModelProperty, value);
				BindToViewModel();
			}
		}

		#endregion

		#region Implementation of IDisposable

		/// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
		public void Dispose()
		{
			DoDispose(false);
		}

		#endregion

		#region Constructor & Finalizer

		/// <summary>
		/// Constructs a TextElementView
		/// </summary>
		public TextElementView()
		{
			InitializeComponent();

			this.WhenAnyValue(_ => _.ViewModel).BindTo(this, _ => _.DataContext);

			_helper = new ElementViewHelper(this);
		}

		~TextElementView()
		{
			DoDispose(true);
		}

		#endregion

		#region Private Methods

		private void BindToViewModel()
		{
			_subscriptions.Add(ViewModel.WhenAnyValue(vm => vm.CornerRadius, vm => vm.RoundedCorners)
				.Subscribe(_ => UpdateViewCornerRadius()));
			_subscriptions.Add(
				ViewModel.WhenAnyValue(vm => vm.BorderThickness)
					.Subscribe(thickness =>
					{
						float pixelThickness = Math.GetDisplayLengthFromInches(thickness, App.Ppi.Width);
						ActualBorder.BorderThickness = new Thickness(pixelThickness);
					}));
			_subscriptions.Add(
				ViewModel.WhenAnyValue(vm => vm.BorderColor)
					.Select(color => color ?? Core.Drawing.Color.Transparent)
					.Subscribe(color =>
					{
						ActualBorder.BorderBrush = new SolidColorBrush(color.ToMediaColor());
					}));

			_subscriptions.Add(this.OneWayBind(ViewModel, vm => vm.FieldName, v => v.FieldName.Text));

			UpdateViewFont();
            _subscriptions.Add(ViewModel.WhenAnyValue(
                vm => vm.FontName,
                vm => vm.FontSize,
                vm => vm.FontBold,
                vm => vm.FontItalics)
				.SubscribeOn(RxApp.MainThreadScheduler)
				.Subscribe(_ => UpdateViewFont()));
            _subscriptions.Add(ViewModel.WhenAnyValue(vm => vm.HorizontalAlignment).Subscribe(_ => UpdateViewHorizontalAlignment()));
			_subscriptions.Add(ViewModel.WhenAnyValue(vm => vm.VerticalAlignment).Subscribe(_ => UpdateViewVerticalAlignment()));
			_subscriptions.Add(ViewModel.WhenAnyValue(vm => vm.Color).Subscribe(_ => UpdateViewColor()));

			_subscriptions.Add(ViewModel.WhenAnyValue(vm => vm.IsBoundToField)
				.ObserveOn(RxApp.MainThreadScheduler)
				.Subscribe(isBound =>
				{
					TextPreview.ParseTree = isBound ? null : ViewModel.ParseTree;
					FieldName.Visibility = isBound ? Visibility.Visible : Visibility.Hidden;
				}));

			_helper.ViewModel = ViewModel;
		}

		private void UpdateViewCornerRadius()
		{
			float radius = Math.GetDisplayLengthFromInches(ViewModel.CornerRadius, App.Ppi.Height);
			ActualBorder.CornerRadius = new CornerRadius
			{
				TopLeft = (ViewModel.RoundedCorners & ElementViewModel.RoundedCornersFlags.TopLeft) != ElementViewModel.RoundedCornersFlags.None ? radius : 0,
				TopRight = (ViewModel.RoundedCorners & ElementViewModel.RoundedCornersFlags.TopRight) != ElementViewModel.RoundedCornersFlags.None ? radius : 0,
				BottomLeft = (ViewModel.RoundedCorners & ElementViewModel.RoundedCornersFlags.BottomLeft) != ElementViewModel.RoundedCornersFlags.None ? radius : 0,
				BottomRight = (ViewModel.RoundedCorners & ElementViewModel.RoundedCornersFlags.BottomRight) != ElementViewModel.RoundedCornersFlags.None ? radius : 0,
			};

			// actually use transparent border thickness to push text to correct location to fit corner radius
			float displayRadius = Math.GetDisplayLengthFromInches(ViewModel.CornerRadius * .25f, App.Ppi.Width);
			RoundedCorners.BorderThickness = new Thickness
			{
				Top = (ViewModel.RoundedCorners & 
					(ElementViewModel.RoundedCornersFlags.TopLeft | ElementViewModel.RoundedCornersFlags.TopRight)) 
						!= ElementViewModel.RoundedCornersFlags.None ? displayRadius : 0,
				Left = (ViewModel.RoundedCorners &
					(ElementViewModel.RoundedCornersFlags.TopLeft | ElementViewModel.RoundedCornersFlags.BottomLeft))
						!= ElementViewModel.RoundedCornersFlags.None ? displayRadius : 0,
				Right = (ViewModel.RoundedCorners &
					(ElementViewModel.RoundedCornersFlags.TopRight | ElementViewModel.RoundedCornersFlags.BottomRight))
						!= ElementViewModel.RoundedCornersFlags.None ? displayRadius : 0,
				Bottom = (ViewModel.RoundedCorners &
					(ElementViewModel.RoundedCornersFlags.BottomLeft | ElementViewModel.RoundedCornersFlags.BottomRight))
						!= ElementViewModel.RoundedCornersFlags.None ? displayRadius : 0,
			};
		}

		private void UpdateViewFont()
		{
		    Font font = ViewModel.Font;

			FontFamily family = Util.Fonts.GetFontFamilyOrDefault(font);
			float size = Math.GetDisplayLengthFromInches(Util.Fonts.GetFontSizeOrDefault(font), App.Ppi.Height);

			TextPreview.Font = font;
			FieldName.FontFamily = family;
			FieldName.FontSize = size;
			FieldName.FontWeight = font.Bold.IsValidAndTrue() ? FontWeights.Bold : FontWeights.Normal;
			FieldName.FontStyle = font.Italics.IsValidAndTrue() ? FontStyles.Oblique : FontStyles.Normal;
		}
		
		private void UpdateViewHorizontalAlignment()
		{
			switch (ViewModel.HorizontalAlignment)
			{
				case Core.Drawing.HorizontalAlignment.Left:
					FieldName.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
					break;

				case Core.Drawing.HorizontalAlignment.Center:
					FieldName.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
					break;

				case Core.Drawing.HorizontalAlignment.Right:
					FieldName.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
					break;
			}

			TextPreview.HorizontalAlignment = ViewModel.HorizontalAlignment;
		}

		private void UpdateViewVerticalAlignment()
		{
			switch (ViewModel.VerticalAlignment)
			{
				case Core.Drawing.VerticalAlignment.Top:
					FieldName.VerticalAlignment = System.Windows.VerticalAlignment.Top;
					break;

				case Core.Drawing.VerticalAlignment.Center:
					FieldName.VerticalAlignment = System.Windows.VerticalAlignment.Center;
					break;

				case Core.Drawing.VerticalAlignment.Bottom:
					FieldName.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
					break;
			}

			TextPreview.VerticalAlignment = ViewModel.VerticalAlignment;
		}

		private void UpdateViewColor()
		{
			SolidColorBrush brush = new SolidColorBrush(Color.FromArgb(ViewModel.Color.A, ViewModel.Color.R, ViewModel.Color.G, ViewModel.Color.B));
			FieldName.Foreground = brush;
			TextPreview.Color = ViewModel.Color;
		}

		private void DoDispose(bool finalize)
		{
			if (_disposed)
				return;

			_subscriptions.ForEach(sub => sub.Dispose());
			_subscriptions.Clear();
			_helper.Dispose();
			_disposed = true;
		}

		#endregion
	}
}
