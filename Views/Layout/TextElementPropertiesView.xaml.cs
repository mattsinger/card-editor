﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Core.Util;
using Core.View_Models.Layout;
using ReactiveUI;
using Color = System.Windows.Media.Color;
using Point = Core.Drawing.Point;
using Size = Core.Drawing.Size;

namespace CardEditor.Views.Layout
{
	/// <summary>
	/// Interaction logic for TextElementPropertiesView.xaml
	/// </summary>
	public partial class TextElementPropertiesView : UserControl, IViewFor<TextElementViewModel>, IPropertiesView
	{
		#region Private Members

		private readonly List<IDisposable> _disposables = new List<IDisposable>();
	    private bool _disposed;

		#endregion

		#region Dependency Properties

		/// <summary>
		/// Dependency property
		/// </summary>
		public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
			typeof(TextElementViewModel),
			typeof(TextElementPropertiesView));

		#endregion

		#region Implementation of IViewFor

		/// <summary>
		/// </summary>
		object IViewFor.ViewModel
		{
			get => ViewModel;
			set => ViewModel = (TextElementViewModel)value;
		}

		#endregion

		#region Implementation of IViewFor<TextElementViewModel>

		/// <summary>
		/// The ViewModel corresponding to this specific View. This should be
		/// a DependencyProperty if you're using XAML.
		/// </summary>
		public TextElementViewModel ViewModel
		{
			get => (TextElementViewModel)GetValue(ViewModelProperty);
			set
			{
				SetValue(ViewModelProperty, value);
				BindToViewModel();
			}
		}

		#endregion

		#region Implementation of IDisposable

		/// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
		public void Dispose()
		{
			DoDispose(false);
		}

        #endregion

        #region Constructor & Finalizer

        /// <summary>
        /// Constructs a TextElementPropertiesView
        /// </summary>
        public TextElementPropertiesView()
		{
			InitializeComponent();

			this.WhenAnyValue(_ => _.ViewModel).BindTo(this, _ => _.DataContext);
		}

	    ~TextElementPropertiesView()
	    {
	        DoDispose(true);
	    }

        #endregion

        #region Private Methods

        private void BindToViewModel()
		{
			BindOrigin();
			BindSize();
			BindOrientation();
			BindCornerRadius();
			BindBorder();

			_disposables.Add(ViewModel.WhenAnyValue(vm => vm.FieldName)
				.DistinctUntilChanged()
				.Subscribe(fieldName =>
				{
					FieldName.Text = fieldName;
				}));
			_disposables.Add(this.WhenAnyValue(v => v.FieldName.Text)
				.Throttle(TimeSpan.FromSeconds(.75))
				.ObserveOn(RxApp.MainThreadScheduler)
				.DistinctUntilChanged()
				.Subscribe(fieldName =>
				{
					ViewModel.FieldName = fieldName;
				}));

			_disposables.Add(ViewModel.WhenAnyValue(vm => vm.Text)
				.DistinctUntilChanged()
				.Subscribe(text =>
				{
					Text.Text = text;
				}));
			_disposables.Add(this.WhenAnyValue(v => v.Text.Text)
				.Throttle(TimeSpan.FromSeconds(.75))
				.ObserveOn(RxApp.MainThreadScheduler)
				.DistinctUntilChanged()
				.Subscribe(text =>
				{
					ViewModel.Text = text;
				}));

			_disposables.Add(ViewModel.WhenAnyValue(vm => vm.IsBoundToField)
				.Subscribe(isBound =>
				{
					Text.IsEnabled = !isBound;
					TextLabel.IsEnabled = !isBound;
					TextLabel.Foreground = new SolidColorBrush(isBound ? Colors.Gray : Colors.Black);
				}));
			
			BindFont();

            _disposables.Add(this.Bind(ViewModel, vm => vm.HorizontalAlignment, v => v.HorAlign.SelectedValue));
            _disposables.Add(this.Bind(ViewModel, vm => vm.VerticalAlignment, v => v.VertAlign.SelectedValue));

			BindColor();
		}

		private void BindOrigin()
		{
			SetPositionText(ViewModel.Origin);
			_disposables.Add(ViewModel.WhenAnyValue(vm => vm.Origin)
					.Subscribe(SetPositionText));
			_disposables.Add(this.WhenAnyValue(v => v.LeftEntry.Text, v => v.TopEntry.Text)
				.Throttle(TimeSpan.FromSeconds(.75f), RxApp.MainThreadScheduler)
				.Select(posTuple =>
				{
					Point? origin = null;
					float left, top;
					if (float.TryParse(posTuple.Item1, out left) &&
						float.TryParse(posTuple.Item2, out top))
						origin = new Point(left, top);

					return origin;
				})
				.Where(origin => origin.HasValue)
				.Subscribe(origin =>
				{
					if (!origin.HasValue)
						throw new NullReferenceException("Somehow ended up with a null origin even though we should have filtered it out");

					ViewModel.Origin = origin.Value;
				}));
		}

		private void BindSize()
		{
			SetSizeText(ViewModel.Size);
			_disposables.Add(ViewModel.WhenAnyValue(vm => vm.Size)
				.Subscribe(SetSizeText));
			_disposables.Add(this.WhenAnyValue(v => v.WidthEntry.Text, v => v.HeightEntry.Text)
				.Throttle(TimeSpan.FromSeconds(.75f), RxApp.MainThreadScheduler)
				.Select(sizeTuple =>
				{
					Size? size = null;
					float width, height;
					if (float.TryParse(sizeTuple.Item1, out width) &&
						float.TryParse(sizeTuple.Item2, out height))
						size = new Size(width, height);

					return size;
				})
				.Where(size => size.HasValue)
				.Subscribe(size =>
				{
					if (!size.HasValue)
						throw new NullReferenceException("Somehow ended up with a null size even though we should have filtered it out");

					ViewModel.Size = size.Value;
				}));
		}

		private void BindOrientation()
		{
			SetOrientationText(ViewModel.Orientation);
			_disposables.Add(ViewModel.WhenAnyValue(vm => vm.Orientation)
				.Subscribe(SetOrientationText));
			_disposables.Add(this.WhenAnyValue(v => v.OrientationEntry.Text)
				.Throttle(TimeSpan.FromSeconds(.75f), RxApp.MainThreadScheduler)
				.Select(orientation =>
				{
					float? output = null;
					float angle;
					if (float.TryParse(orientation, out angle))
						output = angle;

					return output;
				})
				.Where(angle => angle.HasValue)
				.Subscribe(angle =>
				{
					if (!angle.HasValue)
						throw new NullReferenceException("Somehow ended up with a null orientation even though we should have filtered it out");

					ViewModel.Orientation = angle.Value;
				}));
		}

		private void BindCornerRadius()
		{
			CornerRadiusEntry.Text = ViewModel.CornerRadius.ToString("0.###", CultureInfo.CurrentCulture);
			_disposables.Add(ViewModel.WhenAnyValue(vm => vm.CornerRadius)
				.DistinctUntilChanged()
				.Subscribe(radius => CornerRadiusEntry.Text = radius.ToString("0.###", CultureInfo.CurrentCulture)));
			_disposables.Add(this.WhenAnyValue(v => v.CornerRadiusEntry.Text)
				.Throttle(TimeSpan.FromSeconds(.75f), RxApp.MainThreadScheduler)
				.Select(radiusText =>
				{
					float? result = null;
					float parsed;
					if (float.TryParse(radiusText, out parsed))
						result = parsed;

					return result;
				})
				.Where(radius => radius.HasValue && radius.Value >= 0)
				.DistinctUntilChanged()
				.Subscribe(radius =>
				{
					if (!radius.HasValue)
						throw new NullReferenceException("Somehow ended up with a null radius even though we should have filtered it out");

					ViewModel.CornerRadius = radius.Value;
				}));

			SetViewCornerRadiusFlags();
			_disposables.Add(ViewModel.WhenAnyValue(vm => vm.RoundedCorners)
				.DistinctUntilChanged()
				.Subscribe(_ => SetViewCornerRadiusFlags()));
			_disposables.Add(
				this.WhenAnyValue(v => v.RoundTopLeft.IsChecked, 
								v => v.RoundTopRight.IsChecked, 
								v => v.RoundBottomLeft.IsChecked, 
								v => v.RoundBottomRight.IsChecked)
				.Throttle(TimeSpan.FromSeconds(.75f), RxApp.MainThreadScheduler)
				.DistinctUntilChanged()
				.Subscribe(_ =>
				{
					SetVmCornerRadiusFlags();
				}));
		}

		private void BindBorder()
		{
			Core.Drawing.Color vmBorderColor = ViewModel.BorderColor ?? Core.Drawing.Color.Transparent;
			BorderColor.SelectedColor = Color.FromArgb(vmBorderColor.A, vmBorderColor.R, vmBorderColor.G, vmBorderColor.B);
			_disposables.Add(this.WhenAnyValue(v => v.BorderColor.SelectedColor)
				.Where(color => color.HasValue)
				.Subscribe(color =>
				{
					if (!color.HasValue)
						throw new NullReferenceException("Somehow ended up with a null color even though we should have filtered it out");

					ViewModel.BorderColor = Core.Drawing.Color.FromArgb(color.Value.A, color.Value.R, color.Value.G, color.Value.B);
				}));
			_disposables.Add(ViewModel.WhenAnyValue(vm => vm.BorderColor)
				.Where(color => color.HasValue)
				.Subscribe(color =>
				{
					if (!color.HasValue)
						throw new NullReferenceException("Somehow ended up with a null color even though we should have filtered it out");

					BorderColor.SelectedColor = Util.Colors.ToMediaColor(color.Value);
				}));

			SetBorderThicknessText(ViewModel.BorderThickness);
			_disposables.Add(ViewModel.WhenAnyValue(vm => vm.BorderThickness)
				.DistinctUntilChanged()
				.Subscribe(SetBorderThicknessText));
			_disposables.Add(this.WhenAnyValue(v => v.BorderThicknessText.Text)
				.Throttle(TimeSpan.FromSeconds(.75f), RxApp.MainThreadScheduler)
				.Select(thicknessText =>
				{
					float? result = null;
					float parsed;
					if (float.TryParse(thicknessText, out parsed))
						result = parsed;

					return result;
				})
				.Where(thickness => thickness.HasValue && thickness.Value >= 0)
				.DistinctUntilChanged()
				.Subscribe(thickness =>
				{
					if (!thickness.HasValue)
						throw new NullReferenceException("Somehow ended up with a null border thickness even though we should have filtered it out");

					ViewModel.BorderThickness = thickness.Value;
				}));
		}

		private void BindFont()
		{
			_disposables.Add(ViewModel.WhenAnyValue(vm => vm.FontSize).Subscribe(size =>
			{
				FontSizeEntry.Text = size.ToString(CultureInfo.CurrentCulture);
			}));
			_disposables.Add(this.WhenAnyValue(v => v.FontSizeEntry.Text)
				.Throttle(TimeSpan.FromSeconds(.75f), RxApp.MainThreadScheduler)
				.Select(text =>
				{
					float size;
					float.TryParse(text, out size);
					return size;
				})
				.Where(size => size > 0)
				.Subscribe(size =>
				{
					ViewModel.FontSize = size;
				}));

			_disposables.Add(ViewModel.WhenAnyValue(vm => vm.FontName)
				.Subscribe(fontName =>
				{
					SetFontSelection();
				}));
			_disposables.Add(this.WhenAnyValue(v => v.FontSelection.SelectedValue)
                .SubscribeOn(RxApp.MainThreadScheduler)
				.Subscribe(selected =>
				{
					FontFamily fontFamily = (FontFamily)selected;
					ViewModel.FontName = fontFamily.ToString();
				}));

		    _disposables.Add(this.Bind(ViewModel, vm => vm.FontBold, v => v.Bold.IsChecked));
            _disposables.Add(this.Bind(ViewModel, vm => vm.FontItalics, v => v.Italics.IsChecked));
        }

		private void BindColor()
		{
			ColorPicker.SelectedColor = Color.FromRgb(ViewModel.Color.R, ViewModel.Color.G, ViewModel.Color.B);
			_disposables.Add(this.WhenAnyValue(v => v.ColorPicker.SelectedColor)
				.Where(color => color.HasValue)
				.Subscribe(color =>
				{
					if (!color.HasValue)
						throw new NullReferenceException("Somehow ended up with a null color even though we should have filtered it out");

					ViewModel.Color = Core.Drawing.Color.FromArgb(color.Value.A, color.Value.R, color.Value.G, color.Value.B);
				}));
			_disposables.Add(ViewModel.WhenAnyValue(vm => vm.Color)
				.Subscribe(color =>
				{
					ColorPicker.SelectedColor = Util.Colors.ToMediaColor(color);
				}));
		}

		private void SetSizeText(Size size)
		{
			WidthEntry.Text = size.Width.ToString("0.###", CultureInfo.CurrentCulture);
			HeightEntry.Text = size.Height.ToString("0.###", CultureInfo.CurrentCulture);
		}

		private void SetPositionText(Point pos)
		{
			LeftEntry.Text = pos.X.ToString("0.###", CultureInfo.CurrentCulture);
			TopEntry.Text = pos.Y.ToString("0.###", CultureInfo.CurrentCulture);
		}

		private void SetOrientationText(float orientation)
		{
			OrientationEntry.Text = orientation.ToString("0.###", CultureInfo.CurrentCulture);
		}

		private void SetBorderThicknessText(float thickness)
		{
			BorderThicknessText.Text = thickness.ToString("0.###", CultureInfo.CurrentCulture);
		}

		private void SetFontSelection()
		{
			FontSelection.SelectedValue = new FontFamily(string.IsNullOrEmpty(ViewModel.FontName) ?
						System.Drawing.FontFamily.GenericSansSerif.Name :
						ViewModel.FontName);
		}

		private void SetViewCornerRadiusFlags()
		{
			RoundTopLeft.IsChecked = (ViewModel.RoundedCorners & ElementViewModel.RoundedCornersFlags.TopLeft)
										!= ElementViewModel.RoundedCornersFlags.None;
			RoundTopRight.IsChecked = (ViewModel.RoundedCorners & ElementViewModel.RoundedCornersFlags.TopRight)
										!= ElementViewModel.RoundedCornersFlags.None;
			RoundBottomLeft.IsChecked = (ViewModel.RoundedCorners & ElementViewModel.RoundedCornersFlags.BottomLeft)
										!= ElementViewModel.RoundedCornersFlags.None;
			RoundBottomRight.IsChecked = (ViewModel.RoundedCorners & ElementViewModel.RoundedCornersFlags.BottomRight)
										!= ElementViewModel.RoundedCornersFlags.None;
		}

		private void SetVmCornerRadiusFlags()
		{
			ElementViewModel.RoundedCornersFlags flags = ElementViewModel.RoundedCornersFlags.None;

			if (RoundTopLeft.IsChecked.IsValidAndTrue())
				flags |= ElementViewModel.RoundedCornersFlags.TopLeft;

			if (RoundTopRight.IsChecked.IsValidAndTrue())
				flags |= ElementViewModel.RoundedCornersFlags.TopRight;

			if (RoundBottomLeft.IsChecked.IsValidAndTrue())
				flags |= ElementViewModel.RoundedCornersFlags.BottomLeft;

			if (RoundBottomRight.IsChecked.IsValidAndTrue())
				flags |= ElementViewModel.RoundedCornersFlags.BottomRight;

			ViewModel.RoundedCorners = flags;
		}

	    private void DoDispose(bool finalize)
	    {
	        if (_disposed)
	            return;

	        _disposed = true;
            _disposables.ForEach(_ => _.Dispose());
            _disposables.Clear();
        }

		#endregion
	}
}
