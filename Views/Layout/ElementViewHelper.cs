﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Core.View_Models.Layout;
using ReactiveUI;
using Math = Core.Util.Math;
using Size = Core.Drawing.Size;

namespace CardEditor.Views.Layout
{
	/// <summary>
	/// Helper class for ElementViews to handle common functionality such as repositioning and resizing
	/// </summary>
	internal class ElementViewHelper : IDisposable
	{
		#region Private Members

		private ElementViewModel _viewModel;
		private readonly Control _view;
		private readonly RotateTransform _transform = new RotateTransform();

		private readonly List<IDisposable> _subscriptions = new List<IDisposable>();
		private bool _disposed;

		#endregion

		#region Public Properties

		/// <summary>
		/// The view's ViewModel
		/// </summary>
		public ElementViewModel ViewModel
		{
			get { return _viewModel; }
			set
			{
				_viewModel = value;
				BindToViewModel();
			}
		}

		#endregion

		#region Public Methods

		/// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
		public void Dispose()
		{
			DoDispose(false);
		}

		#endregion

		#region Constructor & Finalizer

		/// <summary>
		/// Constructs an ElementViewHelper
		/// </summary>
		/// <param name="view">The helped view</param>
		public ElementViewHelper(Control view)
		{
			_view = view;
			view.RenderTransform = _transform;
		}

		/// <summary>
		/// Finalizer
		/// </summary>
		~ElementViewHelper()
		{
			DoDispose(true);
		}

		#endregion

		#region Private Methods

		private void BindToViewModel()
		{
			_subscriptions.Add(ViewModel.Parent.WhenAnyValue(layout => layout.FullSize)
				.Subscribe(size =>
				{
					UpdateViewOrigin();
					UpdateViewSize();
				}));

			UpdateViewOrigin();
			_subscriptions.Add(ViewModel.WhenAnyValue(vm => vm.Origin)
				.Subscribe(_ =>
				{
					UpdateViewOrigin();
				}));

			UpdateViewSize();
			_subscriptions.Add(ViewModel.WhenAnyValue(vm => vm.Size)
				.Subscribe(_ =>
				{
					UpdateViewSize();
				}));

			UpdateViewOrientation();
			_subscriptions.Add(ViewModel.WhenAnyValue(vm => vm.Orientation)
				.Subscribe(_ =>
				{
					UpdateViewOrientation();
				}));

			_subscriptions.Add(ViewModel.WhenAnyValue(vm => vm.IsEditorVisible)
				.Subscribe(isVisible =>
				{
					_view.Visibility = isVisible ? Visibility.Visible : Visibility.Hidden;
				}));
		}

		private void UpdateViewOrigin()
		{
			Size displayPos = Math.GetDisplaySizeFromInches(new Size(ViewModel.Origin) + ViewModel.Parent.Bleed, App.Ppi);
			Canvas.SetLeft(_view, displayPos.Width);
			Canvas.SetTop(_view, displayPos.Height);
		}

		private void UpdateViewSize()
		{
			Size displaySize = Math.GetDisplaySizeFromInches(ViewModel.Size, App.Ppi);
			_view.Width = displaySize.Width;
			_view.Height = displaySize.Height;

			_transform.CenterX = _view.Width / 2;
			_transform.CenterY = _view.Height / 2;
		}

		private void UpdateViewOrientation()
		{
			_transform.Angle = ViewModel.Orientation;
		}

		private void DoDispose(bool finalize)
		{
			if (_disposed)
				return;

			_subscriptions.ForEach(sub => sub.Dispose());
			_subscriptions.Clear();

			_disposed = true;
		}

		#endregion
	}
}
