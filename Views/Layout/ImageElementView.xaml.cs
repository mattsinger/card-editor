﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using CardEditor.Util;
using Core.View_Models.Layout;
using ReactiveUI;
using Math = Core.Util.Math;

namespace CardEditor.Views.Layout
{
	/// <summary>
	/// Interaction logic for ImageElementView.xaml
	/// </summary>
	public partial class ImageElementView : UserControl, IViewFor<ImageElementViewModel>
	{
		private readonly List<IDisposable> _subscriptions = new List<IDisposable>();
		private readonly ElementViewHelper _helper;
		private bool _disposed;

		#region Dependency Properties

		/// <summary>
		/// Dependency property
		/// </summary>
		public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
			typeof(ImageElementViewModel),
			typeof(ImageElementView));

		#endregion

		#region Implementation of IViewFor

		/// <summary>
		/// </summary>
		object IViewFor.ViewModel
		{
			get => ViewModel;
			set => ViewModel = (ImageElementViewModel)value;
		}

		#endregion

		#region Implementation of IViewFor<ImageElementViewModel>

		/// <summary>
		/// The ViewModel corresponding to this specific View. This should be
		/// a DependencyProperty if you're using XAML.
		/// </summary>
		public ImageElementViewModel ViewModel
		{
			get => (ImageElementViewModel)GetValue(ViewModelProperty);
			set
			{
				SetValue(ViewModelProperty, value);
				BindToViewModel();
			}
		}

		#endregion

		#region Implementation of IDisposable

		/// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
		public void Dispose()
		{
			DoDispose(false);
		}

		#endregion

		#region Constructor & Finalizer

		/// <summary>
		/// Constructs an ImageElementView
		/// </summary>
		public ImageElementView()
		{
			InitializeComponent();

			this.WhenAnyValue(_ => _.ViewModel).BindTo(this, _ => _.DataContext);

			_helper = new ElementViewHelper(this);
		}

		~ImageElementView()
		{
			DoDispose(true);
		}

		#endregion

		#region Private Methods

		private void BindToViewModel()
		{
			UpdateViewCornerRadius();
			_subscriptions.Add(ViewModel.WhenAnyValue(vm => vm.CornerRadius, vm => vm.RoundedCorners)
				.Subscribe(_ => UpdateViewCornerRadius()));
			_subscriptions.Add(
				ViewModel.WhenAnyValue(vm => vm.BorderThickness)
					.Subscribe(thickness =>
					{
						float pixelThickness = Math.GetDisplayLengthFromInches(thickness, App.Ppi.Width);
						ImageRegion.BorderThickness = new Thickness(pixelThickness);
					}));
			_subscriptions.Add(
				ViewModel.WhenAnyValue(vm => vm.BorderColor)
					.Select(color => color ?? Core.Drawing.Color.Transparent)
					.Subscribe(color =>
					{
						ImageRegion.BorderBrush = new SolidColorBrush(color.ToMediaColor());
					}));

			_subscriptions.Add(this.OneWayBind(ViewModel, vm => vm.FieldName, v => v.FieldName.Text));
			_subscriptions.Add(ViewModel.WhenAnyValue(vm => vm.IsBoundToField)
				.Subscribe(isBound =>
				{
					Image.Opacity = isBound ? 0 : 1;
				}));

			SetStretchMode(ViewModel.Stretch);
			_subscriptions.Add(ViewModel.WhenAnyValue(vm => vm.Stretch).Subscribe(SetStretchMode));

			UpdateViewHorizontalAlignment();
			_subscriptions.Add(ViewModel.WhenAnyValue(vm => vm.HorizontalAlignment).Subscribe(_ => UpdateViewHorizontalAlignment()));

			UpdateViewVerticalAlignment();
			_subscriptions.Add(ViewModel.WhenAnyValue(vm => vm.VerticalAlignment).Subscribe(_ => UpdateViewVerticalAlignment()));

			_helper.ViewModel = ViewModel;
		}

		private void SetStretchMode(bool stretch)
		{
			Image.Stretch = stretch ? Stretch.Uniform : Stretch.None;
		}

		private void UpdateViewCornerRadius()
		{
			float radius = Math.GetDisplayLengthFromInches(ViewModel.CornerRadius, App.Ppi.Height);
			ImageRegion.CornerRadius = new CornerRadius
			{
				TopLeft = (ViewModel.RoundedCorners & ElementViewModel.RoundedCornersFlags.TopLeft) != ElementViewModel.RoundedCornersFlags.None ? radius : 0,
				TopRight = (ViewModel.RoundedCorners & ElementViewModel.RoundedCornersFlags.TopRight) != ElementViewModel.RoundedCornersFlags.None ? radius : 0,
				BottomLeft = (ViewModel.RoundedCorners & ElementViewModel.RoundedCornersFlags.BottomLeft) != ElementViewModel.RoundedCornersFlags.None ? radius : 0,
				BottomRight = (ViewModel.RoundedCorners & ElementViewModel.RoundedCornersFlags.BottomRight) != ElementViewModel.RoundedCornersFlags.None ? radius : 0,
			};
		}

		private void UpdateViewHorizontalAlignment()
		{
			switch (ViewModel.HorizontalAlignment)
			{
				case Core.Drawing.HorizontalAlignment.Left:
					Image.AlignmentX = AlignmentX.Left;
					FieldName.HorizontalAlignment = HorizontalAlignment.Left;
					break;

				case Core.Drawing.HorizontalAlignment.Center:
					Image.AlignmentX = AlignmentX.Center;
					FieldName.HorizontalAlignment = HorizontalAlignment.Center;
					break;

				case Core.Drawing.HorizontalAlignment.Right:
					Image.AlignmentX = AlignmentX.Right;
					FieldName.HorizontalAlignment = HorizontalAlignment.Right;
					break;
			}
		}

		private void UpdateViewVerticalAlignment()
		{
			switch (ViewModel.VerticalAlignment)
			{
				case Core.Drawing.VerticalAlignment.Top:
					Image.AlignmentY = AlignmentY.Top;
					FieldName.VerticalAlignment = VerticalAlignment.Top;
					break;

				case Core.Drawing.VerticalAlignment.Center:
					Image.AlignmentY = AlignmentY.Center;
					FieldName.VerticalAlignment = VerticalAlignment.Center;
					break;

				case Core.Drawing.VerticalAlignment.Bottom:
					Image.AlignmentY = AlignmentY.Bottom;
					FieldName.VerticalAlignment = VerticalAlignment.Bottom;
					break;
			}
		}

		private void DoDispose(bool finalize)
		{
			if (_disposed)
				return;

			_subscriptions.ForEach(sub => sub.Dispose());
			_subscriptions.Clear();
			_helper.Dispose();
			_disposed = true;
		}

		#endregion
	}
}
