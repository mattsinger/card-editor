﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Reactive;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using CardEditor.Util;
using Core.View_Models.Glyph_Set;
using ReactiveUI;

namespace CardEditor.Views.Glyph_Set
{
	/// <summary>
	/// Interaction logic for GlyphSetView.xaml
	/// </summary>
	public partial class GlyphSetView : UserControl, IViewFor<GlyphSetViewModel>
	{
		private readonly ReorderHelper _reorderHelper;

		/// <summary>
		/// Dependency property
		/// </summary>
		public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
			typeof(GlyphSetViewModel),
			typeof(GlyphSetView));

		/// <summary>
		/// Constructs a GlyphSetView
		/// </summary>
		public GlyphSetView()
		{
			InitializeComponent();

			this.WhenAnyValue(_ => _.ViewModel).BindTo(this, _ => _.DataContext);
			this.OneWayBind(ViewModel, vm => vm.GlyphList, v => v.GlyphGrid.ItemsSource);
			this.Bind(ViewModel, vm => vm.SelectedGlyph, v => v.GlyphGrid.SelectedItem);

			this.BindCommand(ViewModel, vm => vm.Add, v => v.AddGlyph);
			this.BindCommand(ViewModel, vm => vm.Remove, v => v.RemoveGlyph);

			_reorderHelper = new ReorderHelper(GlyphGrid);
			_reorderHelper.ItemReordered += (sender, args) =>
			{
				ViewModel.SelectedGlyph = args.Item as GlyphViewModel;
			};
		}

		#region Implementation of IViewFor

		/// <summary>
		/// </summary>
		object IViewFor.ViewModel
		{
			get => ViewModel;
			set => ViewModel = (GlyphSetViewModel)value;
		}

		#endregion

		#region Implementation of IViewFor<GlyphSetViewModel>

		/// <summary>
		/// The ViewModel corresponding to this specific View. This should be
		/// a DependencyProperty if you're using XAML.
		/// </summary>
		public GlyphSetViewModel ViewModel
		{
			get => (GlyphSetViewModel)GetValue(ViewModelProperty);
			set
			{
				SetValue(ViewModelProperty, value);
				BindToViewModel();
			}
		}

        #endregion

        #region WPF Handlers

        private void KeyDownHandler(object sender, KeyEventArgs args)
        {
            if (args.Key == Key.Delete && ViewModel.SelectedGlyph != null)
            {
                ICommand command = ViewModel.Remove;
                command.Execute(null);
                args.Handled = true;
            }
        }
        
        private void IdClickHandler(object sender, MouseButtonEventArgs args)
        {
            if (args.ClickCount != 2)
                return;

            ICommand command = ViewModel.Rename;
            command.Execute(null);
        }

        #endregion

        #region Private Methods

        private void SelectImageFile_OnClick(object sender, RoutedEventArgs e)
		{
			ICommand command = ViewModel.SelectedGlyph.SelectPath;
			command.Execute(null);
		}

		private void BindToViewModel()
		{
			ViewModel.GetGlyphId.RegisterHandler(_ =>
			{
				_.SetOutput(GetNewGlyphId());
			});
			ViewModel.DuplicateGlyphId.RegisterHandler(_ =>
			{
				MessageBox.Show(Application.Current.MainWindow, "That glyph Id is already in use", "Duplicate glyph Id", MessageBoxButton.OK, MessageBoxImage.Warning);
				_.SetOutput(new Unit());
			});
		}

		private string GetNewGlyphId()
		{
			Dialogs.TextEntryDialog dialog = new Dialogs.TextEntryDialog
			{
				Title = "Enter new glyph ID",
				Owner = Application.Current.MainWindow
			};
			dialog.ShowDialog();

			if (dialog.DialogResult.HasValue && dialog.DialogResult.Value)
				return dialog.Text;

			return null;
		}

		#endregion
	}
}
