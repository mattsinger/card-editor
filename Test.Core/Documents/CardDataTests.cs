﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using Core.Documents;
using NUnit.Framework;
using Test.Shared;

namespace Test.Core.Documents
{
	[TestFixture]
	public class CardDataTests
	{
		#region Id

		[Test(Description = "When a CardData is constructed, it should not have an Id")]
		[Category("Documents")]
		[Category("Data Set")]
		public void WhenConstructed_IdIsNull()
		{
			CardData data = new CardData();

			Assert.IsNull(data.Id);
		}

		[Test(Description = "An API user should be able to set a CardData's Id")]
		[Category("Documents")]
		[Category("Data Set")]
		[TestCaseSource(typeof(TestCases), nameof(TestCases.StringInputs))]
		public void SetId_SetsValue(string id)
		{
			CardData data = new CardData
			{
				Id = id
			};

			Assert.AreEqual(id, data.Id);
		}

		#endregion

		#region Fields

		[Test(Description = "When a CardData is constructed, it should have an empty list of Fields")]
		[Category("Documents")]
		[Category("Data Set")]
		public void WhenConstructed_FieldsIsEmpty()
		{
			CardData data = new CardData();

			Assert.IsEmpty(data.Fields);
		}

		[Test(Description = "An API user should be able to add CardFields to a CardData")]
		[Category("Documents")]
		[Category("Data Set")]
		public void Fields_AddField_AddsFieldToList([Values(1, 2, 3)] int numFields)
		{
			CardData data = new CardData();

			for (int i = 1; i <= numFields; i++)
			{
				CardField field = new CardField();

				data.Fields.Add(field);

				Assert.That(data.Fields.Contains(field));
				Assert.AreEqual(i, data.Fields.Count);
			}
		}

		[Test(Description = "An API user should be able to remove CardFields from a CardData")]
		[Category("Documents")]
		[Category("Data Set")]
		public void Fields_RemoveField_RemovesFieldsFromList([Values(1, 2, 3)] int numFields)
		{
			List<CardField> fields = new List<CardField>
			{
				new CardField(),
				new CardField(),
				new CardField()
			};

			CardData data = new CardData();
			foreach (CardField field in fields)
				data.Fields.Add(field);

			for (int i = 1; i <= numFields; i++)
			{
				data.Fields.Remove(fields[i - 1]);

				Assert.That(!data.Fields.Contains(fields[i -1]));
				Assert.AreEqual(fields.Count - i, data.Fields.Count);
			}
		}

		#endregion
	}
}
