﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Core.Documents;
using Core.Drawing;
using NUnit.Framework;
using Test.Shared;

namespace Test.Core.Documents
{
	[TestFixture(typeof(ImageElement))]
	[TestFixture(typeof(TextElement))]
	[TestFixture(typeof(ColorElement))]
	public class ElementTests<TElement> where TElement : Element, new()
	{
		#region Name

		[Test(Description = "When a Element is constructed, it should not have an Name")]
		[Category("Documents")]
		[Category("Layout")]
		public void WhenConstructed_NameIsNull()
		{
			TElement element = new TElement();

			Assert.IsNull(element.Name);
		}

		[Test(Description = "An API user should be able to set a Element's Name")]
		[Category("Documents")]
		[Category("Layout")]
		[TestCaseSource(typeof(TestCases), nameof(TestCases.StringInputs))]
		public void SetName_SetsValue(string name)
		{
			TElement element = new TElement
			{
				Name = name
			};

			Assert.AreEqual(name, element.Name);
		}

		#endregion

		#region FieldName

		[Test(Description = "When a Element is constructed, it should not have an FieldName")]
		[Category("Documents")]
		[Category("Layout")]
		public void WhenConstructed_FieldNameIsNull()
		{
			TElement element = new TElement();

			Assert.IsNull(element.FieldName);
		}

		[Test(Description = "An API user should be able to set a Element's FieldName")]
		[Category("Documents")]
		[Category("Layout")]
		[TestCaseSource(typeof(TestCases), nameof(TestCases.StringInputs))]
		public void SetFieldName_SetsValue(string name)
		{
			TElement element = new TElement
			{
				FieldName = name
			};

			Assert.AreEqual(name, element.FieldName);
		}

		#endregion

		#region Origin

		[Test(Description = "When a Element is constructed, it should have an Origin at 0,0")]
		[Category("Documents")]
		[Category("Layout")]
		public void WhenConstructed_OriginIs0X0Y()
		{
			TElement element = new TElement();

			Assert.AreEqual(0.0f, element.Origin.X);
			Assert.AreEqual(0.0f, element.Origin.Y);
		}

		[Test(Description = "An API user should be able to set a Element's Origin")]
		[Category("Documents")]
		[Category("Layout")]
		public void SetOrigin_SetsValue([ValueSource(typeof(TestCases), nameof(TestCases.PointInputs))] Point origin)
		{
			TElement element = new TElement
			{
				Origin = origin
			};

			Assert.AreEqual(origin, element.Origin);
		}

		#endregion

		#region Size

		[Test(Description = "When a Element is constructed, it should have an initial size of 1x1")]
		[Category("Documents")]
		[Category("Layout")]
		public void WhenConstructed_SizeIs1x1()
		{
			TElement element = new TElement();

			Assert.AreEqual(1.0f, element.Size.Width);
			Assert.AreEqual(1.0f, element.Size.Height);
		}

		[Test(Description = "An API user should be able to set a Element's Size")]
		[Category("Documents")]
		[Category("Layout")]
		public void SetSize_SetsValue([ValueSource(typeof(TestCases), nameof(TestCases.SizeInputs))] Size size)
		{
			TElement element = new TElement
			{
				Size = size
			};

			Assert.AreEqual(size, element.Size);
		}

		#endregion
	}
}
