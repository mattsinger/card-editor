﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using Core.Documents;
using NUnit.Framework;

namespace Test.Core.Documents
{
	[TestFixture]
	public class WorkspaceTests
	{
		#region Documents

		[Test(Description = "When a Workspace is constructed, it should have an empty list of Documents")]
		[Category("Documents")]
		[Category("Workspace")]
		public void WhenConstructed_DocumentsIsEmpty()
		{
			Workspace ws = new Workspace();

			Assert.IsEmpty(ws.Documents);
		}

		[Test(Description = "An API user should be able to add Documents to a Workspace")]
		[Category("Documents")]
		[Category("Workspace")]
		public void Documents_AddField_AddsFieldToList([Values(1, 2, 3)] int numDocuments)
		{
			Workspace ws = new Workspace();

			for (int i = 1; i <= numDocuments; i++)
			{
				string field = string.Empty;

				ws.Documents.Add(field);

				Assert.That(ws.Documents.Contains(field));
				Assert.AreEqual(i, ws.Documents.Count);
			}
		}

		[Test(Description = "An API user should be able to remove Documents from a Workspace")]
		[Category("Documents")]
		[Category("Workspace")]
		public void Documents_RemoveField_RemovesDocumentsFromList([Values(1, 2, 3)] int numDocuments)
		{
			List<string> fields = new List<string>
			{
				"1",
				"2",
				"3"
			};

			Workspace ws = new Workspace();
			foreach (string field in fields)
				ws.Documents.Add(field);

			for (int i = 1; i <= numDocuments; i++)
			{
				ws.Documents.Remove(fields[i - 1]);

				Assert.That(!ws.Documents.Contains(fields[i - 1]));
				Assert.AreEqual(fields.Count - i, ws.Documents.Count);
			}
		}

		#endregion
	}
}
