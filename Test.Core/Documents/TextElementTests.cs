﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections;
using Core.Documents;
using Core.Drawing;
using NUnit.Framework;
using Test.Shared;

namespace Test.Core.Documents
{
	[TestFixture]
	public class TextElementTests
	{
		#region Text

		[Test(Description = "When a TextElement is constructed, it should not have a Text")]
		[Category("Documents")]
		[Category("Layout")]
		public void WhenConstructed_TextIsNull()
		{
			TextElement element = new TextElement();

			Assert.IsNull(element.Text);
		}

		[Test(Description = "An API user should be able to set a TextElement's Text")]
		[Category("Documents")]
		[Category("Layout")]
		[TestCaseSource(typeof(TestCases), nameof(TestCases.StringInputs))]
		public void SetText_SetsValue(string text)
		{
			TextElement element = new TextElement
			{
				Text = text
			};

			Assert.AreEqual(text, element.Text);
		}

		#endregion

		#region Font

		[Test(Description = "When a TextElement is constructed, it should have a null Font size")]
		[Category("Documents")]
		[Category("Layout")]
		public void WhenConstructed_FontIsDefaultSize10()
		{
			TextElement element = new TextElement();
			
			Assert.IsNull(element.Font.Size);
		}

		#endregion

		#region HorizontalAlignment

		[Test(Description = "When a TextElement is constructed, it should have a left HorizontalAlignment")]
		[Category("Documents")]
		[Category("Layout")]
		public void WhenConstructed_HorizontalAlignmentIsLeft()
		{
			TextElement element = new TextElement();

			Assert.AreEqual(HorizontalAlignment.Left, element.HorizontalAlignment);
		}

		[Test(Description = "An API user should be able to set a TextElement's HorizontalAlignment")]
		[Category("Documents")]
		[Category("Layout")]
		public void SetHorizontalAlignment_SetsValue([ValueSource(nameof(HorAlignInputs))] HorizontalAlignment alignment)
		{
			TextElement element = new TextElement
			{
				HorizontalAlignment = alignment
			};

			Assert.AreEqual(alignment, element.HorizontalAlignment);
		}

		#endregion

		#region VerticalAlignment

		[Test(Description = "When a TextElement is constructed, it should have a top VerticalAlignment")]
		[Category("Documents")]
		[Category("Layout")]
		public void WhenConstructed_VerticalAlignmentIsLeft()
		{
			TextElement element = new TextElement();

			Assert.AreEqual(VerticalAlignment.Top, element.VerticalAlignment);
		}

		[Test(Description = "An API user should be able to set a TextElement's VerticalAlignment")]
		[Category("Documents")]
		[Category("Layout")]
		public void SetVerticalAlignment_SetsValue([ValueSource(nameof(HorAlignInputs))] VerticalAlignment alignment)
		{
			TextElement element = new TextElement
			{
				VerticalAlignment = alignment
			};

			Assert.AreEqual(alignment, element.VerticalAlignment);
		}

		#endregion

		#region Color

		[Test(Description = "When a TextElement is constructed, it should have an initial default Color of full black")]
		[Category("Documents")]
		[Category("Layout")]
		public void WhenConstructed_ColorIsBlack()
		{
			TextElement element = new TextElement();

			Assert.AreEqual(Color.Black, element.Color);
		}

		[Test(Description = "An API user should be able to set a TextElement's Color")]
		[Category("Documents")]
		[Category("Layout")]
		public void SetColor_SetsValue([ValueSource(nameof(ColorInputs))] Color color)
		{
			TextElement element = new TextElement
			{
				Color = color
			};

			Assert.AreEqual(color, element.Color);
		}

		#endregion

		#region Inputs

		/// <summary>
		/// Font inputs
		/// </summary>
		public static IEnumerable FontInputs
		{
			get
			{
				yield return null;
				yield return new Font(null, 10);
			}
		}

		/// <summary>
		/// HorizontalAlignment inputs
		/// </summary>
		public static IEnumerable HorAlignInputs => Enum.GetValues(typeof(HorizontalAlignment));

		/// <summary>
		/// VeritcalAlignment inputs
		/// </summary>
		public static IEnumerable VertAlignInputs => Enum.GetValues(typeof(VerticalAlignment));

		/// <summary>
		/// Color inputs
		/// </summary>
		public static IEnumerable ColorInputs
		{
			get
			{
				yield return null;
				yield return Color.FromArgb(0, 0, 0, 0);
			}
		}

		#endregion
	}
}
