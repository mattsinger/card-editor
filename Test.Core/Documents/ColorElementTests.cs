﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
using Core.Documents;
using Core.Drawing;
using NUnit.Framework;
using Test.Shared;

namespace Test.Core.Documents
{
	[TestFixture]
	public class ColorElementTests
	{
		#region Color

		[Test(Description = "When a ColorElement is constrcuted, it should not have a Color")]
		[Category("Documents")]
		[Category("Layout")]
		public void WhenConstructed_ColorIsNull()
		{
			ColorElement element = new ColorElement();

			Assert.IsNull(element.Color);
		}

		[Test(Description = "An API user should be able to set a ColorElement's Color")]
		[Category("Documents")]
		[Category("Layout")]
		[TestCaseSource(typeof(TestCases), nameof(TestCases.ColorInputs))]
		public void SetColor_SetsValue(Color color)
		{
			ColorElement element = new ColorElement
			{
				Color = color
			};

			Assert.AreEqual(color, element.Color);
		}

		#endregion
	}
}
