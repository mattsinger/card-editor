﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Core.Documents;
using NUnit.Framework;
using Test.Shared;

namespace Test.Core.Documents
{
	[TestFixture]
	public class CardFieldTests
	{
		#region Key

		[Test(Description = "When a Card Field is constructed, it should not have a Key")]
		[Category("Documents")]
		[Category("Data Set")]
		public void WhenConstructed_KeyIsNull()
		{
			CardField field = new CardField();

			Assert.IsNull(field.Key);
		}

		[Test(Description = "An API user should be able to set a Card Field's Key")]
		[Category("Documents")]
		[Category("Data Set")]
		[TestCaseSource(typeof(TestCases), nameof(TestCases.StringInputs))]
		public void SetKey_SetsValue(string key)
		{
			CardField field = new CardField
			{
				Key = key
			};

			Assert.AreEqual(key, field.Key);
		}

		#endregion

		#region Value

		[Test(Description = "When a Card Field is constructed, it should not have a Value")]
		[Category("Documents")]
		[Category("Data Set")]
		public void WhenConstructed_ValueIsNull()
		{
			CardField field = new CardField();

			Assert.IsNull(field.Value);
		}

		[Test(Description = "An API user should be able to set a Card Field's Value")]
		[Category("Documents")]
		[Category("Data Set")]
		[TestCaseSource(typeof(TestCases), nameof(TestCases.StringInputs))]
		public void SetValue_SetsValue(string value)
		{
			CardField field = new CardField
			{
				Value = value
			};

			Assert.AreEqual(value, field.Value);
		}

		#endregion
	}
}
