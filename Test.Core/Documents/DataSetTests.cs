﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Documents;
using NUnit.Framework;
using Test.Shared;

namespace Test.Core.Documents
{
	[TestFixture]
	public class DataSetTests
	{
		#region FieldDescriptor

			#region FieldType

			[Test(Description = "When a DataSet FieldDescriptor is constructed, it should have a Text type")]
			[Category("Documents")]
			[Category("Data Set")]
			public void FieldDescriptor_WhenConstructed_TypeIsText()
			{
				DataSet.FieldDescriptor data = new DataSet.FieldDescriptor();

				Assert.AreEqual(FieldType.Text, data.Type);
			}

			public static IEnumerable GetFieldTypes => Enum.GetValues(typeof(FieldType)).Cast<FieldType>();

			[Test(Description = "An API user should be able to set a Card Field's FieldType")]
			[Category("Documents")]
			[Category("Data Set")]
			public void FieldDescriptor_SetType_SetsValue([ValueSource(nameof(GetFieldTypes))] FieldType dataType)
			{
				DataSet.FieldDescriptor data = new DataSet.FieldDescriptor
				{
					Type = dataType
				};

				Assert.AreEqual(dataType, data.Type);
			}

			#endregion

			#region Key

			[Test(Description = "When a DataSet FieldDescriptor is constructed, it should not have a Key")]
			[Category("Documents")]
			[Category("Data Set")]
			public void FieldDescriptor_WhenConstructed_KeyIsNull()
			{
				DataSet.FieldDescriptor data = new DataSet.FieldDescriptor();

				Assert.IsNull(data.Key);
			}

			[Test(Description = "An API user should be able to set a Card Field's Key")]
			[Category("Documents")]
			[Category("Data Set")]
			[TestCaseSource(typeof(TestCases), nameof(TestCases.StringInputs))]
			public void FieldDescriptor_SetKey_SetsValue(string key)
			{
				DataSet.FieldDescriptor data = new DataSet.FieldDescriptor
				{
					Key = key
				};

				Assert.AreEqual(key, data.Key);
			}

		#endregion

		#endregion

		#region Data

		[Test(Description = "When a DataSet is constructed, it should have an empty list of Data")]
		[Category("Documents")]
		[Category("Data Set")]
		public void WhenConstructed_DataIsEmpty()
		{
			DataSet dataSet = new DataSet();

			Assert.IsEmpty(dataSet.Data);
		}

		[Test(Description = "An API user should be able to add CardData to a DataSet")]
		[Category("Documents")]
		[Category("Data Set")]
		public void Data_AddCardData_AddsCardDataToList([Values(1, 2, 3)] int numData)
		{
			DataSet dataSet = new DataSet();

			for (int i = 1; i <= numData; i++)
			{
				CardData data = new CardData();

				dataSet.Data.Add(data);

				Assert.That(dataSet.Data.Contains(data));
				Assert.AreEqual(i, dataSet.Data.Count);
			}
		}

		[Test(Description = "An API user should be able to remove CardDatas from a DataSet")]
		[Category("Documents")]
		[Category("Data Set")]
		public void Data_RemoveCardData_RemovesDataFromList([Values(1, 2, 3)] int numData)
		{
			List<CardData> datas = new List<CardData>
			{
				new CardData(),
				new CardData(),
				new CardData()
			};

			DataSet dataSet = new DataSet();
			foreach (CardData data in datas)
				dataSet.Data.Add(data);

			for (int i = 1; i <= numData; i++)
			{
				dataSet.Data.Remove(datas[i - 1]);

				Assert.That(!dataSet.Data.Contains(datas[i-1]));
				Assert.AreEqual(datas.Count - i, dataSet.Data.Count);
			}
		}

		#endregion

		#region FieldDescriptors

		[Test(Description = "When a DataSet is constructed, it should have an empty list of FieldDescriptors")]
		[Category("Documents")]
		[Category("Data Set")]
		public void WhenConstructed_FieldDescriptorsIsEmpty()
		{
			DataSet dataSet = new DataSet();

			Assert.IsEmpty(dataSet.FieldDescriptors);
		}

		[Test(Description = "An API user should be able to add FieldDescriptor to a DataSet")]
		[Category("Documents")]
		[Category("Data Set")]
		public void Data_AddFieldDescriptor_AddsToList([Values(1, 2, 3)] int numData)
		{
			DataSet dataSet = new DataSet();

			for (int i = 1; i <= numData; i++)
			{
				DataSet.FieldDescriptor desc = new DataSet.FieldDescriptor();

				dataSet.FieldDescriptors.Add(desc);

				Assert.That(dataSet.FieldDescriptors.Contains(desc));
				Assert.AreEqual(i, dataSet.FieldDescriptors.Count);
			}
		}

		[Test(Description = "An API user should be able to remove FieldDescriptors from a DataSet")]
		[Category("Documents")]
		[Category("Data Set")]
		public void Data_RemoveFieldDescriptor_RemovesFromList([Values(1, 2, 3)] int numData)
		{
			List<DataSet.FieldDescriptor> descs = new List<DataSet.FieldDescriptor>
			{
				new DataSet.FieldDescriptor(),
				new DataSet.FieldDescriptor(),
				new DataSet.FieldDescriptor()
			};

			DataSet dataSet = new DataSet();
			foreach (DataSet.FieldDescriptor desc in descs)
				dataSet.FieldDescriptors.Add(desc);

			for (int i = 1; i <= numData; i++)
			{
				dataSet.FieldDescriptors.Remove(descs[i - 1]);

				Assert.That(!dataSet.FieldDescriptors.Contains(descs[i - 1]));
				Assert.AreEqual(descs.Count - i, dataSet.FieldDescriptors.Count);
			}
		}

		#endregion

		#region FrontLayout

		[Test(Description = "When a DataSet is constructed, it should have no FrontLayout")]
		[Category("Documents")]
		[Category("Data Set")]
		public void WhenConstructed_FrontLayoutIsNull()
		{
			DataSet dataSet = new DataSet();

			Assert.IsNull(dataSet.FrontLayout);
		}

		[Test(Description = "An API user should be able to set a DataSet's FrontLayout")]
		[Category("Documents")]
		[Category("Data Set")]
		[TestCaseSource(typeof(TestCases), nameof(TestCases.StringInputs))]
		public void SetFrontLayout_SetsValue(string input)
		{
			DataSet dataSet = new DataSet
			{
				FrontLayout = input
			};

			Assert.AreEqual(input, dataSet.FrontLayout);
		}

		#endregion

		#region BackLayout

		[Test(Description = "When a DataSet is constructed, it should have no BackLayout")]
		[Category("Documents")]
		[Category("Data Set")]
		public void WhenConstructed_BackLayoutIsNull()
		{
			DataSet dataSet = new DataSet();

			Assert.IsNull(dataSet.BackLayout);
		}

		[Test(Description = "An API user should be able to set a DataSet's BackLayout")]
		[Category("Documents")]
		[Category("Data Set")]
		[TestCaseSource(typeof(TestCases), nameof(TestCases.StringInputs))]
		public void SetBackLayout_SetsValue(string input)
		{
			DataSet dataSet = new DataSet
			{
				BackLayout = input
			};

			Assert.AreEqual(input, dataSet.BackLayout);
		}

		#endregion
	}
}
