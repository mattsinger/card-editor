﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using Core.Documents;
using Core.Drawing;
using Moq;
using NUnit.Framework;
using Test.Shared;

namespace Test.Core.Documents
{
	[TestFixture]
	public class LayoutTests
	{
		#region Size

		[Test(Description = "When a Layout is constructed, it should have an initial size of 0x0")]
		[Category("Documents")]
		[Category("Layout")]
		public void WhenConstructed_SizeIs10x10()
		{
			Layout layout = new Layout();

			Assert.AreEqual(0.0f, layout.Size.Width);
			Assert.AreEqual(0.0f, layout.Size.Height);
		}

		[Test(Description = "An API user should be able to set a Layout's Size")]
		[Category("Documents")]
		[Category("Layout")]
		public void SetSize_SetsValue([ValueSource(typeof(TestCases), nameof(TestCases.SizeInputs))] Size size)
		{
			Layout layout = new Layout
			{
				Size = size
			};

			Assert.AreEqual(size, layout.Size);
		}

		#endregion

		#region Elements

		[Test(Description = "When a Layout is constructed, it should have an empty list of Elements")]
		[Category("Documents")]
		[Category("Layout")]
		public void WhenConstructed_ElementsIsEmpty()
		{
			Layout layout = new Layout();

			Assert.IsEmpty(layout.Elements);
		}

		[Test(Description = "An API user should be able to add Elements to a Layout")]
		[Category("Documents")]
		[Category("Layout")]
		public void Elements_AddField_AddsFieldToList([Values(1, 2, 3)] int numElements)
		{
			Layout layout = new Layout();

			for (int i = 1; i <= numElements; i++)
			{
				Element element = CreateMockElement();

				layout.Elements.Add(element);

				Assert.That(layout.Elements.Contains(element));
				Assert.AreEqual(i, layout.Elements.Count);
			}
		}

		[Test(Description = "An API user should be able to remove Elements from a Layout")]
		[Category("Documents")]
		[Category("Layout")]
		public void Elements_RemoveField_RemovesElementsFromList([Values(1, 2, 3)] int numElements)
		{
			List<Element> fields = new List<Element>
			{
				CreateMockElement(),
				CreateMockElement(),
				CreateMockElement()
			};

			Layout layout = new Layout();
			foreach (Element field in fields)
				layout.Elements.Add(field);

			for (int i = 1; i <= numElements; i++)
			{
				layout.Elements.Remove(fields[i - 1]);

				Assert.That(!layout.Elements.Contains(fields[i - 1]));
				Assert.AreEqual(fields.Count - i, layout.Elements.Count);
			}
		}

		#endregion

		#region Mock Object Factory

		public Element CreateMockElement()
		{
			Mock<Element> mockElement = new Mock<Element>();

			return mockElement.Object;
		}

		#endregion
	}
}
