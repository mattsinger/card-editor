﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using Core.Documents;
using NUnit.Framework;
using Test.Shared;

namespace Test.Core.Documents
{
	[TestFixture]
	public class GlyphSetTests
	{
		#region Glyph

		#region Id

		[Test(Description = "When a Glyph is constructed, it should not have an Id")]
		[Category("Documents")]
		[Category("Glyph Set")]
		public void WhenConstructed_IdIsNull()
		{
			GlyphSet.Glyph glyph = new GlyphSet.Glyph();

			Assert.IsNull(glyph.Id);
		}

		[Test(Description = "An API user should be able to set a Glyph's Id")]
		[Category("Documents")]
		[Category("Glyph Set")]
		[TestCaseSource(typeof(TestCases), nameof(TestCases.StringInputs))]
		public void SetId_SetsValue(string id)
		{
			GlyphSet.Glyph glyph = new GlyphSet.Glyph
			{
				Id = id
			};

			Assert.AreEqual(id, glyph.Id);
		}

		#endregion

		#region RelativePath

		[Test(Description = "When a Glyph is constructed, it should not have a RelativePath")]
		[Category("Documents")]
		[Category("Glyph Set")]
		public void WhenConstructed_RelativePathIsNull()
		{
			GlyphSet.Glyph glyph = new GlyphSet.Glyph();

			Assert.IsNull(glyph.RelativePath);
		}

		[Test(Description = "An API user should be able to set a Glyph's RelativePath")]
		[Category("Documents")]
		[Category("Glyph Set")]
		[TestCaseSource(typeof(TestCases), nameof(TestCases.StringInputs))]
		public void SetRelativePath_SetsValue(string path)
		{
			GlyphSet.Glyph glyph = new GlyphSet.Glyph
			{
				RelativePath = path
			};

			Assert.AreEqual(path, glyph.RelativePath);
		}

		#endregion

		#endregion Glyph

		#region Glyphs

		[Test(Description = "When a Glyph is constructed, it should have an empty list of Glyphs")]
		[Category("Documents")]
		[Category("Glyph Set")]
		public void WhenConstructed_GlyphsIsEmpty()
		{
			GlyphSet glyphSet = new GlyphSet();

			Assert.IsEmpty(glyphSet.Glyphs);
		}

		[Test(Description = "An API user should be able to add CardGlyphs to a Glyph")]
		[Category("Documents")]
		[Category("Glyph Set")]
		public void Glyphs_AddField_AddsFieldToList([Values(1, 2, 3)] int numGlyphs)
		{
			GlyphSet glyphSet = new GlyphSet();

			for (int i = 1; i <= numGlyphs; i++)
			{
				GlyphSet.Glyph glyph = new GlyphSet.Glyph();

				glyphSet.Glyphs.Add(glyph);

				Assert.That(glyphSet.Glyphs.Contains(glyph));
				Assert.AreEqual(i, glyphSet.Glyphs.Count);
			}
		}

		[Test(Description = "An API user should be able to remove CardGlyphs from a Glyph")]
		[Category("Documents")]
		[Category("Glyph Set")]
		public void Glyphs_RemoveField_RemovesGlyphsFromList([Values(1, 2, 3)] int numGlyphs)
		{
			List<GlyphSet.Glyph> glyphs = new List<GlyphSet.Glyph>
			{
				new GlyphSet.Glyph(),
				new GlyphSet.Glyph(),
				new GlyphSet.Glyph()
			};

			GlyphSet glyphSet = new GlyphSet();
			foreach (GlyphSet.Glyph glyph in glyphs)
				glyphSet.Glyphs.Add(glyph);

			for (int i = 1; i <= numGlyphs; i++)
			{
				glyphSet.Glyphs.Remove(glyphs[i - 1]);

				Assert.That(!glyphSet.Glyphs.Contains(glyphs[i - 1]));
				Assert.AreEqual(glyphs.Count - i, glyphSet.Glyphs.Count);
			}
		}

		#endregion
	}
}
