﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Core.Documents;
using NUnit.Framework;
using Test.Shared;

namespace Test.Core.Documents
{
	[TestFixture]
	public class DocumentAttributeTests
	{
		#region Extension

		[Test(Description = "When a DocumentAttribute is constructed, it should not have an Extension")]
		[Category("Documents")]
		[Category("Document Attribute")]
		public void WhenConstructed_ExtensionIsNull()
		{
			DocumentAttribute data = new DocumentAttribute();

			Assert.IsNull(data.Extension);
		}

		[Test(Description = "An API user should be able to set a DocumentAttribute's Extension")]
		[Category("Documents")]
		[Category("Document Attribute")]
		[TestCaseSource(typeof(TestCases), nameof(TestCases.StringInputs))]
		public void SetExtension_SetsValue(string id)
		{
			DocumentAttribute data = new DocumentAttribute
			{
				Extension = id
			};

			Assert.AreEqual(id, data.Extension);
		}

		#endregion

		#region Description

		[Test(Description = "When a DocumentAttribute is constructed, it should not have an Description")]
		[Category("Documents")]
		[Category("Document Attribute")]
		public void WhenConstructed_DescriptionIsNull()
		{
			DocumentAttribute data = new DocumentAttribute();

			Assert.IsNull(data.Description);
		}

		[Test(Description = "An API user should be able to set a DocumentAttribute's Description")]
		[Category("Documents")]
		[Category("Document Attribute")]
		[TestCaseSource(typeof(TestCases), nameof(TestCases.StringInputs))]
		public void SetDescription_SetsValue(string id)
		{
			DocumentAttribute data = new DocumentAttribute
			{
				Description = id
			};

			Assert.AreEqual(id, data.Description);
		}

		#endregion

		#region GroupingHeader

		[Test(Description = "When a DocumentAttribute is constructed, it should not have an GroupingHeader")]
		[Category("Documents")]
		[Category("Document Attribute")]
		public void WhenConstructed_GroupingHeaderIsNull()
		{
			DocumentAttribute data = new DocumentAttribute();

			Assert.IsNull(data.GroupingHeader);
		}

		[Test(Description = "An API user should be able to set a DocumentAttribute's GroupingHeader")]
		[Category("Documents")]
		[Category("Document Attribute")]
		[TestCaseSource(typeof(TestCases), nameof(TestCases.StringInputs))]
		public void SetGroupingHeader_SetsValue(string id)
		{
			DocumentAttribute data = new DocumentAttribute
			{
				GroupingHeader = id
			};

			Assert.AreEqual(id, data.GroupingHeader);
		}

		#endregion

		#region CanBeReferenced

		[Test(Description = "When a DocumentAttribute is constructed, the CanBeReferenced flag should be true by default")]
		[Category("Documents")]
		[Category("Document Attribute")]
		public void WhenConstructed_CanBeReferencedIsTrue()
		{
			DocumentAttribute data = new DocumentAttribute();

			Assert.IsTrue(data.CanBeReferenced);
		}

		[Test(Description = "An API user should be able to set a DocumentAttribute's CanBeReferenced")]
		[Category("Documents")]
		[Category("Document Attribute")]
		public void SetCanBeReferenced_SetsValue([Values(true, false)] bool value)
		{
			DocumentAttribute data = new DocumentAttribute
			{
				CanBeReferenced = value
			};

			Assert.AreEqual(value, data.CanBeReferenced);
		}

		#endregion
	}
}
