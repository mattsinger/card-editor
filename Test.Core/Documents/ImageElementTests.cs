﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Core.Documents;
using NUnit.Framework;
using Test.Shared;

namespace Test.Core.Documents
{
	[TestFixture]
	public class ImageElementTests
	{
		#region Path

		[Test(Description = "When an ImageElement is constructed, it should not have an Path")]
		[Category("Documents")]
		[Category("Layout")]
		public void WhenConstructed_NameIsNull()
		{
			ImageElement element = new ImageElement();

			Assert.IsNull(element.Path);
		}

		[Test(Description = "An API user should be able to set an ImageElement's Path")]
		[Category("Documents")]
		[Category("Layout")]
		[TestCaseSource(typeof(TestCases), nameof(TestCases.StringInputs))]
		public void SetName_SetsValue(string path)
		{
			ImageElement element = new ImageElement
			{
				Path = path
			};

			Assert.AreEqual(path, element.Path);
		}

		#endregion

		#region Stretch

		[Test(Description = "When an ImageElement is constructed, it should not be set to Stretch")]
		[Category("Documents")]
		[Category("Layout")]
		public void WhenConstructed_StretchIsFalse()
		{
			ImageElement element = new ImageElement();

			Assert.IsFalse(element.Stretch);
		}

		[Test(Description = "An API user should be able to set an ImageElement's Stretch")]
		[Category("Documents")]
		[Category("Layout")]
		public void SetStretch_SetsValue([Values(true, false)] bool stretch)
		{
			ImageElement element = new ImageElement
			{
				Stretch = stretch
			};

			Assert.AreEqual(stretch, element.Stretch);
		}

		#endregion
	}
}
