﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Core.Drawing;
using NUnit.Framework;
using Test.Shared;

namespace Test.Core.Drawing
{
	[TestFixture]
	public class PointTests
	{
		[Test(Description = "When constructed by default, a Point should locate at the system origin")]
		[Category("Drawing")]
		public void Construct_DefaultConstructor_LocatedAtOrigin()
		{
			Point point = new Point();	

			Assert.AreEqual(0, point.X);
			Assert.AreEqual(0, point.Y);
		}

		[Test(Description = "An API user should be able to construct a Point from floats")]
		[Category("Drawing")]
		public void Construct_FromFloats_ReturnsPoint([ValueSource(typeof(TestCases), nameof(TestCases.FloatInputs))] float input)
		{
			Point point = new Point(input, input);

			Assert.AreEqual(input, point.X);
			Assert.AreEqual(input, point.Y);
		}

		[Test(Description = "An API user should be able to copy-construct a Point")]
		[Category("Drawing")]
		public void Construct_FromPoint_ReturnsPoint([ValueSource(typeof(TestCases), nameof(TestCases.FloatInputs))] float input)
		{
			Point point1 = new Point(input, input);

			Point point2 = new Point(point1);

			Assert.AreEqual(point1.X, point2.X);
			Assert.AreEqual(point1.Y, point2.Y);
		}

		[Test(Description = "An API user should be able to construct a Point from a Size")]
		[Category("Drawing")]
		public void Construct_FromSize_ReturnsPoint([ValueSource(typeof(TestCases), nameof(TestCases.FloatInputs))] float input)
		{
			Size size = new Size(input, input);

			Point point = new Point(size);

			Assert.AreEqual(size.Width, point.X);
			Assert.AreEqual(size.Height, point.Y);
		}
	}
}
