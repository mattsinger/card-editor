﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using Core.Drawing;
using NUnit.Framework;
using Test.Shared;

namespace Test.Core.Drawing
{
	[TestFixture]
	public class SizeTests
	{
		[Test(Description = "When constructed by default, a Size should locate at the system origin")]
		[Category("Drawing")]
		public void Construct_DefaultConstructor_LocatedAtOrigin()
		{
			Size size = new Size();

			Assert.AreEqual(0, size.Width);
			Assert.AreEqual(0, size.Height);
		}

		[Test(Description = "An API user should be able to construct a Size from floats")]
		[Category("Drawing")]
		public void Construct_FromFloats_ReturnsSize([ValueSource(typeof(TestCases), nameof(TestCases.FloatInputs))] float input)
		{
			Size size = new Size(input, input);

			Assert.AreEqual(input, size.Width);
			Assert.AreEqual(input, size.Height);
		}

		[Test(Description = "An API user should be able to copy-construct a Size")]
		[Category("Drawing")]
		public void Construct_FromSize_ReturnsSize([ValueSource(typeof(TestCases), nameof(TestCases.FloatInputs))] float input)
		{
			Size size1 = new Size(input, input);

			Size size2 = new Size(size1);

			Assert.AreEqual(size1.Width, size2.Width);
			Assert.AreEqual(size1.Height, size2.Height);
		}

		[Test(Description = "An API user should be able to construct a Size from a Point")]
		[Category("Drawing")]
		public void Construct_FromPoint_ReturnsSize([ValueSource(typeof(TestCases), nameof(TestCases.FloatInputs))] float input)
		{
			Point point = new Point(input, input);

			Size size = new Size(point);

			Assert.AreEqual(point.X, size.Width);
			Assert.AreEqual(point.Y, size.Height);
		}

		[Test(Description = "An API user should be able to get the inverse of a Size")]
		[Category("Drawing")]
		public void Inverse_ReturnsInvertedSize([ValueSource(typeof(TestCases), nameof(TestCases.FloatInputs))] float width,
												[ValueSource(typeof(TestCases), nameof(TestCases.FloatInputs))] float height)
		{
			Size size = new Size(width, height);

			Size inverse = size.Inverse();

			Assert.AreEqual(width, inverse.Height);
			Assert.AreEqual(height, inverse.Width);
		}

		[Test(Description = "An API user should be able to sum two Sizes")]
		[Category("Drawing")]
		public void AddOperator_ReturnsSum([ValueSource(typeof(TestCases), nameof(TestCases.NonNullSizes))] Size lhs,
											[ValueSource(typeof(TestCases), nameof(TestCases.NonNullSizes))] Size rhs)
		{
			Size sum = lhs + rhs;

			Assert.AreEqual(sum.Width, lhs.Width + rhs.Width);
			Assert.AreEqual(sum.Height, lhs.Height + rhs.Height);
		}

		[Test(Description = "An API user should be able to subtract two Sizes")]
		[Category("Drawing")]
		public void SubtractOperator_ReturnsDifference([ValueSource(typeof(TestCases), nameof(TestCases.NonNullSizes))] Size lhs,
											[ValueSource(typeof(TestCases), nameof(TestCases.NonNullSizes))] Size rhs)
		{
			Size diff = lhs - rhs;

			Assert.AreEqual(diff.Width, lhs.Width - rhs.Width);
			Assert.AreEqual(diff.Height, lhs.Height - rhs.Height);
		}

		[Test(Description = "An API user should be able to multiply scale a Size")]
		[Category("Drawing")]
		public void MultiplyOperator_ReturnsScale([ValueSource(typeof(TestCases), nameof(TestCases.NonNullSizes))] Size lhs,
											[ValueSource(typeof(TestCases), nameof(TestCases.FloatInputs))] float scale)
		{
			Size scaled = lhs * scale;

			Assert.AreEqual(scaled.Width, lhs.Width * scale);
			Assert.AreEqual(scaled.Height, lhs.Height * scale);
		}

		[Test(Description = "An API user should be able to divide scale a Size by a non-zero input")]
		[Category("Drawing")]
		public void DivideOperator_NonZeroDividend_ReturnsScale([ValueSource(typeof(TestCases), nameof(TestCases.NonNullSizes))] Size lhs,
											[ValueSource(typeof(TestCases), nameof(TestCases.NonZeroFloatInputs))] float scale)
		{
			Size scaled = lhs / scale;

			Assert.AreEqual(scaled.Width, lhs.Width / scale);
			Assert.AreEqual(scaled.Height, lhs.Height / scale);
		}

		[Test(Description = "An API user should not be able to divide scale a Size by 0")]
		[Category("Drawing")]
		public void DivideOperator_ZeroDividend_ThrowsException()
		{
			Size size = new Size(1,1);

			Assert.Throws<DivideByZeroException>(() =>
			{
				Size newSize = size / 0.0f;
			});
		}

		[Test(Description = "Two sizes should be considered equal if their widths and heights are respectively within .01 of each other")]
		[Category("Drawing")]
		public void EqualityOperator_SizesWithinTolerance_ReturnsTrue()
		{
			Size lhs = new Size(5, 8);
			Size rhs = new Size(5.001f, 7.999f);

			Assert.IsTrue(lhs == rhs);
		}

		[Test(Description = "Two sizes should be considered unequal if their widths and heights are not within .01 respectively of each other")]
		[Category("Drawing")]
		public void EqualityOperator_SizesOutsideTolerance_ReturnsFalse()
		{
			Size lhs = new Size(5, 8);
			Size rhs = new Size(5.011f, 7.989f);

			Assert.IsFalse(lhs == rhs);
		}

		[Test(Description = "Two sizes should be considered equal if their widths and heights are respectively within .01 of each other")]
		[Category("Drawing")]
		public void InequalityOperator_SizesWithinTolerance_ReturnsFalse()
		{
			Size lhs = new Size(5, 8);
			Size rhs = new Size(5.001f, 7.999f);

			Assert.IsFalse(lhs != rhs);
		}

		[Test(Description = "Two sizes should be considered unequal if their widths and heights are not within .01 respectively of each other")]
		[Category("Drawing")]
		public void InequalityOperator_SizesOutsideTolerance_ReturnsTrue()
		{
			Size lhs = new Size(5, 8);
			Size rhs = new Size(5.011f, 7.989f);

			Assert.IsTrue(lhs != rhs);
		}

		[Test(Description = "Two sizes should be considered equal if their widths and heights are respectively within .01 of each other")]
		[Category("Drawing")]
		public void Equals_SizesWithinTolerance_ReturnsTrue()
		{
			Size lhs = new Size(5, 8);
			Size rhs = new Size(5.001f, 7.999f);

			Assert.IsTrue(lhs.Equals(rhs));
		}

		[Test(Description = "Two sizes should be considered unequal if their widths and heights are not within .01 respectively of each other")]
		[Category("Drawing")]
		public void Equals_SizesOutsideTolerance_ReturnsFalse()
		{
			Size lhs = new Size(5, 8);
			Size rhs = new Size(5.011f, 7.989f);

			Assert.IsFalse(lhs.Equals(rhs));
		}

		[Test(Description = "A Size should never be considered equal to null")]
		[Category("Drawing")]
		public void Equals_CompareToNull_ReturnsFalse()
		{
			Size size = new Size();

			Assert.IsFalse(size.Equals(null));
		}
	}
}
