﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections;
using Core.Drawing;
using NUnit.Framework;

namespace Test.Core.Drawing
{
	[TestFixture]
	public class ColorTests
	{
		#region Construction

		[Test(Description = "When constructed, a Color should have 0 for all values")]
		[Category("Drawing")]
		public void WhenConstructed_AllValues0()
		{
			Color color = new Color();

			Assert.AreEqual(0, color.A);
			Assert.AreEqual(0, color.R);
			Assert.AreEqual(0, color.G);
			Assert.AreEqual(0, color.B);
		}

		#endregion

		#region Properties

		[Test(Description = "An API user should be able to set a Color's Alpha")]
		[Category("Drawing")]
		public void SetAlpha_SetsValue([ValueSource(nameof(ValidBytes))] byte input)
		{
			Color color = new Color
			{
				A = input
			};

			Assert.AreEqual(input, color.A);
		}

		[Test(Description = "An API user should be able to set a Color's Red")]
		[Category("Drawing")]
		public void SetRed_SetsValue([ValueSource(nameof(ValidBytes))] byte input)
		{
			Color color = new Color
			{
				R = input
			};

			Assert.AreEqual(input, color.R);
		}

		[Test(Description = "An API user should be able to set a Color's Green")]
		[Category("Drawing")]
		public void SetGreen_SetsValue([ValueSource(nameof(ValidBytes))] byte input)
		{
			Color color = new Color
			{
				G = input
			};

			Assert.AreEqual(input, color.G);
		}

		[Test(Description = "An API user should be able to set a Color's Blue")]
		[Category("Drawing")]
		public void SetBlue_SetsValue([ValueSource(nameof(ValidBytes))] byte input)
		{
			Color color = new Color
			{
				B = input
			};

			Assert.AreEqual(input, color.B);
		}

		#endregion

		#region FromARGB

		[Test(Description = "An API user should be able to construct from valid ARGB integer values")]
		[Category("Drawing")]
		public void FromARGB_ValidValues_WithAlpha_ReturnsMatchingColor([ValueSource(nameof(ValidByteInts))] int value)
		{
			Color color = Color.FromArgb(value, value, value, value);

			Assert.AreEqual(value, color.A);
			Assert.AreEqual(value, color.R);
			Assert.AreEqual(value, color.G);
			Assert.AreEqual(value, color.B);
		}

		[Test(Description = "An API user should be able to construct from valid RGB integer values, and the Alpha should be max value")]
		[Category("Drawing")]
		public void FromARGB_ValidValues_WithoutAlpha_ReturnsMatchingColorMaxAlpha([ValueSource(nameof(ValidByteInts))] int value)
		{
			Color color = Color.FromArgb(value, value, value);

			Assert.AreEqual(Byte.MaxValue, color.A);
			Assert.AreEqual(value, color.R);
			Assert.AreEqual(value, color.G);
			Assert.AreEqual(value, color.B);
		}

		#endregion

		public static IEnumerable ValidBytes
		{
			get
			{
				yield return Byte.MinValue;
				yield return (byte) 100;
				yield return Byte.MaxValue;
			}
		}

		public static IEnumerable ValidByteInts
		{
			get
			{
				yield return (int) Byte.MinValue;
				yield return 100;
				yield return (int) Byte.MaxValue;
			}
		}
	}
}
