﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using Core.Drawing;
using NUnit.Framework;

namespace Test.Core.Drawing
{
	[TestFixture]
	public class FontTests
	{
		[Test(Description = "An API user should be able to construct a Font with a non-null Name and positive Size")]
		[Category("Drawing")]
		public void Construct_NonNullName_PositiveSize_ReturnsFont()
		{
			Font font = new Font("test", 1);
			Assert.AreEqual("test", font.Name);
			Assert.AreEqual(1, font.Size);
		}

		[Test(Description = "An API user should be able to construct a Font with a null Name and a positive Size")]
		[Category("Drawing")]
		public void Construct_NullName_PositiveSize_ReturnsFont()
		{
			Font font = new Font(null, 1);
			Assert.IsNull(font.Name);
			Assert.AreEqual(1, font.Size);
		}

		[Test(Description = "An API user should not be able to construct a Font with a non-positive Size")]
		[Category("Drawing")]
		public void Construct_NonPositiveSize_ThrowsException([Values(0, -1)] float badSize)
		{
			Assert.Throws<ArgumentOutOfRangeException>(() =>
			{
				Font font = new Font("test", badSize);
			});
		}
	}
}
