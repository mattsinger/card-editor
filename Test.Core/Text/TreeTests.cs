﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Linq;
using System.Text;
using Core.Drawing;
using Core.Text;
using Core.View_Models;
using Core.View_Models.Glyph_Set;
using Moq;
using NUnit.Framework;
using ReactiveUI;
using Test.Shared;

namespace Test.Core.Text
{
	[TestFixture]
	class TreeTests
	{
		private const string Text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.";
		private const string GlyphFormat = "<glyph name=\"{0}\" />";

		#region Parse

		[Test(Description = "When parsing a null or empty string, the tree should be empty")]
		public void Parse_NullOrEmptyText_TreeIsEmpty([Values(null, "")] string input)
		{
			Tree tree = new Tree(CreateWorkspace());
			tree.Parse(input);

			Assert.AreEqual(0, tree.Children.Count);
		}

		[Test(Description = "When parsing plain text, the tree should wrap the text in a single TextNode")]
		public void Parse_PlainText_TreeHasOneTextNode()
		{
			Tree tree = new Tree(CreateWorkspace());
			tree.Parse(Text);

			Assert.AreEqual(1, tree.Children.Count);
			
			TextNode textNode = tree.Children[0] as TextNode;

			Assert.IsNotNull(textNode);
			Assert.AreEqual(Text, textNode.Text);
		}

		#region Glyphs

		[Test(Description = "When parsing only a glyph with a valid Id, the tree should have a single GlyphNode with the matching id")]
		public void Parse_GlyphOnly_ValidGlyphId_TreeHasOneGlyphNode()
		{
			string glyphName = "test";
			string input = GlyphText(glyphName);

			Tree tree = new Tree(CreateWorkspace());
			tree.Parse(input);

			Assert.AreEqual(1, tree.Children.Count);

			GlyphNode glyphNode = tree.Children[0] as GlyphNode;

			Assert.IsNotNull(glyphNode);
			Assert.AreEqual(glyphName, glyphNode.Id);
		}

		[Test(Description = "When parsing only a glyph without a valid Id, the tree should be empty")]
		public void Parse_GlyphOnly_InvalidGlyphId_TreeIsEmpty()
		{
			string input = GlyphText(string.Empty);

			Tree tree = new Tree(CreateWorkspace());
			tree.Parse(input);

			Assert.AreEqual(0, tree.Children.Count);
		}

		[Test(Description = "When parsing a text followed by a glyph, the tree should populate correctly")]
		public void Parse_TextThenGlyph_TreePopulatesCorrectly()
		{
			string glyphName = "test";
			string input = Text + GlyphText(glyphName);

			Tree tree = new Tree(CreateWorkspace());
			tree.Parse(input);

			Assert.AreEqual(2, tree.Children.Count);

			ValidateText(tree.Children[0], Text);
			ValidateGlyph(tree.Children[1], glyphName);
		}

		[Test(Description = "When parsing a glyph followed by text, the tree should populate correctly")]
		public void Parse_GlyphThenText_TreePopulatesCorrectly()
		{
			string glyphName = "test";
			string input = GlyphText(glyphName) + Text;

			Tree tree = new Tree(CreateWorkspace());
			tree.Parse(input);

			Assert.AreEqual(2, tree.Children.Count);

			ValidateGlyph(tree.Children[0], glyphName);
			ValidateText(tree.Children[1], Text);
		}

		[Test(Description = "When parsing a glyph surrounded by text, the tree should populate correctly")]
		public void Parse_TextGlyphText_TreePopulatesCorrectly()
		{
			string glyphName = "test";
			string input = Text + GlyphText(glyphName) + Text;

			Tree tree = new Tree(CreateWorkspace());
			tree.Parse(input);

			Assert.AreEqual(3, tree.Children.Count);

			ValidateText(tree.Children[0], Text);
			ValidateGlyph(tree.Children[1], glyphName);
			ValidateText(tree.Children[2], Text);
		}

		[Test(Description = "When parsing a glyph, the tree should include the created GlyphNode")]
		public void Parse_Glyph_GlyphListContainsParsedNode()
		{
			string glyphName = "test";
			string input = GlyphText(glyphName);

			Tree tree = new Tree(CreateWorkspace());
			tree.Parse(input);

			Assert.AreEqual(tree.Children.Count, tree.Glyphs.Count);

			Assert.IsTrue(tree.Glyphs.Contains(tree.Children[0]));
		}

		[Test(Description = "When parsing multiple glyphs, the tree should include the created GlyphNodes")]
		public void Parse_MultipleGlyphs_GlyphListContainsParsedNodes()
		{
			string glyphName = "test";
			string input = GlyphText(glyphName) + GlyphText(glyphName) + GlyphText(glyphName);

			Tree tree = new Tree(CreateWorkspace());
			tree.Parse(input);

			Assert.AreEqual(tree.Children.Count, tree.Glyphs.Count);

			for (int i = 0; i < 3; i++)
			{
				Assert.IsTrue(tree.Glyphs.Contains(tree.Children[i]));
			}
		}

		[Test(Description = "When parsing glyphs in sub-trees, the tree should include the created GlyphNodes")]
		public void Parse_SubTreeGlyphs_GlyphListContainsParsedNodes()
		{
			string glyphName = "test";
			string input = Text + FontSpanText(contents:GlyphText(glyphName)) + Text;

			Tree tree = new Tree(CreateWorkspace());
			tree.Parse(input);

			FontSpan fontSpan = tree.Children[1] as FontSpan;
			Assert.AreEqual(fontSpan.Children.Count, tree.Glyphs.Count);

			Assert.IsTrue(tree.Glyphs.Contains(fontSpan.Children[0]));
		}

		#endregion

		#region Font Spans

		[Test(Description = "When parsing a font tag, the tree should populate with a FontSpan node")]
		public void Parse_FontSpanOnly_TreeHasFontSpanNode()
		{
			string input = FontSpanText();

			Tree tree = new Tree(CreateWorkspace());
			tree.Parse(input);

			Assert.AreEqual(1, tree.Children.Count);

			Assert.IsInstanceOf<FontSpan>(tree.Children[0]);
		}

		[Test(Description = "When parsing a font element with a name attribute, the FontSpan should have the same name")]
		public void Parse_FontSpan_WithName_FontSpanNodeHasName()
		{
			string fontName = "test";
			string input = FontSpanText(name: fontName);

			Tree tree = new Tree(CreateWorkspace());
			tree.Parse(input);

			ValidateFontSpan(tree.Children[0], name:fontName);
		}

		[Test(Description = "When parsing a font tag with a size attribute, the FontSpan should have the same size")]
		public void Parse_FontSpanWithSize_FontSpanNodeHasSize()
		{
			float size = 10.0f;
			string input = FontSpanText(size: size);

			Tree tree = new Tree(CreateWorkspace());
			tree.Parse(input);

			ValidateFontSpan(tree.Children[0], size: size);
		}

	    [Test(Description = "When parsing a font tag with a bold attribute, the FontSpan should have the same bold value")]
	    public void Parse_FrontSpanWithBold_FontSpanNodeHasBoldValue([Values(true, false)] bool bold)
	    {
	        string input = FontSpanText(bold: bold);

            Tree tree = new Tree(CreateWorkspace());
            tree.Parse(input);

            ValidateFontSpan(tree.Children[0], bold: bold);
	    }

        [Test(Description = "When parsing a font tag with an italics attribute, the FontSpan should have the same italics value")]
        public void Parse_FrontSpanWithItalics_FontSpanNodeHasItalicsValue([Values(true, false)] bool italics)
        {
            string input = FontSpanText(italic: italics);

            Tree tree = new Tree(CreateWorkspace());
            tree.Parse(input);

            ValidateFontSpan(tree.Children[0], italic: italics);
        }

        #region Colors

        #region Hex format

        [Test(Description = "When parsing a font tag with a color of RGB hex values, the FontSpan should have the same color")]
		public void Parse_FontSpanWithColor_RgbHex_FontSpanHasColor([ValueSource(typeof(TestCases), nameof(TestCases.ColorInputs))] Color color)
		{
			// when given only RGB values, alpha is assumed to be full
			Color testColor = new Color
			{
				A = 255,
				R = color.R,
				G = color.G,
				B = color.B
			};
			string input = FontSpanText(color: $"#{testColor.R:X2}{testColor.G:X2}{testColor.B:X2}");

			Tree tree = new Tree(CreateWorkspace());
			tree.Parse(input);

			ValidateFontSpan(tree.Children[0], color:testColor);
		}

		[Test(Description = "When parsing a font tag with a color of ARGB hex values, the FontSpan should have the same color")]
		public void Parse_FontSpanWithColor_ArgbHex_FontSpanHasColor([ValueSource(typeof(TestCases), nameof(TestCases.ColorInputs))] Color color)
		{
			string input = FontSpanText(color: $"#{color.A:X2}{color.R:X2}{color.G:X2}{color.B:X2}");

			Tree tree = new Tree(CreateWorkspace());
			tree.Parse(input);

			ValidateFontSpan(tree.Children[0], color: color);
		}

		#endregion

		#region Int format

		[Test(Description = "When parsing a font tag with a color of RGB integer values, the FontSpan should have the same color")]
		public void Parse_FontSpanWithColor_RgbInt_FontSpanHasColor([ValueSource(typeof(TestCases), nameof(TestCases.ColorInputs))] Color color)
		{
			// when given only RGB values, alpha is assumed to be full
			Color testColor = new Color
			{
				A = 255,
				R = color.R,
				G = color.G,
				B = color.B
			};
			string input = FontSpanText(color: $"{testColor.R},{testColor.G},{testColor.B}");

			Tree tree = new Tree(CreateWorkspace());
			tree.Parse(input);

			ValidateFontSpan(tree.Children[0], color: testColor);
		}

		[Test(Description = "When parsing a font tag with a color of ARGB integer values, the FontSpan should have the same color")]
		public void Parse_FontSpanWithColor_ArgbInt_FontSpanHasColor([ValueSource(typeof(TestCases), nameof(TestCases.ColorInputs))] Color color)
		{
			string input = FontSpanText(color: $"{color.A},{color.R},{color.G},{color.B}");

			Tree tree = new Tree(CreateWorkspace());
			tree.Parse(input);

			ValidateFontSpan(tree.Children[0], color: color);
		}

		#endregion

		#region Name format

		// NOTE: this test needs the Sequential attribute to make sure we get the 1:1 mapping of colorName to expectedColor.
		// Both ValueSource IEnumerables should report their respective named color values in the same order.
		[Test(Description = "When parsing a font tag with a named color, the FontSpan should have the same color"), Sequential]
		public void Parse_FontSpanWithColor_NamedColor_FontSpanHasColor(
			[ValueSource(typeof(TestCases), nameof(TestCases.NamedColorNames))] string colorName,
			[ValueSource(typeof(TestCases), nameof(TestCases.NamedColorInputs))] Color expectedColor)
		{
			string input = FontSpanText(color: colorName);

			Tree tree = new Tree(CreateWorkspace());
			tree.Parse(input);

			ValidateFontSpan(tree.Children[0], color: expectedColor);
		}

		#endregion

		#endregion

		#region Children

		[Test(Description = "When parsing text inside font tags, the resulting TextNode should be a child of the FontSpan")]
		public void Parse_FontSpanWithTextContent_FontSpanHasTextChild()
		{
			string text = "This is a test";
			string input = FontSpanText(contents: text);

			Tree tree = new Tree(CreateWorkspace());
			tree.Parse(input);

			Assert.AreEqual(1, tree.Children.Count);
			FontSpan fontSpan = tree.Children[0] as FontSpan;
			Assert.IsNotNull(fontSpan);

			Assert.AreEqual(1, fontSpan.Children.Count);
			ValidateText(fontSpan.Children[0], text);
		}

		[Test(Description = "When parsing a glyph inside font tags, the resulting GlyphNode should be a child of the FontSpan")]
		public void Parse_FontSpanWithGlyphContent_FontSpanHasGlyphChild()
		{
			string glyphName = "TestGlyph";
			string glyphText = GlyphText(glyphName);
			string input = FontSpanText(contents: glyphText);

			Tree tree = new Tree(CreateWorkspace());
			tree.Parse(input);

			Assert.AreEqual(1, tree.Children.Count);
			FontSpan fontSpan = tree.Children[0] as FontSpan;
			Assert.IsNotNull(fontSpan);

			Assert.AreEqual(1, fontSpan.Children.Count);
			ValidateGlyph(fontSpan.Children[0], glyphName);
		}

		[Test(Description = "When parsing glyphs and text inside font tags, the resulting nodes should be children of the FontSpan")]
		public void Parse_FontSpanWithTextAndGlyphContent_FontSpanHasChildren()
		{
			string text1 = "This is a test";
			string text2 = "This is another test";
			string glyphName = "TestGlyph";
			string input = FontSpanText(contents: text1 + GlyphText(glyphName) + text2);

			Tree tree = new Tree(CreateWorkspace());
			tree.Parse(input);

			Assert.AreEqual(1, tree.Children.Count);
			FontSpan fontSpan = tree.Children[0] as FontSpan;
			Assert.IsNotNull(fontSpan);

			Assert.AreEqual(3, fontSpan.Children.Count);
			ValidateText(fontSpan.Children[0], text1);
			ValidateGlyph(fontSpan.Children[1], glyphName);
			ValidateText(fontSpan.Children[2], text2);
		}

		[Test(Description = "When parsing a font tag inside font tags, the resulting FontSpan should be a child of the initial FontSpan")]
		public void Parse_FontSpanWithFontSpanContent_FontSpanHasFontSpanChild()
		{
			string input = FontSpanText(contents: FontSpanText());

			Tree tree = new Tree(CreateWorkspace());
			tree.Parse(input);

			Assert.AreEqual(1, tree.Children.Count);
			FontSpan fontSpan = tree.Children[0] as FontSpan;
			Assert.IsNotNull(fontSpan);

			Assert.AreEqual(1, fontSpan.Children.Count);
			ValidateFontSpan(fontSpan.Children[0]);
		}

		[Test(Description = "When parsing font tags that are surrounded by other content, the tree should parse it correctly")]
		public void Parse_FontSpanWithinText_TreeParsesCorrectly()
		{
			string leftText = "Left Text";
			string rightText = "Right Text";
			string innerText = "This is a test";
			string input = leftText + FontSpanText(contents: innerText) + rightText;

			Tree tree = new Tree(CreateWorkspace());
			tree.Parse(input);

			Assert.AreEqual(3, tree.Children.Count);
			ValidateText(tree.Children[0], leftText);
			ValidateText(tree.Children[2], rightText);

			FontSpan fontSpan = tree.Children[1] as FontSpan;
			Assert.IsNotNull(fontSpan);

			Assert.AreEqual(1, fontSpan.Children.Count);
			ValidateText(fontSpan.Children[0], innerText);
		}

		#endregion

		#endregion

		#endregion

		#region Helper Methods

		private IWorkspaceViewModel CreateWorkspace()
		{
			ReactiveList<IGlyphViewModel> glyphs = new ReactiveList<IGlyphViewModel>();
			Mock<IWorkspaceViewModel> workspaceMock = new Mock<IWorkspaceViewModel>();
			workspaceMock.SetupGet(ws => ws.Glyphs).Returns(glyphs);
			return workspaceMock.Object;
		}

		private string GlyphText(string glyph)
		{
			return string.Format(GlyphFormat, glyph);
		}

		// passing null contents uses a self-terminating element. Any other contents will include an end element
		private string FontSpanText(string name = null, float? size = null, bool? bold = null, bool? italic = null, string color = null, string contents = null)
		{
			StringBuilder builder = new StringBuilder("<font ");

			if (name != null)
				builder.Append($"name=\"{name}\"");

			if (size.HasValue)
				builder.Append($"size=\"{size.Value}\"");

            if (bold.HasValue)
                builder.Append($"bold=\"{bold.Value}\"");

            if (italic.HasValue)
                builder.Append($"italics=\"{italic.Value}\"");

            if (color != null)
				builder.Append($"color=\"{color}\"");

            builder.Append(contents == null ? "/>" : $">{contents}</font>");

			return builder.ToString();
		}

		private void ValidateText(Node node, string expectedText)
		{
			TextNode textNode = node as TextNode;
			Assert.IsNotNull(textNode);
			Assert.AreEqual(expectedText, textNode.Text);
		}

		private void ValidateGlyph(Node node, string expectedId)
		{
			GlyphNode glyphNode = node as GlyphNode;
			Assert.IsNotNull(glyphNode);
			Assert.AreEqual(expectedId, glyphNode.Id);
		}

		private void ValidateFontSpan(Node node, string name = null, float? size = null, bool? bold = null, bool? italic = null, Color? color = null)
		{
			FontSpan fontSpan = node as FontSpan;
			Assert.IsNotNull(fontSpan);
            Assert.IsNotNull(fontSpan.Font);

			if (!string.IsNullOrEmpty(name))
				Assert.AreEqual(name, fontSpan.Font.Name);
			else
				Assert.IsNull(fontSpan.Font.Name);

			if (size.HasValue)
				Assert.AreEqual(size, fontSpan.Font.Size);
			else
				Assert.IsNull(fontSpan.Font.Size);

            if (bold.HasValue)
                Assert.AreEqual(bold, fontSpan.Font.Bold);
            else
                Assert.IsNull(fontSpan.Font.Bold);

            if (italic.HasValue)
                Assert.AreEqual(italic, fontSpan.Font.Italics);
            else
                Assert.IsNull(fontSpan.Font.Italics);

            if (color.HasValue)
				Assert.AreEqual(color, fontSpan.Color);
			else
				Assert.IsNull(fontSpan.Color);
		}

		#endregion
	}
}
