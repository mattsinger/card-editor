﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Math = System.Math;

namespace CardEditor.Controls
{
	/// <summary>
	/// Interaction logic for RepositionControl.xaml
	/// </summary>
	public partial class RepositionControl : UserControl
	{
		#region Dependency Properties

		/// <summary>
		/// RepositionTarget dependency property, the target of resizing. May not necessarily be the direct parent.
		/// </summary>
		public static readonly DependencyProperty RepositionTargetProperty = DependencyProperty.Register("RepositionTarget",
			typeof(FrameworkElement),
			typeof(ResizeControl));

		#endregion

		#region Properties

		/// <summary>
		/// The target of resizing, may not necessarily be the direct parent
		/// </summary>
		public FrameworkElement RepositionTarget
		{
			get { return (FrameworkElement)GetValue(RepositionTargetProperty); }
			set { SetValue(RepositionTargetProperty, value); }
		}

		/// <summary>
		/// The view scale, used to adjust mouse inputs
		/// </summary>
		public float Scale
		{
			get { return _scale; }
			set
			{
				if (value <= 0)
					throw new ArgumentOutOfRangeException(nameof(value), @"Scale must be a positive value");

				_scale = value;
			}
		}

		/// <summary>
		/// The parent view's orientation in radians
		/// </summary>
		public float Orientation
		{
			get { return _orientation; }
			set
			{
				_orientation = value;
				RecalculateOffsets();
			}
		}

		/// <summary>
		/// Whether the control is currently repositioning
		/// </summary>
		public bool IsRepositioning
		{
			get { return _isMoving; }
			protected set
			{
				if (_isMoving == value)
					return;

				_isMoving = value;
				IsRepositioningChanged?.Invoke(this, EventArgs.Empty);
			}
		}

		/// <summary>
		/// The list of horizontal positions which the control can snap to
		/// </summary>
		public List<HorizontalSnapPoint> HorizontalSnapPoints { get; set; }

		/// <summary>
		/// The hoirzontal difference at which the position snaps to one of the HorizontalSnapValues
		/// </summary>
		public float HorizontalSnapTolerance { get; set; }

		/// <summary>
		/// The horizontal snap points to which the reposition target is bound
		/// </summary>
		public List<HorizontalSnapPoint> SnappedHorizontalPoints { get; } = new List<HorizontalSnapPoint>();

		/// <summary>
		/// The list of vertical positions which the control can snap to
		/// </summary>
		public List<VerticalSnapPoint> VerticalSnapPoints { get; set; }

		/// <summary>
		/// The vertical difference at which the position snaps to one of the VerticalSnapValues
		/// </summary>
		public float VerticalSnapTolerance { get; set; }

		/// <summary>
		/// The vertical snap points to which the reposition target is bound
		/// </summary>
		public List<VerticalSnapPoint> SnappedVerticalPoints { get; } = new List<VerticalSnapPoint>();

		#endregion

		#region Events

		/// <summary>
		/// Event fired when IsRepositioning changes
		/// </summary>
		public EventHandler IsRepositioningChanged;

		/// <summary>
		/// Event fired when the set of snapped horizontal points has changed
		/// </summary>
		public EventHandler SnappedHorizontalPointsChanged;

		/// <summary>
		/// Event fired when the set of snapped vertical points has changed
		/// </summary>
		public EventHandler SnappedVerticalPointsChanged;

		#endregion

		#region Public Methods

		/// <summary>
		/// Begins mouse capturing
		/// </summary>
		/// <param name="args">The event arguments that occurred to begin capturing</param>
		public void BeginCapture(MouseEventArgs args)
		{
			if (IsRepositioning)
				return;

			IsRepositioning = true;
			_mousePos = args.GetPosition(Application.Current.MainWindow);
			CaptureMouse();
			Mouse.OverrideCursor = Cursors.SizeAll;
		}

		/// <summary>
		/// Ends mouse capturing
		/// </summary>
		public void EndCapture()
		{
			if (!IsRepositioning)
				return;

			IsRepositioning = false;
			_mousePos = null;
			ReleaseMouseCapture();
			Mouse.OverrideCursor = null;
		}

		#endregion

		#region Private Members

		private bool _isMoving;
		private Point? _mousePos;
		private float _scale = 1.0f;
		private float _orientation;
		private Vector _adjust;

		#endregion

		/// <summary>
		/// Constructs a RepositionControl
		/// </summary>
		public RepositionControl()
		{
			InitializeComponent();
		}

		#region WPF Event Handlers

		private void MouseDownHandler(object sender, MouseButtonEventArgs args)
		{
			BeginCapture(args);
			args.Handled = false;
		}

		private void MouseMoveHandler(object sender, MouseEventArgs args)
		{
			if (!IsRepositioning)
				return;

			if (_mousePos == null)
				throw new InvalidOperationException("Cannot click-and-drag without a mouse position");

			Point curPos = args.GetPosition(Application.Current.MainWindow);
			Vector diff = (curPos - _mousePos.Value)/Scale;
			
			_mousePos += Translate(diff);
		}

		private void MouseUpHandler(object sender, MouseButtonEventArgs args)
		{
			EndCapture();
		}

		#endregion

		#region Private Methods

		// returns actual change in position
		private Vector Translate(Vector diff)
		{
			if (RepositionTarget == null)
				throw new NullReferenceException("RepositionTarget not defined");

			double oldX = Canvas.GetLeft(RepositionTarget);
			double newX = oldX + diff.X;

			double oldY = Canvas.GetTop(RepositionTarget);
			double newY = oldY + diff.Y;

			// calculate all valid horizontal snap points
			if (HorizontalSnapPoints != null)
			{
				newX = CalculateHorizontalSnapValue(newX);
			}

			// calculate all valid vertical snap points
			if (VerticalSnapPoints != null)
			{
				newY = CalculateVerticalSnapValue(newY);
			}

			Canvas.SetLeft(RepositionTarget, newX);
			Canvas.SetTop(RepositionTarget, newY);

			return new Vector(newX - oldX, newY - oldY);
		}

		private void RecalculateOffsets()
		{
			float halfWidth = (float)RepositionTarget.Width / 2;
			float halfHeight = (float)RepositionTarget.Height / 2;
			Vector bottomLeft = new Vector(-halfWidth, halfHeight);
			Vector bottomRight = new Vector(halfWidth, halfHeight);

			Matrix rotate = Matrix.Identity;
			rotate.Rotate(_orientation);                // uses degrees
			bottomLeft = rotate.Transform(bottomLeft);
			bottomRight = rotate.Transform(bottomRight);

			_adjust.X = Math.Max(Math.Abs(bottomLeft.X), Math.Abs(bottomRight.X)) - halfWidth;
			_adjust.Y = Math.Max(Math.Abs(bottomLeft.Y), Math.Abs(bottomRight.Y)) - halfHeight;
		}

		private double CalculateHorizontalSnapValue(double newX)
		{
			float width = (float)RepositionTarget.Width;
			float smallestDiff = float.MaxValue;
			double snapX = newX;

			List<HorizontalSnapPoint> validHSnaps = new List<HorizontalSnapPoint>();
			foreach (HorizontalSnapPoint hSnap in HorizontalSnapPoints)
			{
				double targetValue;
				switch (hSnap.Alignment)
				{
					case HorizontalAlignment.Left:
						targetValue = hSnap.Value + _adjust.X;
						break;

					case HorizontalAlignment.Center:
						targetValue = hSnap.Value - width / 2;
						break;

					case HorizontalAlignment.Right:
						targetValue = hSnap.Value - (width + _adjust.X);
						break;

					default:
						continue;
				}

				float targetDiff = (float)(targetValue - newX);
				if (!Core.Util.Math.AreEquivalent(targetDiff, 0.0f, HorizontalSnapTolerance))
					continue;

				if (Core.Util.Math.AreEquivalent(targetDiff, smallestDiff))
				{
					validHSnaps.Add(hSnap);
				}
				else if (Math.Abs(targetDiff) < Math.Abs(smallestDiff))
				{
					validHSnaps.Clear();
					validHSnaps.Add(hSnap);
					smallestDiff = targetDiff;
					snapX = targetValue;
				}
			}
			if (SnappedHorizontalPoints.Except(validHSnaps).Any() ||
				validHSnaps.Except(SnappedHorizontalPoints).Any())
			{
				SnappedHorizontalPoints.Clear();
				SnappedHorizontalPoints.AddRange(validHSnaps);
				SnappedHorizontalPointsChanged?.Invoke(this, EventArgs.Empty);
			}

			return snapX;
		}

		private double CalculateVerticalSnapValue(double newY)
		{
			List<VerticalSnapPoint> validVSnaps = new List<VerticalSnapPoint>();
			float height = (float) RepositionTarget.Height;
			float smallestDiff = float.MaxValue;
			double snapY = newY;

			foreach (VerticalSnapPoint vSnap in VerticalSnapPoints)
			{
				double targetValue;
				switch (vSnap.Alignment)
				{
					case VerticalAlignment.Top:
						targetValue = vSnap.Value + _adjust.Y;
						break;

					case VerticalAlignment.Center:
						targetValue = vSnap.Value - height / 2;
						break;

					case VerticalAlignment.Bottom:
						targetValue = vSnap.Value - (height + _adjust.Y);
						break;

					default:
						continue;
				}

				float targetDiff = (float)(targetValue - newY);
				if (!Core.Util.Math.AreEquivalent(targetDiff, 0.0f, VerticalSnapTolerance))
					continue;

				if (Core.Util.Math.AreEquivalent(targetDiff, smallestDiff))
				{
					validVSnaps.Add(vSnap);
				}
				else if (Math.Abs(targetDiff) < Math.Abs(smallestDiff))
				{
					validVSnaps.Clear();
					validVSnaps.Add(vSnap);
					smallestDiff = targetDiff;
					snapY = targetValue;
				}
			}
			if (SnappedVerticalPoints.Except(validVSnaps).Any() ||
				validVSnaps.Except(SnappedVerticalPoints).Any())
			{
				SnappedVerticalPoints.Clear();
				SnappedVerticalPoints.AddRange(validVSnaps);
				SnappedVerticalPointsChanged?.Invoke(this, EventArgs.Empty);
			}

			return snapY;
		}

		#endregion
	}
}
