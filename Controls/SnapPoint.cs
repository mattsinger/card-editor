﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Windows;

namespace CardEditor.Controls
{
	/// <summary>
	/// Base class to define a snap point
	/// </summary>
	/// <remarks>
	/// It would seem reasonable to use either Value or Element, but not both, but we have cases
	/// where we don't want to snap to the exact edge but some part inside of the snapped Element
	/// </remarks>
	public class SnapPoint
	{
		/// <summary>
		/// The snap point value
		/// </summary>
		public double Value { get; set; }

		/// <summary>
		/// The related element being snapped to
		/// </summary>
		public FrameworkElement Element { get; set; }
	}

	/// <summary>
	/// Defines a horizontal snap point
	/// </summary>
	public class HorizontalSnapPoint : SnapPoint
	{
		/// <summary>
		/// Which side of the control it should snap to
		/// </summary>
		public HorizontalAlignment Alignment { get; set; }
	}

	/// <summary>
	/// Defines a vertical snap point
	/// </summary>
	public class VerticalSnapPoint : SnapPoint
	{
		/// <summary>
		/// Which side of the control it should snap to
		/// </summary>
		public VerticalAlignment Alignment { get; set; }
	}
}
