﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace CardEditor.Controls
{
	/// <summary>
	/// Adorner that indicates insertion position
	/// </summary>
	/// <remarks>
	/// Derived from sample code found online, but original source unknown
	/// </remarks>
	public class InsertAdorner : Adorner, IDisposable
	{
		#region Statics

		private static readonly Pen Pen;
		private static readonly PathGeometry Triangle;

		// Create the pen and triangle in a static constructor and freeze them to improve performance.
		static InsertAdorner()
		{
			Pen = new Pen {Brush = Brushes.Gray, Thickness = 2};
			Pen.Freeze();

			LineSegment firstLine = new LineSegment(new Point(0, -5), false);
			firstLine.Freeze();
			LineSegment secondLine = new LineSegment(new Point(0, 5), false);
			secondLine.Freeze();

			var figure = new PathFigure { StartPoint = new Point(5, 0) };
			figure.Segments.Add(firstLine);
			figure.Segments.Add(secondLine);
			figure.Freeze();

			Triangle = new PathGeometry();
			Triangle.Figures.Add(figure);
			Triangle.Freeze();
		}

		#endregion

		#region Private Members

		private readonly AdornerLayer _adornerLayer;
		private bool _disposed;

		#endregion

		#region Constructor & Finalizer

		/// <summary>Initializes a new instance of the <see cref="T:System.Windows.Documents.Adorner" /> class.</summary>
		/// <param name="adornedElement">The element to bind the adorner to.</param>
		/// <param name="adornerLayer">The layer the adorner belongs to</param>
		/// <exception cref="T:System.ArgumentNullException">adornedElement is null.</exception>
		public InsertAdorner(UIElement adornedElement, AdornerLayer adornerLayer) : base(adornedElement)
		{
			_adornerLayer = adornerLayer;
			IsHitTestVisible = false;

			_adornerLayer.Add(this);
		}

		/// <summary>
		/// Finalizer
		/// </summary>
		~InsertAdorner()
		{
			DoDispose(true);
		}

		#endregion

		#region Public Properties

		/// <summary>
		/// Whether the adorner is for the top half or bottom half of the adorned element
		/// </summary>
		public bool InTopHalf { get; set; }

		#endregion

		#region Public Methods

		/// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
		public void Dispose()
		{
			DoDispose(false);
		}

		#endregion

		#region Private & Protected Methods

		/// <summary>When overridden in a derived class, participates in rendering operations that are directed by the layout system. The rendering instructions for this element are not used directly when this method is invoked, and are instead preserved for later asynchronous use by layout and drawing. </summary>
		/// <param name="drawingContext">The drawing instructions for a specific element. This context is provided to the layout system.</param>
		protected override void OnRender(DrawingContext drawingContext)
		{
			double height = InTopHalf ? 0 : AdornedElement.RenderSize.Height;
			Point start = new Point(0, height);
			Point end = new Point(AdornedElement.RenderSize.Width, height);

			drawingContext.DrawLine(Pen, start, end);

			drawingContext.PushTransform(new TranslateTransform(start.X, start.Y));
			drawingContext.DrawGeometry(Pen.Brush, null, Triangle);
			drawingContext.Pop();
		}


		private void DoDispose(bool finalize)
		{
			if (_disposed)
				return;

			_disposed = true;
			_adornerLayer.Remove(this);
		}

		#endregion
	}
}
