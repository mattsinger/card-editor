﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Reactive.Linq;
using System.Windows;
using Core.Drawing;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using CardEditor.Util;
using Core.Text;
using ReactiveUI;
using Color = Core.Drawing.Color;
using GlyphNodeView = CardEditor.Views.Card_Preview.GlyphNodeView;
using HorizontalAlignment = Core.Drawing.HorizontalAlignment;
using Nullable = Core.Util.Nullable;
using VerticalAlignment = Core.Drawing.VerticalAlignment;

namespace CardEditor.Controls
{
	/// <summary>
	/// Interaction logic for TextPreviewControl.xaml
	/// </summary>
	public partial class TextPreviewControl : UserControl
	{
		#region Private Members

		private Font _font;
		private Color _color = Color.Black;
		private VerticalAlignment _vertAlign = VerticalAlignment.Top;
		private HorizontalAlignment _horzAlign = HorizontalAlignment.Left;

		private Tree _tree;
		private IDisposable _treeDisposable;

		private float _ppi = App.Ppi.Height;

		#endregion

		#region Properties

		/// <summary>
		/// The base text font
		/// </summary>
		public Font Font
		{
			get => _font;
			set
			{
				_font = value;
				UpdateFontFamily();
				UpdateFontSize();
                UpdateFontBold();
                UpdateFontItalics();
				ReflowText();
			}
		}

		/// <summary>
		/// The base text color
		/// </summary>
		public Color Color
		{
			get => _color;
			set
			{
				_color = value;
				UpdateColor();
			}
		}

		/// <summary>
		/// The text's horizontal alignment
		/// </summary>
		public new HorizontalAlignment HorizontalAlignment
		{
			get => _horzAlign;
			set
			{
				_horzAlign = value;
				UpdateHorzAlign();
			}
		}

		/// <summary>
		/// The text's vertical alignment
		/// </summary>
		public new VerticalAlignment VerticalAlignment
		{
			get => _vertAlign;
			set
			{
				_vertAlign = value;
				UpdateVertAlign();
			}
		}

		/// <summary>
		/// The parse tree used to build text
		/// </summary>
		public Tree ParseTree
		{
			get => _tree;
			set
			{
				_tree = value;
				UpdateTree();
			}
		}

		/// <summary>
		/// The visual scale of the text, used to scale font and glyph sizes for better rendering.
		/// </summary>
		public float Ppi
		{
			get => _ppi;
			set
			{
				_ppi = value;
				UpdateFontSize();
				ReflowText();
			}
		}

		#endregion

		#region Constructor

		/// <summary>
		/// Constructs a TextPreviewControl
		/// </summary>
		public TextPreviewControl()
		{
			InitializeComponent();
		}

		#endregion

		#region Private Methods

		private void UpdateFontFamily()
		{
			FontFamilyConverter ffc = new FontFamilyConverter();
			FontFamily family = (FontFamily)ffc.ConvertFromString(string.IsNullOrEmpty(_font?.Name) ?
					System.Drawing.FontFamily.GenericSansSerif.Name :
					_font.Name);

			TextFlow.FontFamily = family;
		}

		private void UpdateFontSize()
		{
			TextFlow.FontSize = Core.Util.Math.GetDisplayLengthFromInches(Util.Fonts.GetFontSizeOrDefault(_font), Ppi);
		}

	    private void UpdateFontBold()
	    {
	        TextFlow.FontWeight = Nullable.IsValidAndTrue(_font.Bold) ? FontWeights.Bold : FontWeights.Normal;
	    }

	    private void UpdateFontItalics()
	    {
	        TextFlow.FontStyle = Nullable.IsValidAndTrue(_font.Italics) ? FontStyles.Oblique : FontStyles.Normal;
	    }

		private void UpdateColor()
		{
			TextFlow.Foreground = new SolidColorBrush(_color.ToMediaColor());
		}

		private void UpdateHorzAlign()
		{
			switch (_horzAlign)
			{
				case HorizontalAlignment.Left:
					TextFlow.TextAlignment = TextAlignment.Left;
					break;

				case HorizontalAlignment.Center:
					TextFlow.TextAlignment = TextAlignment.Center;
					break;

				case HorizontalAlignment.Right:
					TextFlow.TextAlignment = TextAlignment.Right;
					break;
			}
		}

		private void UpdateVertAlign()
		{
			switch (_vertAlign)
			{
				case VerticalAlignment.Top:
					TextViewer.VerticalAlignment = System.Windows.VerticalAlignment.Top;
					break;

				case VerticalAlignment.Center:
					TextViewer.VerticalAlignment = System.Windows.VerticalAlignment.Center;
					break;

				case VerticalAlignment.Bottom:
					TextViewer.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
					break;
			}
		}

		private void UpdateTree()
		{
			_treeDisposable?.Dispose();
			if (_tree != null)
				_treeDisposable = _tree.Children.ShouldReset
					.ObserveOn(RxApp.MainThreadScheduler)
					.Subscribe(_ => ReflowText());
			ReflowText();
		}

		private void ReflowText()
		{
			Paragraph.Inlines.Clear();
			if (_tree != null)
				PopulateInlines(Paragraph.Inlines, _tree, _font);
		}

		private void PopulateInlines(InlineCollection inlines, Container nodeContainer, Font currentFont)
		{
			foreach (Node node in nodeContainer.Children)
			{
				TextNode textNode = node as TextNode;
				if (textNode != null)
				{
					inlines.Add(textNode.Text);
					continue;
				}

				GlyphNode glyphNode = node as GlyphNode;
				if (glyphNode != null)
				{
				    double fontHeight = System.Math.Ceiling(
				        Core.Util.Math.GetDisplayLengthFromInches(Util.Fonts.GetFontSizeOrDefault(currentFont), Ppi));

					inlines.Add
					(
						new InlineUIContainer
						(
							new GlyphNodeView
							{
								ViewModel = glyphNode,
								Height = fontHeight
							}
						)
						{
							BaselineAlignment = BaselineAlignment.Center
						}
					);
				}

				FontSpan fontSpan = node as FontSpan;
				if (fontSpan != null)
				{
					Font spanFont = fontSpan.Font;

				    Font newFont = new Font(
				        string.IsNullOrEmpty(spanFont.Name) ?
				            currentFont.Name :
				            spanFont.Name)
				    {
				        Size = spanFont.Size ?? currentFont.Size,
                        Bold = spanFont.Bold ?? currentFont.Bold,
                        Italics = spanFont.Italics ?? currentFont.Italics
				    };

					Span span = new Span
					{
						FontFamily = Util.Fonts.GetFontFamily(newFont),
						FontSize = Core.Util.Math.GetDisplayLengthFromInches(Util.Fonts.GetFontSizeOrDefault(newFont), Ppi),
                        FontWeight = Nullable.IsValidAndTrue(newFont.Bold) ? FontWeights.Bold : FontWeights.Normal,
                        FontStyle = Nullable.IsValidAndTrue(newFont.Italics) ? FontStyles.Italic : FontStyles.Normal
					};

					if (fontSpan.Color.HasValue)
					{
						span.Foreground = new SolidColorBrush(fontSpan.Color.Value.ToMediaColor());
					}

					inlines.Add(span);
					PopulateInlines(span.Inlines, fontSpan, newFont);
				}
			}
		}

		#endregion
	}
}
