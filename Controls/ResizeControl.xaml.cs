﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace CardEditor.Controls
{
	/// <summary>
	/// Wrapper to help resize a parent element
	/// </summary>
	public partial class ResizeControl : UserControl
	{
		private static readonly double OneOverRootTwo = 1/Math.Sqrt(2);
		private static readonly double AxisThreshold = Math.Sin(Core.Util.Math.ToRadians(22.5));

		#region Dependency Properties
		
		/// <summary>
		/// Resize target dependency property, the target of resizing. May not necessarily be the direct parent.
		/// </summary>
		public static readonly DependencyProperty ResizeTargetProperty = DependencyProperty.Register("ResizeTarget",
			typeof(FrameworkElement),
			typeof(ResizeControl));

		#endregion

		#region Properties

		/// <summary>
		/// The target of resizing, may not necessarily be the direct parent
		/// </summary>
		public FrameworkElement ResizeTarget
		{
			get { return (FrameworkElement) GetValue(ResizeTargetProperty); }
			set { SetValue(ResizeTargetProperty, value); }
		}

		/// <summary>
		/// The view scale, used to adjust mouse inputs
		/// </summary>
		public float Scale
		{
			get { return _scale; }
			set
			{
				if (value <= 0)
					throw new ArgumentOutOfRangeException(nameof(value), @"Scale must be a positive value");

				_scale = value;
			}
		}

		/// <summary>
		/// The parent view's orientation in degrees
		/// </summary>
		public float Orientation
		{
			get { return _orientation; }
			set
			{
				_orientation = value;
				UpdateCursors();
				RecalculateOffsets();
			}
		}

		/// <summary>
		/// Whether the control is currently resizing
		/// </summary>
		public bool IsResizing => CurrentState.HasValue;

		/// <summary>
		/// The current resize state. If null, then the control is not currently resizing.
		/// </summary>
		public ResizeState? CurrentState
		{
			get { return _currentState; }
			set
			{
				if (value == _currentState)
					return;

				_currentState = value;
				IsResizingChanged?.Invoke(this, EventArgs.Empty);
			}
		}

		/// <summary>
		/// The list of horizontal positions which the control can snap to
		/// </summary>
		public List<HorizontalSnapPoint> HorizontalSnapPoints { get; set; }

		/// <summary>
		/// The hoirzontal difference at which the position snaps to one of the HorizontalSnapValues
		/// </summary>
		public float HorizontalSnapTolerance { get; set; }

		/// <summary>
		/// The horizontal snap points to which the reposition target is bound
		/// </summary>
		public List<HorizontalSnapPoint> SnappedHorizontalPoints { get; } = new List<HorizontalSnapPoint>();

		/// <summary>
		/// The list of vertical positions which the control can snap to
		/// </summary>
		public List<VerticalSnapPoint> VerticalSnapPoints { get; set; }

		/// <summary>
		/// The vertical difference at which the position snaps to one of the VerticalSnapValues
		/// </summary>
		public float VerticalSnapTolerance { get; set; }

		/// <summary>
		/// The vertical snap points to which the reposition target is bound
		/// </summary>
		public List<VerticalSnapPoint> SnappedVerticalPoints { get; } = new List<VerticalSnapPoint>();

		#endregion

		#region Events

		/// <summary>
		/// Event fired when IsResizing changes
		/// </summary>
		public EventHandler IsResizingChanged;

		/// <summary>
		/// Event fired when the set of snapped horizontal points has changed
		/// </summary>
		public EventHandler SnappedHorizontalPointsChanged;

		/// <summary>
		/// Event fired when the set of snapped vertical points has changed
		/// </summary>
		public EventHandler SnappedVerticalPointsChanged;

		#endregion

		#region Public Methods

		public enum ResizeState
		{
			/// <summary>
			/// User is resizing on the left side
			/// </summary>
			Left,

			/// <summary>
			/// User is resizing on the right side
			/// </summary>
			Right,

			/// <summary>
			/// User is resizing on the top side
			/// </summary>
			Top,

			/// <summary>
			/// User is resizing on the bottom side
			/// </summary>
			Bottom,

			/// <summary>
			/// User is resizing on the top-left corner
			/// </summary>
			TopLeft,

			/// <summary>
			/// User is resizing on the top-right corner
			/// </summary>
			TopRight,

			/// <summary>
			/// User is resizing on the bottom-left corner
			/// </summary>
			BottomLeft,

			/// <summary>
			/// User is resizing on the bottom-left corner
			/// </summary>
			BottomRight,
		}

		public void BeginCaptureMode(ResizeState state, MouseEventArgs args)
		{
			if (IsResizing)
				return;

			CurrentState = state;
			_mousePos = args.GetPosition(Application.Current.MainWindow);
			CaptureMouse();
		}

		public void EndCaptureMode()
		{
			if (!IsResizing)
				return;

			CurrentState = null;
			_mousePos = null;
			ReleaseMouseCapture();
		}

		#endregion

		#region Private Members

		private ResizeState? _currentState;
		private Point? _mousePos;
		private float _scale = 1.0f;
		private float _orientation;
		private Vector _adjust;

		#endregion

		/// <summary>
		/// Constructs a ResizeControl
		/// </summary>
		public ResizeControl()
		{
			InitializeComponent();
		}

		#region WPF Event Handlers

		private void MouseDownHandler(object sender, MouseButtonEventArgs args)
		{
			args.Handled = false;
		}

		private void MouseMoveHandler(object sender, MouseEventArgs args)
		{
			if (!IsResizing)
				return;

			if (_mousePos == null)
				return;

			Point curPos = args.GetPosition(Application.Current.MainWindow);
			Vector input = (curPos - _mousePos.Value) / Scale;
			Vector growDir = GetOrientedGrowDirection(CurrentState, _orientation);
			
			Vector actualInput = Resize(growDir, input);

			_mousePos += actualInput;

			args.Handled = true;
		}

		private void MouseUpHandler(object sender, MouseButtonEventArgs args)
		{
			if (args.ChangedButton != MouseButton.Left)
				return;

			EndCaptureMode();
		}

		private void LeftGrabber_OnMouseDown(object sender, MouseButtonEventArgs args)
		{
			if (args.ChangedButton != MouseButton.Left)
				return;

			BeginCaptureMode(ResizeState.Left, args);
			args.Handled = true;
		}

		private void RightGrabber_OnMouseDown(object sender, MouseButtonEventArgs args)
		{
			if (args.ChangedButton != MouseButton.Left)
				return;

			BeginCaptureMode(ResizeState.Right, args);
			args.Handled = true;
		}

		private void TopGrabber_OnMouseDown(object sender, MouseButtonEventArgs args)
		{
			if (args.ChangedButton != MouseButton.Left)
				return;

			BeginCaptureMode(ResizeState.Top, args);
			args.Handled = true;
		}

		private void BottomGrabber_OnMouseDown(object sender, MouseButtonEventArgs args)
		{
			if (args.ChangedButton != MouseButton.Left)
				return;

			BeginCaptureMode(ResizeState.Bottom, args);
			args.Handled = true;
		}

		private void TopLeftGrabber_OnMouseDown(object sender, MouseButtonEventArgs args)
		{
			if (args.ChangedButton != MouseButton.Left)
				return;

			BeginCaptureMode(ResizeState.TopLeft, args);
			args.Handled = true;
		}

		private void TopRightGrabber_OnMouseDown(object sender, MouseButtonEventArgs args)
		{
			if (args.ChangedButton != MouseButton.Left)
				return;

			BeginCaptureMode(ResizeState.TopRight, args);
			args.Handled = true;
		}

		private void BottomLeftGrabber_OnMouseDown(object sender, MouseButtonEventArgs args)
		{
			if (args.ChangedButton != MouseButton.Left)
				return;

			BeginCaptureMode(ResizeState.BottomLeft, args);
			args.Handled = true;
		}

		private void BottomRightGrabber_OnMouseDown(object sender, MouseButtonEventArgs args)
		{
			if (args.ChangedButton != MouseButton.Left)
				return;

			BeginCaptureMode(ResizeState.BottomRight, args);
			args.Handled = true;
		}

		#endregion

		#region Private Methods

		// returns a Vector of the actual change made
		private Vector Resize(Vector growDir, Vector input)
		{
			if (ResizeTarget == null)
				throw new NullReferenceException("ResizeTarget not defined");
			
			Vector growInput = growDir * input * growDir;   // projects the input onto the growth direction of a grabber

			double left = Canvas.GetLeft(ResizeTarget);
			double top = Canvas.GetTop(ResizeTarget);
			double width = ResizeTarget.Width;
			double height = ResizeTarget.Height;
			double right = left + width;
			double bottom = top + height;

			double oldX, newX, oldY, newY;
			bool snappedX = false, snappedY = false;
			if (growDir.X > 0)
			{
				oldX = right;
				newX = Math.Max(left + 1, oldX + growInput.X);
			}
			else
			{
				oldX = left;
				newX = oldX + growInput.X;
			}

			if (HorizontalSnapPoints != null)
				newX = CalculateHorizontalSnapValue(newX, growDir.X < 0, out snappedX);

			if (growDir.Y > 0)
			{
				oldY = bottom;
				newY = Math.Max(top + 1, oldY + growInput.Y);
			}
			else
			{
				oldY = top;
				newY = oldY + growInput.Y;
			}

			if (VerticalSnapPoints != null)
				newY = CalculateVerticalSnapValue(newY, growDir.Y < 0, out snappedY);

			// if we have valid snap points, snap to the closer of the horizontal or vertical points
			if (snappedX || snappedY)
			{
				double xDiff = newX - oldX;
				double yDiff = newY - oldY;
				double compX = snappedX ? Math.Abs(xDiff) : float.MaxValue;
				double compY = snappedY ? Math.Abs(yDiff) : float.MaxValue;

				if (compX <= compY)
				{
					if (!snappedX)
						throw new InvalidOperationException("Horizontal snap value should exist");

					if (growInput.X != 0)
						newY = oldY + growInput.Y * (xDiff / growInput.X);
				}
				else
				{
					if (!snappedY)
						throw new InvalidOperationException("Vertical snap value should exist");

					if (growInput.Y != 0)
						newX = oldX + growInput.X * (yDiff / growInput.Y);
				}
			}

			if (growDir.X > 0)
			{
				ResizeTarget.Width = newX - left;
			}
			else
			{
				Canvas.SetLeft(ResizeTarget, newX);
				ResizeTarget.Width = width + left - newX;
			}

			if (growDir.Y > 0)
			{
				ResizeTarget.Height = newY - top;
			}
			else
			{
				Canvas.SetTop(ResizeTarget, newY);
				ResizeTarget.Height = height + top - newY;
			}

			RecalculateOffsets();

			return new Vector(newX - oldX, newY - oldY);
		}

		private Vector GetOrientedGrowDirection(ResizeState? state, double orientationDeg)
		{
			Vector growDir = new Vector();

			// We get some weirdness when using Resize diffs in cases where it also Translates.
			// We prefer to use Translate diffs over Resize.
			switch (state)
			{
				case ResizeState.Left:
					growDir.X = -1;
					break;

				case ResizeState.Right:
					growDir.X = 1;
					break;

				case ResizeState.Top:
					growDir.Y = -1;
					break;

				case ResizeState.Bottom:
					growDir.Y = 1;
					break;

				case ResizeState.TopLeft:
					growDir.X = -OneOverRootTwo;
					growDir.Y = -OneOverRootTwo;
					break;

				case ResizeState.TopRight:
					growDir.X = OneOverRootTwo;
					growDir.Y = -OneOverRootTwo;
					break;

				case ResizeState.BottomLeft:
					growDir.X = -OneOverRootTwo;
					growDir.Y = OneOverRootTwo;
					break;

				case ResizeState.BottomRight:
					growDir.X = OneOverRootTwo;
					growDir.Y = OneOverRootTwo;
					break;

				default:
					throw new InvalidOperationException("Current resize state must be valid");
			}

			Matrix rotation = Matrix.Identity;
			rotation.Rotate(orientationDeg);  // takes degrees for some reason?
			return rotation.Transform(growDir);
		}

		void UpdateCursors()
		{
			UpdateCursor(TopLeftGrabber, ResizeState.TopLeft);
			UpdateCursor(TopGrabber, ResizeState.Top);
			UpdateCursor(TopRightGrabber, ResizeState.TopRight);
			UpdateCursor(BottomLeftGrabber, ResizeState.BottomLeft);
			UpdateCursor(BottomGrabber, ResizeState.Bottom);
			UpdateCursor(BottomRightGrabber, ResizeState.BottomRight);
			UpdateCursor(LeftGrabber, ResizeState.Left);
			UpdateCursor(RightGrabber, ResizeState.Right);
		}

		void UpdateCursor(FrameworkElement element, ResizeState state)
		{
			Vector dir = GetOrientedGrowDirection(state, _orientation);
			double dotX = dir * new Vector(1, 0);
			double dotY = dir * new Vector(0, 1);

			if (Math.Abs(dotX) <= AxisThreshold)			// mostly vertical
				element.Cursor = Cursors.SizeNS;
			else if (Math.Abs(dotY) <= AxisThreshold)		// mostly horizontal
				element.Cursor = Cursors.SizeWE;
			else if (Math.Sign(dotX) == Math.Sign(dotY))	// same axis sign, top-left to bottom-right
				element.Cursor = Cursors.SizeNWSE;
			else											// opposite axis sign, top-right to bottom-left
				element.Cursor = Cursors.SizeNESW;
		}

		private void RecalculateOffsets()
		{
			float halfWidth = (float)ResizeTarget.Width / 2;
			float halfHeight = (float)ResizeTarget.Height / 2;
			Vector bottomLeft = new Vector(-halfWidth, halfHeight);
			Vector bottomRight = new Vector(halfWidth, halfHeight);

			Matrix rotate = Matrix.Identity;
			rotate.Rotate(_orientation);				// uses degrees
			bottomLeft = rotate.Transform(bottomLeft);
			bottomRight = rotate.Transform(bottomRight);

			_adjust.X = Math.Max(Math.Abs(bottomLeft.X), Math.Abs(bottomRight.X)) - halfWidth;
			_adjust.Y = Math.Max(Math.Abs(bottomLeft.Y), Math.Abs(bottomRight.Y)) - halfHeight;
		}

		private double CalculateHorizontalSnapValue(double newX, bool leftEdge, out bool snapped)
		{
			float width = (float)ResizeTarget.Width;
			float left = (float) Canvas.GetLeft(ResizeTarget);
			float smallestDiff = float.MaxValue;
			double snapX = newX;

			List<HorizontalSnapPoint> validHSnaps = new List<HorizontalSnapPoint>();
			foreach (HorizontalSnapPoint hSnap in HorizontalSnapPoints)
			{
				double targetValue;
				switch (hSnap.Alignment)
				{
					case HorizontalAlignment.Left:
					case HorizontalAlignment.Right:
						targetValue =  leftEdge ? 
							hSnap.Value + _adjust.X :
							hSnap.Value - _adjust.X;
						break;

					case HorizontalAlignment.Center:
						targetValue = leftEdge ?
							hSnap.Value - width / 2 :
							2 * hSnap.Value - left;
						break;

					default:
						continue;
				}

				float targetDiff = (float)(targetValue - newX);
				if (!Core.Util.Math.AreEquivalent(targetDiff, 0.0f, HorizontalSnapTolerance))
					continue;

				if (Core.Util.Math.AreEquivalent(targetDiff, smallestDiff))
				{
					validHSnaps.Add(hSnap);
				}
				else if (Math.Abs(targetDiff) < Math.Abs(smallestDiff))
				{
					validHSnaps.Clear();
					validHSnaps.Add(hSnap);
					smallestDiff = targetDiff;
					snapX = targetValue;
				}
			}

			if (SnappedHorizontalPoints.Except(validHSnaps).Any() ||
				validHSnaps.Except(SnappedHorizontalPoints).Any())
			{
				SnappedHorizontalPoints.Clear();
				SnappedHorizontalPoints.AddRange(validHSnaps);
				SnappedHorizontalPointsChanged?.Invoke(this, EventArgs.Empty);
			}

			snapped = validHSnaps.Any();

			return snapX;
		}

		private double CalculateVerticalSnapValue(double newY, bool topEdge, out bool snapped)
		{
			List<VerticalSnapPoint> validVSnaps = new List<VerticalSnapPoint>();
			float height = (float)ResizeTarget.Height;
			float top = (float) Canvas.GetTop(ResizeTarget);
			float smallestDiff = float.MaxValue;
			double snapY = newY;

			foreach (VerticalSnapPoint vSnap in VerticalSnapPoints)
			{
				double targetValue;
				switch (vSnap.Alignment)
				{
					case VerticalAlignment.Top:
					case VerticalAlignment.Bottom:
						targetValue = topEdge ? 
							vSnap.Value + _adjust.Y :
							vSnap.Value - _adjust.Y;
						break;

					case VerticalAlignment.Center:
						targetValue = topEdge ?
							vSnap.Value - height / 2 :
							2 * vSnap.Value - top;
						break;

					default:
						continue;
				}

				float targetDiff = (float)(targetValue - newY);
				if (!Core.Util.Math.AreEquivalent(targetDiff, 0.0f, VerticalSnapTolerance))
					continue;

				if (Core.Util.Math.AreEquivalent(targetDiff, smallestDiff))
				{
					validVSnaps.Add(vSnap);
				}
				else if (Math.Abs(targetDiff) < Math.Abs(smallestDiff))
				{
					validVSnaps.Clear();
					validVSnaps.Add(vSnap);
					smallestDiff = targetDiff;
					snapY = targetValue;
				}
			}
			if (SnappedVerticalPoints.Except(validVSnaps).Any() ||
				validVSnaps.Except(SnappedVerticalPoints).Any())
			{
				SnappedVerticalPoints.Clear();
				SnappedVerticalPoints.AddRange(validVSnaps);
				SnappedVerticalPointsChanged?.Invoke(this, EventArgs.Empty);
			}

			snapped = validVSnaps.Any();

			return snapY;
		}

		#endregion
	}
}
