﻿/*	This file is part of Card Editor
 *	Copyright © 2016-2019 Matt Singer
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Resources;
using Core.Util;
using Math = Core.Util.Math;

namespace CardEditor.Controls
{
	/// <summary>
	/// Interaction logic for OrientationControl.xaml
	/// </summary>
	public partial class OrientationControl : UserControl
	{
		#region Dependency Properties

		/// <summary>
		/// OrientationTarget dependency property, the target of orienting. May not necessarily be the direct parent.
		/// </summary>
		public static readonly DependencyProperty OrientationTargetProperty = DependencyProperty.Register("OrientationTarget",
			typeof(FrameworkElement),
			typeof(OrientationControl),
			new PropertyMetadata
			{
				PropertyChangedCallback = OrientationTargetChanged
			});

		/// <summary>
		/// Orientation dependency property, the clockwise rotation of the target in degrees
		/// </summary>
		public static readonly DependencyProperty OrientationProperty = DependencyProperty.Register("Orientation",
			typeof(float),
			typeof(OrientationControl),
			new PropertyMetadata
			{
				PropertyChangedCallback = OrientationChanged
			});

		/// <summary>
		/// Snap Range dependency property, angle difference in degrees at which control snaps to SnapValues
		/// </summary>
		public static readonly DependencyProperty SnapRangeProperty = DependencyProperty.Register("SnapRange",
			typeof(float),
			typeof(OrientationControl));

		#endregion

		#region Statics

		private static readonly Cursor OrientCursor;
		static OrientationControl()
		{
			StreamResourceInfo stream = Application.GetResourceStream(
				new Uri("/CardEditor;component/Resources/rotate.cur", UriKind.RelativeOrAbsolute));

			if (stream == null)
				throw new FileNotFoundException("Failed to find rotate cursor");

			OrientCursor = new Cursor(stream.Stream);
		}

		private static void OrientationTargetChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
		{
			OrientationControl orient = obj as OrientationControl;

			orient?.UnbindTarget(args.OldValue as FrameworkElement);
			orient?.BindTarget(args.NewValue as FrameworkElement);
		}

		private static void OrientationChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
		{
			OrientationControl orient = obj as OrientationControl;

			orient?.SetAngle((float)args.NewValue);
		}

		#endregion

		#region Private Members

		private bool _isMoving;
		private readonly RotateTransform _transform = new RotateTransform();

		#endregion

		#region Properties

		/// <summary>
		/// The target of orienting, may not necessarily be the direct parent
		/// </summary>
		public FrameworkElement OrientationTarget
		{
			get { return (FrameworkElement)GetValue(OrientationTargetProperty); }
			set { SetValue(OrientationTargetProperty, value); }
		}

		/// <summary>
		/// The orientation of the target, in degrees clockwise
		/// </summary>
		public float Orientation
		{
			get { return (float) GetValue(OrientationProperty); }
			set { SetValue(OrientationProperty, value); }
		}

		/// <summary>
		/// Whether the control is currently orienting
		/// </summary>
		public bool IsOrienting
		{
			get { return _isMoving; }
			protected set
			{
				if (_isMoving == value)
					return;

				_isMoving = value;
				IsOrientingChanged?.Invoke(this, EventArgs.Empty);
			}
		}

		/// <summary>
		/// The list of orientation values which the control can snap to
		/// </summary>
		public List<float> SnapValues { get; set; }

		/// <summary>
		/// The angle difference at which the orientation snaps to one of the SnapValues
		/// </summary>
		public float SnapRange
		{
			get { return (float) GetValue(SnapRangeProperty); }
			set { SetValue(SnapRangeProperty, value); }
		}

		#endregion

		#region Events

		/// <summary>
		/// Event fired when IsOrienting changes
		/// </summary>
		public EventHandler IsOrientingChanged;

		#endregion

		#region Constructor

		/// <summary>
		/// Constructs an OrientationControl
		/// </summary>
		public OrientationControl()
		{
			InitializeComponent();

			Grabber.Cursor = OrientCursor;
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Begins mouse capturing
		/// </summary>
		/// <param name="args">The event arguments that occurred to begin capturing</param>
		public void BeginCapture(MouseEventArgs args)
		{
			if (IsOrienting)
				return;

			IsOrienting = true;
			CaptureMouse();
			Mouse.OverrideCursor = OrientCursor;
		}

		/// <summary>
		/// Ends mouse capturing
		/// </summary>
		public void EndCapture()
		{
			if (!IsOrienting)
				return;

			IsOrienting = false;
			ReleaseMouseCapture();
			Mouse.OverrideCursor = null;
		}

		#endregion

		#region WPF Event Handlers

		private void MouseDownHandler(object sender, MouseButtonEventArgs args)
		{
			BeginCapture(args);
			args.Handled = false;
		}

		private void MouseMoveHandler(object sender, MouseEventArgs args)
		{
			if (!IsOrienting)
				return;
			
			Point curPos = args.GetPosition(Application.Current.MainWindow);
			Point originPos = OrientationTarget.TransformToAncestor(Application.Current.MainWindow)
									.Transform(new Point(_transform.CenterX, _transform.CenterY));

			Vector diff = curPos - originPos;
			float targetAngle = 0;
			if (!Math.AreEquivalent((float) diff.Length, 0))
				targetAngle = (float)System.Math.Atan2(diff.X, -diff.Y).ToDegrees();

			SnapValues?.ForEach(orient =>
			{
				if (Math.AreEquivalent(orient, targetAngle, SnapRange))
				{
					targetAngle = orient;
				}
			});

			Orientation = targetAngle;
		}

		private void MouseUpHandler(object sender, MouseButtonEventArgs args)
		{
			EndCapture();
		}

		private void TargetWidthChangedHandler(object sender, EventArgs e)
		{
			_transform.CenterX = OrientationTarget.ActualWidth / 2;
		}

		private void TargetHeightChangedHandler(object sender, EventArgs e)
		{
			_transform.CenterY = OrientationTarget.ActualHeight / 2;
		}

		#endregion

		#region Private Methods

		private void UnbindTarget(FrameworkElement oldTarget)
		{
			if (oldTarget == null)
				return;

			oldTarget.RenderTransform = null;

			DependencyPropertyDescriptor widthDesc = DependencyPropertyDescriptor.FromProperty(ActualWidthProperty, typeof(FrameworkElement));
			DependencyPropertyDescriptor heightDesc = DependencyPropertyDescriptor.FromProperty(ActualHeightProperty, typeof(FrameworkElement));

			widthDesc.RemoveValueChanged(oldTarget, TargetWidthChangedHandler);
			heightDesc.RemoveValueChanged(oldTarget, TargetHeightChangedHandler);
		}

		private void BindTarget(FrameworkElement newTarget)
		{
			if (newTarget == null)
				return;

			newTarget.RenderTransform = _transform;
			_transform.CenterX = newTarget.Width / 2;
			_transform.CenterY = newTarget.Height / 2;

			DependencyPropertyDescriptor widthDesc = DependencyPropertyDescriptor.FromProperty(ActualWidthProperty, typeof(FrameworkElement));
			DependencyPropertyDescriptor heightDesc = DependencyPropertyDescriptor.FromProperty(ActualHeightProperty, typeof(FrameworkElement));

			widthDesc.AddValueChanged(newTarget, TargetWidthChangedHandler);
			heightDesc.AddValueChanged(newTarget, TargetHeightChangedHandler);
		}

		private void SetAngle(double angle)
		{
			_transform.Angle = angle;
		}

		#endregion
	}
}
